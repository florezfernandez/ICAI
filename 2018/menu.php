<nav class="navbar navbar-primary" role="navigation">
	<div class="container-fluid">
		<div class="navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="index.php"><span class='glyphicon glyphicon-home'></span></a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Conference <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.php?pid=call">Call for Papers</a></li>
						<li><a href="index.php?pid=dates">Important Dates</a></li>
						<li><a href="index.php?pid=submission">Submission</a></li>
						<li><a href="index.php?pid=call4W">Call for Workshops</a></li>
						<li><a href="index.php?pid=acceptedWorkshops">Accepted Workshops</a></li>
					</ul></li>
				<li><a href="index.php?pid=committees">Committees</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Program <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.php?pid=keynote">Keynote Speakers</a></li>
						<li><a href="index.php?pid=acceptedPapers">Accepted Papers</a></li>
						<li><a href="index.php?pid=program">General Program</a></li>
						<li><a href="index.php?pid=tutorials">Tutorials</a></li>
					</ul></li>
				<li><a href="index.php?pid=registration">Registration</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Venue <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.php?pid=venue">Conference Venue</a></li>
						<li><a href="index.php?pid=accommodation">Accommodation</a></li>
					</ul></li>				
			</ul>
		</div>
	</div>
</nav>