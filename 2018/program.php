<h3>Program</h3>
<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="30%" class="text-center">Thursday 1 November</th>
		<th width="30%" class="text-center">Friday 2 November</th>
		<th width="30%" class="text-center">Saturday 3 November</th>
	</tr>
	<tr>
		<td>8:00-9:00</td>
		<td class="active">Registration <br> Conference Opening<br>
		<i>Aula Maxima</i></td>
		<td class="success">Keynote: Waldemar Karwowski<br> <i>Aula Maxima</i>
		</td>
		<td rowspan="2" class="info">ICAI Robotic Autonomy Session<br> <i>Sala
				Norte</i><br>ICAI Software Design Engineering Session<br> <i>Sala
				Sur</i></td>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td class="success">Keynote: Sanjay Misra<br> <i>Aula Maxima</i></td>
		<td class="success">Keynote: Alejandro Correa Bahnsen<br> <i>Aula
				Maxima</i>
		</td>
	</tr>
	<tr>
		<td>10:00-10:30</td>
		<td class="active text-center">Coffee Break</td>
		<td class="active text-center">Coffee Break</td>
		<td class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>10:30-11:30</td>
		<td class="success">Keynote: Valérie Gauthier<br> <i>Aula Maxima</i>
		</td>
		<td rowspan="2" class="info">ICAI Data Analysis Session<br> <i>Sala
				Norte</i><br>ICAI Health Care Information Systems Session<br> <i>Sala
				Sur</i></td>
		<td rowspan="2" class="active">Conference Closing<br> <i>Hemiciclo</i><br>
			Touristic Activity<br> <i>Gold Museum</i></td>
	</tr>
	<tr>
		<td>11:30-12:30</td>
		<td class="success">Keynote: David Benavides<br> <i>Aula Maxima</i>
		</td>

	</tr>
	<tr>
		<td>12:30-14:00</td>
		<td class="active text-center">Lunch</td>
		<td class="active text-center">Lunch</td>
	</tr>
	<tr>
		<td>14:00-15:30</td>
		<td class="warning">WSSC<br> <i>Sala Norte</i><br>WEESR<br> <i>Sala
				Sur</i></td>
		<td class="info">ICAI IT Architectures Session<br> <i>Sala Norte</i><br>ICAI
			Learning Management Systems<br> <i>Sala Sur</i><br></td>
	</tr>
	<tr>
		<td>15:30-16:00</td>
		<td class="active text-center">Coffee Break</td>
		<td class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>16:00-17:30</td>
		<td class="warning">WITS<br> <i>Sala Norte</i><br>WDEA<br> <i>Sala Sur</i></td>
		<td class="info">ICAI Decision Systems Session<br> <i>Sala Norte</i><br>ICAI
			Mobile Information Processing Systems Session<br> <i>Sala Sur</i></td>
	</tr>
	<tr>
		<td>18:00-20:00</td>
		<td class="danger text-center">Cocktail<br>Poster Exibition<br>Library
			second floor
		</td>
		<td class="danger text-center">Tutorials<br>Sala Computo Norte, Sala
			Computo Sur
		</td>
	</tr>
</table>

<h3>Detailed Program</h3>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th colspan="2" class="text-center">Thursday 1 November</th>
	</tr>
	<tr>
		<td>8:00-9:00</td>
		<td colspan="2" class="active"><strong>Registration<br>Conference
				Opening
		</strong> <br> <i>Aula Maxima</i></td>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td colspan="2" class="success"><strong>Keynote: Sanjay Misra.
				Improving Quality of Research Publications in ICT in Developing
				Countries</strong><br> <i>Aula Maxima</i></td>
	</tr>
	<tr>
		<td>10:00-10:30</td>
		<td colspan="2" class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>10:30-11:30</td>
		<td colspan="2" class="success"><strong>Keynote: Valérie Gauthier. An
				Introduction to Post-Quantum Cryptology</strong><br> <i>Aula Maxima</i></td>
	</tr>
	<tr>
		<td>11:30-12:30</td>
		<td colspan="2" class="success"><strong>Keynote: David Benavides.
				Challenges on software variability and product lines</strong><br> <i>Aula
				Maxima</i></td>
	</tr>
	<tr>
		<td>12:30-14:00</td>
		<td colspan="2" class="active text-center">Lunch</td>
	</tr>
	<tr>
		<td>14:00-15:30</td>
		<td class="warning" width="45%"><strong>WSSC: WorkShop on Smart
				Sustainable Cities</strong><br> <i>Sala Norte</i><br>Session Chair:
			Cesar Diaz
			<ul>
				<li><strong>Design and Development of a Portable Instrument to
						Measure Water Turbidity</strong><br>John Edisson Rodríguez, Fabian
					Molano, Andrés Julián Aristizábal Cardona, Yulia Ivanova and Daniel
					Ospina</li>
				<li><strong>Methodological model based on Gophish to face phishing
						vulnerabilities in SME</strong><br>Julio Alexander
					Rodríguez-Corzo, Alix E. Rojas and Camilo Mejía-Moncayo</li>
				<li><strong>Relaxation state induction through binaural acoustic
						stimuli</strong><br>Marcelo Herrera, Ricardo Alfonso Jimenez,
					Oscar Acosta and Shymmy García</li>
				<li><strong>Energy Analysis of the Tertiary Sector of Colombia and
						Demand Estimation Using LEAP</strong> <br>Jorge Augusto Nieves,
					Andrés Julián Aristizábal Cardona, Isaac Dyner, Omar Báez and
					Daniel Ospina</li>
				<li><strong>Economic Viability of Hydrogen Generators in
						Transportation Vehicles</strong><br>Wilson Acevedo, Luis Gaitan,
					Andrés Julián Aristizábal Cardona and Daniel Ospina</li>

			</ul></td>
		<td class="warning" width="45%"><strong>WEESR: Workshop on Empirical
				Experiences on Software Reuse</strong><br> <i>Sala Sur</i><br>Session
			Chair: Jaime Chavarriaga
			<ul>
				<li><strong>A Comparative Study for Scoping a Software Process Line</strong><br>Pablo
					Hernando Ruiz, Cecilia Camacho and Julio Ariel Hurtado</li>
				<li><strong>How useful and understandable is the APPLIES framework?</strong><br>Luisa
					Fernanda Rincón Pérez, Jaime Chavarriaga, Raul Mazo and Camille
					Salinesi</li>
				<li><strong>A Collaborative Method for a Tangible Software Product
						Line Scoping</strong><br>Marta Cecilia Camacho, Julio Hurtado,
					Francisco Álvarez and Pablo Ruiz</li>
				<li><strong>Using Software Product Lines to Support Language
						Rehabilitation Therapies: an Experience Report</strong><br>Juan
					Carlos Martinez, Maria Constanza Pabon, Luisa Fernanda Rincón,
					Erika Jissel Gutierrez Beltran, Martin Sierra, Gloria Alvarez,
					Diego Linares, Andrés Darío Castillo, Anita Portilla and Valeria
					Almanza</li>
			</ul></td>
	</tr>
	<tr>
		<td>15:30-16:00</td>
		<td colspan="2" class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>16:00-17:30</td>
		<td class="warning" width="45%"><strong>WITS: Workshop on Intelligent
				Transportation Systems</strong><br> <i>Sala Norte</i><br>Session
			Chair: Luis Miguel Nuñez
			<ul>
				<li><strong>Keynote: Klaus Bans <br>ITS in Colombia and Latin
						America: An state of the art
				</strong></li>
				<li><strong>A prototype to manage the share of assisted bicycles on
						Bogota as a creative form of Public Transportation</strong><br>Ricardo
					Gonzalez, Luis Fajardo and David Flores</li>
				<li><strong>A real-time optimization model for bus control in a
						transit corridor: comparison of social objectives</strong>Claudia
					Cristina Bocanegra-Herrera, Delio Alexander Balcázar-Camacho and
					José Felix Vega</li>
				<li><strong>Discussion: ITS in Colombia</strong></li>
			</ul></td>
		<td class="warning" width="45%"><strong>WDEA: Workshop on Data
				Engineering and Analytics</strong><br> <i>Sala Sur</i><br>Session
			Chair: Carenne Ludeña
			<ul>
				<li><strong>Application of Machine Learning Techniques for PM10
						Levels Forecast in Bogotá</strong><br>Laura Melissa Montes Martin,
					Nicolas Mejia Martinez, Juan Felipe Franco and Ivan Mura</li>
				<li><strong>Model of neural networks for fertilizer recommendation
						and amendments in pasture crops</strong><br>Rafael Hernandez
					Moreno, Olmer Garcia and Luis Alejandro Arias R.</li>
				<li><strong>Formalistic Modelling Based on Pattern Recognition
						Applied to the Knowledge and Human Talent Sector in Ecuador</strong><br>Adrían
					Guayasamín, Walter Fuertes, Eduardo Campaña and Theofilos
					Toulkeridis</li>
				<li><strong>Analysis and Data Visualization of Labor Demand in
						Ecuadorian Territory</strong><br>Ana Maria Pazmino Delgado and

					Diaz</li>
			</ul></td>
	</tr>
	<tr>
		<td>18:00-20:00</td>
		<td colspan="2" class="danger text-center"><strong>Cocktail <br>(Sponsored
				by Manar Technologies)<br>Poster Exibition
		</strong><br>Library second floor</td>
	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th colspan="2" class="text-center">Friday 2 November</th>
	</tr>
	<tr>
		<td>8:00-9:00</td>
		<td colspan="2" class="success"><strong>Keynote: Waldemar Karwowski.
				Data Analytics for a Global Socio-Economic Development</strong> <br>
			<i>Aula Maxima</i></td>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td colspan="2" class="success"><strong>Keynote: Alejandro Correa
				Bahnsen: Phishing and Malicious TLS Certificate Detection using
				Artificial Intelligence</strong><br> <i>Aula Maxima</i></td>
	</tr>
	<tr>
		<td>10:00-10:30</td>
		<td colspan="2" class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>10:30-11:30</td>
		<td rowspan="2" class="info"><strong>ICAI Data Analysis Session</strong><br>
			<i>Sala Norte</i><br>Session Chair: Ixent Galpin
			<ul>
				<li><strong>Digital Observatory of Social Appropriation of Knowledge
						of a Territory</strong><br>Leidy Alexandra Lozano, Guillermo
					Antonio Gaona-Ramirez</li>
				<li><strong>Evaluation of the bias of Student Performance Data with
						assistance of Expert Teacher</strong><br>Cinthia Vegega, Pablo
					Pytel, Luciano Straccia, María Florencia Pollo-Cattaneo</li>
				<li><strong>Social Media Competitive Intelligence: Measurement and
						Visualization from a Higher Education Organization</strong><br>Olmer
					Garcia, Oscar Granado, Fran Romero</li>
				<li><strong>Towards Automated Advertising Strategy Definition Based
						on Analytics</strong><br>Vladimir Sanchez Riaño, O. Diaz, Carenne
					Ludeña, Liliana Catherine Suarez Baez, Jairo Sojo</li>

			</ul></td>
		<td rowspan="2" class="info" width="45%"><strong>ICAI Health Care
				Information Systems Session</strong><br> <i>Sala Sur</i><br>Session
			Chair: Hector Florez
			<ul>
				<li><strong>A system for the acquisition of data to predict
						pulmonary tuberculosis</strong><br>Juan D. Doria, Alvaro D.
					Orjuela-Cañón, Jorge E. Camargo</li>
				<li><strong>Enabling semantic interoperability of disease
						surveillance data in health information exchange systems for
						community health workers</strong><br>Nikodemus Angula, Nomusa
					Dlodlo</li>
				<li><strong>Enabling the Medical Applications Engine</strong><br>Fernando
					Yepes Calderon, Nolan Rea, J. Gordon McComb</li>
				<li><strong>Use of Virtual Reality using render semi-realistic as an
						alternative medium for the treatment of phobias. Case Study:
						Arachnophobia</strong><br>Jonathan Almeida, David Suárez, Freddy
					Tapia, Graciela Guerrero</li>
			</ul></td>
	</tr>
	<tr>
		<td>11:30-12:30</td>

	</tr>
	<tr>
		<td>12:30-14:00</td>
		<td colspan="2" class="active text-center">Lunch</td>
	</tr>
	<tr>
		<td>14:00-15:30</td>
		<td class="info" width="45%"><strong>ICAI IT Architectures Session</strong>
			<br> <i>Sala Norte</i><br>Session Chair: Jaime Chavarriaga
			<ul>
				<li><strong>Automating Information Security Risk Assessment for IT
						Services</strong><br>Sandra Rueda, Oscar Avila</li>
				<li><strong>A Security based Reference Architecture for
						Cyber-Physical Systems</strong><br>Shafiq ur Rehman, Andrea
					Iannella, Volker Gruhn</li>
				<li><strong>An Effective Wireless Media Access Controls Architecture
						for Content Delivery Networks</strong><br>Ayegba Alfa Abraham,
					Abubakar Adinoyi Sadiku, Sanjay Misra, Adewole Adewumi, Ravin
					Ahuja, Robertas Damasevicius, Rytis Maskeliunas</li>
				<li><strong>Smart-Solar Irrigation System (SMIS) for Sustainable
						Agriculture</strong><br>Olusola Abayomi-Alli, Modupe Odusami,
					Daniel Ojinaka, Olamilekan Shobayo, Sanjay Misra, Robertas
					Damasevicius, Rytis Maskeliunas</li>
			</ul></td>
		<td class="info"><strong>ICAI Learning Management Systems Session</strong><br>
			<i>Sala Sur</i><br>Session Chair: Olmer Garcia
			<ul>
				<li><strong>Applying the Flipped Classroom Model using a VLE for
						Foreign Languages Learning</strong><br>Oscar Mendez, Hector Florez
				</li>
				<li><strong>An Educational Math Game for High School Students in
						Sub-Saharan Africa</strong><br>Damilola Oyesiku, Adewole Adewumi,
					Sanjay Misra, Ravin Ahuja, Robertas Damasevicius, Rytis Maskeliunas
				</li>
				<li><strong>Selecting attributes for inclusion in an educational
						recommender system using the Multi-Attribute Utility Theory</strong><br>Munyaradzi
					Maravanyika, Nomusa Dlodlo</li>
			</ul></td>
	</tr>
	<tr>
		<td>15:30-16:00</td>
		<td colspan="2" class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>16:00-17:30</td>
		<td class="info"><strong>ICAI Decision Systems Session</strong><br> <i>Sala
				Norte</i><br>Session Chair: Oscar Acosta
			<ul>
				<li><strong>CHIN: Classification with meta-paths in Heterogeneous
						Information Networks</strong><br>Jinli Zhang, Zongli Jiang, Tong
					Li</li>
				<li><strong>Decision Model for the Pharmaceutical Distribution of
						Insulin</strong><br>Mariana Jacobo-Cabrera, Santiago-Omar
					Caballero-Morales, José-Luís Martínez-Flores, Patricia Cano-Olivos
				</li>
				<li><strong>Global Snapshot File Tracker</strong><br>Carlos E.
					Gómez, Jaime Chavarriaga, David C. Bonilla, Harold E. Castro</li>
			</ul></td>
		<td class="info"><strong>ICAI Mobile Information Processing Systems
				Session</strong><br> <i>Sala Sur</i><br>Session Chair: Hector Florez
			<ul>
				<li><strong>Android Malware Detection: A survey</strong><br>Modupe
					Odusami, Olusola Abayomi-Alli, Sanjay Misra, Olamilekan Shobayo,
					Robertas Damasevicius, Rytis Maskeliunas</li>
				<li><strong>Architectural Approaches for Phonemes Recognition
						Systems</strong><br>Luis Wanumen, Hector Florez</li>
				<li><strong>Towards a framework for the adoption of mobile
						Information Communication Technology dynamic capabilities for
						Namibian Small and Medium Enterprises</strong><br>Albertina
					Sumaili, Nomusa Dlodlo and Jude Odiakaosa Osakwe</li>
			</ul></td>
	</tr>
	<tr>
		<td>18:00-20:00</td>
		<td class="danger text-center" width="45%"><strong>Tutorial Manar
				Technologies</strong><br>Sala Computo Norte</td>
		<td class="danger text-center" width="45%"><strong>Tutorial IT
				Performa</strong><br>Sala Computo Norte</td>
	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th colspan="2" class="text-center">Saturday 3 November</th>
	</tr>
	<tr>
		<td>8:00-9:00</td>
		<td rowspan="2" class="info" width="45%"><strong>ICAI Robotic Autonomy
				Session</strong><br> <i>Sala Norte</i><br>Session Chair: Guillermo
			Guarnizo
			<ul>
				<li><strong>3D scene reconstruction Based on a 2D moving LiDAR</strong><br>Harold
					F Murcia, Maria Fernanda Monroy, Luis Fernando Mora</li>
				<li><strong>GPU-Implementation of a Sequential Monte Carlo technique
						for the Localization of an Ackerman robot</strong><br>Olmer
					Garcia, David Acosta, Cesar Diaz</li>
				<li><strong>Modulation of Central Pattern Generators (CPG) for the
						locomotion planning of an articulated robot</strong><br>Edgar
					Mario Rico Mesa, Jesús-Antonio Hernández-Riveros</li>
			</ul></td>
		<td rowspan="2" class="info" width="45%"><strong>ICAI Software Design
				Engineering Session</strong><br> <i>Sala Sur</i><br>Session Chair:
			Gilberto Pedraza
			<ul>
				<li><strong>Methodology for the retrofitting of manufacturing
						resources for migration of SME towards industry 4.0</strong><br>Juan
					David Contreras Pérez, Ruth Edmy Cano Buitrón, José Isidro García
					Melo</li>
				<li><strong>Model Driven Engineering approach to configure Software
						reusable components</strong><br>Hector Florez, Marcelo Leon</li>
				<li><strong>Multi-SPLOT: Supporting Multi-user Configurations with
						Constraint Programming</strong><br>Sebastian Velásquez-Guevara,
					Gilberto Pedraza, Jaime Chavarriaga</li>
			</ul></td>
	</tr>
	<tr>
		<td>9:00-10:00</td>

	</tr>
	<tr>
		<td>10:00-10:30</td>
		<td colspan="2" class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>10:30-11:30</td>
		<td colspan="2" class="active text-center"><strong>Conference Closing</strong><br>
			<i>Hemiciclo</i><br>
		<strong> Touristic Activity</strong><br> <i>Gold Museum</i>
	
	</tr>
</table>