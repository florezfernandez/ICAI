<h3>Registration</h3>
<p>
	At least one author of each accepted paper must register by August 2<sup>nd</sup>
	2018 to ensure its inclusion in the conference proceedings. Each
	registration will allow the publication and presentation of just one
	paper. Authors must pay an extra fee if they wish to publish and
	present more than one paper. One registration permits only the
	participation of one author in the conference. In addition, the
	registration fee includes:
</p>
<ul>
	<li>Conference material</li>
	<li>Lunch on working days of the conference</li>
	<li>Coffee breaks</li>
</ul>

<h3>Registration Fees</h3>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<table class="table table-hover table-bordered">
				<tr class="info">
					<th class="text-center">Registration</th>
					<th class="text-center">Fee</th>
				</tr>
				<tr>
					<td>Regular Author</td>
					<td class="text-center">350 USD</td>
				</tr>
				<tr>
					<td>Student Author <br>IEEE Author<br>Workshop Author
					</td>
					<td class="text-center">250 USD</td>
				</tr>
				<tr>
					<td>One Additional Paper</td>
					<td class="text-center">150 USD</td>
				</tr>
				<tr>
					<td>Poster Author</td>
					<td class="text-center">150 USD</td>
				</tr>
				<tr>
					<td>Attendee</td>
					<td class="text-center">100 USD</td>
				</tr>
			</table>
		</div>
	</div>
</div>