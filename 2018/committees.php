<h3>General Chairs</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia</li>
	<li>Cesar Diaz, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Jaime Chavarriaga, Ph.D. Universidad de los Andes, Colombia</li>
</ul>

<h3>Organizing Committee</h3>
<ul>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Olmer García, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Rafael Hernandez, M.Sc. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Carenne Ludena, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Edgar Ruiz, M.Sc. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Edgar Vargas, M.Sc. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
</ul>

<h3>Workshops Committee</h3>
<ul>
	<li>Jaime Chavarriaga, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Olmer García, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
</ul>

<h3>Publication Chair</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia</li>
</ul>

<h3>Program Committee</h3>
<ol>
	<li>Fernanda Almeida, Ph.D. Universidade Federal do ABC, Brazil</li>
	<li>Diego Angulo, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Andres Aristizabal, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Oscar Avila, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Jorge Bacca, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia</li>
	<li>David Becerra, Ph.D. McGill University, Canada</li>
	<li>Xavier Besseron, Ph.D. University of Luxembourg, Luxembourg</li>
	<li>Dominik Bork, Ph.D. Universität Wien, Austria</li>
	<li>Francisco Brasileiro, Ph.D. Universidade Federal de Campina Grande, Brazil</li>
	<li>Robert Buchmann, Ph.D. Universitatea Babes-Bolyai, Romania</li>
	<li>Raymundo Buenrostro, Ph.D. Universidad de Colima, Mexico</li>
	<li>Monica Castañeda, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Jaime Chavarriaga, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Erol Chioasca, Ph.D. The University of Manchester, United Kingdom</li>
	<li>Cesar Diaz, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Diana Diaz, Ph.D. Wayne State University, United States of America</li>
	<li>Helga Duarte, Ph.D. Universidad Nacional de Colombia, Colombia</li>
	<li>Silvia Fajardo, Ph.D. Universidad de Colima, Mexico</li>
	<li>Mauri Ferrandin, Ph.D. Universidade Federal de Santa Catarina, Brazil</li>
	<li>Hans-Georg Fill, Ph.D. Universität Bamberg, Germany</li>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia</li>
	<li>Efraín Fonseca, Ph.D. Universidad de las Fuerzas Armadas ESPE, Ecuador</li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Paulo Gaona, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia</li>
	<li>Kelly Garces, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Maira García, Ph.D. Universidad EAN, Colombia</li>
	<li>Olmer García, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Leonardo Garrido, Ph.D. Tecnológico de Monterrey, Mexico</li>
	<li>Matias Gerard, Ph.D. Universidad Nacional del Litoral, Argentina</li>
	<li>Cecilia Giuffra, Ph.D. Universidade Federal de Santa Catarina, Brazil</li>
	<li>Raphael Gomes, Ph.D. Instituto Federal de Goiás, Brazil</li>
	<li>Jānis Grabis, Ph.D.	Rīgas Tehniskā Universitāte, Latvia</li>
	<li>Guillermo Guarnizo, Ph.D. Universidad Santo Tomas, Colombia</li>
	<li>Antonio Guerrero, Ph.D. Universidad de Colima, Mexico</li>
	<li>Jens Gulden, Ph.D. Universität Duisburg Essen, Germany</li>
	<li>Cesar Hernandez, Ph.D. Universidad Mauela Beltran, Colombia</li>
	<li>Ta’id Holmes, Ph.D. Deutsche Telekom Technik GmbH, Germany</li>
	<li>Gilles Hubert, Ph.D. Institut de Recherche en Informatique de Toulouse, France</li>
	<li>Manfred Jeusfeld, Ph.D. Högskolan i Skövde, Sweeden</li>
	<li>Monika Kaczmarek, Ph.D. Universität Duisburg Essen, Germany</li>
	<li>Dimitris Karagiannis, Ph.D. Universität Wien, Austria</li>
	<li>Rodrigo Kato, Ph.D. Universidade Federal de Minas Gerais, Brazil</li>
	<li>Daniel Katz, Ph.D. University of Illinois Urbana-Champaign, United States of America</li>
	<li>Samee Khan, Ph.D. North Dakota State University, United States of America</li>
	<li>Paula Lago, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Robert Laurini, Ph.D. Knowledge Systems Institute, United States of America</li>
	<li>Maria Leitner, Ph.D. AIT Austrian Institute of Technology, Austria</li>
	<li>Marcelo Leon, Ph.D. Universidad Estatal Península de Santa Elena, Ecuador</li>
	<li>Tong Li, Ph.D. Beijing University of Technology, China</li>
	<li>Sandra Londoño, Ph.D. Universidad del Valle, Colombia</li>
	<li>Orlando Lopez, Ph.D. Universidad El Bosque, Colombia</li>
	<li>Carenne Ludena, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Thomas Luft, Ph.D. Lehrstuhl für Konstruktionstechnik, Germany</li>
	<li>Jose Marquez, Ph.D. Universidad del Norte, Colombia</li>
	<li>Juan Mendivelso, Ph.D. Universidad Nacional de Colombia, Colombia</li>
	<li>Osval Montesinos, Ph.D. Universidad de Colima, Mexico</li>
	<li>German Montoya, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Jose Moreno, Ph.D. Institut de Recherche en Informatique de Toulouse, France</li>
	<li>Iván Mura, Ph.D. Universidad de los Andes, Colombia</li>	
	<li>Sergio Nesmachnow, Ph.D. Universidad de la Republica, Uruguay</li>	
	<li>German Obando, Ph.D. Universidad del Rosario, Colombia</li>
	<li>Diego Ordoñez, Ph.D. Universidad Tecnológica Equinoccial, Ecuador</li>	
	<li>Xavier Oriol, Ph.D. Universitat Politècnica de Catalunya, Spain</li>
	<li>Alis Pataquiva, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
	<li>Vicente Perez, Ph.D. Fundación para el Fomento de la Investigación Sanitaria y Biomédica de la Comunidad Valenciana, Spain</li>
	<li>Yoann Pitarch, Ph.D. Institut de Recherche en Informatique de Toulouse, France</li>	
	<li>Juan Posada, Ph.D. Universitat Politècnica de València, Spain</li>
	<li>Irwing Reascos, Ph.D. Universidad Técnica del Norte, Ecuador</li>
	<li>Jairo Rodas, Ph.D. Universidad Nacional de Colombia, Colombia</li>
	<li>Leonardo Rodriguez, Ph.D. Universidad EAN, Colombia</li>
	<li>Marcela Ruiz, Ph.D. Universiteit Utrecht, Netherlands</li>
	<li>Karina Salvatierra, Ph.D. Universidad Nacional de Misiones, Argentina</li>
	<li>Mario Sanchez, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Gloria Sandoval, Ph.D. Universidad Nacional de Colombia, Colombia</li>
	<li>Christoph Schütz, Ph.D. Johannes Kepler Universität Linz, Austria</li>
	<li>Gorica Tapandjieva, Ph.D. Ecole Polytechnique Fédérale de Lausanne, Switzerland</li>
	<li>Andrei Tchernykh, Ph.D. Centro de Investigación Científica y de Educación Superior de Ensenada, Mexico</li>
	<li>German Vargas, Ph.D. Universidad El Bosque, Colombia</li>
	<li>John Vasquez, Ph.D. Universidad de los Andes, Colombia</li>
	<li>German Vega, Ph.D. Laboratoire Informatique de Grenoble, France</li>
	<li>Jorge Villalobos, Ph.D. Universidad de los Andes, Colombia</li>
	<li>Sebastian Zapata, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
</ol>