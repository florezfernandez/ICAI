<h3>Organized by</h3>
<div class="text-center">
	<a href="https://www.udistrital.edu.co" target="_blank"><img
		src="../img/logos/ud.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Universidad Distrital Francisco Jose de Caldas"></a>
	<a href="http://www.utadeo.edu.co/en/utadeo-english-version" target="_blank"><img
		src="../img/logos/utadeo.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Universidad de Bogota Jorge Tadeo Lozano"></a>
</div>
<h3>Sponsored by</h3>
<div class="text-center">
	<a href="http://www.itiud.org" target="_blank"><img
		src="../img/logos/iti.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Information Technologies Innovation Research Group"></a> 
	<a href="https://www.ucf.edu/" target="_blank"><img
		src="../img/logos/ucf.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="University of Central Florida"></a>
	<a href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer2.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a>
	<a href="http://www.acis.org.co/" target="_blank"><img
		src="../img/logos/acis.jpg" height="80" data-toggle="tooltip"
		data-placement="bottom" title="Asociación Colombiana de Ingenieros de Sistemas"></a>
	<a href="https://www.cyxtera.com/" target="_blank"><img
		src="../img/logos/cyxtera.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="Cyxtera"></a><br>
	<a href="http://www.itperforma.com/" target="_blank"><img
		src="../img/logos/itperforma.png" height="80" data-toggle="tooltip"
		data-placement="bottom"
		title="itPerforma"></a> <br>
	<a href="https://manar.com.co/" target="_blank"><img
		src="../img/logos/manar.jpg" height="60" data-toggle="tooltip"
		data-placement="bottom"
		title="Manar Technologies"></a> 
</div>
<h3>Supported by</h3>
<div class="text-center">
	<a href="https://www.ieee.org/" target="_blank"><img
		src="../img/logos/ieee.jpg" height="80" data-toggle="tooltip"
		data-placement="bottom" title="IEEE"></a>
</div>