<?php
$dir = "img/city/";
$dirObj = opendir($dir);
$pictures = array();
while (($file = readdir($dirObj)) != null) {
    if ($file != ".." && $file != ".") {
        array_push($pictures, $file);
    }
}
closedir($dirObj);
sort($pictures);
?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<?php
for ($i = 0; $i < count($pictures); $i ++) {
    echo "<li data-target='#myCarousel' data-slide-to='" . $i . "'", ($i == 0) ? "class='active'" : "", "></li>";
}
?>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<?php
for ($i = 0; $i < count($pictures); $i ++) {
    echo "<div class='", ($i == 0) ? "item active" : "item", "'>\n";
    echo "<img src='" . $dir . $pictures[$i] . "' width='100%'>\n";
    echo "</div>\n";
}
?>
	</div>

	<!-- Left and right controls -->
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span> <span
		class="sr-only">Previous</span>
	</a> <a class="right carousel-control" href="#myCarousel"
		data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<br>
<div class="row">

		<h3>ICAI 2018</h3>
		<p>The First International Conference on Applied Informatics (ICAI)
			aims to bring together researchers and practitioners working in
			different domains in the field of informatics in order to exchange
			their expertise and to discuss the perspectives of development and
			collaboration</p>
		<p>
			ICAI 2018 was held in the <strong>Universidad de Bogota Jorge
				Tadeo Lozano</strong> located in <strong>Bogota, Colombia</strong>,
			from 1 to 3 November 2018. It is organized
			by the Universidad Distrital Francisco Jose de Caldas and the
			Universidad de Bogotá Jorge Tadeo Lozano.
		</p>
		<p>ICAI 2018 is proudly sponsored by: University of Central Florida,
			Information Technologies Innovation Research Group, Springer, ACIS,
			Cyxtera, itPerforma, and Manar Technologies.</p>

		<p>
			ICAI 2018 proceedings has been published with Springer in their <strong><a
				href="http://www.springer.com/series/7899" target="_blank">Communications
					in Computer and Information Science (CCIS) series</a> Volume 942</strong>.
			The book name of the proceedings is <strong><a
				href="https://www.springer.com/us/book/9783030015343"
				target="_blank">Applied Informatics</a></strong>.
		</p>

		<h4>
			<mark>
				<strong>Papers are available in <a
					href="https://link.springer.com/book/10.1007/978-3-030-01535-0"
					target="_blank">https://link.springer.com/book/10.1007/978-3-030-01535-0</a>
				</strong>
			</mark>
		</h4>


		<p>
			Download certificates <a
				href="http://icaianalytics.itiud.org/certificates.php"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a>
		</p>

		<h3>Best Paper Award</h3>
		<p>
			The paper <strong>CHIN: Classification with meta-paths in
				Heterogeneous Information Networks</strong> <a
				href="https://link.springer.com/chapter/10.1007/978-3-030-01535-0_5"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a> authored by <i>Jinli Zhang, Zongli
				Jiang, Tong Li</i> from the <strong>Beijing University of
				Technology, China</strong> has been selected as Best Paper of ICAI
			2018.
		</p>
</div>