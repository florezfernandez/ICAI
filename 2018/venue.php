<h3>Venue</h3>
<p>
	ICAI 2018 will be held in the <strong>Universidad de Bogota Jorge Tadeo
		Lozano</strong> located in <strong>Bogota, Colombia</strong>. The
	university address is Carrera 4 # 22-61. Next figure presents he campus
	map.
</p>
<div class="text-center">
	<img src="img/campusMap.png" width="400px">
</div>

<h3>Transportation</h3>

<p>
	From the Bogota´s international airport <strong>El Dorado</strong>,
	there are the following options to go to the university:
</p>
<ul>
	<li><strong>Transmilenio</strong>. Transmilenio is the public
		transportation system in Bogota <a
		href="http://www.sitp.gov.co/plano_de_estaciones_y_portales_de_transmilenio"
		target="_blank"><span class="glyphicon glyphicon-new-window"
			aria-hidden="true"></span></a>. From the El Dorado airport, it is
		possible to take the route M86 in the transmilenio bus station, which
		is located in front of the gate 3 in the first floor. Later, it is
		neccesary to make a connection in the station <em>Portal Dorado</em>.
		The connecting route is 1. This route goes until the station <em>Universidades</em>,
		which is the closest station to the <em>Universidad de Bogota Jorge
			Tadeo Lozano</em>. Transmilenio ticket costs COP 2.300 (i.e., USD 0.8
		approx); however, it is necessary to buy just once, a transportation
		card named <em>Tu llave</em> that costs COP 3.000 (i.e., USD 1.1
		approx). This card can be bought in the airport. The schedule of the
		route 1 is: Monday to Friday 4:30am - 11:00pm, Saturday 5:00am -
		11:00pm, Sundays and Holidays 5:30am - 10:00pm. The schedule of the
		route M86 is: Monday to Friday 4:30am - 10:00pm, Saturday 5:00am -
		10:00pm, Sundays and Holidays 6:00am - 7:00am and 2:00pm - 9:00pm.
		(Monday 5th November 2018 is Holiday in Colombia)</li>
	<li><strong>Taxi</strong>. It is possible to get a safe taxi in the
		airport. The trip from the airport to the university can vary from 15
		to 30 minutes depending on the traffic. The trip can cost up to COP
		20.000 (i.e., USD 7.2 approx) depending on the route taken by the taxi
		driver. Unfortunately, taxi drivers use to take inappropiate routes in
		order to charge more.</li>
	<li><strong>Uber</strong>. It is possible to get uber in the airport.
		The trip from the airport to the university can vary from 15 to 30
		minutes depending on the traffic. The trip usually cost COP 25.000
		(i.e., USD 9 approx) depending on the dynamic rate.</li>
</ul>

<h3>Tourism in Bogota</h3>
<ol>
	<li><strong>Monserrate</strong>. Monserrate is a mountain in the city
		center of Bogota. It rises to 3.152 metres above the sea level. On the
		top, there is a church built in the 17th century.</li>

	<li><strong>Gold Museum</strong>. The gold museum has a collection of
		55.000 pieces. The gold museum displays a selection of pre-Colombian
		gold pieces from diferent indigenous cultures such as Calima,
		Quimbaya, Muisca, Zenú, Tierradentro, San Agustín, Tolima, Tairona,
		and Urabá. The museum contains the largest collection of gold
		artifacts in the world.</li>
	<li><strong>Botero Museum</strong>. The museum has a collection of 123
		works of Fernando Botero and 85 of other artists for a total of 208
		works of art.</li>
	<li><strong>Colombian Currency Museum</strong>. It contains a
		collection of colombian coins and bills. In addition, it includes an
		art collection from the Colombian Republic Bank</li>
	<li><strong>National Museum of Colombia</strong>. The museum is the
		biggest and oldest museum in Colombia. The museum contains a
		collection of over 20,000 pieces including works of art and objects
		representing different national history periods</li>
	<li><strong>Quinta de Bolivar Museum</strong>. It is a colonial house
		that was the residence of Simon Bolivar, who was one of the main
		people of the independence war. It is now used as a museum dedicated
		to Bolivar´s life and times</li>
	<li><strong>Military Museum of Colombia</strong>. It exibits weapons,
		uniforms, models, and other elements of the historic evolution of the
		national army, navy, andd air force.</li>
	<li><strong>Bogota Museum of Modern Art</strong>. Its collection
		includes art pieces from the 19th century. These piaces are from Latin
		America, Europa, and North America.</li>
	<li><strong>Planetarium of Bogota</strong>. It is a cultural place for
		cientific divulgation from a didactic perspective. Its aim is to
		motivate and facilitate the inmersion in some topics of science,
		technology, and art.</li>
</ol>

<iframe
	src="https://www.google.com/maps/d/u/2/embed?mid=1qo6Ek5SLQ00JOc_0LVcmHgfny0ae-U7S"
	width="640" height="480"></iframe>
