<h3>Call for Papers</h3>
<p>ICAI aims to bring together researchers and practitioners interested
	in all topics related to informatics. Submitted papers should be
	related with one or more of the main topics proposed for the
	conference:</p>
<ul>
	<li>Artificial Intelligence and Decision Support Systems</li>
	<li>Bioinformatics</li>
	<li>Business Analytics</li>
	<li>Cloud Computing</li>
	<li>Databases and Information Systems Integration</li>
	<li>Enterprise Information Systems Applications</li>
	<li>Geoinformatics</li>
	<li>Information Technologies in Education</li>
	<li>Software and Systems Modeling</li>
	<li>Software Architectures</li>
</ul>

<h3>Submission Guidelines</h3>
<p>
	Authors must submit an original full paper (12 to 15 pages) that has
	not previously been published. All contributions must be written in
	english using latex following the Springer template (<a
		href="http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines"
		target="_blank">http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines</a>).
	The first anonymous version must be submitted in PDF.
</p>

<h3>Review Process</h3>
<p>
	All submissions will be reviewed by 3 experts. <strong>Authors must
		remove personal details, acknowledgements section and any other
		information related to the authors identity</strong>.
</p>

<h3>Submission Process</h3>

<p>
	To submit or upload a paper please go to <a
		href="https://easychair.org/conferences/?conf=icai2018"
		target="_blank">https://easychair.org/conferences/?conf=icai2018</a>
</p>

<h3>Proceedings</h3>

<p>It is planned to publish the proceedings of ICAI 2018 with Springer
	in their Communications in Computer and Information Science (CCIS)
	series (final approval pending)</p>
	
<h3>Abstracting/Indexing</h3>
<p>CCIS is abstracted/indexed in DBLP, Google Scholar, EI-Compendex,
	SCImago, Scopus. CCIS volumes are also submitted for the inclusion in
	ISI Proceedings.</p>	