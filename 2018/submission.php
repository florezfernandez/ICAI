<h3>Submission Guidelines</h3>
<p>
	Authors must submit an original full paper (12 to 15 pages) that has
	not previously been published. All contributions must be written in
	english using latex following the Springer template (<a
		href="http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines"
		target="_blank">http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines</a>).
	The first anonymous version must be submitted in PDF.
</p>

<h3>Review Process</h3>
<p>
	All submissions will be reviewed by 3 experts. <strong>Authors must
		remove personal details, acknowledgements section and any other
		information related to the authors identity</strong>. For accepted papers, it is neccesary to fill, print, sign, and scan the <a href="CCIS_SIP_CtP_Contract_Book_Contributor_Consent_to_Publish_ICAI.pdf" target="_blank">copyright form</a>
</p>

<h3>Submission Process</h3>

<p>
	To submit or upload a paper please go to <a
		href="https://easychair.org/conferences/?conf=icai2018"
		target="_blank">https://easychair.org/conferences/?conf=icai2018</a>
</p>
<div class="text-center">
	<a href="https://easychair.org" target="_blank"><img
		src="../img/logos/easychair.png" width="150" data-toggle="tooltip"
		data-placement="bottom" title="Easychair"></a>
</div>

<h3>Proceedings</h3>

<p>ICAI 2018 proceedings will be published with Springer
	in their <strong><a
		href="http://www.springer.com/series/7899" target="_blank">Communications in Computer and Information Science (CCIS)
	series</a> Volume 942.</strong> You can find ICAI 2018 in the list of forthcomming CCIS proceedings <a href="ftp://ftp.springernature.com/cs-proceeding/ccis/CCISforth.pdf" target="_blank">ftp://ftp.springernature.com/cs-proceeding/ccis/CCISforth.pdf</a></p>

<div class="text-center">
	<a href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer.jpg" width="250" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="http://www.springer.com/series/7899" target="_blank"><img
		src="../img/logos/ccis.jpg" width="150" data-toggle="tooltip"
		data-placement="bottom"
		title="Communications in Computer Information Science"></a>
</div>

<h3>Abstracting/Indexing</h3>
<p>CCIS is abstracted/indexed in DBLP, Google Scholar, EI-Compendex,
	SCImago, Scopus. CCIS volumes are also submitted for the inclusion in
	ISI Proceedings.</p>

<div class="text-center">
	<a href="http://dblp.org/" target="_blank"><img src="../img/logos/dbpl.png"
		height="100" data-toggle="tooltip" data-placement="bottom"
		title="DBPL"></a> <a href="https://scholar.google.com" target="_blank"><img
		src="../img/logos/googlescholar.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Google Scholar"></a> <a
		href="https://www.elsevier.com/solutions/engineering-village/content/compendex"
		target="_blank"><img src="../img/logos/ei.png" height="100"
		data-toggle="tooltip" data-placement="bottom" title="Ei Compendex"></a>
	<a href="http://www.scimagojr.com/" target="_blank"><img
		src="../img/logos/scimago.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="SCImago"></a> <a
		href="https://www.scopus.com/" target="_blank"><img
		src="../img/logos/scopus.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Scopus"></a> <a
		href="https://webofknowledge.com/" target="_blank"><img
		src="../img/logos/isi.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="ISI Web of Knowledge"></a>
</div>