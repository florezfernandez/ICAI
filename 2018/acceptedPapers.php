<h3>Accepted Papers</h3>
<h4>Data Analysis</h4>
<ul>
	<li><strong>Digital Observatory of Social Appropriation of Knowledge of
			a Territory</strong>
		<ul>
			<li>Leidy Alexandra Lozano, IS-PCE, Colombia</li>
			<li>Guillermo Antonio Gaona-Ramirez, IS-PCE, Colombia</li>
		</ul></li>
	<li><strong>Evaluation of the bias of Student Performance Data with
			assistance of Expert Teacher</strong>
		<ul>
			<li>Cinthia Vegega, National Technological University of Buenos
				Aires, Argentina</li>
			<li>Pablo Pytel, National Technological University of Buenos Aires,
				Argentina</li>
			<li>Luciano Straccia, National Technological University of Buenos
				Aires, Argentina</li>
			<li>María Florencia Pollo-Cattaneo, National Technological University
				of Buenos Aires, Argentina</li>
		</ul></li>
	<li><strong>Social Media Competitive Intelligence: Measurement and
			Visualization from a Higher Education Organization</strong>
		<ul>
			<li>Olmer Garcia, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
			<li>Oscar Granado, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
			<li>Fran Romero, Universidad El Bosque, Colombia</li>
		</ul></li>
	<li><strong>Towards Automated Advertising Strategy Definition Based on
			Analytics</strong>
		<ul>
			<li>Vladimir Sanchez Riaño, Universidad de Bogota Jorge Tadeo Lozano,
				Colombia</li>
			<li>Cesar O. Diaz, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
			<li>Carenne Ludeña, Universidad de Bogota Jorge Tadeo Lozano,
				Colombia</li>
			<li>Liliana Catherine Suarez Baez, Universidad de Bogota Jorge Tadeo
				Lozano, Colombia</li>
			<li>Jairo Sojo, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
		</ul></li>
</ul>

<h4>Decision Systems</h4>
<ul>
	<li><strong>CHIN: Classification with meta-paths in Heterogeneous
			Information Networks</strong>
		<ul>
			<li>Jinli Zhang, Beijing University of Technology, China</li>
			<li>Zongli Jiang, Beijing University of Technology, China</li>
			<li>Tong Li, Beijing University of Technology, China</li>
		</ul></li>

	<li><strong>Decision Model for the Pharmaceutical Distribution of
			Insulin</strong>
		<ul>
			<li>Mariana Jacobo-Cabrera, Universidad Popular Autonoma del Estado
				de Puebla, Mexico</li>
			<li>Santiago-Omar Caballero-Morales, Universidad Popular Autonoma del
				Estado de Puebla, Mexico</li>
			<li>José-Luís Martínez- Flores, Universidad Popular Autonoma del
				Estado de Puebla, Mexico</li>
			<li>Patricia Cano-Olivos, Universidad Popular Autonoma del Estado de
				Puebla, Mexico</li>
		</ul></li>
	<li><strong>Global Snapshot File Tracker</strong>
		<ul>
			<li>Carlos E. Gómez, Universidad de los Andes-Universidad del
				Quindío, Colombia</li>
			<li>Jaime Chavarriaga, Universidad de los Andes, Colombia</li>
			<li>David C. Bonilla, Universidad de los Andes, Colombia</li>
			<li>Harold E. Castro, Universidad de los Andes, Colombia</li>
		</ul></li>
</ul>

<h4>Health care information systems</h4>
<ul>
	<li><strong>A system for the acquisition of data to predict pulmonary
			tuberculosis</strong>
		<ul>
			<li>Juan D. Doria, Universidad Antonio Nariño, Colombia</li>
			<li>Alvaro D. Orjuela-Cañón, Universidad Antonio Nariño, Colombia</li>
			<li>Jorge E. Camargo, Universidad Antonio Nariño, Colombia</li>
		</ul></li>
	<li><strong>Enabling semantic interoperability of disease surveillance
			data in health information exchange systems for community health
			workers</strong>
		<ul>
			<li>Nikodemus Angula, Namibia University of Science and Technology,
				Namibia</li>
			<li>Nomusa Dlodlo, Namibia University of Science and Technology,
				Namibia</li>
		</ul></li>
	<li><strong>Enabling the Medical Applications Engine</strong>
		<ul>
			<li>Fernando Yepes Calderon, Children’s Hospital Los Angeles, United
				States of America</li>
			<li>Nolan Rea, Children’s Hospital Los Angeles, United States of
				America</li>
			<li>J. Gordon McComb, University of Southern California, United
				States of America</li>
		</ul></li>
	<li><strong>Use of Virtual Reality using render semi-realistic as an
			alternative medium for the treatment of phobias. Case Study:
			Arachnophobia</strong>
		<ul>
			<li>Jonathan Almeida, Universidad de las Fuerzas Armadas ESPE,
				Ecuador</li>
			<li>David Suárez, Universidad de las Fuerzas Armadas ESPE, Ecuador</li>
			<li>Freddy Tapia, Universidad de las Fuerzas Armadas ESPE, Ecuador</li>
			<li>Graciela Guerrero, Universidad de las Fuerzas Armadas ESPE,
				Ecuador</li>
		</ul></li>
</ul>

<h4>IT Architectures</h4>
<ul>
	<li><strong>A Security based Reference Architecture for Cyber-Physical
			Systems</strong>
		<ul>
			<li>Shafiq ur Rehman, University of Duisburg-Essen, Germany</li>
			<li>Andrea Iannella, University of Duisburg-Essen, Germany</li>
			<li>Volker Gruhn, University of Duisburg-Essen, Germany</li>
		</ul></li>
	<li><strong>An Effective Wireless Media Access Controls Architecture
			for Content Delivery Networks</strong>
		<ul>
			<li>Ayegba Alfa Abraham, Kogi State College of Education, Nigeria</li>
			<li>Abubakar Adinoyi Sadiku, Kogi State College of Education, Nigeria</li>
			<li>Sanjay Misra, Covenant University, Nigeria</li>
			<li>Adewole Adewumi, Covenant University, Nigeria</li>
			<li>Ravin Ahuja, University of Delhi, India</li>
			<li>Robertas Damasevicius, Kaunas University of Technology, Lithuania</li>
			<li>Rytis Maskeliunas, Kaunas University of Technology, Lithuania</li>
		</ul></li>
	<li><strong>Automating Information Security Risk Assessment for IT
			Services</strong>
		<ul>
			<li>Sandra Rueda, Universidad de los Andes, Colombia</li>
			<li>Oscar Avila, Universidad de los Andes, Colombia</li>
		</ul></li>
	<li><strong>Smart-Solar Irrigation System (SMIS) for Sustainable
			Agriculture</strong>
		<ul>
			<li>Olusola Abayomi-Alli, Covenant University, Nigeria</li>
			<li>Modupe Odusami, Covenant University, Nigeria</li>
			<li>Daniel Ojinaka, Covenant University, Nigeria</li>
			<li>Olamilekan Shobayo, Covenant University, Nigeria</li>
			<li>Sanjay Misra, Covenant University, Nigeria</li>
			<li>Robertas Damasevicius, Kaunas University of Technology, Lithuania</li>
			<li>Rytis Maskeliunas, Kaunas University of Technology, Lithuania</li>
		</ul></li>
</ul>

<h4>Learning management systems</h4>
<ul>
	<li><strong>Applying the Flipped Classroom Model using a VLE for
			Foreign Languages Learning</strong>
		<ul>
			<li>Oscar Mendez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
			<li>Hector Florez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
		</ul></li>
	<li><strong>An Educational Math Game for High School Students in
			Sub-Saharan Africa</strong>
		<ul>
			<li>Damilola Oyesiku, Covenant University, Nigeria</li>
			<li>Adewole Adewumi, Covenant University, Nigeria</li>
			<li>Sanjay Misra, Covenant University, Nigeria</li>
			<li>Ravin Ahuja, University of Delhi, India</li>
			<li>Robertas Damasevicius, Kaunas University of Technology, Lithuania</li>
			<li>Rytis Maskeliunas, Kaunas University of Technology, Lithuania</li>
		</ul></li>
	<li><strong>Selecting attributes for inclusion in an educational
			recommender system using the Multi-Attribute Utility Theory</strong>
		<ul>
			<li>Munyaradzi Maravanyika, Namibia University of Science and
				Technology, Namibia</li>
			<li>Nomusa Dlodlo, Namibia University of Science and Technology,
				Namibia</li>
		</ul></li>
</ul>

<h4>Mobile information processing systems</h4>
<ul>
	<li><strong>Android Malware Detection: A survey</strong>
		<ul>
			<li>Modupe Odusami, Covenant University, Nigeria</li>
			<li>Olusola Abayomi-Alli, Covenant University, Nigeria</li>
			<li>Sanjay Misra, Covenant University, Nigeria</li>
			<li>Olamilekan Sobayo, Covenant University, Nigeria</li>
			<li>Robertas Damasevicius, Kaunas University of Technology, Lithuania</li>
			<li>Rytis Maskeliunas, Kaunas University of Technology, Lithuania</li>
		</ul></li>
	<li><strong>Architectural Approaches for Phonemes Recognition Systems</strong>
		<ul>
			<li>Luis Wanumen, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
			<li>Hector Florez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
		</ul></li>
	<li><strong>Towards a framework for the adoption of mobile Information
			Communication Technology dynamic capabilities for Namibian Small and
			Medium Enterprises</strong>
		<ul>
			<li>Albertina Sumaili, Namibia University of Science and Technology,
				Namibia</li>
			<li>Nomusa Dlodlo, Namibia University of Science and Technology,
				Namibia</li>
			<li>Jude Osakwe, Namibia University of Science and Technology,
				Namibia</li>
		</ul></li>
</ul>

<h4>Robotic Autonomy</h4>
<ul>
	<li><strong>3D scene reconstruction Based on a 2D moving LiDAR</strong>
		<ul>
			<li>Harold F Murcia, Universidad de Ibagué, Colombia</li>
			<li>Maria Fernanda Monroy, Universidad de Ibagué, Colombia</li>
			<li>Luis Fernando Mora, Universidad de Ibagué, Colombia</li>
		</ul></li>
	<li><strong>GPU-Implementation of a Sequential Monte Carlo technique
			for the Localization of an Ackerman robot</strong>
		<ul>
			<li>Olmer Garcia, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
			<li>David Acosta, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
			<li>Cesar Diaz, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
		</ul></li>
	<li><strong>Modulation of Central Pattern Generators (CPG) for the
			locomotion planning of an articulated robot</strong>
		<ul>
			<li>Edgar Mario Rico Mesa, SENA, Colombia</li>
			<li>Jesús-Antonio Hernández-Riveros, Universidad Nacional de
				Colombia, Colombia</li>
		</ul></li>
</ul>

<h4>Software design engineering</h4>
<ul>
	<li><strong>Methodology for the retrofitting of manufacturing resources
			for migration of SME towards industry 4.0</strong>
		<ul>
			<li>Juan David Contreras Pérez, Pontificia Universidad Javeriana,
				Colombia</li>
			<li>Ruth Edmy Cano Buitrón, Universidad del Valle, Colombia</li>
			<li>José Isidro García Melo, Universidad del Valle, Colombia</li>
		</ul></li>
	<li><strong>Model Driven Engineering approach to configure Software
			reusable components</strong>
		<ul>
			<li>Hector Florez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
			<li>Marcelo Leon, Universidad Estatal Peninsula de Santa Elena Santa
				Elena, Ecuador</li>
		</ul></li>
	<li><strong>Multi-SPLOT: Supporting Multi-user Configurations with
			Constraint Programming</strong>
		<ul>
			<li>Sebastian Velásquez-Guevara, Universidad Piloto de Colombia,
				Colombia</li>
			<li>Gilberto Pedraza, Universidad Piloto de Colombia, Colombia</li>
			<li>Jaime Chavarriaga, Universidad de los Andes, Colombia</li>
		</ul></li>
</ul>
