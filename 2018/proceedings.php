<h3>2018</h3>
<ul>
	<li>ICAI by Springer CCIS Volume 942: <a href="https://link.springer.com/book/10.1007/978-3-030-01535-0" target="_blank">https://link.springer.com/book/10.1007/978-3-030-01535-0</a>.</li>
	<li>ICAI Workshops by IEEE: <a href="https://ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=8542640" target="_blank">https://ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=8542640</a>.</li>
</ul>