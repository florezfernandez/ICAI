<h3>Keynote Speakers for ICAI 2018</h3>

<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/db.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>David Benavides</strong> <a
				href="http://www.lsi.us.es/~dbc/en/?Home" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
		</h4>
		<p>David Benavides is an Associate Professor in the Universidad of
			Seville (Spain) since 2010. He is part of the Applied Software
			Engineering Research Group (ISA) and is one of the most recognized
			researcher on software product lines, software configuration and
			automated analysis of software variability. He is the chair of the
			Steering Comitte of the Software Product Line Conference and is part
			of the program Committes of many others of the most important
			conferences on software variability in the world. He has published
			more than a hundred of papers in conferences and journals. One of his
			papers, with more than one thousand of citations, has been considered
			the "most influential paper" in the subject. .</p>
		<h4>
			<strong>Keynote: Challenges on software variability and product lines</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> Reusing software have been a constant
			challenge through the 50 years of the software engineering. In the
			last decades, the variability has been considered a fundamental
			aspect to manage the systemic reuse of software, and to propose
			approaches such as system families, software product lines, software
			factories, feature-based development and generative programming have
			been proposed based on the analysis of variability. In this talk,
			David will talk and reflect on the challenges that exist in these
			subjects that many authors consider the foundation for developing
			software products in the next decades. .
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/ac.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Alejandro Correa Bahnsen</strong> <a
				href="https://www.linkedin.com/in/albahnsen/" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
		</h4>
		<p>Alejandro Correa Bahnsen is the Vice President of Research at
			Cyxtera Technologies. With a passion for machine learning, he
			considers himself a technology evangelist of data science. He has
			more than a decade of experience applying the use and development of
			Artificial Intelligence to real-world issues such as cybersecurity,
			risk management and marketing. In addition to advising the Cyxtera's
			executive team and customers on unique cybersecurity challenges,
			Alejandro manages the research and data science teams. He also
			creates and develops machine learning algorithms related to phishing
			detection, user identification, network defense, fraud prevention and
			malware detection. He is constantly improving Cyxteras' products with
			data science and artificial intelligence capabilities. Alejandro
			holds a Ph.D. in Machine Learning and Pattern Recognition from
			Luxembourg University. He has published over 20 academic and
			industrial papers in noteworthy peer-reviewed publications. He also
			taught the following subjects on a university level: econometrics,
			financial risk management, deep learning and natural language
			processing.</p>
		<h4>
			<strong>Keynote: Phishing and Malicious TLS Certificate Detection
				using Artificial Intelligence </strong>
		</h4>
		<p>
			<strong>Abstract:</strong> 91% of cybercrimes and attacks start with
			a phishing email. This means that cyber security researchers must
			focus on detecting phishing in all of its settings and uses. However,
			they face many challenges as they go up against sophisticated and
			intelligent attackers. As a result, they must use cutting-edge
			Machine Learning and Artificial Intelligence techniques to combat
			existing and emerging criminal tactics. Encryption is a tool that is
			widely used across the internet to secure legitimate communications,
			but is now being used by cybercriminals to hide their messages and
			carry out successful malware and phishing attacks while avoiding
			detection. Further aiding criminals is the fact that web browsers
			display a green lock symbol in the URL bar when a connection to a
			website is encrypted, creating false security in users who are more
			likely to enter their personal information into the page. The rise of
			attacks using encrypted sites means that information security
			researchers must explore new techniques to detect, classify, and take
			countermeasures against criminal traffic. So far, there is no
			standard approach for detecting malicious TLS certificates in the
			wild. Cyxtera researchers proposed a method for identifying malicious
			web certificates using deep neural networks and the content of TLS
			certificates to successfully identify malware certificates with an
			accuracy of 95 percent. In addition to combatting existing attacks,
			researchers must focus on the future of fraud. As Artificial
			Intelligence and Machine Learning become crucial to cyber security,
			criminals will undoubtedly begin to harness these powerful tools to
			enhance their attacks. Cyxtera researchers created an algorithm
			called DeepPhish to simulate the results of the weaponization of AI
			by real life cybercriminals, and came to the staggering conclusion
			that intelligent algorithms could increase their attack success by up
			to 3000%.
		</p>
	</div>

</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/vg.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Valérie Gauthier</strong> <a
				href="http://www.urosario.edu.co/Profesores/Listado-de-profesores/G/Gauthier-Umana-Valerie/"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a>
		</h4>
		<p>Valérie Gauthier is Mathematician from Universidad de los Andes,
			Bogota, Colombia. Winner of a grant from the European Community in
			the Erasmus Mundus masters program: ALGANT in algebra, geometry and
			number theory at University of Bordeaux I, France and Università
			degli Studi di Padova, Italy. She obtained her Ph.D. in applied
			mathematics, in the area of post-quantum cryptography from Danmarks
			Tekniske Universitet (DTU). In addition, she was a postdoctoral
			researcher at the laboratory GREYC Université de Caen Normandie,
			France in the AMMAC team and a postdoctoral researcher at the
			Mathematics Department at the Universidad de los Andes. She is the
			head of the Applied Mathematics and Computer Science Department at
			Universidad del Rosario, Bogotá, Colombia. Her research areas of
			interest are cryptography, coding theory and its applications. She is
			also interested in innovation ecosystems.</p>
		<h4>
			<strong>Keynote: An Introduction to Post-Quantum Cryptology</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> In all time, to be able to communicate in
			a safe way has been essential in political, diplomatic, economic and
			military affairs. In this era, when the information is digital,
			cryptology (the art of keeping the information secret) plays an even
			more important role, being the only tool that allows us to protect
			communications, commerce, security in databases and, therefore, our
			identity. In this talk I will give a brief introduction to
			cryptology, from the classical one to the postquantum challenge, I
			will focus particularly in code-based cryptography.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/wk.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Waldemar Karwowski</strong> <a
				href="http://iems.ucf.edu/people/waldemar-karwowski" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
		</h4>
		<p>Waldemar Karwowski, Ph.D., D.Sc., P.E. is Pegasus Professor and
			Chairman, Department of Industrial Engineering and Management Systems
			and Executive Director, Institute for Advanced Systems Engineering,
			University of Central Florida, Orlando, USA. He holds an M.S. (1978)
			in Production Engineering and Management from the Technical
			University of Wroclaw, Poland, and a Ph.D. (1982) in Industrial
			Engineering from Texas Tech University. He was awarded D.Sc. degree
			in management science by the State Institute for Organization and
			Management in Industry, Poland (2004). He also received honorary
			degrees (Doctor Honoris Causa) from the following institutions in
			Europe: South Ukrainian Odessa State Pedagogical University of
			Ukraine, (Odessa, Ukraine), Technical University of Kosice (Kosice,
			Slovakia), and Moscow State Institute of Radio, Electronics and
			Automation (MIREA Technical University) (Moscow, Russia). He is Past
			President of the International Ergonomics Association (2000-2003),
			and past President of the Human Factors and Ergonomics Society
			(2007). Dr. Karwowski served on the Committee on Human Factors/Human
			Systems Integration, National Research Council, the National
			Academies, USA (2007–2011). He has over 500 publications focused on
			human systems integration, safety, cognitive, human-centered-design,
			neuro-fuzzy systems, soft computing, nonlinear dynamics, and
			neuroergonomics. Professor Karwowski serves as Co-Editor-in-Chief of
			Theoretical Issues in Ergonomics Science journal (Taylor and Francis,
			Ltd).</p>
		<h4>
			<strong>Keynote: Data Analytics for a Global Socio-Economic
				Development</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> There is an increasing need to translate a
			vast amount of data into usable knowledge that can drive effective
			decision-making at the micro- and macro-societal levels. Industry,
			academia, and government are all looking for people with technical
			skills who can support discovery, management, interpretation, and
			translation of data into actionable insights that will facilitate the
			design of future technology, infrastructure, and human habitat. This
			presentation will highlight the importance of the field of data
			analytics for accelerated technological and socio-economic growth
			around the globe. Recent efforts by the Department of Industrial
			Engineering and Management Systems (IEMS) at the University of
			Central Florida (UCF), in the area of data analytics, which includes
			two new programs of MS in Data Analytics and MS in Healthcare Systems
			Engineering, will be discussed. In addition, an example of a large
			SocialSim research program funded by DARPA that focuses on scalable
			simulations for a better understanding of online social behaviors and
			their evolution will be provided.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/sm.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Sanjay Misra</strong> <a
				href="http://covenantuniversity.edu.ng/Profiles/Misra-Sanjay#.U_-3ifldXR0"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a>
		</h4>
		<p>Sanjay Misra is Full Professor of Computing Engineering since
			January 2010 at Coventant University, Nigeria. He has 25 years of
			wide experience in academic administration and researches in various
			universities in Asia, Europe, and Africa. He is the most productive
			researcher (SciVal Scopus analysis) in Nigeria, having more than 165
			publications during 2012-2017, but a total around 300 in the area of
			Software Engineering. Most of those publications are in WOS-70 in
			JCR/SCIE Journals. In addition, He got several awards for outstanding
			publications (e.g., 2014 IET Software Premium Award in UK,
			TUBITAK-Turkish Higher Education, and Atilim University). He has
			delivered more than 80 keynotes/invited talks in several
			international conferences and institutes (travelled more than 50
			countries). He is EIC of Advances in IT Personnel and Project
			Management IGI Global, IJPS, and Covenant Journal of ICT. He is
			founder chair of 3 annual international symposiums/workshops in
			Software Engineering (SEPA, ISSQ, and TTSDP). His current researches
			cover the areas of (but not limited to): Software quality assurance,
			project management, health informatics, AI, intelligent systems,
			cognitive informatics and web engineering. Sanjay holds a Ph.D. in
			Software Engineering, a Ph.D. in Applied Physics, a M.Sc. in Solid
			State Electronics, and a B.Sc in Physics and Chemistry.</p>
		<h4>
			<strong>Keynote: Improving Quality of Research Publications in ICT
				Researches in Developing Countries</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> The number of articles in ICT related high
			impact journals (in Journal Citation Report (JCR by Thomson Reuters))
			are limited from the authors of developing countries. This work
			presents some interesting results by considering a case study of a
			developing country and analyses the reasons for this backdrop.
			Finally, we discuss how to improve the quality of the research
			publication in existing environment and available resources.
		</p>
	</div>
</div>