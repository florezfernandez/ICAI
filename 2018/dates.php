<h3>Important Dates</h3>
<ul>
<li>Paper Submission: <del>May 31<sup>st</sup> 2018</del> June 8<sup>th</sup> 2018</li>
<li>Paper Notification: <del>July 5<sup>th</sup> 2018</del> July 20<sup>th</sup> 2018</li>
<li>Camera Ready: <del>August 2<sup>nd</sup> 2018</del> August 13<sup>th</sup> 2018.
	<ul><li><strong>Note:</strong> For accepted papers, it is neccesary to fill, print, sign, and scan the <a href="CCIS_SIP_CtP_Contract_Book_Contributor_Consent_to_Publish_ICAI.pdf" target="_blank">copyright form</a></li></ul></li>
<li>Authors Registration Deadline: August 20<sup>th</sup> 2018</li>
</ul>

<h3>Conditions</h3>
<ul>
<li>All dates are according to time zone -5 GMT</li>
<li>All accepted papers must be presented by an author, who must be registered</li>
</ul>