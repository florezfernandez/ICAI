<h3>Accommodation</h3>
<p>
	Suggested accommodations for ICAI 2018 participants
	from 31<sup>st</sup> October to 5<sup>th</sup> November 2018 in the following hotels:
</p>

<ul>
	<li><strong>Hotel Grand Park</strong> <a
				href="http://www.hotelgrandpark.com.co/" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a> 
	<ul>
		<li>Buffet breakfast</li>
		<li>WiFi</li>
		<li>Swimming pool</li>
		<li>Sauna</li>
		<li>Jacuzzi</li>
	</ul>
	<li><strong>Hotel Santa Monica</strong> <a
				href="https://www.hotelsantamonica.co/" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a> 
	<ul>
		<li>WiFi</li>
	</ul>	
	<li><strong>Hotel Monserrat</strong> <a
				href="http://www.hotelmonserratspa.inf.travel" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a> 
	<ul>
		<li>Breakfast</li>
		<li>WiFi</li>
	</ul>	
	<li><strong>Hotel La Sabana</strong> <a
				href="http://www.lasabanahotel.com" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a> 
	<ul>
		<li>Breakfast</li>
		<li>WiFi</li>
	</ul>	

</ul>