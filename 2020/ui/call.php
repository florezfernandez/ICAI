<h3>Call for Papers</h3>
<p class="text-justify">ICAI aims to bring together researchers and practitioners interested
	in all topics related to informatics. Submitted papers should be
	related to one or more of the main topics proposed for the
	conference:</p>
<ul>
	<li>Artificial Intelligence</li>
	<li>Bioinformatics</li>
	<li>Business Analytics</li>
	<li>Cloud Computing</li>
	<li>Data Analysis</li>
	<li>Decision Systems</li>
	<li>Enterprise Information Systems Applications</li>
	<li>Gamification and Serious Games</li>
	<li>Geoinformatics</li>
	<li>Health Care Information Systems</li>
	<li>Interaction Design and Usability</li>
	<li>IT Architectures</li>
	<li>Learning Management Systems</li>
	<li>Mobile Information Processing Systems</li>
	<li>Robotic Autonomy</li>	
	<li>Software and Systems Modeling</li>
	<li>Software Architectures</li>
	<li>Software Design Engineering</li>
</ul>

<?php 
include 'dates.php';
include 'submission.php';
?>