<h3>Program</h3>
<div class="alert alert-success" role="alert">
	<strong>The program is fixed to Nigeria Local time (GMT+1)</strong><br>
	Pre-recorded presentations are available.
</div>
<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="30%" class="text-center">Thursday 29 October</th>
		<th width="30%" class="text-center">Friday 30 October</th>
		<th width="30%" class="text-center">Saturday 31 October</th>
	</tr>
	<tr>
		<td>9:00-9:30</td>
		<td></td>
		<td class="table-success" rowspan="2">Keynote: Juanlu J. Laredo<br> <i>URL:
				<a href="https://covenantuniversity-edu-ng.zoom.us/j/94134094426"
				target="_blank">Zoom</a>
		</i></td>
	</tr>
	<tr>
		<td>9:30-10:00</td>
		<td class="table-danger">Conference Opening<br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/98815297226?pwd=TmVEajB0MWQ3V0tJaDF1eUI5WDRIQT09"
				target="_blank">Zoom</a></i></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success">Keynote: Rajkumar Buyya<br> <i>URL: Same
				than Conference Opening</i></td>
		<td class="table-success">Keynote: Vincenzo Piuri<br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/91848130474"
				target="_blank">Zoom</a></i>
		
		<td class="table-danger">Conference Closing<br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/98162965869"
				target="_blank">Zoom</a></i>
		</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-info">ICAI Artificial Intelligence Session<br> <i>URL:
				<a href="https://covenantuniversity-edu-ng.zoom.us/j/94726351547"
				target="_blank">Zoom</a>
		</i>
		</td>
		<td class="table-info">ICAI Data Analysis &amp; Decision Systems
			Sessions<br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/91063442810"
				target="_blank">Zoom</a></i>
		</td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-info">ICAI Artificial Intelligence &amp; Image
			Processing &amp; Simulation and Emulation Sessions<br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/93865275735"
				target="_blank">Zoom</a></i>
		</td>
		<td class="table-info">ICAI Data Analysis &amp; Software Design
			Engineering Sessions <br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/92789163964"
				target="_blank">Zoom</a></i>
		</td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info">ICAI Business Process Management &amp; Cloud
			Computing &amp; Health Care Information Systems &amp; Human-Computer
			Interaction &amp; Learning Management Systems Sessions<br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/94797599416"
				target="_blank">Zoom</a></i>
		</td>
		<td class="table-warning">Workshops WAAI &amp; WDEA &amp; WSSC<br> <i>URL:
				<a href="https://covenantuniversity-edu-ng.zoom.us/j/99048666938"
				target="_blank">Zoom</a>
		</i> <br> <br>Workshops AIESD <br> <i>URL: <a
				href="https://covenantuniversity-edu-ng.zoom.us/j/99148673187"
				target="_blank">Zoom</a></i>
		</td>
	</tr>
</table>

<h4>Leyend</h4>
<table class="table">
	<tr>
		<td class="table-info" width="5%"></td>
		<td width="45%">ICAI Sessions (Presentations of papers in CCIS)</td>
		<td class="table-success" width="5%"></td>
		<td width="45%">Keynotes</td>
	</tr>
	<tr>
		<td class="table-warning" width="5%"></td>
		<td width="45%">ICAI Workshops Sessions (Presentations of papers in
			CEUR-WS)</td>
		<td class="table-danger" width="5%"></td>
		<td width="45%">Social Activities</td>
	</tr>
</table>

<h3>Detailed Program</h3>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center">Thursday 29 October</th>
	</tr>
	<tr>
		<td>9:30-10:00</td>
		<td class="table-danger"><strong>Conference Opening</strong></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success"><strong>Keynote: Rajkumar Buyya. New
				Frontiers in Cloud and Edge Computing for Big Data &amp;
				Internet-of-Things Applications</strong><br> Session Chair: Sanjay
			Misra</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-info"><strong>ICAI Artificial Intelligence Session</strong><br>
			Session Chair: Fernando Yepes-Calderon
			<ul>
				<li><strong>A machine learning model to detect fake voice</strong><br>
					Yohanna Rodríguez-Ortega, Dora María Ballesteros, Diego Renza</li>
				<li><strong>A Q-Learning Hyperheuristic Binarization Framework to
						balance Exploration and Exploitation</strong><br> Diego Tapia,
					Broderick Crawford, Ricardo Soto, Felipe Cisternas, José Lemus,
					Mauricio Castillo, José García, Wenceslao Palma, Fernando Paredes,
					Sanjay Misra</li>

				<li><strong>Artificial Neural Network Model for Steel Strip Tandem
						Cold Mill Power Prediction</strong><br> Danilo G. de Oliveira,
					Eliton M. da Silva, Fabiano J. F. Miranda, José F. S. Filho, Rafael
					S. Parpinelli</li>

				<li><strong>Fairly ranking the neediest in worldwide social systems.
						An artificial intelligence approach born in cov2 pandemic outbreak</strong><br>
					Alvin Gregory, Eduardo Yepes Calderon, Fernando Jimenez, J. Gordon
					McComb, Fernando Yepes-Calderon</li>
				<li><strong>Intelligent Digital Tutor to Assemble Puzzles based on
						Artificial Intelligence Techniques</strong><br> Sara M. Cachique,
					Edgar S. Correa, C. H. Rodriguez-Garavito</li>

				<li><strong>Machine Learning Methodologies against Money Laundering
						in Non-Banking Correspondents</strong><br> Jorge Guevara, Olmer
					Garcia-Bedoya, Oscar Granados</li>

				<li><strong>Predicting polypharmacy side effects based on an
						enhanced domain knowledge graph</strong><br> Ruiyi Wang, Tong Li,
					Zhen Yang, Haiyang Yu</li>
			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-info"><strong>ICAI Artificial Intelligence &amp;
				Image Processing &amp; Simulation and Emulation Sessions</strong><br>Session
			Chair: Hector Florez<br>
			<ul>
				<li><strong>Prostate Cancer Diagnosis Automation Using Supervised
						Artificial Intelligence. A Systematic Literature Review</strong><br>
					Camilo Espinosa, Manuel Garcia, Fernando Yepes-Calderon, J. Gordon
					McComb, Hector Florez</li>

				<li><strong>The Bio-I capsule. Preventing contagion of aerial
						pathogens with real-time reporting in Evalu@</strong><br> Fernando
					Yepes-Calderon, Andres Felipe Giraldo Quiceno, Jose Fabian Carmona
					Orozco, J. Gordon McComb,</li>
				<li><strong>Comparison of Gabor Filters and LBP descriptors applied
						to spoofing attack detection in facial images</strong><br> Wendy
					Valderrama, Andrea Magadán, Raúl Pinto, José Ruiz,</li>

				<li><strong>Deep Transfer Learning Model for Automated Screening of
						Cervical Cancer Cells using Multi-Cell Images</strong><br> Vinay
					Khobragade, Naunika Jain, Dilip Singh Sisodia,</li>

				<li><strong>Tissue differentiation based on classification of
						morphometric features of nuclei</strong><br> Dominika Dudzińska,
					Adam Piórkowski</li>
				<li><strong>Early Breast Cancer Detection by Using a Sensor-Antenna</strong><br>
					H. F. Guarnizo-Mendez, N. P. Rodríguez Rincón, P. P Plata
					Rodríguez, M. A. Poloche Arango, D. F Márquez Romero</li>
				<li><strong>Temperature sensing in Hyperthermia Study in Breast
						Cancer Treatment using optical fiber Bragg gratings</strong><br>
					Andrés Triana, C. Camilo Cano, Hector F. Guarnizo-Mendez, Mauricio
					A. Poloche</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info"><strong>ICAI Business Process Management &amp;
				Cloud Computing &amp; Health Care Information Systems &amp;
				Human-Computer Interaction &amp; Learning Management Systems
				Sessions</strong><br> Session Chair: Christian Grevisse
			<ul>
				<li><strong>A Grammatical Model for the Specification of
						Administrative Workflow using Scenario as Modelling Unit</strong><br>Milliam
					Maxime Zekeng Ndadji, Maurice Tchoupe Tchendji, Clémentin Tayou
					Djamegni</li>
				<li><strong>A Linux Scheduler to Limit the Slowdown Generated by
						Volunteer Computing Systems</strong><br>Jaime Chavarriaga, Antonio
					de-la-Vega, Eduardo Rosales, Harold Castro
				
				<li><strong>Reliability analysis in Desktop Cloud systems</strong><br>
					Carlos E. Gómez, Jaime Chavarriaga, Harold E. Castro</li>
				<li><strong>A Framework for BYOD Continuous Authentication: Case
						Study with Soft-Keyboard Metrics for Healthcare Environment</strong><br>
					Luis de-Marcos, Carlos Cilleruelo, Javier Junquera, José-Javier
					Martínez-Herráiz</li>
				<li><strong>Diabetes Link: Platform for Self-Control and Monitoring
						People with Diabetes</strong><br> Enzo Rucci, Lisandro Delía,
					Joaquín Pujol, Paula Erbino, Armando De Giusti, Juan José
					Gagliardino</li>
				<li><strong>Design and Prototyping of a Wearable Kinesthetic Haptic
						Feedback System to Support Mid-Air Interactions in Virtual
						Environments</strong><br> Ekati Ekaterini Maria Sagia, Modestos
					Stavrakis</li>
				<li><strong>Peer Validation and Generation Tool for Question Banks
						in Learning Management Systems</strong><br> Andres Sanchez-Martin,
					Luis Barreto, Johana Martinez, Nikolay Reyes-Jalizev, Carolina
					Astudillo, Santiago Escobar, Osberth De Castro</li>
			</ul></td>
	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center" colspan="2">Friday 30 October</th>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td class="table-success" colspan="2"><strong>Keynote: Juanlu J.
				Laredo. A gentle introduction to computational neuroscience</strong><br>
			Session Chair: Christian Grevisse</td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success" colspan="2"><strong>Keynote: Vincenzo Piuri.
				Artificial Intelligence in Cloud Computing and Internet-of-Things</strong><br>
			Session Chair: Sanjay Misra</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-info" colspan="2"><strong>ICAI Data Analysis &amp;
				Decision Systems Sessions</strong><br> Session Chair: Olmer Garcia
			<ul>
				<li><strong>A Case-Based Approach to Assess Employees’ Satisfaction
						in Times of the Pandemic</strong><br> Ana Fernandes, Margarida
					Figueiredo, Almeida Dias, Jorge Ribeiro, José Neves, Henrique
					Vicente</li>

				<li><strong>A Data-Driven approach for automatic classification of
						extreme precipitation events: Preliminary results</strong><br> J.
					González-Vergara, D. Escobar-González, D. Chaglla-Aguagallo, D. H.
					Peluffo-Ordóñez,</li>

				<li><strong>A data-driven method for measuring the negative impact
						of sentiment towards China in the context of COVID-19</strong><br>
					Laura M. Muñoz, Maria Fernanda Ramirez, Jorge E. Camargo</li>

				<li><strong>A Virtual Wallet Product Recommender System based on
						Collaborative Filtering</strong><br> David Rodolfo Prieto-Torres,
					Ixent Galpin</li>
				<li><strong>A Multi-objective Evolutionary Algorithms Approach to
						Optimize a Task Scheduling Problem</strong><br> Nicolás Cobos,
					Ixtli Barbosa, Germán A. Montoya, Carlos Lozano-Garzon</li>

				<li><strong>Diabetes Classification Techniques: A Brief
						State-of-the-Art Literature Review</strong><br> Jeffrey O.
					Agushaka, Absalom E. Ezugwu</li>

				<li><strong>Learning from students' perception on professors through
						opinion mining</strong><br> Vladimir Vargas-Calderón, Juan S.
					Flórez, Leonel F. Ardila, Nicolas Parra-A, Jorge E. Camargo, Nelson
					Vargas</li>

			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-info" colspan="2"><strong>ICAI Data Analysis &amp;
				Software Design Engineering Sessions</strong><br>Session Chair:
			Ixent Galpin<br>
			<ul>
				<li><strong>Evaluating Models for a Higher Education Course
						Recommender System using State Exam Results</strong><br> Jenny
					Mayerly Díaz-Díaz, Ixent Galpin</li>
				<li><strong>Future scenarios of water security: a case of Bogotá
						river basin, Colombia</strong><br> Andres Chavarro, Monica
					Castaneda, Sebastian Zapata, Isaac Dyner</li>
				<li><strong>Inverse Data Visualization Framework (IDVF): Towards a
						prior-knowledge-driven data visualization</strong><br> M.
					Vélez-Falconi, J. González-Vergara, D. H. Peluffo-Ordóñez</li>
				<li><strong>The Application of DBSCAN Algorithm to Improve Variogram
						Estimation and Interpretation in Irregularly-sampled Fields</strong><br>
					O. O. Mosobalaje, O. D. Orodu, D. Ogbe African</li>
				<li><strong>Numerical Model-Software for Predicting Rock Formation
						Failure-Time using Fracture Mechanics</strong><br> Emmanuel E.
					Okoro, Samuel E. Sanni, Amarachukwu A. Ibe, Paul Igbinedion,
					Babalola Oni</li>
				<li><strong>Validation of Software Requirements Specifications by
						Students</strong><br> Alberto Sampaio, Isabel Braga Sampaio</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td width="45%" class="table-warning"><strong>Workshops WAAI &amp;
				WDEA &amp; WSSC</strong><br> Workshop Chair: Florencia
			Pollo-Cattaneo
			<ul>
				<li><strong>Comparative Study of Distance Measures for
						Non-Supervised Methods Fuzzy C-means &amp; K-means Applied in
						Image Segmentation</strong><br> Martin Velez, Josue Marin, Selena
					Jimenez, Lorena Guachi</li>
				<li><strong>Edge solution with machine learning and open data to
						interpret signs for people with visual disability</strong><br>
					Fabian Velosa, Hector Florez</li>
				<li><strong>Demand forecasting for inventory management using
						limited data sets. Case study on the Oil industry</strong><br>
					Jorge Ivan Romero Gelvez, Esteban Felipe Villamizar, Olmer
					Garcia-Bedoya, Jorge Herrera</li>
				<li><strong>Forecasting Credit Card Attrition using Machine Learning
						Models</strong><br> Carlos Alvaro Rico-Poveda, Ixent Galpin</li>
				<li><strong>Precision Agriculture Technology evaluation using
						combined AHP and GRA for data acquisition in Apiculture</strong><br>Jorge
					Ivan Romero Gelvez, Sandra Viviana Beltrán-Fernández, Andres Julian
					Aristizabal Cardona, Sebastian Zapata Ramirez, Monica Castañeda
					Riascos</li>
				<li><strong>Time Series Models for Colombian TRM</strong><br>Luis
					Giraldo-Alvarado, Javier Riascos-Ochoa</li>
				<li><strong>A Quality Evaluation of Wastewater in Quito's
						Metropolitan Area for Environmental Sustainability</strong><br>Marcelo
					León, Raquel Ibarra, Maria de La O Barroso González</li>
				<li><strong>Towards the Construction of a Smart City Model in Bogota</strong><br>María
					Clara Sánchez Vanegas, Álvaro Gutiérrez Rodríguez, José David López
					García, Manuel Dávila Sguerra, Laura González, Luis Cobo, César
					Díaz</li>

			</ul></td>
		<td width="45%" class="table-warning"><strong>Workshops AIESD</strong><br>
			Workshop Chair: Marcelo Leon
			<ul>
				<li><strong>A Scrum Implementation Plan by Phases and Oriented in
						the Team Members Case Study</strong><br>Olga Calderón-Martínez,
					Alix E. Rojas, Camilo Mejía-Moncayo</li>
				<li><strong>An immersive experience in the virtual 3D VirBELA
						environment for leadership development in students of
						technological programs in business administration During Covid19
						quarantine in Colombia</strong><br>Carlos Eduardo Mora-Beltrán,
					Alix E. Rojas, Camilo Mejía-Moncayo</li>
				<li><strong>Analysis of the Conservation Model of Territorial Forest
						and Vegetation Protection in Azuay - Ecuador</strong><br>Marcelo
					Leon, Maribel Alomoto, Maria de La O Barroso Gonzalez</li>
				<li><strong>Drilling and completion material management improvement
						by stochastic modeling</strong><br>Carlos Felipe
					Acevedo-Gutierrez, Gabriel Villalobos-Camargo, Jorge Ivan Romero
					Gelvez</li>
				<li><strong>How to Measure the Performance of a Smart City</strong><br>Teresa
					Guarda, Isabel Lopes, Maria Isabel Ribeiro, Pedro Oliveira, António
					José Fernandes</li>
				<li><strong>The impact of virtual platforms in the field of the
						knowledge and information society as a learning resource</strong><br>Erika
					Lucía Gonzalez Carrión, María Lorena Muñoz Vallejo, Johnny Héctor
					Sánchez Landín</li>
				<li><strong>The Mataquí, Pimampiro River Micro Watershed Management
						Experience and Its Problems Towards Sustainability</strong><br>Concepción
					Espinosa-Manosalvas, Fausto Calderon-Pineda, René Faruk Garzozi
					Pincay, Roxana Álvarez-Acosta, Yamel Sofia Garzozi Pincay</li>

			</ul></td>
	</tr>
</table>
