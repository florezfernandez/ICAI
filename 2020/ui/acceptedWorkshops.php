<h3>Accepted Workshops</h3>

<h4>
	First International Workshop on Applied Artificial Intelligence (WAAI)
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Artificial
	Intelligence is an annual event intended to be an important forum of
	the Artificial Intelligence (AI) community, organized by the GRUPO
	GEMIS of the Universidad Tecnológica Nacional Facultad Regional Buenos
	Aires. The workshop aims to provide a forum for researchers and AI
	community members to discuss and exchange ideas and experiences on
	diverse topics of AI. It also seeks to strengthen a space for
	discussion of innovative ideas as well as development of emerging
	technologies and practical experiences related to Artificial
	Intelligence in general and each of its technologies in particular.
	Submissions are invited on both applications of AI and new tools and
	foundations.
</p>

<h4>
	Second International Workshop on Applied Informatics for Economy,
	Society, and Development (AIESD) 
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Informatics for
	Economy, Society and Development organized by the SAEES Research Group
	of ABREC (Ecuador) seeks to promote discussion and dissemination of
	current economics research, systems for territorial intelligence,
	e-participation, e-democracy and territorial economic development; with
	special emphasis on social and environmental development. The objective
	of this second edition of AIESD 2020 is to discuss aspects related to
	computer science applied in society. The idea is to learn the different
	techniques and tools that are used to study social phenomena, economic
	growth, the interaction between economic analysis and decision-making.
	The workshop seeks to promote an atmosphere of dialogue among the
	community of professionals working on issues related to technology and
	its application in the economy as well as society.
</p>

<h4>
	Third International Workshop on Data Engineering and Analytics (WDEA) 
</h4>
<p>
	<strong>Abstract:</strong> Data has become pervasive. Data Analytics,
	Data Mining, Data Science, or more generally the practice of
	identifying patterns or constructing mathematical models based on data,
	is at the heart of most new business models or projects. This, coupled
	with what has been termed the Big Data phenomenon, used to describe
	challenging scenarios involving either datasets that are too big to be
	handled using traditional approaches, datasets with diverse formats and
	semantics, data streams that need to be actioned in real time, or a
	combination of these, has led to a whole new series of rapidly evolving
	tools, algorithms and methods which combine data engineering, computer
	science, statistics and mathematics. The aim of this workshop is to
	present recent results at the intersection of these areas, discussing
	new methods or algorithms, or applications including Machine Learning,
	Databases, optimization or any type of algorithms which consider
	managing or obtaining value from data.
</p>

<h4>
	Third International Workshop on Smart and Sustainable Cities (WSSC) 
</h4>
<p>
	<strong>Abstract:</strong> The International Telecommunication Union
	ITU define a smart sustainable city as: “An innovative city that uses
	information and communication technologies (ICTs) and other means to
	improve quality of life, efficiency of urban operation and services,
	and competitiveness, while ensuring that it meets the needs of present
	and future generations with respect to economic, social, environmental
	as well as cultural aspects”. The aim of this Workshop is to present
	recent results in this area, discussing new methods or algorithms, or
	applications in different areas including architecture, cultural,
	tourism, mobility, among others.
</p>

<h3>Important Dates</h3>
<ul>
	<li>Paper Submission: August 31, 2020</li>
	<li>Paper Notification: Septemeber 14, 2020</li>
	<li>Camera Ready: Septemeber 28, 2020</li>
	<li>Authors Registration: Septemeber 28, 2020</li>
</ul>

<h3>Proceedings</h3>
<p>Proceedings will be submitted to CEUR Workshop Proceedings for online publication.</p>