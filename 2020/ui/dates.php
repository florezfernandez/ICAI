<h3>Important Dates</h3>
<ul>
	<li>Paper Submission:
		<ul>
			<li><del>
					July 5<sup>th</sup> 2020
				</del></li>
			<li><strong>July 19<sup>th</sup> 2020
			</strong></li>
		</ul>
	</li>
	<li>Paper Notification:
		<ul>
			<li><del>
					July 26<sup>th</sup> 2020
				</del></li>
			<li><strong>August 16<sup>th</sup> 2020
			</strong></li>
		</ul>
	</li>
	<li>Camera Ready:
		<ul>
			<li><del>
					August 16<sup>th</sup> 2020
				</del></li>
			<li><strong>August 30<sup>th</sup> 2020
			</strong></li>
		</ul>
	</li>
	<li>Authors Registration:
		<ul>
			<li><del>
					August 16<sup>th</sup> 2020
				</del></li>
			<li><strong>August 30<sup>th</sup> 2020
			</strong></li>
		</ul>
	</li>
</ul>
