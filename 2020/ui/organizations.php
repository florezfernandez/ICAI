<hr>
<h3>Organized by</h3>
<div class="text-center">
	<a href="https://www.udistrital.edu.co" target="_blank"><img
		src="../img/logos/ud.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Distrital Francisco Jose de Caldas"></a> <a
		href="https://covenantuniversity.edu.ng/" target="_blank"><img
		src="../img/logos/ucovenant.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Covenant University"></a> <a
		href="https://www.futa.edu.ng/" target="_blank"><img
		src="../img/logos/futm.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Federal University of Technology Minna"></a>
</div>
<h3>Sponsored by</h3>
<div class="text-center">
	<a href="http://www.itiud.org" target="_blank"><img
		src="../img/logos/iti.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Information Technologies Innovation Research Group"></a> <a
		href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer2.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="https://www.strategicbp.net/" target="_blank"><img
		src="../img/logos/sbp.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Strategic Business Platforms"></a>
</div>
<hr>