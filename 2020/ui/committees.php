<h3>General Chairs</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Sanjay Misra, Ph.D. Covenant University, Nigeria <a href="https://orcid.org/0000-0002-3556-9331" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Honorary Chairs</h3>
<ul>
	<li>AAA Atayero, Covenant University, Nigeria</li>
	<li>Charles Ayo, Trinity University, Nigeria</li>
	<li>Abdullahi Bala, Federal University of Technology Minna, Nigeria</li>
</ul>

<h3>Steering Committee</h3>
<ul>
	<li>Jaime Chavarriaga, Ph.D. Universidad de los Andes, Colombia <a href="https://orcid.org/0000-0002-8372-667X" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Cesar Diaz, Ph.D. OCOX AI, Colombia <a href="https://orcid.org/0000-0002-9132-2747" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0001-7020-6328" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Olmer García, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0002-6964-3034" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Christian Grévisse, Ph.D. Université du Luxembourg, Luxembourg <a href="https://orcid.org/0000-0002-9585-1160" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Sanjay Misra, Ph.D. Covenant University, Nigeria <a href="https://orcid.org/0000-0002-3556-9331" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Fernando Yepes-Calderon, Ph.D. Children's Hospital Los Angeles, United States <a href="https://orcid.org/0000-0001-9184-787X" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Organizing Committee</h3>
<ul>
    <li>Shafi'i Muhammad Abdulhamid, Ph.D. Federal University of Technology Minna, Nigeria</li>
    <li>Joseph Adebayo Ojeniyi, Ph.D. Federal University of Technology Minna, Nigeria</li>
    <li>Emmanuel Adetiba, Ph.D. Covenant University, Nigeria</li>
    <li>Adeyinka Ajao Adewale, Ph.D. Covenant University, Nigeria</li>
    <li>Adewole Adewumi, Ph.D. Algonquin College Ottawa, Canada</li>
	<li>Adoghe Anthony, Ph.D. Covenant University, Nigeria</li>
    <li>Aderonke Atinuke Oni, Ph.D. Covenant University, Nigeria</li>
	<li>Amborse Azeta, Ph.D. Covenant University, Nigeria</li>
    <li>Joke Badejo, Ph.D. Covenant University, Nigeria</li>
    <li>Onyeka Emebo, Ph.D. Covenant University, Nigeria</li>
    <li>Azubuike Ezenwoke, Ph.D. Covenant University, Nigeria</li>
    <li>Francis Idachaba, Ph.D. Covenant University, Nigeria</li>
	<li>Ismaila Idris, Ph.D. Federal University of Technology Minna, Nigeria</li>
	<li>Nicholas Iwokwagh, Ph.D. Federal University of Technology Minna, Nigeria</li>
	<li>Sanjay Misra, Ph.D. Covenant University, Nigeria <a href="https://orcid.org/0000-0002-3556-9331" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
    <li>Isaac Odun-Ayo, Ph.D. Covenant University, Nigeria</li>
    <li>Modupe Odusami, Ph.D. Covenant University, Nigeria</li>
    <li>Morufu Olalere, Ph.D. Federal University of Technology Minna, Nigeria</li>
	<li>Jonathan Oluranti, Ph.D. Covenant University, Nigeria</li>
	<li>David Omole, Ph.D. Covenant University, Nigeria</li>
    <li>Victor Onomza Waziri, Ph.D. Federal University of Technology Minna, Nigeria</li>
    <li>Rajesh Prasad, Ph.D. African University of Science and Technology, Nigeria</li>
    <li>Isaac Samuel, Ph.D. Covenant University, Nigeria</li>
</ul>

<h3>Workshops Committee</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0001-7020-6328" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Christian Grévisse, Ph.D. Université du Luxembourg, Luxembourg <a href="https://orcid.org/0000-0002-9585-1160" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>International Advisory Committee</h3>
<ul>
	<li>Matthew Adigun, Ph.D. University of Zululand, South Africa</li>
	<li>Ricardo Colomo-Palacios, Ph.D. Ostford University College, Norway</li>
	<li>Luis Fernandez Sanz, Ph.D. Universidad de Alcala, Spain</li>
	<li>Murat Koyuncu, Ph.D. Atilim University, Turkey</li>
	<li>Raj Kumar Buyya, Ph.D. University of Melbourne, Australia</li>
	<li>Cristian Mateos, Ph.D. Universidad Nacional del Centro de la Provincia de Buenos Aires, Argentina</li>
	<li>Victor Mbarika, Ph.D. Southern University, USA</li>
</ul>


<h3>Program Committee</h3>
<ol>
<?php 
$pc = fopen("docs/pc.csv", "r");
while(!feof($pc)) {
    $data = explode(";", trim(fgets($pc)));
    if($data[0] != ""){
        echo "<li>" . $data[0] . " " . $data[1] . ", Ph.D. " . $data[3] . ", " . $data[4] . (($data[7]!="")?" <a href='" . $data[7] . "' target='_blank'><img src='../img/logos/orcid.png' ></a>":"") . "</li>\n";        
    }
}
?>
</ol>