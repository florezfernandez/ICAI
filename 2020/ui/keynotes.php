<h3>Keynote Speakers for ICAI 2020</h3>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/rb.jpg" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Rajkumar Buyya</strong> <a href="http://www.buyya.com"
				target="_blank"><span class="fas fa-external-link-alt"
				aria-hidden="true"></span></a>
		</h5>
		<p>
			Dr. Rajkumar Buyya is a Redmond Barry Distinguished Professor and
			Director of the Cloud Computing and Distributed Systems (CLOUDS)
			Laboratory at the University of Melbourne, Australia. He is also
			serving as the founding CEO of Manjrasoft, a spin-off company of the
			University, commercializing its innovations in Cloud Computing. He
			has authored over 750 publications and seven text books including
			"Mastering Cloud Computing" published by McGraw Hill, China Machine
			Press, and Morgan Kaufmann for Indian, Chinese and international
			markets respectively. Dr. Buyya is one of the highly cited authors in
			computer science and software engineering worldwide (h-index=135,
			g-index=300, 97, 100+ citations). "A Scientometric Analysis of Cloud
			Computing Literature" by German scientists ranked Dr. Buyya as the
			World's Top-Cited (#1) Author and the World's Most-Productive (#1)
			Author in Cloud Computing. Dr. Buyya is recognised as Web of Science
			“Highly Cited Researcher” for four consecutive years since 2016, IEEE
			Fellow, Scopus Researcher of the Year 2017 with Excellence in
			Innovative Research Award by Elsevier, and the “Best of the World”,
			in Computing Systems field, by The Australian 2019 Research Review. <br>Software
			technologies for Grid, Cloud, and Fog computing developed under
			Dr.Buyya's leadership have gained rapid acceptance and are in use at
			several academic institutions and commercial enterprises in 50
			countries around the world. Dr. Buyya has led the establishment and
			development of key community activities, including serving as
			foundation Chair of the IEEE Technical Committee on Scalable
			Computing and five IEEE/ACM conferences. These contributions and
			international research leadership of Dr. Buyya are recognized through
			the award of "2009 IEEE Medal for Excellence in Scalable Computing"
			from the IEEE Computer Society TCSC. Manjrasoft's Aneka Cloud
			technology developed under his leadership has received "Frost and
			Sullivan New Product Innovation Award". He served as founding
			Editor-in-Chief of the IEEE Transactions on Cloud Computing. He is
			currently serving as Editor-in-Chief of Software: Practice and
			Experience, a long standing journal in the field established ~50
			years ago.
		</p>
		<h5>
			<strong>Keynote: New Frontiers in Cloud and Edge Computing for Big
				Data &amp; Internet-of-Things Applications </strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Computing is being transformed to a model
			consisting of services that are delivered in a manner similar to
			utilities such as water, electricity, gas, and telephony. In such a
			model, users access services based on their requirements without
			regard to where the services are hosted or how they are delivered.
			Cloud computing paradigm has turned this vision of "computing
			utilities" into a reality. It offers infrastructure, platform, and
			software as services, which are made available to consumers as
			subscription-oriented services. Cloud application platforms need to
			offer (1) APIs and tools for rapid creation of elastic applications
			and (2) a runtime system for deployment of applications on
			geographically distributed computing infrastructure in a seamless
			manner. <br>The Internet of Things (IoT) paradigm enables seamless
			integration of cyber-and-physical worlds and opening up opportunities
			for creating new class of applications for domains such as smart
			cities and smart healthcare. The emerging Fog/Edge computing paradigm
			is extends Cloud computing model to edge resources for latency
			sensitive IoT applications with a seamless integration of
			network-wide resources all the way from edge to the Cloud. <br>This
			keynote presentation will cover (a) 21st century vision of computing
			and identifies various IT paradigms promising to deliver the vision
			of computing utilities; (b) innovative architecture for creating
			elastic Clouds integrating edge resources and managed Clouds, (c)
			Aneka, a Cloud Application Platform, for rapid development of
			Cloud/Big Data applications and their deployment on private/public
			Clouds with resource provisioning driven by SLAs, (d) a novel FogBus
			software framework with Blockchain-based data-integrity management
			for facilitating end-to-end IoT-Fog/Edge-Cloud integration for
			execution of sensitive IoT applications, (e) experimental results on
			deploying Cloud and Big Data/ IoT applications in engineering, and
			health care, satellite image processing, and smart cities on elastic
			Clouds; and (f) directions for delivering our 21st century vision
			along with pathways for future research in Cloud and Edge/Fog
			computing.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/jl.png" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Juanlu J. Laredo</strong>
		</h5>
		<p>Juanlu J. Laredo received his Ph.D. in Computer Science in 2010
			from the University of Granada (Spain) with a thesis entitled
			“Peer-to-Peer Evolutionary Computation: A Study of Viability”. He was
			a postdoctoral fellow at the University of Luxembourg (Luxembourg)
			before being appointed as a tenured Associate Professor at the
			University of Le Havre Normandy (France) where he currently works.
			His main research interests focus on nature-inspired problem solving
			and complex systems, being particularly interested in applications
			domains such as energy-efficiency, post-disaster relief and
			neuroscience. He has authored over 80 publications in peer-reviewed
			international journals, proceedings of international conferences and
			book chapters.</p>
		<h5>
			<strong>Keynote: A gentle introduction to computational neuroscience
			</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Neuroscience is certainly one of the most
			rapidly growing fields of research on applied informatics. In
			unprecedented efforts, scientific funding agencies are investing in
			long-term projects to unveil the mysteries behind the functioning of
			nervous systems. The European Human Brain Project, the Chinese China
			Brain project, the Japanese Brain/MINDS or the American Brain
			initiative are just some examples. We may find an answer to this
			interest in a race in which neuroscience is behind great advances in
			artificial intelligence, bio-inspired control systems or advances in
			medicine and biology. This keynote proposes a gentle introduction to
			all these aspects from the perspective of computational neuroscience
			which attempts to model nervous systems using computational support.
			In particular, we will exemplify the design of a conductance-based
			model of a neuron and show how traditional algorithms in computer
			science such as meta-heuristics can be applied to fine-tune these
			models.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/vp.png" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Vincenzo Piuri</strong> <a
				href="http://homes.di.unimi.it/piuri/" target="_blank"><span
				class="fas fa-external-link-alt" aria-hidden="true"></span></a>
		</h5>
		<p>Vincenzo Piuri has received his Ph.D. in computer engineering at
			Politecnico di Milano, Italy (1989). He is Full Professor in computer
			engineering at the Università degli Studi di Milano, Italy (since
			2000). He has been Associate Professor at Politecnico di Milano,
			Italy and Visiting Professor at the University of Texas at Austin and
			at George Mason University, USA. His main research interests are:
			artificial intelligence, computational intelligence, intelligent
			systems, machine learning, pattern analysis and recognition, signal
			and image processing, biometrics, intelligent measurement systems,
			industrial applications, digital processing architectures, fault
			tolerance, dependability, and cloud computing infrastructures.
			Original results have been published in more than 400 papers in
			international journals, proceedings of international conferences,
			books, and book chapters. He is Fellow of the IEEE, Distinguished
			Scientist of ACM, and Senior Member of INNS. He is President of the
			IEEE Systems Council (2020-21), and has been IEEE Vice President for
			Technical Activities (2015), IEEE Director, President of the IEEE
			Computational Intelligence Society, Vice President for Education of
			the IEEE Biometrics Council, Vice President for Publications of the
			IEEE Instrumentation and Measurement Society and the IEEE Systems
			Council, and Vice President for Membership of the IEEE Computational
			Intelligence Society. He is Editor-in-Chief of the IEEE Systems
			Journal (2013-19), and Associate Editor of the IEEE Transactions on
			Cloud Computing, and has been Associate Editor of the IEEE
			Transactions on Computers, the IEEE Transactions on Neural Networks,
			the IEEE Transactions on Instrumentation and Measurement, and IEEE
			Access. He received the IEEE Instrumentation and Measurement Society
			Technical Award (2002) and the IEEE TAB Hall of Honor (2019). He is
			Honorary Professor at: Obuda University, Hungary; Guangdong
			University of Petrochemical Technology, China; Northeastern
			University, China; Muroran Institute of Technology, Japan; and the
			Amity University, India.</p>
		<h5>
			<strong>Keynote: Artificial Intelligence in Cloud Computing and
				Internet-of-Things</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Recent years have seen a growing interest
			among users in the migration of their applications to the Cloud
			computing and Internet-of-Things environments. However, due to high
			complexity, Cloud-based and Internet-of-Things infrastructures need
			advanced components for supporting applications and advanced
			management techniques for increasing the efficiency. Adaptivity and
			autonomous learning abilities become extremely useful to support
			configuration and dynamic adaptation of these infrastructures to the
			changing needs of the users as well as to create adaptable
			applications. This self-adaptation ability is increasingly essential
			especially for non expert managers as well as for application
			designers and developers with limited competences in tools for
			achieving this ability. Artificial intelligence is a set of
			techniques which greatly can improve both the creation of
			applications and the management of these infrastructures. This talk
			will discuss the use of artificial intelligence in supporting the
			creation of applications in cloud and IoT infrastructures as well as
			their use in the various aspects of infrastructure management.
		</p>
	</div>
</div>