<div class="col-lg-3 col-md-4 text-center">
	<img src="../img/logos/icai.png" width="200">
</div>
<div class="col-lg-9 col-md-12">
	<h4>
		<i><strong>Third International Conference on Applied Informatics</strong></i>
	</h4>
	<h5>
		29 to 31 October 2020 <br> Covenant University, Ota, Nigeria <i class="text-danger">ONLINE</i>
	</h5>	
</div>