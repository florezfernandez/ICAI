<?php
$dir = "img/city/";
$dirObj = opendir($dir);
$pictures = array();
while (($file = readdir($dirObj)) != null) {
    if ($file != ".." && $file != ".") {
        array_push($pictures, $file);
    }
}
closedir($dirObj);
sort($pictures);
?>
<div id="carouselExampleControls" class="carousel slide"
	data-ride="carousel">
	<div class="carousel-inner">
    <?php
    for ($i = 0; $i < count($pictures); $i ++) {
        echo "<div class='carousel-item", ($i == 0) ? " active" : "", "'>\n";
        echo "<img src='" . $dir . $pictures[$i] . "' class='d-block w-100' >\n";
        echo "</div>\n";
    }
    ?>
  </div>
	<a class="carousel-control-prev" href="#carouselExampleControls"
		role="button" data-slide="prev"> <span
		class="carousel-control-prev-icon" aria-hidden="true"></span> <span
		class="sr-only">Previous</span>
	</a> <a class="carousel-control-next" href="#carouselExampleControls"
		role="button" data-slide="next"> <span
		class="carousel-control-next-icon" aria-hidden="true"></span> <span
		class="sr-only">Next</span>
	</a>
</div>