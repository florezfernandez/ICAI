<br>
<div class="row">
	<div class="col">
		<h3>
			ICAI 2020 <span class="text-danger">ONLINE</span>
		</h3>
		<p class="text-justify">The Third International Conference on Applied
			Informatics (ICAI) aims to bring together researchers and
			practitioners working in different domains in the field of
			informatics in order to exchange their expertise and to discuss the
			perspectives of development and collaboration</p>
		<p class="text-justify">
			Due to restrictions related to the COVID-19 pandemic, ICAI 2020 was
			held <strong class="text-danger">ONLINE</strong> in collaboration
			with the <strong>Covenant University</strong> located in <strong>Ota,
				Nigeria</strong>, from 29 to 31 October 2020. It is organized by the
			ITI Research Group that belongs to the Universidad Distrital
			Francisco Jose de Caldas, Covenant University, and Federal University
			of Technology.
		</p>
		<p>
			ICAI 2020 proceedings has been published with <strong>Springer</strong>
			in their <strong>Communications in Computer and Information Science
				(CCIS)</strong> series.
		</p>
		<p class="text-justify">ICAI 2020 is proudly sponsored by: Information
			Technologies Innovation Research Group, Springer, and Strategic
			Business Platforms.</p>
		<p>ICAI has been previously held in Madrid, Spain (2019) and Bogota,
			Colombia (2018).</p>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>Best Paper Award</strong>
			</h5>
			The paper <strong>The Application of DBSCAN Algorithm to Improve
				Variogram Estimation and Interpretation in Irregularly-Sampled
				Fields </strong> <a
				href="https://link.springer.com/chapter/10.1007/978-3-030-61702-8_20"
				target="_blank"><span class="fas fa-external-link-alt"></span></a>
			authored by <strong><i>O. O. MosobalajeEmail authorO. D. OroduD. Ogbe</i></strong>
			from the <strong>Covenant University, Nigeria</strong> and the <strong>African
				University of Science and Technology, Nigeria</strong> has been
			selected as <strong>Best Paper</strong> of ICAI 2020.
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Certificates</strong>
			</h5>
			ICAI 2020 certificates are available at: <a
				href="https://icai-c.itiud.org/certificate.php?edition=2020"
				target="_blank">ICAI-C</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2020 Proceedings in CCIS</strong>
			</h5>
			ICAI 2020 proceedings in Communications in Computer and Information
			Science (CCIS) Volume 1277 by Springer are now available at <a
				href="https://link.springer.com/book/10.1007/978-3-030-61702-8"
				target="_blank">https://link.springer.com/book/10.1007/978-3-030-61702-8</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2020 Workshops Proceedings in CEUR</strong>
			</h5>
			ICAI 2020 workshops proceedings in CEUR Workshops Proceedings are now
			available at <a href="http://ceur-ws.org/Vol-2714/" target="_blank">http://ceur-ws.org/Vol-2714/</a>
		</div>
	</div>
</div>