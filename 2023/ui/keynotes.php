<h3>Keynote Speakers for ICAI 2023</h3>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/ha.jpg" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Hernan Astudillo</strong>
		</h5>
		<p>Hernán Astudillo Professor of Informatics at Universidad Técnica
			Federico Santa María (UTFSM), Chile, currently on sabbatical year;
			Senior Researcher at the Intitute for Technology and Innovation in
			Health and Wellness (ITiSB) of Universidad Andrés Bello (UNAB), Viña
			del Mar (Chile); and Associate Researcher at CCTVal (Valparaíso,
			Chile). He is Informatics Engineer (UTFSM, 1988) and Ph.D.
			Information and Computer Science (Georgia Tech, 1995), and has been
			Senior Application Architect at MCI Systemhouse and Financial Systems
			Arquitects (USA), Chief Architect at Solunegocios (Chile), and
			visiting professor at USP (Brazil). His main research interest is
			Software Architecture, and particularly the identification,
			evaluation and reuse of technological decisions (like architecture
			tacticas, patterns, and trade-offs) using imperfect information,
			especially for microservices and IoT. He is Principal Investigator of
			the Toeska R&D team, where he does research, teaching and technology
			transfer in software architecture and process improvement, and their
			applications in e-Health, Digital Government, and Cultural
			Informatics. He has chaired the UTFSM's Doctorate in Informatics
			Engineering and Master of Information Technology programs, been
			Executive Secretary of CLEI (the Association of Latin-American
			Informatics departmants), has published over 150 articles in
			peer-reviewed conferences and journals, supervised tens of graduate
			theses, organized several national and international conferences, and
			lead R&D projects and international colaborations. Finally, he is
			member of IEEE, ACM, SCCC, IASA, and INCOSE.</p>
		<h5>
			<strong>Keynote: Security smells for microservices</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Many IT companies are delivering their
			businesses through microservices, given their potential for high
			scalability and availability. Hence, securing them is crucial. The
			software engineering community uses the notion of of “bad smells” to
			describe undesirable design feturess, which are used to identify
			potential "refactorings" that may mitigate or even eliminate said
			risks. This talk will present the design, execution and results of a
			wide study of scientific and industrial sources to gather smells
			identified by researchers and practitioners, as well as the
			refactorings used to mitigate their effects. We will describe the
			findings on 58 primary studies, and will describe a taxonomy that we
			proposed, which associates each smell with security properties it may
			violate and refactorings that mitigate its effects. Then, we will
			explore a tarde-offs model to help determine whether it is worth
			refactoring a given smell or it's better just to tolerate its risks.
			Finally, we will present ongoing work on doing triage" of smells by
			severity. This is part of an ongoing work program to help
			microservice designers (1) understand the security risks they may
			incur, and (2) decide if/when to apply refactoring to them.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/sb.png" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Simone Belli</strong>
		</h5>
		<p>Senior Researcher in the Department of Social Anthropology and
			Social Psychology of the Complutense University of Madrid, and
			Visiting Professor at Yachay Tech and University of Valle d'Aosta.
			Visiting professor at the University of California and Universidad
			Michoacana de San Nicolás de Hidalgo. Postdoctoral researcher at the
			San Diego State University, Charles III University of Madrid and the
			Autonomous University of Madrid, by Paulo Freire Innovative
			Technology Post-Doctoral Visiting Scholar, Alliance of 4
			Universities, respectively. Invited Professor at the University of
			Bergamo, University of Aosta Valley, and University of Helsinki as a
			Professor in Summer School Course "Emotions and Interculturality".
			Research is focused on understanding why emotions have a strong
			relationship with language and how it is possible to express these
			emotions in online/offline spaces, and professional and innovative
			contexts and Artificial Intelligence. 131 international conferences
			and seminars, some of them as invited speaker, and published 89
			papers, book chapters, and scientific reviews, in four languages on
			the topics of Social Psychology, Innovation, Emotions, and
			Communication Technology.</p>
		<h5>
			<strong>Keynote: Emotions and Artificial Intelligence: Between game
				and reality</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Emotions have long been considered as a
			unique human trait, but recent advances in Artificial Intelligence
			(AI) have led to an increasing interest in the incorporation of
			emotional intelligence into machines. Natural Language Processing,
			computer vision and affective computing techniques, among others, are
			becoming involved in the detection and analysis of human emotions.
			The main aim would be to allow machines to properly recognize,
			interpret and respond to human emotions, enhancing their ability to
			interact with us in more meaningful ways, especially in multimodal
			and multimedia. Its potential applications encompass a wide range of
			disciplines, from Customer Relations Management services to
			Healthcare. However, there are also ethical concerns regarding
			privacy, discrimination and potential abuse or misuse that must be
			addressed. The objectives of this conference are based on
			understanding how emotions can be integrated into machines and how
			this integration can improve human-machine interactions. This
			conference tries to explore various techniques to detect and
			interpret human emotions, as well as the development of algorithms
			that allow machines to respond appropriately to these emotions. Also,
			this conference intends to help in the creation of future AIs that
			are more responsive, empathetic and intuitive in their interactions
			with humans, which will lead to better user experiences in fields
			such as healthcare, education and entertainment. In addition, ethical
			considerations around data privacy, bias leading to discrimination or
			transparency will be carefully considered, in order to ensure AI is
			developed and used responsibly and beneficially, following the
			international guidelines.
		</p>
	</div>
</div>

