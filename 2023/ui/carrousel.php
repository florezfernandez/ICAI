<?php
$dir = "img/city/";
$dirObj = opendir($dir);
$pictures = array();
while (($file = readdir($dirObj)) != null) {
    if ($file != ".." && $file != ".") {
        array_push($pictures, $file);
    }
}
closedir($dirObj);
sort($pictures);
?>
<div id="carousel" class="carousel slide"
	data-bs-ride="carousel">
	<div class="carousel-inner">
    <?php
    for ($i = 0; $i < count($pictures); $i ++) {
        echo "<div class='carousel-item", ($i == 0) ? " active" : "", "'>\n";
        echo "<img src='" . $dir . $pictures[$i] . "' class='d-block w-100' >\n";
        echo "</div>\n";
    }
    ?>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carousel" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carousel" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
 </div>