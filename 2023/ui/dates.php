<h3>Important Dates</h3>
<ul>
	<li>Paper Submission:
		<ul>
			<li><strong>July 16, 2023</strong></li>
			<li><del>July 2, 2023</del></li>
		</ul>
	</li>
	<li>Paper Notification:
		<ul>			
			<li><strong>August 13, 2023</strong></li>
			<li><del>July 30, 2023</del></li>
		</ul>
	</li>
	<li>Camera Ready:
		<ul>
			<li><strong>September 3, 2023</strong></li>
			<li><del>August 27, 2023</del></li>
		</ul>
	</li>
	<li>Authors Registration:
		<ul>
			<li><strong>September 3, 2023</strong></li>
			<li><del>August 27, 2023</del></li>
		</ul>
	</li>
</ul>