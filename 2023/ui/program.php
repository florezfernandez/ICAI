<h3>Program</h3>
<div class="alert alert-success" role="alert">
	<strong>The program is fixed to Ecuadorian Local time (GMT-5)</strong>
</div>
<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="30%" class="text-center">Thursday 26 October</th>
		<th width="30%" class="text-center">Friday 27 October</th>
		<th width="30%" class="text-center">Saturday 28 October</th>
	</tr>
	<tr>
		<td>10:00-10:30</td>
		<td class="table-danger"><strong>Conference Opening</strong><br> <i>Room:
				<strong>Aula Harvard</strong><br> URL: <a
				href="https://us02web.zoom.us/j/86155471516?pwd=V0oyMXR3SjQ4a2ZNalBWTlhyZkw0Zz09">Zoom</a>
				Access code: 414180
		</i></td>
	</tr>
	<tr>
		<td>10:30-11:30</td>
		<td class="table-success"><strong>Keynote: Simone Belli</strong><br> <i>Room:
				<strong>Aula Harvard</strong><br> URL: <a
				href="https://us02web.zoom.us/j/86155471516?pwd=V0oyMXR3SjQ4a2ZNalBWTlhyZkw0Zz09">Zoom</a>
				Access code: 414180
		</i></td>
		<td class="table-success"><strong>Keynote: Hernan Astudillo</strong><br>
			<i>Room: <strong>Aula Harvard</strong><br> URL: <a
				href="https://us02web.zoom.us/j/87467226137?pwd=d1kxSURzYUpJYVBhaEVyWEhpaExvQT09">Zoom</a>
				Access code: 315736
		</i>
		
		<td class="table-danger"><strong>Touristic Activity</strong><br> <i>Location:
				<strong>Pending</strong>
		</i></td>
	</tr>
	<tr>
		<td>11:30-13:30</td>
		<td class="table-warning"><strong>Workshop AIESD</strong><br> <i>Room:
				<strong>Aula Harvard</strong><br> URL: <a
				href="https://us02web.zoom.us/j/86155471516?pwd=V0oyMXR3SjQ4a2ZNalBWTlhyZkw0Zz09">Zoom</a>
				Access code: 414180
		</i><br> <br> <strong>Workshops WAAI &amp; WKMIT</strong><br> <i>Room:
				<strong>Aula Sorbona</strong><br>URL: <a
				href="https://us02web.zoom.us/j/84752888852?pwd=WjFjWmZ1KzZYczNCVjhndFJFU0RMdz09">Zoom</a>
				Access code: 202942
		</i><br> <br> <strong>Workshops WITS &amp; WSM</strong><br> <i>Room: <strong>Aula
					Oxford</strong><br> URL: <a
				href="https://us02web.zoom.us/j/85975121208?pwd=OGVOby9JSTUxM2NkWkFMd3JwNC9iQT09">Zoom</a>
				Access code: 202942
		</i></td>
		<td class="table-info"><strong>Data Analysis </strong><br> <i>Room: <strong>Aula
					Harvard</strong><br> <i>URL: <a
					href="https://us02web.zoom.us/j/87467226137?pwd=d1kxSURzYUpJYVBhaEVyWEhpaExvQT09">Zoom</a>
					Access code: 315736
			</i><br> <br> <strong>Geoinformatics &amp; Health Care Information
					Systems </strong><br> <i>Room: <strong>Aula Sorbona</strong><br>URL:
					<a
					href="https://us02web.zoom.us/j/85192879256?pwd=Nk5OWHRsNG1ZU2JDd2k4ckFZdGtBUT09">Zoom</a>
					Access code: 853459
			</i></td>
	</tr>
	<tr>
		<td>13:30-14:30</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:30-16:30</td>
		<td class="table-info"><strong>Artificial Intelligence</strong> <br> <i>Room:
				<strong>Aula Harvard</strong><br> URL: <a
				href="https://us02web.zoom.us/j/86155471516?pwd=V0oyMXR3SjQ4a2ZNalBWTlhyZkw0Zz09">Zoom</a>
				Access code: 414180
		</i> <br> <br> <strong>Decision Systems</strong> <br> <i>Room: <strong>Aula
					Sorbona</strong><br>URL: <a
				href="https://us02web.zoom.us/j/84752888852?pwd=WjFjWmZ1KzZYczNCVjhndFJFU0RMdz09">Zoom</a>
				Access code: 202942

		</i></td>
		<td class="table-info"><strong>Enterprise Information Systems
				Applications &amp; Learning Management Systems</strong> <br> <i>Room:
				<strong>Aula Harvard</strong><br> URL: <a
				href="https://us02web.zoom.us/j/87467226137?pwd=d1kxSURzYUpJYVBhaEVyWEhpaExvQT09">Zoom</a>
				Access code: 315736
		</i> <br> <br> <strong>Interdisciplinary Information Studies &amp;
				Virtual and Augmented Reality</strong> <br> <i>Room: <strong>Aula
					Sorbona</strong><br>URL: <a
				href="https://us02web.zoom.us/j/85192879256?pwd=Nk5OWHRsNG1ZU2JDd2k4ckFZdGtBUT09">Zoom</a>
				Access code: 853459

		</i></td>
	</tr>
	<tr>
		<td>17:00-19:00</td>
		<td class="table-danger"><strong>Cocktail and Cultural Activity</strong><br>
			<i>Room: <strong>Pending</strong>
		</i></td>
		<td class="table-danger"><strong>Conference Closing</strong><br> <i>Room:
				<strong>Aula Harvard</strong><br> URL: <a
				href="https://us02web.zoom.us/j/87467226137?pwd=d1kxSURzYUpJYVBhaEVyWEhpaExvQT09">Zoom</a>
				Access code: 315736
		</i></td>
	</tr>

</table>

<h4>Leyend</h4>
<table class="table">
	<tr>
		<td class="table-info" width="5%"></td>
		<td width="45%">ICAI Sessions (Presentations of papers in CCIS)</td>
		<td class="table-success" width="5%"></td>
		<td width="45%">Keynotes</td>
	</tr>
	<tr>
		<td class="table-warning" width="5%"></td>
		<td width="45%">ICAI Workshops Sessions (Presentations of papers in
			CEUR-WS)</td>
		<td class="table-danger" width="5%"></td>
		<td width="45%">Social Activities</td>
	</tr>
</table>

<h3>Detailed Program</h3>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center" colspan="6">Thursday 26 October</th>
	</tr>
	<tr>
		<td>10:00-10:30</td>
		<td class="table-danger" colspan="6"><strong>Conference Opening</strong></td>
	</tr>
	<tr>
		<td>10:30-11:30</td>
		<td class="table-success" colspan="6"><strong>Keynote: Simone Belli. <i>Emotions
					and Artificial Intelligence: Between game and reality</i></strong><br>
			Session Chair: Ixent Galpin</td>
	</tr>
	<tr>
		<td>11:30-13:30</td>
		<td width="30%" class="table-warning" colspan="2"><strong>Workshop
				AIESD </strong><br> Session Chair: Marcelo Leon
			<ul>
				<li><strong>Analysis Model for the Identification of Factors that
						Influence Students Academic Performance </strong><br> Camilo
					Sandoval, David Tabla, Hector Florez</li>
				<li><strong>Ecuadorian Companies’ Assets, Income, and Corporate
						Social Responsibility </strong><br> Arnaldo Vergara-Romero, Petr
					Sed’a, Rafael Sorhegui-Ortega, Lisette Garnica-Jarrin</li>
				<li><strong>Energy Poverty in ALADI Member Countries: a Study based
						on Multi-criteria Methods </strong><br> Anderson Argothy, Angela
					Arévalo, Laura E. Moreta, Fernando Herrera</li>
				<li><strong>E-waste and Circular Economy: An Approach to the Latin
						American Case</strong><br> Diana Morales-Urrutia</li>
				<li><strong>Regarding the Selection of a Trading Strategy in
						Efficient Markets </strong><br>Arnaldo Vergara-Romero, Petr Sed'a,
					César Pozo-Estupiñan, Lisette Garnica-Jarrin</li>
				<li><strong>SAS Classification in the creation of new companies in
						Ecuador </strong><br>Daniel Mosquera, Fabricio Echeverria, Marcelo
					León, Luis Torres-Barrera, Eladio Freire</li>
			</ul></td>
		<td width="30%" class="table-warning" colspan="2"><strong>Workshops
				WAAI &amp; WKMIT</strong><br> Session Chair: Maria F. Pollo-Cattaneo
			<ul>
				<li><strong>Artificial Intelligence in Higher Education: A
						Bibliometric Analysis </strong><br> Huseyin Bicen, Razvan Bogdan,
					Sebastian I. Petruc</li>
				<li><strong>Depression and Anxiety Diagnosis Using Unsupervised
						Learning Approach</strong><br> Martín Di Felice, Ariel Deroche,
					Ilan Trupkin, Parag Chatterjee, María F. Pollo-Cattaneo</li>
				<li><strong>Development of a HIPAA-compliant synchronizer to Enable
						Automatic Reporting and Inter-institutional Collaboration in
						Healthcare Systems Worldwide </strong><br> Fernando
					Yepes-Calderon, J. Gordon McComb</li>
				<li><strong>Analysis of open data interoperability through catalogs
						in Latin America </strong><br> Roxana Martínez, Gastón Lacuesta,
					Ezequiel Ricciardi</li>
				<li><strong>ADescriptors for Technology Key Area in a Knowledge
						Management Maturity Model </strong><br> Luciano Straccia, María F.
					Pollo-Cattaneo</li>
			</ul></td>
		<td width="30%" class="table-warning" colspan="2"><strong>Workshops
				WITS &amp; WSM</strong><br> Session Chair: Hector Florez
			<ul>
				<li><strong>Predictive Modelling of Traffic Accidents in Bogota,
						Colombia: Uncovering Key Contributing Factors </strong><br>
					Sebastián Castellanos, Alejandra Baena, Juan Camilo Ramírez</li>
				<li><strong>Leveraging a Model Transformation Chain for
						Semi-Automatic Source Code Generation on the Android Platform</strong><br>
					Daniel Sanchez</li>
				<li><strong>Management Framework of Software Development: a
						Systematic Review </strong><br> Marcos Espinoza-Mina, Alejandra
					Colina Vargas, Natalia Montero Ramos</li>
				<li><strong>Migration from Monolithic Applications to Microservices:
						A Systematic Literature Mapping on Approaches, Challenges, and
						Anti-patterns </strong><br> Fernando Muraca, María F.
					Pollo-Cattaneo</li>
			</ul></td>
	</tr>
	<tr>
		<td>13:30-14:30</td>
		<td class="table-active text-center" colspan="6">Lunch</td>
	</tr>
	<tr>
		<td>14:30-16:30</td>
		<td width="45%" class="table-info" colspan="3"><strong>Artificial
				Intelligence </strong><br>Session Chair: Christian Grevisse<br>
			<ul>
				<li><strong>A Feature Selection Method Based on Rough Set Attribute
						Reduction and Classical Filter-based Feature Selection for
						Categorical Data Classification </strong><br>Oluwafemi Oriola,
					Eduan Kotzé, Ojonoka Atawodi</li>
				<li><strong>A semantic enhanced course recommender system via
						knowledge graphs for limited user information scenarios </strong><br>Juan
					Sanguino, Ruben Manrique, Olga Mariño</li>
				<li><strong>Enhancing Face Anti-Spoofing systems through synthetic
						image generation </strong><br>César Vega, Ruben Manrique</li>
				<li><strong>Identifying Public Tenders of Interest using
						Classification Models: A Comparative Analysis </strong><br>Yeersainth
					Figueroa-Gómez, Ixent Galpin</li>
				<li><strong>Mapping Brand Territories using ChatGPT </strong><br>Luisa
					Fernanda Rodriguez-Sarmiento, Ixent Galpin, Vladimir Sanchez-Riaño
				</li>
				<li><strong>Predictive Modeling for Detection of Depression Using
						Machine Learning </strong><br>Martín Di Felice, Ariel Deroche,
					Ilan Trupkin, Parag Chatterjee, María F. Pollo-Cattaneo</li>
				<li><strong>Stock Price Prediction: Impact of Volatility on Model
						Accuracy </strong><br>Juan Parada-Rodriguez, Ixent Galpin</li>
			</ul></td>
		<td width="45%" class="table-info" colspan="3"><strong>Decision
				Systems </strong><br> Session Chair: Hector Florez
			<ul>
				<li><strong>CA Bio-Inspired-based Salp Swarm Algorithm Enabled with
						Deep Learning for Alzheimer’s Classification </strong><br> Joseph
					Bamidele Awotunde, Sunday Adeola Ajagbe, Hector Florez</li>
				<li><strong>A Scientometric Analysis of Virtual Tourism Technology
						Use in The Tourism Industry </strong><br> Sri Sulastri, Achmad
					Nurmandi, Aulia Nur Kasiwi</li>
				<li><strong>A tool to predict payment default in financial
						institutions </strong><br> Dulce Rivero, Laura Guerra, Willi
					Narvaez, Stalin Arciniegas</li>
				<li><strong>Ensuring Intrusion Detection for IoT Services through an
						Improved CNN </strong><br> Sunday Adeola Ajagbe, Joseph Bamidele
					Awotunde, Hector Florez</li>
				<li><strong>Prediction Value of a Real Estate in the city of Quito
						Post Pandemic </strong><br> Wladimir Vilca, Joe Carrion-Jumbo,
					Diego Riofrío-Luzcando, César Guevara</li>
				<li><strong>Simulation model to assess household water saving
						devices in Bogota city </strong><br> Andrés Chavarro, Mónica
					Castañeda, Sebastian Zapata, Isaac Dyner</li>
				<li><strong>Towards Reliable App Marketplaces: Machine Learning
						Based Detection of Fraudulent Reviews </strong><br> Angel Fiallos,
					Erika Anton</li>
			</ul></td>

	</tr>
	<tr>
		<td>17:00-19:00</td>
		<td width="45%" class="table-danger" colspan="6"><strong>Cocktail and
				Cultural Activity </strong></td>

	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center" colspan="2">Friday 27 October</th>
	</tr>
	<tr>
		<td>10:30-11:30</td>
		<td class="table-success" colspan="2"><strong>Keynote: Hernan
				Astudillo. <i>Security smells for microservices</i>
		</strong><br> Session Chair: Hector Florez</td>
	</tr>
	<tr>
		<td>11:30-13:30</td>
		<td width="45%" class="table-info"><strong>Geoinformatics &amp; Health
				Care Information Systems </strong><br> Session Chair: Hector Florez
			<ul>
				<li><strong>Prospects of UAVs in agricultural mapping </strong><br>
					Paulo Escandón-Panchana, Gricelda Herrera-Franco, Sandra Martínez
					Cuevas, Fernando Morante-Carballo</li>
				<li><strong>Comparative analysis of spatial and environmental data
						in informal settlements, from point clouds and RPAS images </strong><br>Carlos
					Alberto Diaz Riveros, Andrés Cuesta Veleño, Julieta Frediani, Rocio
					Rodriguez Tarducci, Daniela Cortizo</li>
				<li><strong>TP53 Genetic Testing and Personalized Nutrition Service
				</strong><br> Jitao Yang</li>
				<li><strong>Gross motor skills development in children with and
						without disabilities: a therapist’s support system based on deep
						learning and Adaboost classifiers </strong><br> Adolfo
					Jara-Gavilanes, Vladimir Robles-Bykbaev</li>
				<li><strong>Lung Cancer Detection: a classification approach
						utilizing oversampling and Support Vector Machines </strong><br>
					Adolfo Jara-Gavilanes, Vladimir Robles-Bykbaev</li>
				<li><strong>Classification of Brain Tumors: A Comparative Approach
						of Shallow and Deep Neural Networks </strong><br>Sebastián Felipe
					Álvarez Montoya, Alix E. Rojas, Luis Fernando Niño Vásquez</li>
			</ul></td>
		<td width="45%" class="table-info"><strong>Enterprise Information
				Systems Applications &amp; Learning Management Systems</strong><br>
			Session Chair: Christian Grevisse
			<ul>
				<li><strong>Challenges to use Role Playing in Software Engineering
						Education: A Rapid Review </strong><br> Mauricio Hidalgo, Hernán
					Astudillo, Laura M. Castro</li>
				<li><strong>Effects of Facial Biometric System at Universidad
						Técnica del Norte, Ecuador: An Analysis Using the Technology
						Acceptance Model (TAM) </strong><br>Garrido Fernando, Reascos
					Irving, Alvarez Francisco, Lanchimba Alex</li>
				<li><strong>Team productivity factors in Agile Software Development:
						an exploratory survey with practitioners </strong><br> Marcela
					Guerrero-Calvache, Giovanni Hernández</li>
				<li><strong>The Internet of Things as a technological tool and its
						application in the management and control of data for Agriculture
						4.0 </strong><br> Mauricio Alfredo Zafra-Aycardi, Dewar
					Rico-Bautista, Diego Armando Mejía-Bugallo, Jorge Antonio
					Sequeda-Serrano</li>
				<li><strong>Work-Life Interference on Employee Well-Being and
						Productivity. A Proof-of-Concept for rapid and continuous
						population analysis using Evalu@</strong><br>Fernando
					Yepes-Calderon, Paulo Andrés Vélez Ángel</li>
				<li><strong>Comparative Quality Analysis of GPT-based Multiple
						Choice Question Generation </strong><br>Christian Grévisse</li>
			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:30</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:30-16:30</td>
		<td width="45%" class="table-info"><strong>Data Analysis</strong><br>Session
			Chair: María F. Pollo-Cattaneo<br>
			<ul>
				<li><strong>Automated diagnosis of prostate cancer using Artificial
						Intelligence. A systematic literature review </strong><br>
					Salvador Soto, Maria F. Pollo-Cattaneo, Fernando Yepes-Calderon</li>
				<li><strong>Bullish Price Patterns in the NASDAQ-100 Stock Index
						Evaluated through Genetic Algorithm </strong><br> Franklin
					Gallegos-Erazo, Jean Anastacio-Aquino, Rene Calero-Córdova</li>
				<li><strong>From Naive Interest to Shortage during COVID-19: A
						Google Trends and News Analysis </strong><br> Alix E. Rojas, Lilia
					Carolina Rojas-Pérez, Camilo Mejia-Moncayo</li>
				<li><strong>Joint Exploration of Kernel Functions Potential for Data
						Representation and Classification: A First Step Toward Interactive
						Interpretable Dimensionality Reduction </strong><br> Yahya
					Aalaila, Ismail Bachchar, Hind Raki, Sami Bamansour, Mouad Elhamdi,
					Kaoutar Benghzial, MacArthur Ortega-Bustamante, Lorena
					Guachi-Guachi, Diego H. Peluffo-Ordóñez</li>
				<li><strong>Measuring the Impact of Digital Government Service: A
						Scientometric Analysis for 2023 </strong><br> Narendra Nafi
					Gumilang, Achmad Nurmandi, Muhammad Younus, Aulia Nur Kasiwi</li>
				<li><strong>Using polarization and alignment to identify
						quick-approval law propositions: An open linked data application </strong><br>Francisco
					Cifuentes-Silva, José Emilio Labra Gayo, Hernán Astudillo, Felipe
					Rivera-Polo</li>
				<li><strong>Utilizing Chatbots as Predictive Tools for Anxiety and
						Depression: A Bibliometric Review </strong><br>María de Lourdes
					Díaz Carrillo, Manuel Osmany Ramírez Pírez, Gustavo Adolfo Lemos
					Chang</li>

			</ul></td>
		<td width="45%" class="table-info"><strong>Interdisciplinary
				Information Studies &amp; Virtual and Augmented Reality </strong><br>Session
			Chair: Simone Belli<br>
			<ul>
				<li><strong>Machine Masquerades a Poet: Using Unsupervised T5
						Transformer for Semantic Style Transformation in Poetry Generation
				</strong><br> Agnij Moitra</li>
				<li><strong>Context and characteristics of software related to
						Ecuadorian scientific production: A bibliometric and content
						analysis study </strong><br> Marcos Espinoza-Mina, Alejandra
					Colina Vargas, Javier Berrezueta Varas</li>
				<li><strong>Exploring the Potential of Genetic Algorithms for
						Optimizing Academic Schedules at the School of Mechatronic
						Engineering: Preliminary Results </strong><br> Johan Alarcón,
					Samantha Buitrón, Alexis Carrillo, Mateo Chuquimarca, Alexis Ortiz,
					Robinson Guachi, D. H. Peluffo-Ordóñez, Lorena Guachi-Guachi</li>
				<li><strong>Changes in the adaptive capacity of livelihood
						vulnerability to climate change in Ecuador's Tropical Commodity
						Crops: Banana and Cocoa </strong><br> Elena Piedra-Bonilla, Yosuny
					Echeverría</li>
				<li><strong>Under the spotlight! Facial Recognition Applications in
						Prison Security: Bayesian Modeling and ISO27001 Standard
						Implementation </strong><br> Diego Donoso, Gino Cornejo, Carlos
					Calahorrano, Santiago Donoso, Erika Escobar</li>
				<li><strong>Design and validation of a Virtual Reality scenery for
						learning radioactivity: HalDron Project. </strong><br> Silvio
					Perez, Diana Olmedo, Fancois Baquero, Veronica Martinez-Gallego,
					Juan Lobos</li>
			</ul></td>
	</tr>
	<tr>
		<td>17:00-19:00</td>
		<td width="45%" class="table-danger" colspan="2"><strong>Conference
				Closing </strong></td>

	</tr>
</table>