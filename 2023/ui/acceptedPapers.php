<h3>Accepted Papers</h3>
<h4>Artificial Intelligence</h4>
<ul>
	<li><strong>A Feature Selection Method Based on Rough Set Attribute
			Reduction and Classical Filter-based Feature Selection for
			Categorical Data Classification </strong>
		<ul>
			<li>Oluwafemi Oriola, Eduan Kotzé, Ojonoka Atawodi</li>
		</ul></li>
	<li><strong>A semantic enhanced course recommender system via knowledge
			graphs for limited user information scenarios</strong>
		<ul>
			<li>Juan Sanguino, Ruben Manrique, Olga Mariño</li>
		</ul></li>
	<li><strong>Enhancing Face Anti-Spoofing systems through synthetic
			image generation </strong>

		<ul>
			<li>César Vega, Ruben Manrique</li>
		</ul></li>
	<li><strong>Identifying Public Tenders of Interest using Classification
			Models: A Comparative Analysis</strong>
		<ul>
			<li>Yeersainth Figueroa-Gómez, Ixent Galpin</li>
		</ul></li>
	<li><strong>Mapping Brand Territories using ChatGPT </strong>
		<ul>
			<li>Luisa Fernanda Rodriguez-Sarmiento, Ixent Galpin, Vladimir
				Sanchez-Riaño</li>
		</ul></li>

	<li><strong>Predictive Modeling for Detection of Depression Using
			Machine Learning </strong>
		<ul>
			<li>Martín Di Felice, Ariel Deroche, Ilan Trupkin, Parag Chatterjee,
				María F. Pollo-Cattaneo</li>
		</ul></li>

	<li><strong>Stock Price Prediction: Impact of Volatility on Model
			Accuracy</strong>
		<ul>
			<li>Juan Parada-Rodriguez, Ixent Galpin</li>
		</ul></li>
</ul>

<h4>Data Analysis</h4>
<ul>
	<li><strong>Automated diagnosis of prostate cancer using Artificial
			Intelligence. A systematic literature review </strong>
		<ul>
			<li>Salvador Soto, Maria F. Pollo-Cattaneo, Fernando Yepes-Calderon</li>
		</ul></li>
	<li><strong>Bullish Price Patterns in the NASDAQ-100 Stock Index
			Evaluated through Genetic Algorithm</strong>
		<ul>
			<li>Franklin Gallegos-Erazo, Jean Anastacio-Aquino, Rene
				Calero-Córdova</li>
		</ul></li>

	<li><strong>From Naive Interest to Shortage during COVID-19: A Google
			Trends and News Analysis</strong>
		<ul>
			<li>Alix E. Rojas, Lilia Carolina Rojas-Pérez, Camilo Mejia-Moncayo</li>
		</ul></li>

	<li><strong>Joint Exploration of Kernel Functions Potential for Data
			Representation and Classification: A First Step Toward Interactive
			Interpretable Dimensionality Reduction </strong>
		<ul>
			<li>Yahya Aalaila, Ismail Bachchar, Hind Raki, Sami Bamansour, Mouad
				Elhamdi, Kaoutar Benghzial, MacArthur Ortega-Bustamante, Lorena
				Guachi-Guachi, Diego H. Peluffo-Ordóñez</li>
		</ul></li>
	<li><strong>Measuring the Impact of Digital Government Service: A
			Scientometric Analysis for 2023</strong>
		<ul>
			<li>Narendra Nafi Gumilang, Achmad Nurmandi, Muhammad Younus, Aulia
				Nur Kasiwi</li>
		</ul></li>

	<li><strong>Using polarization and alignment to identify quick-approval
			law propositions: An open linked data application</strong>
		<ul>
			<li>Francisco Cifuentes-Silva, José Emilio Labra Gayo, Hernán
				Astudillo, Felipe Rivera-Polo</li>
		</ul></li>
	<li><strong>Utilizing Chatbots as Predictive Tools for Anxiety and
			Depression: A Bibliometric Review</strong>
		<ul>
			<li>María de Lourdes Díaz Carrillo, Manuel Osmany Ramírez Pírez,
				Gustavo Adolfo Lemos Chang</li>
		</ul></li>

</ul>

<h4>Decision Systems</h4>
<ul>
	<li><strong>A Bio-Inspired-based Salp Swarm Algorithm Enabled with Deep
			Learning for Alzheimer’s Classification </strong>
		<ul>
			<li>Joseph Bamidele Awotunde, Sunday Adeola Ajagbe, Hector Florez</li>
		</ul></li>
	<li><strong>A Scientometric Analysis of Virtual Tourism Technology Use
			in The Tourism Industry </strong>
		<ul>
			<li>Sri Sulastri, Achmad Nurmandi, Aulia Nur Kasiwi</li>
		</ul></li>
	<li><strong>A tool to predict payment default in financial institutions
	</strong>
		<ul>
			<li>Dulce Rivero, Laura Guerra, Willi Narvaez, Stalin Arciniegas</li>
		</ul></li>
	<li><strong>Ensuring Intrusion Detection for IoT Services through an
			Improved CNN </strong>
		<ul>
			<li>Sunday Adeola Ajagbe, Joseph Bamidele Awotunde, Hector Florez</li>
		</ul></li>
	<li><strong>Prediction Value of a Real Estate in the city of Quito Post
			Pandemic</strong>
		<ul>
			<li>Wladimir Vilca, Joe Carrion-Jumbo, Diego Riofrío-Luzcando, César
				Guevara</li>
		</ul></li>
	<li><strong>Simulation model to assess household water saving devices
			in Bogota city</strong>
		<ul>
			<li>Andrés Chavarro, Mónica Castañeda, Sebastian Zapata, Isaac Dyner</li>
		</ul></li>
	<li><strong>Towards Reliable App Marketplaces: Machine Learning Based
			Detection of Fraudulent Reviews </strong>
		<ul>
			<li>Angel Fiallos, Erika Anton</li>
		</ul></li>
</ul>

<h4>Enterprise Information Systems Applications</h4>
<ul>
	<li><strong>Challenges to use Role Playing in Software Engineering
			Education: A Rapid Review </strong>
		<ul>
			<li>Mauricio Hidalgo, Hernán Astudillo, Laura M. Castro</li>
		</ul></li>
	<li><strong>Effects of Facial Biometric System at Universidad Técnica
			del Norte, Ecuador: An Analysis Using the Technology Acceptance Model
			(TAM) </strong>
		<ul>
			<li>Garrido Fernando, Reascos Irving, Alvarez Francisco, Lanchimba
				Alex</li>
		</ul></li>
	<li><strong>Team productivity factors in Agile Software Development: an
			exploratory survey with practitioners </strong>
		<ul>
			<li>Marcela Guerrero-Calvache, Giovanni Hernández</li>
		</ul></li>
	<li><strong>The Internet of Things as a technological tool and its
			application in the management and control of data for Agriculture 4.0
	</strong>
		<ul>
			<li>Mauricio Alfredo Zafra-Aycardi, Dewar Rico-Bautista, Diego
				Armando Mejía-Bugallo, Jorge Antonio Sequeda-Serrano</li>
		</ul></li>
	<li><strong>Work-Life Interference on Employee Well-Being and
			Productivity. A Proof-of-Concept for rapid and continuous population
			analysis using Evalu@ </strong>
		<ul>
			<li>Fernando Yepes-Calderon, Paulo Andrés Vélez Ángel</li>
		</ul></li>
</ul>

<h4>Geoinformatics</h4>
<ul>
	<li><strong>Prospects of UAVs in agricultural mapping</strong>
		<ul>
			<li>Paulo Escandón-Panchana, Gricelda Herrera-Franco, Sandra Martínez
				Cuevas, Fernando Morante-Carballo</li>
		</ul></li>
	<li><strong>Comparative analysis of spatial and environmental data in
			informal settlements, from point clouds and RPAS images</strong>
		<ul>
			<li>Carlos Alberto Diaz Riveros, Andrés Cuesta Veleño, Julieta
				Frediani, Rocio Rodriguez Tarducci, Daniela Cortizo</li>
		</ul></li>
</ul>

<h4>Health Care Information Systems</h4>
<ul>
	<li><strong>TP53 Genetic Testing and Personalized Nutrition Service</strong>
		<ul>
			<li>Jitao Yang</li>
		</ul></li>
	<li><strong>Gross motor skills development in children with and without
			disabilities: a therapist’s support system based on deep learning and
			Adaboost classifiers </strong>
		<ul>
			<li>Adolfo Jara-Gavilanes, Vladimir Robles-Bykbaev</li>
		</ul></li>
	<li><strong>Lung Cancer Detection: a classification approach utilizing
			oversampling and Support Vector Machines </strong>
		<ul>
			<li>Adolfo Jara-Gavilanes, Vladimir Robles-Bykbaev</li>
		</ul></li>
	<li><strong>Classification of Brain Tumors: A Comparative Approach of
			Shallow and Deep Neural Networks </strong>
		<ul>
			<li>Sebastián Felipe Álvarez Montoya, Alix E. Rojas, Luis Fernando
				Niño Vásquez</li>
		</ul></li>
</ul>

<h4>Interdisciplinary Information Studies</h4>
<ul>
	<li><strong>Machine Masquerades a Poet: Using Unsupervised T5
			Transformer for Semantic Style Transformation in Poetry Generation </strong>
		<ul>
			<li>Agnij Moitra</li>
		</ul></li>
	<li><strong>Context and characteristics of software related to
			Ecuadorian scientific production: A bibliometric and content analysis
			study </strong>
		<ul>
			<li>Marcos Espinoza-Mina, Alejandra Colina Vargas, Javier Berrezueta
				Varas</li>
		</ul></li>
	<li><strong>Exploring the Potential of Genetic Algorithms for
			Optimizing Academic Schedules at the School of Mechatronic
			Engineering: Preliminary Results </strong>
		<ul>
			<li>Johan Alarcón, Samantha Buitrón, Alexis Carrillo, Mateo
				Chuquimarca, Alexis Ortiz, Robinson Guachi, D. H. Peluffo-Ordóñez,
				Lorena Guachi-Guachi</li>
		</ul></li>
	<li><strong>Changes in the adaptive capacity of livelihood
			vulnerability to climate change in Ecuador's Tropical Commodity
			Crops: Banana and Cocoa </strong>
		<ul>
			<li>Elena Piedra-Bonilla, Yosuny Echeverría</li>
		</ul></li>
	<li><strong>Under the spotlight! Facial Recognition Applications in
			Prison Security: Bayesian Modeling and ISO27001 Standard
			Implementation </strong>
		<ul>
			<li>Diego Donoso, Gino Cornejo, Carlos Calahorrano, Santiago Donoso,
				Erika Escobar</li>
		</ul></li>
</ul>

<h4>Learning Management Systems</h4>
<ul>
	<li><strong>Comparative Quality Analysis of GPT-based Multiple Choice
			Question Generation</strong>
		<ul>
			<li>Christian Grévisse</li>
		</ul></li>
</ul>

<h4>Virtual and Augmented Reality</h4>
<ul>
	<li><strong>Design and validation of a Virtual Reality scenery for
			learning radioactivity: HalDron Project. </strong>
		<ul>
			<li>Silvio Perez, Diana Olmedo, Fancois Baquero, Veronica
				Martinez-Gallego, Juan Lobos</li>
		</ul></li>
</ul>