<hr>
<h3>Organized by</h3>
<div class="text-center">
	<a href="https://www.udistrital.edu.co" target="_blank"><img
		src="../img/logos/ud.png" height="120" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Distrital Francisco Jose de Caldas"></a> <a
		href="https://www.ecotec.edu.ec/" target="_blank"><img
		src="../img/logos/ecotec.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="Universidad Ecotec"></a>
</div>
<h3>Sponsored by</h3>
<div class="text-center">
	<a href="http://www.itiud.org" target="_blank"><img
		src="../img/logos/iti.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="ITI Research Group"></a> <a
		href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer2.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="http://www.evalualos.com/" target="_blank"><img
		src="../img/logos/sbp.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Science Based Platforms"></a><a><img
		src="../img/logos/ceur.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="CEUR Workshop Proceedigns"></a>
</div>
<hr>