<h3>Venue</h3>
<p>
	ICAI 2023 will be held at the <strong>Universidad Ecotec</strong>
	located in <strong>Guayaquil, Ecuador</strong>, campus Samborondón.
</p>
<iframe
	src="https://www.google.com/maps/d/u/2/embed?mid=1VmgKGeKskK0TT5MTkCWhCvV6rfW0JRU&ehbc=2E312F"
	width="640" height="480"></iframe>

<h4>Transportation</h4>
<p>
	Attendees must arrive to Guayaquil through the José Joaquín de Olmedo
	International Airport <a
		href="https://www.tagsa.aero/en_aeropuertojjo.html" target="_blank"><span
		class="fas fa-external-link-alt"></span></a>.
</p>

<p>The Universidad Ecotec will provide local transportation from the
	hotels to the campus Samborondón.</p>

<h4>Accomodation</h4>

The following hotels are suggested. They include corporate rates.
<ul>
	<li>Hotel Puerto Pacífico <a href="https://hotelpuertopacifico.com/"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
		<table class="table table-striped table-hover w-auto">
			<thead>
				<tr>
					<th class="text-center">Room Type</th>
					<th class="text-center">Corporate Rate</th>
					<th class="text-center">Corporate Rate plus Tax</th>
				</tr>
			</thead>
			<tbody class="table-group-divider">
				<tr>
					<td>Executive</td>
					<td class="text-center">$57,38</td>
					<td class="text-center">$71,50</td>
				</tr>
				<tr>
					<td>Superior</td>
					<td class="text-center">$65,57</td>
					<td class="text-center">$81,50</td>
				</tr>
				<tr>
					<td>Doble</td>
					<td class="text-center">$65,57</td>
					<td class="text-center">$81,50</td>
				</tr>
				<tr>
					<td>Suite</td>
					<td class="text-center">$77,87</td>
					<td class="text-center">$96,50</td>
				</tr>
				<tr>
					<td>Triple</td>
					<td class="text-center">$86,06</td>
					<td class="text-center">$106,50</td>
				</tr>
			</tbody>
		</table> To book acommodations in the Hotel Puerto Pacífico with
		corporate rates, please contact Elsi Valverde <i>+593 98 555 7777</i>
	</li>
	<li>Hotel Wyndham Guayaquil Puerto Santa Ana <a
		href="https://www.wyndhamhotels.com/es-xl/wyndham/guayaquil-ecuador/wyndham-guayaquil/overview"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
		<table class="table table-striped table-hover w-auto">
			<thead>
				<tr>
					<th class="text-center">Room Type</th>
					<th class="text-center">Corporate Rate</th>
				</tr>
			</thead>
			<tbody class="table-group-divider">
				<tr>
					<td>Deluxe Double</td>
					<td class="text-center">$119</td>
				</tr>
				<tr>
					<td>Deluxe Single</td>
					<td class="text-center">$109</td>
				</tr>
				<tr>
					<td>Premium Double</td>
					<td class="text-center">$139</td>
				</tr>
				<tr>
					<td>Premium Single</td>
					<td class="text-center">$129</td>
				</tr>
				<tr>
					<td>Junior Suite Double</td>
					<td class="text-center">$239</td>
				</tr>
				<tr>
					<td>Junior Suite Single</td>
					<td class="text-center">$219</td>
				</tr>
				<tr>
					<td>Presidential Suite Double</td>
					<td class="text-center">$419</td>
				</tr>
				<tr>
					<td>Presidential Suite Single</td>
					<td class="text-center">$399</td>
				</tr>
			</tbody>
		</table>
	</li>
</ul>
