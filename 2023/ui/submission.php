<h3>Submission Guidelines</h3>
<p class="text-justify">
	Authors must submit an original full paper (12 to 16 pages) that has
	not previously been published. All contributions must be written in
	english. Authors should consult Springer's authors' guidelines and use
	their proceedings templates, either for LaTeX or for MSWord (<a
		href="http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines"
		target="_blank">http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines</a>),
	for the preparation of their papers. Springer encourages authors to
	include their ORCIDs in their papers. In addition, the corresponding
	author of each accepted paper, acting on behalf of all of the authors
	of that paper, must complete and sign a <strong>License to Publish</strong>
	form, through which the copyright for their paper is transferred to
	Springer.
</p>

<p class="text-justify">
	We encourage Overleaf users to use Springer's proceedings template for
	LaTeX <a
		href="https://www.overleaf.com/latex/templates/springer-lecture-notes-in-computer-science/kzwwpvhwnvfj"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>.
</p>
<div class="text-center">
	<a href="https://www.overleaf.com" target="_blank"><img
		src="../img/logos/overleaf.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="Overleaf"></a>
</div>

<h3>Review Process</h3>
<p class="text-justify">
	All submissions will be reviewed by at least 2 experts. <strong>Authors
		must remove personal details, acknowledgments section and any other
		information related to the authors' identity</strong>. In addition,
	all submissions will be screened by Turnitin. <strong><span
		class="text-danger">Papers with overal similarity index over 20% or
			single source similarity index over 10% will be rejected without
			revision</span></strong>.
</p>
<div class="text-center">
	<a href="https://www.turnitin.com/" target="_blank"><img
		src="../img/logos/turnitin.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="iThenticate"></a>
</div>

<h3>Submission Process</h3>

<p class="text-justify">
	The submission is handled by EquinOCS from Springer. To submit or
	upload a paper please go to <a
		href="https://equinocs.springernature.com/service/ICAI2023"
		target="_blank">https://equinocs.springernature.com/service/ICAI2023</a>.
	<strong>The first anonymous version must be submitted in PDF</strong>.
</p>
<div class="text-center">
	<a href="https://easychair.org" target="_blank"><img
		src="../img/logos/equinocs.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="EquinOCS"></a>
</div>

<h3>Proceedings</h3>

<p>
	ICAI 2023 proceedings will be published with <strong>Springer</strong>
	in their <strong><a href="http://www.springer.com/series/7899"
		target="_blank">Communications in Computer and Information Science
			(CCIS) series</a></strong>. The proceedings of all ICAI editios have
	been published with Springer CCIS series <a
		href="https://link.springer.com/conference/icai2" target="_blank"><span
		class="fas fa-external-link-alt"></span></a>.<br>Selected best papers
	will published in <strong><a
		href="https://www.springer.com/journal/42979" target="_blank">SN
			Computer Science</a></strong> by Springer.
</p>

<div class="text-center">
	<a href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer.jpg" width="250" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="http://www.springer.com/series/7899" target="_blank"><img
		src="../img/logos/ccis.jpg" width="150" data-toggle="tooltip"
		data-placement="bottom"
		title="Communications in Computer Information Science"></a> <a
		href="https://www.springer.com/journal/42979" target="_blank"><img
		src="../img/logos/sncs.jpg" width="150" data-toggle="tooltip"
		data-placement="bottom" title="SN Computer Science"></a>
</div>


<h3>Abstracting/Indexing</h3>
<p>CCIS is abstracted/indexed in DBLP, Google Scholar, EI-Compendex,
	SCImago, Scopus. CCIS volumes are also submitted for the inclusion in
	ISI Proceedings.</p>

<div class="text-center">
	<a href="http://dblp.org/" target="_blank"><img
		src="../img/logos/dbpl.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="DBPL"></a> <a
		href="https://scholar.google.com" target="_blank"><img
		src="../img/logos/googlescholar.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Google Scholar"></a> <a
		href="https://www.elsevier.com/solutions/engineering-village/content/compendex"
		target="_blank"><img src="../img/logos/ei.png" height="100"
		data-toggle="tooltip" data-placement="bottom" title="Ei Compendex"></a>
	<a href="http://www.scimagojr.com/" target="_blank"><img
		src="../img/logos/scimago.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="SCImago"></a> <a
		href="https://www.scopus.com/" target="_blank"><img
		src="../img/logos/scopus.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Scopus"></a> <a
		href="https://webofknowledge.com/" target="_blank"><img
		src="../img/logos/isi.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="ISI Web of Knowledge"></a>
</div>

<h3>Conditions</h3>
<ul>
	<li><strong>Papers that do not comply with the LNCS template or number
			of pages will be rejected without revision</strong>.</li>
	<li><strong>Papers out of scope will be rejected without revision</strong>.</li>
	<li><strong>All accepted papers should be presented by an author, who
			must be registered. </strong></li>
	<li><strong>Authors of accepted papers <span class="text-danger">cannot
				be changed</span></strong>.</li>
</ul>

<h3>Associated Journals</h3>
<p>Best papers will be published in:</p>
<ul>
	<li>SN Computer Science <a
		href="https://www.springer.com/journal/42979" target="_blank"><span
			class="fas fa-external-link-alt"></span></a>
		<ul>
			<li>Indexed In ACM, BDLP, Google Scholar, Scopus</li>
		</ul>
	</li>
</ul>
<p>Extended version of selected papers will be invited to publish in:</p>
<ul>
	<li>ParadigmPlus <a
		href="https://journals.itiud.org/index.php/paradigmplus/index"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
		<ul>
			<li>Indexed In Google Scholar, DOAJ</li>
			<li>Submitted for indexation to Scopus</li>
		</ul>
	</li>
</ul>