<br>
<div class="row">
	<div class="col">
		<h3>ICAI 2023</h3>
		<p class="text-justify">
			The 6<sup>th</sup> International Conference on Applied Informatics
			(ICAI) aims to bring together researchers and practitioners working
			in different domains in the field of informatics in order to exchange
			their expertise and to discuss the perspectives of development and
			collaboration
		</p>
		<p class="text-justify">
			ICAI 2023 was held at the <strong>Universidad Ecotec</strong>
			located in <strong>Guayaquil, Ecuador</strong>, from 25 to 28 October
			2023 in <strong class="text-danger">BLENDED</strong> form (i.e., with
			the option to participate remotely online). It is organized by the
			Universidad Ecotec and the ITI Research Group that belongs to the
			Universidad Distrital Francisco Jose de Caldas.
		</p>
		<p class="text-justify">ICAI 2023 is proudly sponsored by ITI Research
			Group, Science Based Platforms, and Springer.</p>
		<p>ICAI has been previously held in:</p>
		<ul>
			<li>2022: Arequipa, Peru <img
				src="https://www.worldometers.info/img/flags/pe-flag.gif"
				height="16px"></li>
			<li>2021: Buenos Aires, Argentina <img
				src="https://www.worldometers.info/img/flags/ar-flag.gif"
				height="16px"></li>
			<li>2020: Ota, Nigeria <img
				src="https://www.worldometers.info/img/flags/ni-flag.gif"
				height="16px"></li>
			<li>2019: Madrid, Spain <img
				src="https://www.worldometers.info/img/flags/sp-flag.gif"
				height="16px"></li>
			<li>2018: Bogota, Colombia <img
				src="https://www.worldometers.info/img/flags/co-flag.gif"
				height="16px"></li>
		</ul>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2023 Best Papers in SN Computer Science</strong>
			</h5>
			ICAI 2023 Best papers in SN Computer Science by Springer are now
			available at <a
				href="https://link.springer.com/journal/42979/topicalCollection/AC_a45b233b3269d18b349668e1c9a1b127"
				target="_blank">https://link.springer.com/journal/42979/topicalCollection/AC_a45b233b3269d18b349668e1c9a1b127</a>
		</div>

		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2023 Proceedings in CCIS </strong>
			</h5>
			ICAI 2023 proceedings in Communications in Computer and Information
			Science (CCIS) Volume 1874 by Springer are now available at <a
				href="https://link.springer.com/book/10.1007/978-3-031-46813-1"
				target="_blank">https://link.springer.com/book/10.1007/978-3-031-46813-1</a>
		</div>

		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Workshops 2023 Proceedings in CEUR-WS</strong>
			</h5>
			ICAI Workshops 2023 proceedings in CEUR Workshop Proceedgins
			(CEUR-WS) Volume 3520 are now available at <a
				href="https://ceur-ws.org/Vol-3520/" target="_blank">https://ceur-ws.org/Vol-3520/</a>
		</div>

		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Certificates</strong>
			</h5>
			ICAI 2023 certificates are available at: <a
				href="https://icai-c.itiud.org/certificate.php?edition=2023"
				target="_blank">ICAI-C</a>
		</div>

		<div class="alert alert-success" role="alert">
			<h5>
				<strong>Best Paper Award</strong>
			</h5>
			The paper <strong>A Semantic Enhanced Course Recommender System via
				Knowledge Graphs for Limited User Information Scenarios</strong> <a
				href="https://link.springer.com/article/10.1007/s42979-023-02399-4"
				target="_blank"><span class="fas fa-external-link-alt"
				aria-hidden="true"></span></a> authored by <strong><i>Juan Sanguino,
					Ruben Manrique, and Olga Mariño</i></strong> from the <strong>Universidad
				de los Andes, Colombia</strong> has been selected as <strong>Best
				Paper</strong> of ICAI 2023.
		</div>
	</div>
</div>