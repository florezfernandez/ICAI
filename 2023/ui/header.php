<div class="col-lg-3 col-md-4 text-center">
	<img src="../img/logos/icai.png" width="200">
</div>
<div class="col-lg-9 col-md-12">
	<h4>
		<i><strong>6<sup>th</sup> International Conference on Applied Informatics</strong></i>
	</h4>
	<h5>
		26 to 28 October 2023 <br>Universidad Ecotec <br> Guayaquil, Ecuador
		<img src="https://www.worldometers.info/img/flags/ec-flag.gif"
			height="16px"> <i class="text-danger">BLENDED</i>
	</h5>
</div>