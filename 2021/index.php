<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ICAI 2021</title>
<link rel="icon" type="image/png" href="../img/logos/icai2.png" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://www.gstatic.com/charts/loader.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<script charset="utf-8">
      $(function () { 
        $("[data-toggle='tooltip']").tooltip(); 
      });
</script>
</head>
<body>
	<br>
	<div class="container">
		<div class="row"><?php include 'ui/header.php';?></div>
		<?php
        include 'ui/menu.php';
        if (empty($_GET['pid'])) {
            include 'ui/carrousel.php';
            include 'ui/home.php';
        } else {
            include 'ui/' . $_GET['pid'] . '.php';
        }
        include 'ui/organizations.php';
        include '../ui/visitors.php';
        ?>
		<div class="text-center text-muted">
			&copy; ITI Research Group<br>2018 - <?php echo date("Y")?> All rights reserved
    	</div>
	</div>
	<br>
</body>
</html>