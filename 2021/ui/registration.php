<h3>Registration</h3>
<p class="text-justify">At least one author of each accepted paper must
	register to ensure its inclusion in the conference proceedings. Each
	registration will allow the publication and presentation of just one
	paper. Authors must pay an extra fee if they wish to publish and
	present more than one paper.</p>

<h3>Registration Fees</h3>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<table class="table table-hover table-bordered table">
				<tr class="info">
					<th class="text-center" rowspan="2">Registration</th>
					<th class="text-center" colspan="2">Fee</th>
				</tr>
				<tr class="info">
					<th class="text-center" nowrap>Non-Argentinian Authors</th>
					<th class="text-center" nowrap>Argentinian Authors</th>
				</tr>
				<tr>
					<td nowrap>Author</td>
					<td class="text-center">150 USD</td>
					<td class="text-center">75 USD</td>
				</tr>
				<tr>
					<td nowrap>Student Author</td>
					<td class="text-center">150 USD</td>
					<td class="text-center">75 USD</td>
				</tr>
				<tr>
					<td nowrap>Workshop Author</td>
					<td class="text-center">150 USD</td>
					<td class="text-center">75 USD</td>
				</tr>
				<tr>
					<td nowrap>Additional Paper</td>
					<td class="text-center">150 USD</td>
					<td class="text-center">75 USD</td>
				</tr>
				<tr>
					<td nowrap>Attendee</td>
					<td class="text-center">0 USD</td>
					<td class="text-center">0 USD</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<p class="text-justify">Registration instructions are sent to authors of
	accepted papers in the corresponding acceptance notification.</p>