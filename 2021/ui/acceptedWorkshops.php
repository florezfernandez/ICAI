<h3>Accepted Workshops</h3>

<h4>
	Second International Workshop on Applied Artificial Intelligence (WAAI)
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Artificial
	Intelligence is an annual event intended to be an important forum of
	the Artificial Intelligence (AI) community, organized by the GRUPO
	GEMIS of the Universidad Tecnológica Nacional Facultad Regional Buenos
	Aires. The workshop aims to provide a forum for researchers and AI
	community members to discuss and exchange ideas and experiences on
	diverse topics of AI. It also seeks to strengthen a space for
	discussion of innovative ideas as well as development of emerging
	technologies and practical experiences related to Artificial
	Intelligence in general and each of its technologies in particular.
	Submissions are invited on both applications of AI and new tools and
	foundations.
	<br>Submission URL: <a href="https://easychair.org/conferences/?conf=waai2021" target="_blank">https://easychair.org/conferences/?conf=waai2021</a>
	<br><strong>Organized by:</strong>	
</p>
<div class="text-center"><img src="../img/logos/utn.png" height="100px"></div>
<hr>

<h4>
	Third International Workshop on Applied Informatics for Economy,
	Society, and Development (AIESD) 
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Informatics for
	Economy, Society and Development organized by the Universidad
	Tecnológica Empresarial de Guayaquil (Ecuador) seeks to promote
	discussion and dissemination of current economics research, systems for
	territorial intelligence, e- participation, e-democracy and territorial
	economic development; with special emphasis on social and environmental
	development. The objective of this third edition of AIESD 2021 is to
	discuss aspects related to computer science applied in society. The
	idea is to learn the different techniques and tools that are used to
	study social phenomena, economic growth, the interaction between
	economic analysis and decision-making. The workshop seeks to promote an
	atmosphere of dialogue among the community of professionals working on
	issues related to technology and its application in the economy as well
	as society.
	<br>Submission URL: <a href="https://easychair.org/conferences/?conf=aiesd2021" target="_blank">https://easychair.org/conferences/?conf=aiesd2021</a>
	<br><strong>Organized by:</strong>
</p>
<div class="text-center"><img src="../img/logos/uteg.png" height="100px"></div>
<hr>

<h4>
	Fourth International Workshop on Data Engineering and Analytics (WDEA)
</h4>
<p>
	<strong>Abstract:</strong> Data has become pervasive. Data Analytics,
	Data Mining, Data Science, or more generally the practice of
	identifying patterns or constructing mathematical models based on data,
	is at the heart of most new business models or projects. This, coupled
	with what has been termed the Big Data phenomenon, used to describe
	challenging scenarios involving either datasets that are too big to be
	handled using traditional approaches, datasets with diverse formats and
	semantics, data streams that need to be actioned in real time, or a
	combination of these, has led to a whole new series of rapidly evolving
	tools, algorithms and methods which combine data engineering, computer
	science, statistics and mathematics. The aim of this workshop is to
	present recent results at the intersection of these areas, discussing
	new methods or algorithms, or applications including Machine Learning,
	Databases, optimization or any type of algorithms which consider
	managing or obtaining value from data.
	<br>Submission URL: <a href="https://easychair.org/conferences/?conf=wdea2021" target="_blank">https://easychair.org/conferences/?conf=wdea2021</a>
	<br><strong>Organized by:</strong>
</p>
<div class="text-center"><img src="../img/logos/utadeo.png" height="80px"></div>
<hr>

<h4>
	First International Workshop on Knowledge Management and Information
	Technologies (WKMIT) 
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Knowledge Management and
	Information Technologies is an annual event intended to be an important
	forum of the Knowledge Management (KM) community, organized by the
	GRUPO GEMIS of the Universidad Tecnológica Nacional Facultad Regional
	Buenos Aires. The knowledge is a strategic resource to obtain better
	results in the organizations and its management promotes continuous
	improvement practices and innovation. A complete and integral knowledge
	management requires an approach from different perspectives: people,
	organizational culture, processes, tools and technologies, among
	others. The workshop aims at providing a forum for researchers and KM
	community members to discuss and exchange ideas and experiences on
	diverse topics of KM. It also seeks to strengthen a space for
	discussion of innovative ideas, development of emerging technologies
	and practical experiences related to Knowledge Management in general
	and each of its technologies in particular.
	<br>Submission URL: <a href="https://easychair.org/conferences/?conf=wkmit2021" target="_blank">https://easychair.org/conferences/?conf=wkmit2021</a>
	<br><strong>Organized by:</strong>
</p>
<div class="text-center"><img src="../img/logos/utn.png" height="100px"></div>
<hr>

<h4>
	Fourth International Workshop on Smart and Sustainable Cities (WSSC) 
</h4>
<p>
	<strong>Abstract:</strong> The International Telecommunication Union
	ITU defines a smart sustainable city as: “An innovative city that uses
	information and communication technologies (ICTs) and other means to
	improve quality of life, efficiency of urban operation and services,
	and competitiveness, while ensuring that it meets the needs of present
	and future generations with respect to economic, social, environmental
	as well as cultural aspects”. The aim of this Workshop is to present
	recent results in this area, discussing new methods or algorithms, or
	applications in different areas including architecture, cultural,
	tourism, mobility, among others.
	<br>Submission URL: <a href="https://easychair.org/my/conference?conf=wssc21" target="_blank">https://easychair.org/my/conference?conf=wssc21</a>
	<br><strong>Organized by:</strong>
</p>
<div class="text-center"><img src="../img/logos/rumbo.png" height="80px"></div>
<hr>

<h3>Important Dates</h3>
<ul>
	<li>Paper Submission: <del>August 29, 2021</del> September 5, 2021</li>
	<li>Paper Notification: <del>September 12, 2021</del> September 19, 2021</li>
	<li>Camera Ready: <del>September 26, 2021</del> October 3, 2021</li>
	<li>Authors Registration: <del>September 26, 2021</del> October 3, 2021</li>
</ul>

<h3>Submission Guidelines</h3>
<p class="text-justify">
	Authors must submit an original full paper (12 to 15 pages) that has
	not previously been published. All contributions must be written in
	English. Authors must follow CEUR styles either for LaTeX or for MSWord
	(<a href="http://ceur-ws.org/Vol-XXX/CEURART.zip" target="_blank">http://ceur-ws.org/Vol-XXX/CEURART.zip</a>),
	for the preparation of their papers. We encourage authors to include
	their ORCIDs in their papers. In addition, the corresponding author of
	each accepted paper, acting on behalf of all of the authors of that
	paper, must complete and sign an <a href="docs/CEUR_ICAIW_2021.pdf"
		target="_blank">Author Agreement</a> form.
</p>

<h3>Proceedings</h3>
<p>Proceedings will be submitted to CEUR Workshop Proceedings for online
	publication.</p>