<br>
<div class="row">
	<div class="col">
		<h3>
			ICAI 2021 <span class="text-danger">ONLINE</span>
		</h3>
		<p class="text-justify">The Fourth International Conference on Applied
			Informatics (ICAI) aims to bring together researchers and
			practitioners working in different domains in the field of
			informatics in order to exchange their expertise and to discuss the
			perspectives of development and collaboration</p>
		<p class="text-justify">
			Due to restrictions related to the COVID-19 pandemic, ICAI 2021 was
			held <strong class="text-danger">ONLINE</strong> in collaboration
			with the <strong>Universidad Tecnológica Nacional Facultad Regional
				Buenos Aires</strong> located in <strong>Buenos Aires, Argentina</strong>,
			from 28 to 30 October 2021. It is organized by the ITI Research Group
			that belongs to the Universidad Distrital Francisco Jose de Caldas
			and the Universidad Tecnológica Nacional Facultad Regional Buenos
			Aires.
		</p>
		<p>
			ICAI 2021 proceedings will be published with <strong>Springer</strong>
			in their <strong>Communications in Computer and Information Science
				(CCIS)</strong> series.
		</p>
		<p class="text-justify">ICAI 2021 is proudly sponsored by: Information
			Technologies Innovation Research Group, Springer, Science Based
			Platforms, and CEUR Workshop Proceedings.</p>
		<p>ICAI has been previously held in Ota, Nigeria (2020); Madrid, Spain
			(2019); and Bogota, Colombia (2018).</p>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2021 Proceedings in CCIS </strong>
			</h5>
			ICAI 2021 proceedings in Communications in Computer and Information
			Science (CCIS) Volume 1455 by Springer are now available at <a
				href="https://link.springer.com/book/10.1007/978-3-030-89654-6"
				target="_blank">https://link.springer.com/book/10.1007/978-3-030-89654-6</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2021 Workshops Proceedings in CEUR</strong>
			</h5>
			ICAI 2021 workshops proceedings in CEUR Workshops Proceedings are now
			available at <a href="http://ceur-ws.org/Vol-2992" target="_blank">http://ceur-ws.org/Vol-2992</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Certificates</strong>
			</h5>
			ICAI 2021 certificates are available at: <a
				href="https://icai-c.itiud.org/certificate.php?edition=2021"
				target="_blank">ICAI-C</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>Best Paper Award</strong>
			</h5>
			The paper <strong>Super-resolution algorithm applied in the zoning of
				aerial images</strong> <a
				href="https://link.springer.com/chapter/10.1007/978-3-030-89654-6_25"
				target="_blank"><span class="fas fa-external-link-alt"
				aria-hidden="true"></span></a> authored by <strong><i>J. A. Baldion,
					E. Cascavita, C. H. Rodriguez-Garavito</i></strong> from the <strong>Universidad
				La Salle, Colombia</strong> has been selected as <strong>Best Paper</strong>
			of ICAI 2021.
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>Best Reviewers Acknowledgement</strong>
			</h5>
			ICAI 2021 acknowledges the remarkable work of the following reviewers
			<ul>
				<li>Victor Darriba, Universidade de Vigo, Spain</li>
				<li>Alexander Bock, Universität Duisburg Essen, Germany</li>
				<li>Jens Gulden, Universiteit Utrecht, Netherlands</li>
				<li>Andrei Tchernykh, Centro de Investigación Científica y de
					Educación Superior de Ensenada, Mexico</li>
				<li>José Rufino, Instituto Politécnico de Bragança, Portugal</li>
				<li>Christian Grévisse, Université du Luxembourg, Luxembourg</li>
				<li>German Vega, Centre National de la Recherche Scientifique,
					France</li>
				<li>Ivan Mura, Duke Kunshan University, China</li>
				<li>Daniel Görlich, SRH Hochschule Heidelberg, Germany</li>
				<li>Jorge Bacca, Fundacion Universitaria Konrad Lorenz, Colombia</li>

			</ul>
		</div>
	</div>

</div>