<h3>Program</h3>
<div class="alert alert-success" role="alert">
	<strong>The program is fixed to Argentina Local time (GMT-3)</strong><br>
	Pre-recorded presentations are available.
</div>
<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="30%" class="text-center">Thursday 28 October</th>
		<th width="30%" class="text-center">Friday 29 October</th>
		<th width="30%" class="text-center">Saturday 30 October</th>
	</tr>
	<tr>
		<td>9:00-9:30</td>
		<td></td>
		<td class="table-success" rowspan="2">Keynote: Luis Rabelo<br> <i>URL:
				<a href="https://meet.google.com/wng-tedn-ctv" target="_blank">Google
					Meet</a>
		</i></td>
	</tr>
	<tr>
		<td>9:30-10:00</td>
		<td class="table-danger">Conference Opening<br> <i>URL: <a
				href="https://meet.google.com/yce-whgr-qzm" target="_blank">Google
					Meet</a></i></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success">Keynote: Jose A. Olivas<br> <i>URL: Same
				than Conference Opening</i></td>
		<td class="table-success">Keynote: Fernando Yepes-Calderon<br> <i>URL:
				<a href="https://meet.google.com/are-acyz-med" target="_blank">Google
					Meet</a>
		</i>
		
		<td class="table-danger">Conference Closing<br> <i>URL: <a
				href="https://meet.google.com/krc-qhub-ddp" target="_blank">Google
					Meet</a></i>
		</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-warning">Workshops AIESD &amp; WKMIT<br> <i>URL: <a
				href="https://meet.google.com/nfa-eqoy-bqu" target="_blank">Google
					Meet</a>
		</i> <br> <br>Workshops WAAI &amp; WDEA<br> <i>URL: <a
				href="https://meet.google.com/iys-bfjn-rsv" target="_blank">Google
					Meet</a></i> <br> <br>Workshop WSSC<br> <i>URL: <a
				href="https://meet.google.com/bgz-hsgt-aqb" target="_blank">Google
					Meet</a></i>
		</td>
		<td class="table-info">ICAI Decision Systems &amp; Image Processing
			Sessions<br> <i>URL: <a href="https://meet.google.com/opu-bfnr-fwd"
				target="_blank">Google Meet</a>
		</i>
		</td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-info">ICAI Artificial Intelligence Session<br> <i>URL:
				<a
				href="https://meet.google.com/cea-aupq-hyp" target="_blank">Google
					Meet</a></i>
		</td>
		<td class="table-info">ICAI Data Analysis &amp; Simulation and
			Emulation Sessions <br> <i>URL: <a
				href="https://meet.google.com/wpg-ncau-rzv" target="_blank">Google
					Meet</a>
		</i>
		</td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info">ICAI Health Care Information Systems &amp;
			Software Design Engineering Sessions<br> <i>URL: <a
				href="https://meet.google.com/hpw-zvxj-upy" target="_blank">Google
					Meet</a></i>
		</td>
		<td class="table-info">ICAI Security Services &amp; Smart Cities &amp;
			Software and Systems Modeling Sessions<br> <i>URL: <a
				href="https://meet.google.com/oaf-wxud-onm" target="_blank">Google
					Meet</a></i>
		</td>
	</tr>
</table>

<h4>Leyend</h4>
<table class="table">
	<tr>
		<td class="table-info" width="5%"></td>
		<td width="45%">ICAI Sessions (Presentations of papers in CCIS)</td>
		<td class="table-success" width="5%"></td>
		<td width="45%">Keynotes</td>
	</tr>
	<tr>
		<td class="table-warning" width="5%"></td>
		<td width="45%">ICAI Workshops Sessions (Presentations of papers in
			CEUR-WS)</td>
		<td class="table-danger" width="5%"></td>
		<td width="45%">Social Activities</td>
	</tr>
</table>

<h3>Detailed Program</h3>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center" colspan="3">Thursday 28 October</th>
	</tr>
	<tr>
		<td>9:30-10:00</td>
		<td class="table-danger" colspan="3"><strong>Conference Opening</strong></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success" colspan="3"><strong>Keynote: Jose A. Olivas.
				Intelligent data analysis guided by Knowledge Engineering</strong><br>
			Session Chair: Ma Florencia Pollo-Cattaneo</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td width="30%" class="table-warning"><strong>Workshops AIESD &amp;
				WKMIT</strong><br> Workshop Chair: Marcelo Leon
			<ul>
				<li><strong>Empirical approach to arable land and livestock using
						co-integration and causality techniques with panel data.</strong><br>
					Micaela Calderon, Marcelo Leon, Sergio Núñez</li>
				<li><strong>Pedagogical model to develop virtual learning objects
						for people with hearing impairment</strong><br> Karen
					Martinez-Zambrano, Jhon Paez, Hector Florez</li>
				<li><strong>Underground drones as a support line for mining
						companies in Ecuador</strong><br> Marcelo Leon, Homero Rodriguez,
					Ibeth Chiluiza, Claudia Rivas</li>
				<li><strong>Knowledge management technologies for a n-layered
						architecture</strong><br> Luciano Straccia, Ma Florencia
					Pollo-Cattaneo, Adriana Maulini</li>
				<li><strong>Successes and failures in software development project
						management: a systematic literature review</strong><br> Patricia
					Gerlero</li>

			</ul></td>
		<td width="30%" class="table-warning"><strong>Workshops WAAI &amp;
				WDEA</strong><br> Workshop Chair: Ma Florencia Pollo-Cattaneo
			<ul>
				<li><strong>Data science applied to oil wells' behavior prediction
						in the Estructura Cruz de Piedra - Lunlunta oil field, Cuyana
						Basin, Argentina</strong><br> Micaela Báez, Luis P. Stinco, Silvia
					P. Barredo, Hernan D. Merlino</li>
				<li><strong>Multi-criteria group requirement prioritization in
						software engineering using fuzzy linguistic labels</strong><br>
					Giovanni Daián Rottoli, Carlos Casanova</li>
				<li><strong>Predictive modeling toward identification of sex from
						lip prints - machine learning in cheiloscopy</strong><br> Agustín
					F. Sabelli, Parag Chatterjee, Ma Florencia Pollo-Cattaneo</li>
				<li><strong>Analysing gender-based violence against Colombian public
						figures on Twitter</strong><br> Juan Sebastian Chaparro-Saenz,
					Ixent Galpin</li>
				<li><strong>Analyzing and predicting NCAA volleyball match outcome
						using machine learning techniques</strong><br> Dhvanil Sanghvi,
					Priya Deshpande, Suhas Shanbhogue, Vishwa Shah</li>


			</ul></td>
		<td width="30%" class="table-warning"><strong>Workshops WSSC</strong><br>
			Workshop Chair: Cesar Diaz
			<ul>
				<li><strong>Forum: Smart Cities - National e International
						Experiences</strong></li>
				<li><strong>Environmental economic estimation: case study in the
						city of Tarija - Bolivia</strong><br> Carlos Jijena, Marcelo Leon,
					Claudia Rivas, Carlos Redroban</li>
				<li><strong>Industry 4.0 and digital transformation in higher
						education through the perspective of smart cites</strong><br>
					Álvaro Gutiérrez-Rodríguez, José David López-García, Ricardo
					Alfonso Sanabria, Sandra Acevedo-Zapata</li>
				<li><strong>Prediction of electrical energy consumption through
						recurrent neural networks</strong><br> Oscar Daniel Diaz-Castillo,
					Andrés Esteban Puerto-Lara, Javier Alejandro Sáenz-Leguizamón,
					Vladimir Ducón-Sosa</li>
			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="3">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-info" colspan="3"><strong>ICAI Artificial
				Intelligence Session</strong><br>Session Chair: Olmer Garcia<br>
			<ul>
				<li><strong>A Chatterbot Based on Genetic Algorithm: Preliminary
						Results</strong><br> Cristian Orellana, Martin Tobar, Yeremy
					Yazan, D. H. Peluffo-Ordóñez, Lorena Guachi-Guachi</li>
				<li><strong>A Supervised Approach to Credit Card Fraud Detection
						using Artificial Neural Network</strong><br> Oluwatobi Noah
					Akande, Sanjay Misra, Hakeem Babalola Akande, Jonathan Oluranti,
					Robertas Damasevicius</li>
				<li><strong>Machine Learning Classification based Techniques for
						Fraud Discovery in Credit Card Datasets</strong><br> Roseline
					Oluwaseun Ogundokun, Sanjay Misra, Opeyemi Eyitayo Ogundokun,
					Jonathan Oluranti, Rytis Maskeliunas</li>
				<li><strong>Object Detection based software system for automatic
						evaluation of Cursogramas images</strong><br> Pablo Pytel, Matias
					Almad, Rocío Leguizamón, Cinthia Vegega, Ma Florencia
					Pollo-Cattaneo</li>
				<li><strong>Sign Language Recognition using Leap MotionBased on
						Time-Frequency Characterization and Conventional Machine Learning
						Techniques</strong><br> D. López-Albán, A. Lopez-Barrera, D.
					Mayorca-Torres, D. Peluffo-Ordoñez</li>
				<li><strong>Solar radiation prediction using machine learning
						techniques</strong><br> Luis Alejandro Caycedo Villalobos, Richard
					Alexander Cortázar Forero, Pedro Miguel Cano Perdomo, José John
					Fredy González Veloza</li>
				<li><strong>Towards More Robust GNN Training with Graph
						Normalization for GraphSAINT</strong><br> Yuying Wang, Qinfen Hao</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info" colspan="3"><strong>ICAI Health Care
				Information Systems & Software Design Engineering Sessions </strong><br>
			Session Chair: Fernando Yepes-Calderon
			<ul>
				<li><strong>Blood Pressure Morphology as a Fingerprint of
						Cardiovascular Health: A Machine Learning Based Approach</strong><br>
					Eugenia Ipar, Nicolás A. Aguirre, Leandro J. Cymberknop, Ricardo L.
					Armentano</li>
				<li><strong>Characterizing Musculoskeletal Disorders. A Case study
						involving kindergarten employees</strong><br> Gonzalo Yepes
					Calderon, Julio Perea Sandoval, Julietha Oviedo Correa, Fredy
					Linero-Moreno, Fernando Yepes-Calderon
				
				<li><strong>Comparison of heart rate variability analysis with
						empirical mode decomposition and Fourier transform</strong><br>
					Javier Zelechower, Fernando Pose, Francisco Redelico, Marcelo Risk</li>
				<li><strong>Evalu@ + Sports. Creatine Phosphokinase and Urea in
						High-Performance Athletes During Competition. A framework for
						predicting injuries caused by fatigue</strong><br> Fernando
					Yepes-Calderon, Alvin David Gregory Tatis, Daniel Santiafo Forero
					Arévalo, Juan Fernando Yepes Zuluaga</li>
				<li><strong>Heart rate variability: validity of autonomic balance
						indicators in ultra-short recordings </strong><br> Jose Gallardo,
					Giannina Bellone, Marcelo Risk</li>
				<li><strong>Architectural Approach for Google Services Integration</strong><br>
					Hamilton Ricaurte, Hector Florez</li>
				<li><strong>Proposal to improve Software Testing in Small and Medium
						Enterprises</strong><br> Melisa Argüello, Carlos Antonio Casanova
					Pietroboni, Karina Elizabeth Cedaro</li>
			</ul></td>
	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center" colspan="2">Friday 29 October</th>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td class="table-success" colspan="2"><strong>Keynote: Luis Rabelo.
				Artificial Intelligence and Simulation: Opportunities in
				Cybersecurity and Blockchain Design</strong><br> Session Chair:
			Cesar Diaz</td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success" colspan="2"><strong>Keynote: Fernando
				Yepes-Calderon. Science and regulations around controlling global
				warming. A desperate call to scientists</strong><br> Session Chair:
			Hector Florez</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-info" colspan="2"><strong>ICAI Decision Systems &amp;
				Image Processing Sessions</strong><br> Session Chair: Giovanni Daian Rottoli
			<ul>
				<li><strong>A Multi-Objective Mathematical Optimization Model and a
						Mobile Application for finding Best Pedestrian Routes considering
						SARS-CoV-2 contagions</strong><br> Juan E. Cantor, Carlos
					Lozano-Garzón, Germán A. Montoya</li>
				<li><strong>An Improved Course Recommendation System based on
						Historical Grade Data using Logistic Regression</strong><br> Idowu
					Dauda Oladipo, Joseph Bamidele Awotunde, Muyideen Abdulraheem,
					Oluwasegun Osemudiame Ige, Ghaniyat Bolanle Balogun, Adekola
					Rasheed Tomori, Fatimoh Abidemi Taofeek-Ibrahim</li>

				<li><strong>Predictive academic performance model to support,
						prevent and decrease the university dropout rate </strong><br>
					Diego Bustamante, Olmer Garcia-Bedoya</li>

				<li><strong>Proposed algorithm to digitize the root-cause analysis
						and 8Ds of the automotive industry</strong><br> Brenda-Alejandra
					Mendez Torres, Patricia Cano-Olivos</li>
				<li><strong>An Improved Machine Learnings Diagnosis Technique for
						COVID-19 Pandemic using Chest X-ray Images</strong><br> Joseph
					Bamidele Awotunde, Sunday Adeola Ajagbe, Matthew A. Oladipupo,
					Jimmisayo A. Awokola, Olakunle S. Afolabi, Mathew O. Timothy,
					Yetunde J. Oguns</li>

				<li><strong>Evaluation of local thresholding algorithms for
						segmentation of white matter hyperintensities in magnetic
						resonance images of the brain</strong><br> Adam Piórkowski, Julia
					Lasek</li>

				<li><strong>Super-resolution algorithm applied in the zoning of
						aerial images</strong><br> J. A. Baldion, E. Cascavita, C. H.
					Rodriguez-Garavito</li>

			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-info" colspan="2"><strong>ICAI Data Analysis &
				Simulation and Emulation Sessions</strong><br>Session Chair: Ixent Galpin<br>
			<ul>
				<li><strong>A complementary approach in the analysis of the human
						gut microbiome applying self-organizing maps and random forest</strong><br>
					Valeria Burgos, Tamara Piñero, Maria Laura Fernandez, Marcelo Risk</li>
				<li><strong>An evaluation of intrinsic mode function characteristic
						of non-Gaussian autorregresive processes </strong><br> Fernando
					Pose, Javier Zelechower, Marcelo Risk, Francisco O. Redelico</li>
				<li><strong>Analysing Documents about Colombian Indigenous Peoples
						through Text Mining</strong><br> Jonathan Stivel Piñeros-Enciso,
					Ixent Galpin</li>
				<li><strong>Application of a continuous improvement method for
						Graphic User Interfaces through Intelligent Systems for a language
						services company</strong><br> Osvaldo Germán Fernández, Pablo
					Pytel, Ma Florencia Pollo-Cattaneo</li>
				<li><strong>Descriptive analysis model to improve the pension
						contributions collection process for Colpensiones</strong><br>
					William M. Bustos C., Cesar O. Diaz</li>
				<li><strong>User Response-based Fake News Detection on Social Media</strong><br>
					Hailay Kidu, Haile Misgna, Tong Li, Zhen Yang</li>
				<li><strong>Optimization of Multi-Level HEED protocol in wireless
						sensor networks</strong><br> Walid Boudhiafi, Tahar Ezzedine</li>



			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info" colspan="2"><strong>ICAI Security Services &
				Smart Cities & Software and Systems Modeling Sessions</strong><br>Session
			Chair: Parag Chatterjee<br>
			<ul>
				<li><strong>An Enhanced Lightweight Speck System for Cloud-based
						Smart Healthcare </strong><br>Muyideen Abdulraheem, Ghaniyat
					Bolanle Balogun, Kazeem Moses Abiodun, Fatimoh Abidemi
					Taofeek-Ibrahim, Adekola Rasheed Tomori, Idowu Dauda Oladipo,
					Joseph Bamidele Awotunde</li>
				<li><strong>Hybrid Algorithm for Symmetric Based Fully Homomorphic
						Encryption</strong><br> Kamaldeen Jimoh Muhammed, Rafiu Mope
					Isiaka, Ayisat Wuraola Asaju-Gbolagade, Kayode Sakariyah Adewole,
					Kazeem Alagbe Gbolagade</li>
				<li><strong>Information encryption and decryption analysis,
						vulnerabilities and reliability implementing the RSA algorithm in
						Python</strong><br> Rocio Rodriguez, Carlos A. Vanegas, Gerardo A.
					Castang</li>
				<li><strong>LIDAR and Camera Data for Smart Urban Traffic
						Monitoring: Challenges of Automated Data Capturing and
						Synchronization</strong><br> Gatis Vitols, Nikolajs Bumanis, Irina
					Arhipova, Inga Meirane</li>
				<li><strong>Mobility in Smart Cities: Spatiality of the travel time
						indicator according to uses and modes of transportation</strong><br>
					Carlos Alberto Diaz Riveros, Karen Astrid Beltran Rodriguez, Cesar
					O. Diaz, Alejandra Juliette Baena Vasquez</li>
				<li><strong>Preliminary studies of the security of the
						cyber-physical smart grids</strong><br> Luis Rabelo, Andres
					Ballestas, Bibi Ibrahim, Javier Valdez</li>
				<li><strong>Enterprise Modeling: A Multi-Perspective Tool-Supported
						Approach</strong><br> Paola Lara Machado, Mario Sánchez, Jorge
					Villalobos</li>



			</ul></td>
	</tr>
</table>