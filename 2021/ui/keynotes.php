<h3>Keynote Speakers for ICAI 2021</h3>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/jo.png" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Jose A. Olivas</strong>
		</h5>
		<p>Doctor Olivas was born in 1964 in Lugo (Spain). He received his
			M.S. degree in Philosophy in 1990 (University of Santiago de
			Compostela, Spain), Master on Knowledge Engineering of the Department
			of Artificial Intelligence, Polytechnic University of Madrid in 1992,
			and his Ph.D. in Computer Science in 2000 (University of Castilla–La
			Mancha, Spain). In 2001 was Postdoc Visiting Scholar at Lotfi A.
			Zadeh’s (father of Fuzzy Logic) BISC (Berkeley Initiative in Soft
			Computing), University of California-Berkeley, USA. He keeps in touch
			with BISC and with the Artificial Intelligence Center of the SRI
			International of the Stanford University (Prof. Richard Waldinger).
			His current main research interests are in the field of Soft
			Computing for Information Retrieval, Data Analysis and Artificial
			Intelligence and Knowledge Engineering applications. He received the
			Environment Research Award 2002 from the Madrid Council (Spain) for
			his PhD. Thesis or the Best Paper Award of the Spanish Association
			for Artificial Intelligence (AEPIA) in 2015. Author of the book
			“Búsqueda eficaz de información en la Web” and more than 250
			scientific contributions. “Honoris causa” Doctorate National
			University of La Plata, Buenos Aires, Argentina, 2020.</p>
		<h5>
			<strong>Keynote: Intelligent data analysis guided by Knowledge
				Engineering</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Nowadays it is very common to link
			Artificial Intelligence only to the (intelligent?) analysis of data,
			and this to the techniques of machine learning, most of them coming
			from statistics. But Artificial Intelligence is much more, something
			very different: providing computational systems with the ability to
			simulate intelligent human behavior, both in reasoning and in acting.
			On the other hand, we are witnessing the phenomenon of the
			'deification' of data, when we know that they are very incomplete
			representations, mostly numerical, with noise, imprecision,
			uncertainty and increasingly, deliberately biased or false, usually
			not with the best intentions. This talk will address all these
			aspects and build bridges between 'correlation' and 'causation'
			through knowledge engineering, finally proposing the development of
			systems that rely on both intelligent data exploitation and knowledge
			about the phenomenon to be addressed.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/lr.jpg" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Luis Rabelo</strong> <a
				href="https://iems.ucf.edu/people/luis-rabelo" target="_blank"><span
				class="fas fa-external-link-alt" aria-hidden="true"></span></a>
		</h5>
		<p>
			Dr. Luis Rabelo received the BS in Electro-Mechanical Engineer from
			the Technological University of Panama in 1983, an MS in Electrical
			Engineering from the Florida Institute of Technology in 1987
			Master's, and a Doctorate (Ph.D.) in Engineering Management from the
			University of Missouri. He was a Postdoc at the University of
			Missouri in Nuclear Engineering. In addition, Dr. Rabelo received a
			dual MS in Systems and Management from the Massachusetts Institute of
			Technology. <br>Dr. Rabelo has worked for the Advanced Technology
			Group (Goodrich) and Honeywell Laboratories. Dr. Rabelo was a NASA
			Fellow from 2002 to 2005. He was a Research Project Manager at NASA
			from 2009 to 2011. He is currently Professor in the Industrial
			Engineering and Management Systems Department at UCF. Dr. Rabelo is
			also the Undergraduate Coordinator/Associate Chair. He has written
			more than 300 articles, a book in Artificial Intelligence, and an
			upcoming book in Engineering Analytics (Taylor and Francis/CRC Press).
			<br>He has been the principal advisor for 39 Master's students and 31
			PhDs. He received the Best Scientific Article Award of the Year 2004
			from the Society of Automotive Engineers, received the distinction
			"ONE NASA" in 2006, and other NASA awards for performance and
			contributions in 2010 and 2011. He was the recipient of the Fulbright
			Distinction in 2008. In November 2008, he received the honor of the
			Technological University of Panama (UTP) of being the Distinguished
			Alumni. In 2011, he was the Henaac Award Winner in Education. In 2013
			and 2018, he received the Forest R. McFarland Award from the Society
			of Automotive Engineering (SAE). Lately, he received the SAE Russell
			S. Springer Award for his article on the modeling of space operations
			systems in 2017.
		</p>
		<h5>
			<strong>Keynote: Artificial Intelligence and Simulation:
				Opportunities in Cybersecurity and Blockchain Design</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> In this keynote, the speaker will
			introduce Artificial Intelligence and Simulation from his own life
			experiences starting in the 1980s and culminating with three
			International/US Patents used in Aviation/Aerospace in the late
			1990’s and two distributed simulation systems with NASA and Defense.
			In addition, he will talk about recent developments of his research
			group in Cybersecurity Frameworks for more Robust Risk Management,
			the Design of Blockchain Systems (justification), and the combination
			of Big Data, Deep Learning, and Distributed Simulation. The
			Cybersecurity framework uses System Dynamics (a derivation of
			continuous simulation) and Discrete-Event simulation. On the other
			hand, the Blockchain justification uses the integration of
			agent-based simulation with deep learning. Big Data, Deep Learning,
			and Distributed Simulation were built to demonstrate the advantages
			of this unique integration.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/fy.jpeg" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Fernando Yepes-Calderon</strong> <a
				href="https://iems.ucf.edu/people/luis-rabelo" target="_blank"><span
				class="fas fa-external-link-alt" aria-hidden="true"></span></a>
		</h5>
		<p>Fernando Yepes-Calderon – Ph.D. was part of the soccer team that
			gained the professional soccer tournament in Colombia (1996). Then,
			he pursued a carrier in electronics engineering and graduated in
			2002. He is the holder of three master's degrees in diverse fields of
			engineering applied to medicine and certified in Colombia (2007),
			France (2009), and Spain (2012). He obtained a Ph.D. degree in
			Biomedicine from Universidad de Barcelona in 2016. Fernando has
			become an expert in proposing methods, and his work covers several
			fields of application, including neurodevelopment, cancer, sports in
			medicine, medical imaging, and applied artificial intelligence.</p>
		<h5>
			<strong>Keynote: Science and regulations around controlling global
				warming. A desperate call to scientists</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Caring for the environment has been a
			priority concern after the establishment of the Kyoto protocol in
			1997. Since then, the globe has warmed 0.86 degrees Celsius.
			According to the Intergovernmental Panel on Climate Change (IPCC)
			there is only 0.5 degrees of range between a sustainable future for
			the planet and one in which life would be unviable. The deterioration
			has already begun, but it would be latent from 2030. The need to join
			scientists in the fight against global warming is irrefutable. A high
			dose of innovation is required to think of a world that moves under
			clean and eco-efficient procedures, also individuals who generate and
			analyze robust data, as well as people capable of clearly disclosing
			achievements in environmental matters. This presentation is an
			invitation to work for the care of the planet.
		</p>
	</div>
</div>