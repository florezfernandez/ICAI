<h3>General Chairs</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Ma Florencia Pollo-Cattaneo, Ph.D. Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina <a href="https://orcid.org/0000-0003-4197-3880" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Steering Committee</h3>
<ul>
	<li>Jaime Chavarriaga, Ph.D. Universidad de los Andes, Colombia <a href="https://orcid.org/0000-0002-8372-667X" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Cesar Diaz, Ph.D. OCOX AI, Colombia <a href="https://orcid.org/0000-0002-9132-2747" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0001-7020-6328" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Olmer García, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0002-6964-3034" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Christian Grévisse, Ph.D. Université du Luxembourg, Luxembourg <a href="https://orcid.org/0000-0002-9585-1160" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Sanjay Misra, Ph.D. Covenant University, Nigeria <a href="https://orcid.org/0000-0002-3556-9331" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Ma Florencia Pollo-Cattaneo, Ph.D. Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina <a href="https://orcid.org/0000-0003-4197-3880" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Fernando Yepes-Calderon, Ph.D. Children's Hospital Los Angeles, United States <a href="https://orcid.org/0000-0001-9184-787X" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Organizing Committee</h3>
<ul>
	<li>Ignacio Bengoechea, Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina</li>
	<li>Paola Britos, Ph.D. Universidad Nacional de Río Negro, Argentina <a href="https://orcid.org/0000-0002-8846-4744" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Andres Bursztyn, Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina</li>
	<li>Ines Casanovas, Ph.D. Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina</li>
	<li>Alejandro Hossian, Ph.D. Universidad Tecnológica Nacional Facultad Regional Neuquén, Argentina</li>
	<li>Hernan Merlino, Ph.D. Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina</li>
	<li>Florencia Pollo-Cattaneo, Ph.D. Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina <a href="https://orcid.org/0000-0003-4197-3880" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Giovanni Rottoli, Ph.D. Universidad Tecnológica Nacional Facultad Regional Concepción del Uruguay, Argentina <a href="https://orcid.org/0000-0002-7623-2591" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Cinthia Vegega, Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina</li>
</ul>


<h3>Workshops Committee</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0001-7020-6328" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Christian Grévisse, Ph.D. Université du Luxembourg, Luxembourg <a href="https://orcid.org/0000-0002-9585-1160" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Program Committee</h3>
<ol>
<?php 
$pc = fopen("docs/pc.csv", "r");
while(!feof($pc)) {
    $data = explode(";", trim(fgets($pc)));
    if($data[0] != ""){
        echo "<li>" . $data[0] . " " . $data[1] . ", Ph.D. " . $data[3] . ", " . $data[4] . (($data[7]!="")?" <a href='" . $data[7] . "' target='_blank'><img src='../img/logos/orcid.png' ></a>":"") . "</li>\n";        
    }
}
?>
</ol>