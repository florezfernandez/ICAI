<h3>Important Dates</h3>
<ul>
	<li>Paper Submission:
		<ul>
			<li><del>July 5, 2021</del></li>
			<li><strong>July 19, 2021</strong></li>
		</ul>
	</li>
	<li>Paper Notification:
		<ul>
			<li><del>August 2, 2021</del></li>
			<li><strong>August 9, 2021</strong></li>
		</ul>
	</li>
	<li>Camera Ready:
		<ul>
			<li><del>August 30, 2021</del></li>
			<li><strong>September 6, 2021</strong></li>
		</ul>
	</li>
	<li>Authors Registration:
		<ul>
			<li><del>August 30, 2021</del></li>
			<li><strong>September 6, 2021</strong></li>
		</ul>
	</li>
</ul>
