<hr>
<h3>Organized by</h3>
<div class="text-center">
	<a href="https://www.udistrital.edu.co" target="_blank"><img
		src="../img/logos/ud.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Distrital Francisco Jose de Caldas"></a> <a
		href="https://www.frba.utn.edu.ar/" target="_blank"><img
		src="../img/logos/utn.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Tecnológica Nacional Facultad Regional Buenos Aires"></a>
</div>
<h3>Sponsored by</h3>
<div class="text-center">
	<a href="http://www.itiud.org" target="_blank"><img
		src="../img/logos/iti.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Information Technologies Innovation Research Group"></a> <a
		href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer2.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="https://www.strategicbp.net/" target="_blank"><img
		src="../img/logos/sbp.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Science Based Platforms"></a> <a
		href="http://ceur-ws.org/" target="_blank"><img
		src="../img/logos/ceur.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="CEUR Workshop Proceedings"></a>
</div>
<hr>