<div class="col-lg-3 col-md-4 text-center">
	<img src="../img/logos/icai.png" width="200">
</div>
<div class="col-lg-9 col-md-12">
	<h4>
		<i><strong>Fourth International Conference on Applied Informatics</strong></i>
	</h4>
	<h5>
		28 to 30 October 2021 <br>Universidad Tecnológica Nacional Facultad Regional Buenos Aires <br> Buenos Aires, Argentina <i class="text-danger">ONLINE</i>
	</h5>	
</div>