<h3>Accepted Papers</h3>
<h4>Artificial Intelligence</h4>
<ul>
	<li><strong>A Chatterbot Based on Genetic Algorithm: Preliminary
			Results</strong>
		<ul>
			<li>Cristian Orellana, Universidad Internacional Del Ecuador, Ecuador</li>
			<li>Martin Tobar , Universidad Internacional Del Ecuador, Ecuador</li>
			<li>Yeremy Yazan, Universidad Internacional Del Ecuador, Ecuador</li>
			<li>D. H. Peluffo-Ordóñez, Mohammed VI Polytechnic University,
				Morocco</li>
			<li>Lorena Guachi-Guachi, Universidad Internacional Del Ecuador,
				Ecuador</li>
		</ul></li>

	<li><strong>A Supervised Approach to Credit Card Fraud Detection using
			Artificial Neural Network</strong>

		<ul>
			<li>Oluwatobi Noah Akande, Landmark University, Nigeria</li>
			<li>Sanjay Misra, Ostfold University College, Norway</li>
			<li>Hakeem Babalola Akande, University of Ilorin, Nigeria</li>
			<li>Jonathan Oluranti, Covenant University, Nigeria</li>
			<li>Robertas Damasevicius, Kaunas University of Technology, Lithuania</li>

		</ul></li>

	<li><strong>Machine Learning Classification based Techniques for Fraud
			Discovery in Credit Card Datasets </strong>
		<ul>
			<li>Roseline Oluwaseun Ogundokun , Landmark University Omu Aran,
				Nigeria</li>
			<li>Sanjay Misra, Ostfold University College, Norway</li>
			<li>Opeyemi Eyitayo Ogundokun, Agricultural and Rural Management
				Training Institute, Nigeria</li>
			<li>Jonathan Oluranti, Covenant University , Nigeria</li>
			<li>Rytis Maskeliunas, Kaunas University of Technology, Lithuania</li>
		</ul></li>

	<li><strong>Object Detection based software system for automatic
			evaluation of Cursogramas images </strong>
		<ul>
			<li>Pablo Pytel, Universidad Tecnológica Nacional, Argentina</li>
			<li>Matias Almad, Universidad Tecnológica Nacional, Argentina</li>
			<li>Rocío Leguizamón,Universidad Tecnológica Nacional, Argentina</li>
			<li>Cinthia Vegega, Universidad Tecnológica Nacional, Argentina</li>
			<li>Ma Florencia Pollo Cattaneo, Universidad Tecnológica Nacional,
				Argentina</li>
		</ul></li>

	<li><strong>Sign Language Recognition using Leap MotionBased on
			Time-Frequency Characterization and Conventional Machine Learning
			Techniques </strong>
		<ul>
			<li>D. López-Albán, Universidad Mariana, Colombia</li>
			<li>A. Lopez-Barrera, Universidad Mariana, Colombia</li>
			<li>D. Mayorca-Torres, Universidad Mariana, Colombia</li>
			<li>D. Peluffo-Ordoñez, Mohammed VI Polytechnic University, Morocco</li>
		</ul></li>

	<li><strong>Solar radiation prediction using machine learning
			techniques</strong>
		<ul>
			<li>Luis Alejandro Caycedo Villalobos, Fundación Universitaria Los
				Libertadores, Colombia</li>
			<li>Richard Alexander Cortázar Forero, Fundación Universitaria Los
				Libertadores, Colombia</li>
			<li>Pedro Miguel Cano Perdomo, Fundación Universitaria Los
				Libertadores, Colombia</li>
			<li>José John Fredy González Veloza, Fundación Universitaria Los
				Libertadores, Colombia</li>
		</ul></li>

	<li><strong>Towards More Robust GNN Training with Graph Normalization
			for GraphSAINT </strong>
		<ul>
			<li>Yuying Wang, Chinese Academy of Sciences, China</li>
			<li>Qinfen Hao, Chinese Academy of Sciences, China</li>

		</ul></li>

</ul>

<h4>Data Analysis</h4>
<ul>
	<li><strong>A complementary approach in the analysis of the human gut
			microbiome applying self-organizing maps and random forest</strong>
		<ul>
			<li>Valeria Burgos, Hospital Italiano de Buenos Aires, Argentina</li>
			<li>Tamara Piñero, Hospital Italiano de Buenos Aires, Argentina</li>
			<li>Maria Laura Fernandez, Universidad de Buenos Aires, Argentina</li>
			<li>Marcelo Risk, Hospital Italiano de Buenos Aires, Argentina</li>
		</ul></li>

	<li><strong>An evaluation of intrinsic mode function characteristic of
			non-Gaussian autorregresive processes </strong>
		<ul>
			<li>Fernando Pose, Hospital Italiano de Buenos Aires, Argentina</li>
			<li>Javier Zelechower, Universidad Tecnológica Nacional, Argentina</li>
			<li>Marcelo Risk,Hospital Italiano de Buenos Aires, Argentina</li>
			<li>Francisco O. Redelico, Hospital Italiano de Buenos Aires,
				Argentina</li>
		</ul></li>

	<li><strong>Analysing Documents about Colombian Indigenous Peoples
			through Text Mining </strong>
		<ul>
			<li>Jonathan Stivel Piñeros-Enciso, Universidad de Bogota Jorge Tadeo
				Lozano, Colombia</li>
			<li>Ixent Galpin, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>

		</ul></li>

	<li><strong>Application of a continuous improvement method for Graphic
			User Interfaces through Intelligent Systems for a language services
			company </strong>
		<ul>
			<li>Osvaldo Germán Fernández, Universidad Tecnológica Nacional,
				Argentina</li>
			<li>Pablo Pytel, Universidad Tecnológica Nacional, Argentina</li>
			<li>María Florencia Pollo Cattaneo, Universidad Tecnológica Nacional,
				Argentina</li>

		</ul></li>

	<li><strong>Descriptive analysis model to improve the pension
			contributions collection process for Colpensiones </strong>
		<ul>
			<li>William M. Bustos C., Universidad de Bogota Jorge Tadeo Lozano,
				Colombia</li>
			<li>Cesar O. Diaz, Universidad ECCI, Colombia</li>

		</ul></li>

	<li><strong>User Response-based Fake News Detection on Social Media </strong>
		<ul>
			<li>Hailay Kidu, Beijing University of Technology, China</li>
			<li>Haile Misgna, Beijing University of Technology, China</li>
			<li>Tong Li, Beijing University of Technology, China</li>
			<li>Zhen Yang, Beijing University of Technology, China</li>
		</ul></li>

</ul>

<h4>Decision Systems</h4>
<ul>
	<li><strong>A Multi-Objective Mathematical Optimization Model and a
			Mobile Application for finding Best Pedestrian Routes considering
			SARS-CoV-2 contagions </strong>
		<ul>
			<li>Juan E. Cantor, Universidad de los Andes, Colombia</li>
			<li>Carlos Lozano-Garzón, Universidad de los Andes, Colombia</li>
			<li>Germán A. Montoya, Universidad de los Andes, Colombia</li>
		</ul></li>

	<li><strong>An Improved Course Recommendation System based on
			Historical Grade Data using Logistic Regression</strong>
		<ul>
			<li>Idowu Dauda Oladipo, University of Ilorin, Nigeria</li>
			<li>Joseph Bamidele Awotunde, University of Ilorin, Nigeria</li>
			<li>Muyideen Abdulraheem, University of Ilorin, Nigeria</li>
			<li>Oluwasegun Osemudiame Ige, University of Ilorin, Nigeria</li>
			<li>Ghaniyat Bolanle Balogun, University of Ilorin, Nigeria</li>
			<li>Adekola Rasheed Tomori, University of Ilorin, Nigeria</li>
			<li>Fatimoh Abidemi Taofeek-Ibrahim, Federal Polytechnic , Nigeria</li>
		</ul></li>

	<li><strong>Predictive academic performance model to support, prevent
			and decrease the university dropout rate </strong>
		<ul>
			<li>Diego Bustamante, Universidad de Bogota Jorge Tadeo Lozano,
				Colombia</li>
			<li>Olmer Garcia-Bedoya, Universidad de Bogota Jorge Tadeo Lozano,
				Colombia</li>
		</ul></li>


	<li><strong>Proposed algorithm to digitize the root-cause analysis and
			8Ds of the automotive industry </strong>
		<ul>
			<li>Brenda-Alejandra Mendez Torres, Universidad Autónoma del Estado
				de Puebla, Mexico</li>
			<li>Patricia Cano-Olivos, Universidad Autónoma del Estado de Puebla,
				Mexico</li>

		</ul></li>

</ul>

<h4>Health Care Information Systems</h4>
<ul>
	<li><strong>Blood Pressure Morphology as a Fingerprint of
			Cardiovascular Health: A Machine Learning Based Approach </strong>
		<ul>
			<li>Eugenia Ipar, Universidad Tecnológica Nacional , Argentina</li>
			<li>Nicolás A. Aguirre, Université de Technologie de Troyes, France</li>
			<li>Leandro J. Cymberknop, Universidad Tecnológica Nacional ,
				Argentina</li>
			<li>Ricardo L. Armentano, Universidad Tecnológica Nacional ,
				Argentina</li>

		</ul></li>

	<li><strong>Characterizing Musculoskeletal Disorders. A Case study
			involving kindergarten employees </strong>
		<ul>
			<li>Gonzalo Yepes Calderon,GYM GROUP, Colombia</li>
			<li>Julio Perea Sandoval, Universidad ECCI, Colombia</li>
			<li>Julietha Oviedo Correa, Universidad ECCI, Colombia</li>
			<li>Fredy Linero-Moreno, Jardin Infantil Eco Kids, Colombia</li>
			<li>Fernando Yepes-Calderon, Science Based Platforms, United States</li>
		</ul></li>

	<li><strong>Comparison of heart rate variability analysis with
			empirical mode decomposition and Fourier transform </strong>
		<ul>
			<li>Javier Zelechower, Universidad Tecnologica Nacional, Argentina</li>
			<li>Fernando Pose, Hospital Italiano de Buenos Aires, Argentina</li>
			<li>Francisco Redelico, Hospital Italiano de Buenos Aires, Argentina</li>
			<li>Marcelo Risk, Hospital Italiano de Buenos Aires, Argentina</li>
		</ul></li>

	<li><strong>Evalu@ + Sports. Creatine Phosphokinase and Urea in
			High-Performance Athletes During Competition. A framework for
			predicting injuries caused by fatigue </strong>
		<ul>
			<li>Fernando Yepes-Calderon, Science Based Platforms LLC, United
				States</li>
			<li>Alvin David Gregory Tatis, Universidad de los Andes, Colombia</li>
			<li>Daniel Santiafo Forero Arévalo, Universidad de Los Andes,
				Colombia</li>
			<li>Juan Fernando Yepes Zuluaga, Science Based Platforms LLC, United
				States</li>
		</ul></li>

	<li><strong>Heart rate variability: validity of autonomic balance
			indicators in ultra-short recordings </strong>
		<ul>
			<li>Jose Gallardo, Universidad Tecnologica Nacional, Argentina</li>
			<li>Giannina Bellone, Catholic University of Argentina, Argentina</li>
			<li>Marcelo Risk, Universidad Tecnológica Nacional, Argentina</li>
		</ul></li>


</ul>

<h4>Image Processing</h4>
<ul>
	<li><strong>An Improved Machine Learnings Diagnosis Technique for
			COVID-19 Pandemic using Chest X-ray Images </strong>
		<ul>
			<li>Joseph Bamidele Awotunde, University of Ilorin, Nigeria</li>
			<li>Sunday Adeola Ajagbe, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Matthew A. Oladipupo, University of Salford, United Kingdom</li>
			<li>Jimmisayo A. Awokola, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Olakunle S. Afolabi, University of Abuja, Nigeria</li>
			<li>Mathew O. Timothy, Federal Polytechnic, Ilaro, Nigeria</li>
			<li>Yetunde J. Oguns, The Polytechnic Ibadan, Nigeria</li>
		</ul></li>

	<li><strong>Evaluation of local thresholding algorithms for
			segmentation of white matter hyperintensities in magnetic resonance
			images of the brain </strong>
		<ul>
			<li>Adam Piórkowski, AGH University Of Science and Technology, Poland</li>
			<li>Julia Lasek, AGH University Of Science and Technology, Poland</li>
		</ul></li>

	<li><strong>Super-resolution algorithm applied in the zoning of aerial
			images </strong>
		<ul>
			<li>J. A. Baldion, Universidad de La Salle, Colombia</li>
			<li>E. Cascavita, Universidad de La Salle, Colombia</li>
			<li>C. H. Rodriguez-Garavito, Universidad de La Salle, Colombia</li>

		</ul></li>
</ul>

<h4>Security Services</h4>
<ul>
	<li><strong>An Enhanced Lightweight Speck System for Cloud-based Smart
			Healthcare </strong>
		<ul>
			<li>Muyideen Abdulraheem, University of Ilorin, Nigeria</li>
			<li>Ghaniyat Bolanle Balogun, University of Ilorin, Nigeria</li>
			<li>Kazeem Moses Abiodun, Landmark University, Nigeria</li>
			<li>Fatimoh Abidemi Taofeek-Ibrahim, Federal Polytechnic, Nigeria</li>
			<li>Adekola Rasheed Tomori, University of Ilorin, Nigeria</li>
			<li>Idowu Dauda Oladipo, University of Ilorin, Nigeria</li>
			<li>Joseph Bamidele Awotunde, University of Ilorin, Nigeria</li>
		</ul></li>
	<li><strong>Hybrid Algorithm for Symmetric Based Fully Homomorphic
			Encryption </strong>
		<ul>
			<li>Kamaldeen Jimoh Muhammed, University of Ilorin, Nigeria</li>
			<li>Rafiu Mope Isiaka, Kwara State University, Nigeria</li>
			<li>Ayisat Wuraola Asaju-Gbolagade, University of Ilorin, Nigeria</li>
			<li>Kayode Sakariyah Adewole, University of Ilorin, Nigeria</li>
			<li>Kazeem Alagbe Gbolagade, Kwara State University, Nigeria</li>

		</ul></li>

	<li><strong>Information encryption and decryption analysis,
			vulnerabilities and reliability implementing the RSA algorithm in
			Python </strong>
		<ul>
			<li>Rocio Rodriguez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
			<li>Carlos A. Vanegas, Universidad Distrital Francisco Jose de
				Caldas, Colombia</li>
			<li>Gerardo A. Castang, Universidad Distrital Francisco Jose de
				Caldas, Colombia</li>

		</ul></li>
</ul>

<h4>Simulation and Emulation</h4>
<ul>
	<li><strong>Optimization of Multi-Level HEED protocol in wireless
			sensor networks </strong>
		<ul>
			<li>Walid Boudhiafi, University of Tunis, Tunisia</li>
			<li>Tahar Ezzedine, University of Tunis, Tunisia</li>
		</ul></li>
</ul>

<h4>Smart Cities</h4>
<ul>
	<li><strong> LIDAR and Camera Data for Smart Urban Traffic Monitoring:
			Challenges of Automated Data Capturing and Synchronization </strong>
		<ul>
			<li>Gatis Vitols, WeAreDots Ltd., Latvia</li>
			<li>Nikolajs Bumanis, Latvia University of Life Sciences and
				Technologies, Latvia</li>
			<li>Irina Arhipova, WeAreDots Ltd., Latvia</li>
			<li>Inga Meirane, WeAreDots Ltd., Latvia</li>
		</ul></li>

	<li><strong> Mobility in Smart Cities: Spatiality of the travel time
			indicator according to uses and modes of transportation </strong>
		<ul>
			<li>Carlos Alberto Diaz Riveros, Corporación Universitaria del Meta,
				Colombia</li>
			<li>Karen Astrid Beltran Rodriguez, Corporación Universitaria del
				Meta, Colombia</li>
			<li>Cesar O. Diaz, Universidad ECCI, Colombia</li>
			<li>Alejandra Juliette Baena Vasquez, Universidad Antonio Nariño,
				Colombia</li>
		</ul></li>

	<li><strong>Preliminary studies of the security of the cyber-physical
			smart grids </strong>
		<ul>
			<li>Luis Rabelo, University of Central Florida, United States</li>
			<li>Andres Ballestas,Universidad de La Sabana , Colombia</li>
			<li>Bibi Ibrahim, University of Central Florida, United States</li>
			<li>Javier Valdez, University of Central Florida, United States</li>
		</ul></li>
</ul>

<h4>Software and Systems Modeling</h4>
<ul>
	<li><strong>Enterprise Modeling: A Multi-Perspective Tool-Supported
			Approach </strong>
		<ul>
			<li>Paola Lara Machado, Technical University of Eindhoven,
				Netherlands</li>
			<li>Mario Sánchez, Universidad de los Andes, Colombia</li>
			<li>Jorge Villalobos, Universidad de los Andes, Colombia</li>

		</ul></li>
</ul>

<h4>Software Design Engineering</h4>
<ul>
	<li><strong>Architectural Approach for Google Services Integration </strong>
		<ul>
			<li>Hamilton Ricaurte, Universidad Distrital Francisco Jose de
				Caldas, Colombia</li>
			<li>Hector Florez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>

		</ul></li>
	<li><strong> Proposal to improve Software Testing in Small and Medium
			Enterprises </strong>
		<ul>
			<li>Melisa Argüello, Universidad Tecnológica Nacional, Argentina</li>
			<li>Carlos Antonio Casanova Pietroboni, Universidad Tecnológica
				Nacional, Argentina</li>
			<li>Karina Elizabeth Cedaro, Universidad Tecnológica Nacional,
				Argentina</li>

		</ul></li>
</ul>