<?php
class Author{
    public $name;
    public $index;
    public $urlScopus;
    public $conferences = array();
    
}
class Conference{
    public $name;
    public $papers = array();
}
class Paper{
    public $title;
    public $url;
}
$file = fopen("docs/prominentAuthors.csv", "r");
$date = explode(";", trim(fgets($file)))[1];
fgets($file);
$authors = array();
$author = new Author();
while(!feof($file)) {
    $data = explode(";", trim(fgets($file)));
    if($data[0] != $author -> name){
        $author = new Author();
        $conference = new Conference();
        array_push($authors, $author);
        $author -> name = $data[0];
        $author -> index = $data[1];
        $author -> urlScopus = $data[2];
        if($data[4] != $conference -> name){
            $conference = new Conference();
            array_push($author -> conferences, $conference);
            $conference -> name = $data[4];
            $paper = new Paper();
            array_push($conference -> papers, $paper);
            $paper -> title = $data[5];
            $paper -> url = $data[6];            
        }else{
            $paper = new Paper();
            array_push($conference -> papers, $paper);
            $paper -> title = $data[5];
            $paper -> url = $data[6];            
        }
    }else{
        if($data[4] != $conference -> name){
            $conference = new Conference();
            array_push($author -> conferences, $conference);
            $conference -> name = $data[4];
            $paper = new Paper();
            array_push($conference -> papers, $paper);
            $paper -> title = $data[5];
            $paper -> url = $data[6];
        }else{
            $paper = new Paper();
            array_push($conference -> papers, $paper);
            $paper -> title = $data[5];
            $paper -> url = $data[6];            
        }
    }
}
?>
<h3>Prominent Authors</h3>
<h6>Last Update: <?php echo $date ?></h6>
<table class="table table-responsive table-striped table-hover">
	<tr class="table-primary">
		<th class="text-center">#</th>
		<th class="text-center">Author</th>
		<th class="text-center" nowrap>h-Index</th>
		<th class="text-center">URL</th>
		<th class="text-center">Published Papers</th>		
	</tr>
<?php 
$i = 1;
foreach ($authors as $currentAuthor){
    echo "<tr>";
    echo "<td class='text-center align-middle'>" . $i++ . "</td>";
    echo "<td nowrap class='align-middle'><strong>" . $currentAuthor -> name . "</strong></td>";
    echo "<td class='text-center align-middle'><strong>" . $currentAuthor -> index . "</strong></td>";
    echo "<td class='text-center align-middle'><a href='" . $currentAuthor -> urlScopus . "' target='_blank'><span class='fas fa-external-link-alt'></span></a></td>";
    echo "<td class='align-middle'>";
    echo "<ul>";
    foreach ($currentAuthor -> conferences as $currentConference){
        echo "<li>" . $currentConference -> name . "</li>";
        echo "<ul>";
        foreach ($currentConference -> papers as $currentPaper){            
            echo "<li><a href='" . $currentPaper -> url . "' target='_blank'>" . $currentPaper -> title . "</a></li>";
        }        
        echo "</ul>";
    }
    echo "</ul>";
    echo "</td>";
    echo "</tr>";
}
?>
</table>





