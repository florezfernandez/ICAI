<h3>Best Papers</h3>
<ul>
	<li><h4>2024</h4>
		<ul>
			<li>Eduardo Puraivan, Irene Renau, Nicolás Riquelme. <strong>Metaphor
					Identification and Interpretation in Corpora with ChatGPT <a
					href="https://link.springer.com/article/10.1007/s42979-024-03331-0"
					target="_blank"><span class="fas fa-external-link-alt"
						aria-hidden="true"></span></a>.
			</strong> Universidad Viña del Mar, Chile
			</li>
		</ul></li>
	<li><h4>2023</h4>
		<ul>
			<li>Juan Sanguino, Ruben Manrique, Olga Mariño. <strong>A Semantic
					Enhanced Course Recommender System via Knowledge Graphs for Limited
					User Information Scenarios <a
					href="https://link.springer.com/article/10.1007/s42979-023-02399-4"
					target="_blank"><span class="fas fa-external-link-alt"
						aria-hidden="true"></span></a>.
			</strong> Universidad de los Andes, Colombia
			</li>
		</ul></li>
	<li><h4>2022</h4>
		<ul>
			<li>Valeria Laynes Fiascunari, Luis Rabelo. <strong>Preliminary Study
					for Impact of Social Media Networks on Traffic Prediction <a
					href="https://link.springer.com/chapter/10.1007/978-3-031-19647-8_15"
					target="_blank"><span class="fas fa-external-link-alt"
						aria-hidden="true"></span></a>.
			</strong> University of Central Florida, United States
			</li>
		</ul></li>
	<li><h4>2021</h4>
		<ul>
			<li>J. A. Baldion, E. Cascavita, C. H. Rodriguez-Garavito. <strong>
					Super-Resolution Algorithm Applied in the Zoning of Aerial Images <a
					href="https://link.springer.com/chapter/10.1007/978-3-030-89654-6_25"
					target="_blank"><span class="fas fa-external-link-alt"
						aria-hidden="true"></span></a>.
			</strong>Universidad La Salle, Colombia
			</li>
		</ul></li>
	<li><h4>2020</h4>
		<ul>
			<li>O. O. Mosobalaje, O. D. Orodu, D. Ogbe. <strong> The Application
					of DBSCAN Algorithm to Improve Variogram Estimation and
					Interpretation in Irregularly-Sampled Fields <a
					href="https://link.springer.com/chapter/10.1007/978-3-030-61702-8_20"
					target="_blank"><span class="fas fa-external-link-alt"
						aria-hidden="true"></span></a>.
			</strong>Covenant University, Nigeria and African University of
				Science and Technology, Nigeria
			</li>
		</ul></li>
	<li><h4>2019</h4>
		<ul>
			<li>Aryobarzan Atashpendar, Christian Grévisse, Steffen Rothkugel. <strong>
					Enhanced Sketchnoting Through Semantic Integration of Learning
					Material <a
					href="https://link.springer.com/chapter/10.1007/978-3-030-32475-9_25"
					target="_blank"><span class="fas fa-external-link-alt"
						aria-hidden="true"></span></a>.
			</strong>University of Luxembourg, Luxembourg
			</li>
		</ul></li>
	<li><h4>2018</h4>
		<ul>
			<li>Jinli Zhang, Zongli Jiang, Tong Li. <strong> CHIN: Classification
					with META-PATH in Heterogeneous Information Networks <a
					href="https://link.springer.com/chapter/10.1007/978-3-030-01535-0_5"
					target="_blank"><span class="fas fa-external-link-alt"
						aria-hidden="true"></span></a>.
			</strong>Beijing University of Technology, China
			</li>
		</ul></li>
</ul>

<h3>Best Reviewers</h3>

<ul>
	<li><h4>2024</h4>
		<ul>
			<li>José Rufino, Instituto Politécnico de Bragança, Portugal</li>
			<li>Jaime Chavarriaga, Pontificia Universidad Javeriana, Colombia</li>
			<li>Carlos Montenegro, Universidad Distrital Francisco Jose de
				Caldas, Colombia</li>
			<li>Daniel Görlich, Hochschule Offenburg, Germany</li>
			<li>Fernanda Almeida, Universidade Federal do ABC, Brazil</li>
			<li>Fernando Yepes-Calderon, Children's Hospital Los Angeles, United
				States</li>
			<li>Diego Peluffo-Ordóñez, Université Mohammed VI Polytechnic,
				Morocco</li>
			<li>German Vega, Centre National de la Recherche Scientifique, France</li>
			<li>Victor Darriba, Universidade de Vigo, Spain</li>
			<li>Filipe Portela, Universidade do Minho, Portugal</li>
		</ul></li>
	<li><h4>2023</h4>
		<ul>
			<li>José Rufino, Instituto Politécnico de Bragança, Portugal</li>
			<li>Silvia Fajardo-Flores, Universidad de Colima, Mexico</li>
			<li>Tamara Piñero, Hospital Italiano de Buenos Aires, Argentina</li>
			<li>Alexander Bock, Universität Duisburg Essen, Germany</li>
			<li>Jaime Chavarriaga, Universidad de los Andes, Colombia</li>
			<li>Fernando Yepes-Calderon, Children's Hospital Los Angeles, United
				States</li>
			<li>Christian Grévisse, Université du Luxembourg, Luxembourg</li>
			<li>Raymundo Buenrostro, Universidad de Colima, Mexico</li>
			<li>Joseph Bamidele Awotunde, University of Ilorin, Nigeria</li>
			<li>German Vega, Centre National de la Recherche Scientifique, France</li>
		</ul></li>

	<li><h4>2022</h4>
		<ul>
			<li>Victor Darriba, Universidade de Vigo, Spain</li>
			<li>Fernando Yepes-Calderon, Children's Hospital Los Angeles, United
				States</li>
			<li>Alber Sanchez, Instituto Nacional de Pesquisas Espaciais, Brazil</li>
			<li>Jorge Bacca, Fundacion Universitaria Konrad Lorenz, Colombia</li>
			<li>Jens Gulden, Universiteit Utrecht, Netherlands</li>
			<li>Alexander Bock, Universität Duisburg Essen, Germany</li>
			<li>Ivan Mura, Duke Kunshan University, China</li>
			<li>Filipe Portela, Universidade do Minho, Portugal</li>
			<li>Diego Peluffo-Ordóñez, Université Mohammed VI Polytechnic,
				Morocco</li>
			<li>Christian Grévisse, Université du Luxembourg, Luxembourg</li>
		</ul></li>

	<li><h4>2021</h4>
		<ul>
			<li>Victor Darriba, Universidade de Vigo, Spain</li>
			<li>Alexander Bock, Universität Duisburg Essen, Germany</li>
			<li>Jens Gulden, Universiteit Utrecht, Netherlands</li>
			<li>Andrei Tchernykh, Centro de Investigación Científica y de
				Educación Superior de Ensenada, Mexico</li>
			<li>José Rufino, Instituto Politécnico de Bragança, Portugal</li>
			<li>Christian Grévisse, Université du Luxembourg, Luxembourg</li>
			<li>German Vega, Centre National de la Recherche Scientifique, France</li>
			<li>Ivan Mura, Duke Kunshan University, China</li>
			<li>Daniel Görlich, SRH Hochschule Heidelberg, Germany</li>
			<li>Jorge Bacca, Fundacion Universitaria Konrad Lorenz, Colombia</li>

		</ul></li>
	<li><h4>2020</h4>
		<ul>
			<li>Jens Gulden, Universiteit Utrecht, Netherlands</li>
			<li>Jaime Chavarriaga, Universidad de los Andes, Colombia</li>
			<li>Victor Darriba, Universidade de Vigo, Spain</li>
			<li>German Vega, Centre National de la Recherche Scientifique, France</li>
			<li>José Rufino, Instituto Politécnico de Bragança, Portugal</li>
			<li>Sergio Minniti, Universita Degli Study di Padova, Italy</li>
			<li>Christian Grévisse, Université du Luxembourg, Luxembourg</li>
			<li>Horacio Hoyos, Rolls-Royce, United Kingdom</li>
			<li>Silvia Fajardo-Flores, Universidad de Colima, Mexico</li>
			<li>Manik Sharma, DAV University, India</li>
		</ul></li>
	<li><h4>2019</h4>
		<ul>
			<li>Jens Gulden, Universität Duisburg Essen, Germany</li>
			<li>Andrei Tchernykh, Centro de Investigación Científica y de
				Educación Superior de Ensenada, Mexico</li>
			<li>Jorge Bacca, Fundacion Universitaria Konrad Lorenz, Colombia</li>
			<li>Jaime Chavarriaga, Universidad de los Andes, Colombia</li>
			<li>Simona Safarikova, Univerzita Palackého v Olomouci, Czech
				Republic</li>
			<li>German Vega, Centre National de la Recherche Scientifique, France</li>
			<li>Jose Maria Diaz, Universidad a Distancia de Madrid, Spain</li>
			<li>Oscar Avila, Universidad de los Andes, Colombia</li>
			<li>Manuel Vilares, Universidade de Vigo, Spain</li>
			<li>Dominic Bork, Universität Wien, Austria</li>
		</ul></li>
	<li><h4>2018</h4>
		<ul>
			<li>Jens Gulden, Universität Duisburg Essen, Germany</li>
			<li>Efraín Fonseca, Universidad de las Fuerzas Armadas ESPE, Ecuador</li>
			<li>Thomas Luft, Lehrstuhl für Konstruktionstechnik, Germany</li>
			<li>Erol Chioasca, The University of Manchester, United Kingdom</li>
			<li>Matias Gerard, Universidad Nacional del Litoral, Argentina</li>
			<li>Mario Sanchez, Universidad de los Andes, Colombia</li>
			<li>Osval Montesinos, Universidad de Colima, Mexico</li>
			<li>Vicente Perez, Fundación para el Fomento de la Investigación
				Sanitaria y Biomédica de la Comunidad Valenciana, Spain</li>
			<li>Xavier Oriol, Universitat Politècnica de Catalunya, Spain</li>
			<li>Jorge Bacca, Fundacion Universitaria Konrad Lorenz, Colombia</li>
		</ul></li>
</ul>
