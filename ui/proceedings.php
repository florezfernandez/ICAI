<h3>2024</h3>
<ul>	
	<li>ICAI Best Papers by Springer SN Computer Science: <a
		href="https://link.springer.com/journal/42979/topicalCollection/AC_8a9a6dfdc7835249b1a19f50e6e3630d"
		target="_blank">https://link.springer.com/journal/42979/topicalCollection/AC_8a9a6dfdc7835249b1a19f50e6e3630d</a>.
	<li>ICAI by Springer CCIS Volume 2236: <a
		href="https://link.springer.com/book/10.1007/978-3-031-75144-8"
		target="_blank">https://link.springer.com/book/10.1007/978-3-031-75144-8</a>.
	</li>
	<li>ICAI by Springer CCIS Volume 2237: <a
		href="https://link.springer.com/book/10.1007/978-3-031-75147-9"
		target="_blank">https://link.springer.com/book/10.1007/978-3-031-75147-9</a>.
	</li>
	<li>ICAI Workshops by CEUR-WS Volume 3795: <a
		href="https://ceur-ws.org/Vol-3795" target="_blank">https://ceur-ws.org/Vol-3795</a>.
	</li>
</ul>
<h3>2023</h3>
<ul>
	<li>ICAI Best Papers by Springer SN Computer Science: <a
		href="https://link.springer.com/journal/42979/topicalCollection/AC_a45b233b3269d18b349668e1c9a1b127"
		target="_blank">https://link.springer.com/journal/42979/topicalCollection/AC_a45b233b3269d18b349668e1c9a1b127</a>.
	<li>ICAI by Springer CCIS Volume 1874: <a
		href="https://link.springer.com/book/10.1007/978-3-031-46813-1"
		target="_blank">https://link.springer.com/book/10.1007/978-3-031-46813-1</a>.
	</li>
	<li>ICAI Workshops by CEUR-WS Volume 3520: <a
		href="https://ceur-ws.org/Vol-3520" target="_blank">https://ceur-ws.org/Vol-3520</a>.
	</li>
</ul>
<h3>2022</h3>
<ul>
	<li>ICAI by Springer CCIS Volume 1643: <a
		href="https://link.springer.com/book/10.1007/978-3-031-19647-8"
		target="_blank">https://link.springer.com/book/10.1007/978-3-031-19647-8</a>.
	</li>
	<li>ICAI Workshops by CEUR-WS Volume 3282: <a
		href="https://ceur-ws.org/Vol-3282" target="_blank">https://ceur-ws.org/Vol-3282</a>.
	</li>
</ul>
<h3>2021</h3>
<ul>
	<li>ICAI by Springer CCIS Volume 1455: <a
		href="https://link.springer.com/book/10.1007/978-3-030-89654-6"
		target="_blank">https://link.springer.com/book/10.1007/978-3-030-89654-6</a>.
	</li>
	<li>ICAI Workshops by CEUR-WS Volume 2992: <a
		href="http://ceur-ws.org/Vol-2992" target="_blank">http://ceur-ws.org/Vol-2992</a>.
	</li>
</ul>

<h3>2020</h3>
<ul>
	<li>ICAI by Springer CCIS Volume 1277: <a
		href="https://link.springer.com/book/10.1007/978-3-030-61702-8"
		target="_blank">https://link.springer.com/book/10.1007/978-3-030-61702-8</a>.
	</li>
	<li>ICAI Workshops by CEUR-WS Volume 2714: <a
		href="http://ceur-ws.org/Vol-2714" target="_blank">http://ceur-ws.org/Vol-2714</a>.
	</li>
</ul>

<h3>2019</h3>
<ul>
	<li>ICAI by Springer CCIS Volume 1051: <a
		href="https://link.springer.com/book/10.1007/978-3-030-32475-9"
		target="_blank">https://link.springer.com/book/10.1007/978-3-030-32475-9</a>.
	</li>
	<li>ICAI Workshops by CEUR-WS Volume 2486: <a
		href="http://ceur-ws.org/Vol-2486" target="_blank">http://ceur-ws.org/Vol-2486</a>.
	</li>
</ul>

<h3>2018</h3>
<ul>
	<li>ICAI by Springer CCIS Volume 942: <a
		href="https://link.springer.com/book/10.1007/978-3-030-01535-0"
		target="_blank">https://link.springer.com/book/10.1007/978-3-030-01535-0</a>.
	</li>
	<li>ICAI Workshops by IEEE: <a
		href="https://ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=8542640"
		target="_blank">https://ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=8542640</a>.
	</li>
</ul>
