<h3>Contact</h3>
<?php
if (isset($_POST['contact'])) {
    if (empty($_POST['g-recaptcha-response'])) {
        echo "<div class='alert alert-danger' role='alert'><p>Please fill the captcha</p></div>";
    } else {
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $data = [
            "secret" => "6LcnDnobAAAAACmU7dJW3WeeeEBJQWyUDCDeEOiI",
            "response" => $_POST['g-recaptcha-response']
        ];
        $options = array(
            "http" => array(
                "header" => "Content-type: application/x-www-form-urlencoded\r\n",
                "method" => "POST",
                "content" => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $result = json_decode($result, true);
        if ($result["success"]) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $to = "Hector Florez <haflorezf@udistrital.edu.co>";
            $subject = "Message from ICAI Contact Form";
            $header = array(
                "From" => "ICAI Contact <contact@itiud.org>",
                "Reply-To" => $name . " <" . $email . ">"
            );
            mail($to, $subject, $message, $header);
            $to = $name . " <" . $email . ">";
            $header = "FROM: ICAI Contact <contact@itiud.org>";
            $message = "Dear " . $name . "\r\n\r\nThank you for contacting ICAI \r\nWe will contact you as soon as possible.\r\nYour message is:\r\n---\r\n" . $message;
            mail($to, $subject, $message, $header);
            echo "<div class='alert alert-success' role='alert'><p>Dear " . $name . "<br> Your message has been sent to the general chair of ICAI.<br>You will receive a copy of this message. <i>You might receive the copy of the message in the spam folder</i></p></div>";
        } else {
            echo "<div class='alert alert-danger' role='alert'><p>Sorry. You look like a robot. The message was not sent.</p></div>";
        }
    }
} else {
    ?>
<script src="https://www.google.com/recaptcha/api.js"></script>
<div class="row">
	<div class="col-md-5">
		<form id="form" role="form" method="post"
			action="index.php?pid=contact">
			<input type="hidden" name="contact" value="contact" />
			<div class="form-group">
				<label>Name</label>
				<div class="input-group">
					<input type="text" class="form-control" name="name" id="name"
						required />
				</div>
				<label>Email</label>
				<div class="input-group" data-validate="email">
					<input type="email" class="form-control" name="email" id="email"
						required />
				</div>
				<label>Message</label>
				<div class="input-group">
					<textarea name="message" id="message" rows="10" cols="50" required></textarea>
				</div>
				<br>
				<div class="g-recaptcha"
					data-sitekey="6LcnDnobAAAAACSZDDiFL6HxqOjUpk0pNrWQi0E3"></div>
				<br> <input type="submit" class="btn btn-primary"
					value="Send Message">
			</div>
		</form>
	</div>
</div>
<?php } ?>