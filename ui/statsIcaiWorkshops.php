<div class="row mt-3">
	<div class="col-lg-4"></div>
	<div class="col-lg-4 text-center">
		<div class="form-group">
			<div class="input-group mb-3">
				<label class="input-group-text" for="inputGroupSelect01">Edition</label>
				<select class=form-select id="edition">
					<option value="-1">Select edition</option>
					<option value="7">2024</option>
					<option value="6">2023</option>
					<option value="5">2022</option>
					<option value="4">2021</option>
					<option value="3">2020</option>
					<option value="2">2019</option>
					<option value="1">2018</option>
				</select>

			</div>
		</div>
	</div>	
</div>
<div id="result"></div>
<script>
$(document).ready(function(){
	$("#edition").change(function(){		
		if($("#edition").val()!=-1){
			$("#result").html("<div class='text-center'><img src='img/loading.gif'></div>");
			var edition = $("#edition").val();
			var path = "stats.php?t=icaiw&e="+edition;			
			$("#result").load(path);
		}else{
			$("#result").html("");
		}
	});
});
</script>	