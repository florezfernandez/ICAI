<br>
<h3>Program</h3>
<div class="alert alert-success" role="alert">
	<strong>The program is fixed to Chilean Local time (GMT-3)</strong>
</div>
<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="30%" class="text-center">Thursday 24 October</th>
		<th width="30%" class="text-center">Friday 25 October</th>
		<th width="30%" class="text-center">Saturday 26 October</th>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td class="table-danger"><strong>Conference Opening</strong><br> <i>Room:
				<strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
		<td></td>
		<td class="table-danger" rowspan="4"><strong>Touristic Activity</strong><br>
			<i>Location: <strong>Campus</strong></i><br> <i>Price: <strong>30.000
					CLP => 31 USD aprox</strong><br>(Includes touristic places fees)</i><br> <i>Registration: <strong><a
					href="https://forms.gle/WYyVsjbR6aMLcfjF9" target="_blank">Form</a></strong></i>
			<ul>
				<li>Quinta Vergara</li>
				<li>Marina Avenue</li>
				<li>Flower Clock</li>
				<li>Spain Avenue</li>
				<li>Great Britain Avenue</li>
				<li>Sotomayor Square</li>
				<li>Bus tour Wall Street Porteño</li>
				<li>Concepción Elevator</li>
				<li>Paseo Gervasoni, Pasaje Gálvez, Paseo Atkinson, Escalera Piano,
					calle Almirante Montt, Pasaje Dimalow</li>
				<li>Reina Victoria Elevator</li>
				<li>Anibal Pinto Square</li>
			</ul></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success"><strong>Keynote: Christian Grevisse</strong><br>
			<i>Room: <strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i>
		
		<td class="table-success"><strong>Keynote: Sven von Brand</strong><br>
			<i>Room: <strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>

	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-warning"><strong>Workshop AIESD</strong><br> <i>Room:
				<strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i><br> <br> <strong>Workshops WAAI &amp; WITS</strong><br> <i>Room:
				<strong>E127</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjFkMWVlZWYtYzVlMy00Nzc1LTlhOTktZjRlZjgzYmM5NWM2%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i><br> <br> <strong>Workshops WSEAI</strong><br> <i>Room: <strong>E128</strong><br>Virtual
				Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjYxODk2ZDMtZDE2Yi00OGRiLThkNjktOGNjZjkwODkxZWRi%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
		<td class="table-info"><strong>Artificial Intelligence (2) </strong><br>
			<i>Room: <strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i><br> <br> <strong>Game Development &amp; <br> Learning Management
				Systems
		</strong><br> <i>Room: <strong>E127</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjFkMWVlZWYtYzVlMy00Nzc1LTlhOTktZjRlZjgzYmM5NWM2%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-warning"><strong>Workshops WKMIT &amp; SCTSD &amp;
				WSM</strong><br> <i>Room: <strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i><br> <br> <strong>Workshop WDEA</strong><br> <i>Room: <strong>E127</strong><br>Virtual
				Room <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjFkMWVlZWYtYzVlMy00Nzc1LTlhOTktZjRlZjgzYmM5NWM2%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
		<td class="table-info"><strong>Decision Systems &amp; <br> Natural
				Language Processing
		</strong><br> <i>Room: <strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i><br> <br> <strong>Cloud Computing &amp; <br> Software
				Architectures
		</strong><br> <i>Room: <strong>E127</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjFkMWVlZWYtYzVlMy00Nzc1LTlhOTktZjRlZjgzYmM5NWM2%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info"><strong>Artificial Intelligence (1)</strong> <br>
			<i>Room: <strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i> <br> <br> <strong>Interdisciplinary Information Studies</strong>
			<br> <i>Room: <strong>E127</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjFkMWVlZWYtYzVlMy00Nzc1LTlhOTktZjRlZjgzYmM5NWM2%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i><br> <br> <strong>Data Analysis</strong> <br> <i>Room: <strong>E128</strong><br>Virtual
				Room <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjYxODk2ZDMtZDE2Yi00OGRiLThkNjktOGNjZjkwODkxZWRi%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
		<td class="table-info"><strong>Bioinformatics &amp; <br>Health Care
				Information Systems
		</strong> <br> <i>Room: <strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_OGYxODFkM2QtOWExOS00ZDBkLTk0MDEtYzUxNjZjZjZiOWU1%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i> <br> <br> <strong>Social and Behavioral Applications &amp; <br>Software
				and Systems Modeling
		</strong> <br> <i>Room: <strong>E127</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjYxODk2ZDMtZDE2Yi00OGRiLThkNjktOGNjZjkwODkxZWRi%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
	</tr>
	<tr>
		<td>18:00-19:00</td>
		<td class="table-danger"><strong>Cocktail and Cultural Activity</strong><br>
			<i>Room: <strong>E126</strong>
		</i></td>
		<td class="table-danger"><strong>Conference Closing</strong><br> <i>Room:
				<strong>E126</strong><br>Virtual Room: <strong><a
					href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YjYxODk2ZDMtZDE2Yi00OGRiLThkNjktOGNjZjkwODkxZWRi%40thread.v2/0?context=%7b%22Tid%22%3a%22d51388ef-6ab0-4363-9f94-d56644a45970%22%2c%22Oid%22%3a%221b99ab08-a68b-4567-a6b5-7557bae7a575%22%7d">Teams</a></strong>
		</i></td>
	</tr>

</table>

<hr>

<h3>Detailed Program</h3>
<div class="alert alert-success" role="alert">
	<strong>At least one author must give an oral presentation. Each
		presentation must last up to 15 minutes, with 5 minutes for questions.</strong>
</div>
<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="90%" class="text-center">Thursday 24 October</th>
	</tr>
	<tr>
		<td>09:00-10:00</td>
		<td class="table-danger"><strong>Conference Opening</strong></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success"><strong>Keynote: Christian Grevisse. <br>Title:
				<i>Technology Enhanced Learning: From Computer Science to Simulation
					in Healthcare</i>
		</strong><br> Session Chair: <strong>Hector Florez</strong></td>

	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-warning"><strong>Workshop AIESD </strong><br> Session
			Chair: <strong>Marcelo Leon</strong>
			<ul>
				<li><strong>Analysis of the relationship between productivity and
						the work environment in Ecuador by 2022 </strong><br> René Faruk
					Garzozi-Pincay, Wendy Elizabeth Wasbrum-Tinoco, Yamel Sofia
					Garzozi-Pincay, Wilfrido Giovanny Wasbrum-Tinoco, Víctor Manuel
					Solórzano-Méndez, Sandy Elizabeth De-La-A-Muñoz</li>
				<li><strong>Complex networks as a methodology for social studies.
						Case: Son de Amores </strong> <br>Carlos Redroban, Luis
					Rivas-Tovar, Augusto Pino, Lissete Reyes</li>
				<li><strong>Evaluation by an Expert Panel of Agroecological
						Practices in Olive Groves from an Environmental, economic, and
						Technical Feasibility Perspective </strong> <br>Arnaldo
					Vergara-Romero, Emilio González-Sánchez, Nazaret M. Montilla-López,
					Manuel Arriaza</li>
				<li><strong>Export destination and firm performance: the case of
						Ecuador and the United States </strong> <br>Hernan Valverde
					Ordoñez, Mary Armijos Yambay, Marcelo Leon, Sara Correa Soto</li>
				<li><strong>Georeferencing of Savings Banks And their role in
						Financial Inclusion in the Salinas canton </strong> <br>Jacqueline
					Bacilio Bejeguen, Rita Villamar Tumbaco</li>
				<li><strong>GIS application for groundwater information processing
						and sustainable use in rural communities </strong> <br>Paúl
					Carrión-Mero, Xavier Benalcazar, Nicole Ramos, Peter Olaya, Emily
					Sánchez-Zambrano, Joselyne Solorzano, María Jaya-Montalvo, Kevin
					Padilla-Cáceres, Alanis Jara-Vargas, Fernando Morante-Carballo</li>
				<li><strong>Media Competencies and Social Development: A systematic
						review </strong> <br>Jhon Ochoa, Erika Lucia Gonzalez-Carrion,
					Luisa Santin, Marcelo Leon</li>
				<li><strong>The Processes of Real Estate Expropriation and the Fair
						Price. Case of the Decentralized Autonomous Government of the
						Municipality of Ambato </strong> <br>Fanny Perez, Marcelo Leon,
					Vladimir Salazar-Gonzalez, Andrea Macias</li>
			</ul>

			<hr> <strong>Workshops WAAI &amp; WITS</strong><br> Session Chair: <strong>Maria
				F. Pollo-Cattaneo</strong>
			<ul>
				<li><strong>A Machine Learning-Based Clinical Decision Support
						System for Mental Health Risk Profiling </strong><br> Juan
					Francisco Paoli, Parag Chatterjee, María F. Pollo-Cattaneo</li>
				<li><strong>Integrated Thermal Monitoring System for Solar PV
						Panels: An Approach Based on TinyML and Edge Computing </strong> <br>Andrés
					David Suárez-Gómez, Jorge Orlando Bareño Quintero</li>
				<li><strong>SnapChef: AI-powered Recipe Suggestions </strong> <br>Santiago
					Ruiz-Rincón, Ixent Galpin</li>
				<li><strong>Traffic Forecasting in Bogota, Colombia, with Attention
						Temporal Graph Convolutional Networks (A3T-GCN) </strong> <br>Juan
					Andrés Bernal-Sánchez, Javier Riascos-Ochoa</li>
				<li><strong>Comparative Study of Tools for the Integration of Linked
						Open Data: Case study with Wikidata Proposal </strong> <br>Roxana
					Martinez, Gonzalo Pereyra Metnik</li>
			</ul>

			<hr> <strong>Workshops WSEAI</strong><br> Session Chair: <strong>Evilasio
				Costa Junior</strong>
			<ul>
				<li><strong>Analytical Model and Reference Architecture for
						QoL-based Systems </strong><br> Pedro A. M. Oliveira, Rossana M.
					C. Andrade, Pedro A. Santos Neto, Wilson Castro, Evilasio Costa
					Junior, Ismayle S. Santos, Victória T. Oliveira</li>
				<li><strong>FallReportAPI: Integration of Digital Health Systems for
						Fall Detection in Hospital Systems </strong> <br>Nadiana Kelly N.
					Mendes, Francisco Victor S. Pinheiro, Victória T. Oliveira,
					Francisco Laurindo C. Junior, Rafael L. Gomes, Rossana M. C.
					Andrade</li>
				<li><strong>IoT Solutions to Assist Patients With Diabetic Foot: A
						Systematic Mapping </strong> <br>Karla Haryanna S. Moura, Valéria
					Lelli, Fabiana G. Marinho</li>
				<li><strong>PARTNER: Development Platform for Self-Adaptive
						IoHTApplication Microservices </strong> <br>Evilasio Costa Junior,
					Francisco Victor da Silva Pinheiro, Rhenara Alves Oliveira, Ismayle
					de Sousa Santos, Rossana Maria de Castro Andrade</li>

			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-warning"><strong>Workshop WKMIT &amp; SCTSD &amp; WSM</strong><br>
			Session Chair: <strong>Luciano Straccia</strong>
			<ul>
				<li><strong>Analysis of the current state of knowledge management
						methods and techniques applied in Argentinian work cooperatives</strong><br>Federico
					Brest</li>
				<li><strong>Innovation and Knowledge Management (IKM) in Higher
						Education: A Crucial Bibliometric Analysis for the time frame
						1997- 2024 </strong> <br>Fezile Ozdamli, Cigdem Hursen, Erinc
					Ercag, Huseyin Bicen</li>
				<li><strong>Knowledge Management: Innovation, Technology and
						Ontologies </strong> <br>Luciano Straccia, María F. Pollo-Cattaneo</li>
				<li><strong>Study of the Diagnosis of Knowledge Management in
						Software Development Companies </strong> <br>Lautaro Ignacio
					Ferrer</li>
				<li><strong>Bibliometric Analysis Of Gastronomic Heritage: Evolution
						And Patterns For Tourims Destinations </strong> <br>Tannia
					Aguirre, Narcisa Vasquez, Homero Rodriguez, Rosa Apolo</li>
				<li><strong>Towards a System for Measuring Source Code Quality </strong>
					<br>Jorge Hernandez, Christian Unigarro, Hector Florez</li>
			</ul>
			<hr> <strong>Workshop WDEA</strong><br> Session Chair: <strong>Ixent
				Galpin</strong>
			<ul>
				<li><strong>A Markovian model for oil wells failure and production
						losses prediction in an oil field in Colombia </strong><br> Diana
					Katherine Gaviria-Olaya, Javier Riascos-Ochoa</li>
				<li><strong>An empirical assessment of discriminative deep learning
						models for multiclassification of COVID-19 X-ray </strong> <br>Sunday
					Adeola Ajagbe, Pragasen Mudali, Matthew Olusegun Adigun</li>
				<li><strong>Data-Driven Insights into Deforestation: Predictive
						Modeling in Colombian Regions </strong> <br>Alvaro Hernán
					Alarcón-López, Ixent Galpin</li>
				<li><strong>Enhancing Workplace Safety through Automated Personal
						Protective Equipment Detection </strong> <br>Juan Camilo Poveda
					Pinilla, Sofia Segura Muñoz, Jorge Ivan Romero Gelvez</li>
				<li><strong>Evaluating Reforestation Techniques in Arid Regions of
						Kenya and Tanzania through Remote Sensing </strong> <br>Yenny
					Paola Betancur-Torres, Ixent Galpin</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info"><strong>Artificial Intelligence (1) </strong><br>
			Session Chair: <strong>Ixent Galpin</strong><br>
			<ul>
				<li><strong>Artificial Intelligence-Based Quantification and
						Prognostic Assessment of CD3, CD8, CD146, and PDGF-Rβ Biomarkers
						in Sporadic Colorectal Cancer </strong><br> Florencia Adriana
					Lohmann, Martín Isac Specterman Zabala, Julieta Natalia Soarez,
					Maximiliano Dádamo, Mónica Alejandra Loresi, María de las Nieves
					Diaz, Walter Hernán Pavicic, Marcela Fabiana Bolontrade, Marcelo
					Raúl Risk, Juan Pablo Santino, Carlos Alberto Vaccaro, Tamara
					Alejandra Piñero</li>
				<li><strong>Automatic differentiation between coriander and parsley
						using MobileNetV2 </strong> <br>Ian Páez, José Arévalo, Mateo
					Martinez, Martin Molina, Robinson Guachi, D. H. Peluffo-Ordóñez,
					Lorena Guachi-Guachi</li>
				<li><strong>Automatic identification of forest areas in the
						”Carolina” Park using ResNet50, EfficientNetB0 and VGG16: A case
						study </strong> <br>Julian Guapaz, Juan Pablo Jervis, Diego Haro,
					Jefferson Padilla, Robinson Guachi, D. H. Peluffo-Ordóñez, Lorena
					Guachi-Guachi</li>
				<li><strong>Core Concept Identification in Educational Resources via
						Knowledge Graphs and Large Language Models </strong> <br>Daniel
					Reales, Rubén Manrique, Christian Grévisse</li>
				<li><strong>Deep Convolutional Neural Network for Autonomic Function
						Estimation in Intensive Care Patients </strong> <br>Javier
					Zelechower, Eduardo San Roman, Ivan Huespe, Valeria Burgos, Jose
					Gallardo, Francisco Redelico, Marcelo Risk</li>
				<li><strong>Deep Learning Techniques for Oral Cancer Detection:
						Enhancing Clinical Diagnosis by ResNet and DenseNet Performance </strong>
					<br>Pablo Ormeño-Arriagada, Eduardo Navarro, Carla Taramasco,
					Gustavo Gatica, Juan Pablo Vásconez</li>
			</ul>
			<hr> <strong>Data Analysis </strong><br> Session Chair: <strong>Hector
				Florez</strong><br>
			<ul>
				<li><strong>Application of Predictive Techniques for Startup
						Survival: The Ecuadorian Case </strong> <br>Marcos Espinoza,
					Alejandra Colina Vargas</li>
				<li><strong>Assessing Player Contributions in League of Legends
						Matches: An Analytical Approach </strong> <br>Cesar Diaz, Manuel
					Perez Parra, Pau Soler Valadés, Aitor Mier Pons</li>
				<li><strong>Improvement of Acute and Chronic Nutritional Status by
						supplying a Metabolic Biostimulator to Children in High Risk of
						Malnutrition. Introducing a Technological Platform to Enable
						Automatic Analyses and Reporting </strong> <br>Juan Sebastian
					Serrano, Fernando Yepes-Calderon</li>
				<li><strong>Relationship between demographic, geographical and
						access to health services factors </strong> <br>Jhonatan Ortega,
					Johanna Torres, Cesar O. Diaz, Fernando Soler</li>

				<li><strong>RGB Image Reconstruction for Precision Agriculture: A
						Systematic Literature Review </strong> <br>Christian Unigarro,
					Hector Florez</li>
			</ul>
			<hr> <strong>Interdisciplinary Information Studies </strong><br>
			Session Chair: <strong>Christian Grevisse</strong><br>
			<ul>
				<li><strong>Analyzing emotional and attentional responses to
						promotional images using a remote eye-tracker device and
						face-reading techniques </strong> <br>Mariana Gómez-Mejía,
					Guillermo Rodríguez-Martínez</li>
				<li><strong>Effect of Early Intervention on Students in a CS1
						Programming Course </strong> <br>Jose Miguel Llanos-Mosquera,
					Julian Andres Quimbayo-Castro, Edisney Garcia-Perdomo, Alvaro
					Hernan Alarcon-Lopez</li>
				<li><strong>Impact of face inversion on eye-tracking data quality: a
						study using the Tobii T-120 </strong> <br>Guillermo
					Rodríguez-Martínez</li>
				<li><strong>Implementation of the ISP (In System Programming) method
						for data recovery on mobile devices </strong> <br>Lidice Haz,
					Jenny Garzón Balcazar, Jaime Orozco Iguasnia, Carlos Sánchez</li>
				<li><strong>Test and validation of a corn grain cleaning and sorting
						machine with smart system integration for agricultural production
						in Cabanaconde – Peru </strong> <br>Bryan Antony Quinta Ccosi</li>
			</ul></td>
	</tr>
	<tr>
		<td>18:00-19:00</td>
		<td class="table-danger"><strong>Cocktail and Cultural Activity </strong></td>

	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="90%" class="text-center">Friday 25 October</th>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success"><strong>Keynote: Sven von Brand.<br>Title: <i>
					Empowering Game Design though Software Architecture in Tormented
					Souls a Case Study</i></strong><br> Session Chair: <strong>Hernan
				Astudillo</strong></td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-info"><strong>Artificial Intelligence (2) </strong><br>
			Session Chair: <strong>Hector Florez</strong><br>
			<ul>
				<li><strong>Intrusion Detection: A Comparison Study of Machine
						Learning Models Using Unbalanced Dataset </strong> <br>Sunday
					Ajagbe, Joseph Bamidele Awotunde, Hector Florez</li>
				<li><strong>Machine Learning Operations Applied to Development and
						Model Provisioning </strong> <br>Oscar Mendez, Jorge Camargo,
					Hector Florez</li>
				<li><strong>MultiMF: a Deep Multimodal Academic Resources
						Recommendation System </strong> <br>Rafael Tejón, Juan Sanguino,
					Ruben Manrique</li>
				<li><strong>NetScribed: A deep learning approach for machine-based
						melody transcription of audio files </strong> <br>Francois
					Volschenk, Dustin van Der Haar</li>
				<li><strong>Safeguarding Patient Data: Advanced Security in Wireless
						Body Area Networks and AI-Driven Healthcare Systems </strong> <br>Vinesh
					Thiruchelvam, Reshiwaran Jegatheswaran, Julia Binti Juremi</li>
				<li><strong>YOLO Convolutional Neural Network for Building Damage
						Detection in Hydrometeorological Disasters Using Satellite Imagery
				</strong> <br>César Luis Moreno González, Germán A. Montoya, Carlos
					Lozano Garzón</li>
			</ul>
			<hr> <strong> Game Development &amp; Learning Management Systems</strong><br>
			Session Chair: <strong>Christian Grévisse </strong><br>
			<ul>
				<li><strong>Code Legends: RPG Game as a Support Tool for Programming
						in High School </strong> <br>Alailson E. S. Gomes, Vitor Márcio D.
					Mota, Pedro Almir M. Oliveira</li>
				<li><strong>Development of mental agility mini-games in a video game
						for early detection of mild cognitive impairment: an innovative
						approach in mental health </strong> <br>Gustavo Adolfo Lemos
					Chang, María de Lourdes Díaz Carrillo, Manuel Osmany Ramírez Pírez</li>
				<li><strong>Aids to Navigation: Learning resource in seafaring
						practices and contribution to the safety of vessels in the
						Province of Santa Elena </strong> <br>Melannie Ortega, Rosalba
					Rodríguez, Jazmín Mena, Byron Albuja</li>
				<li><strong>RasPatient Pi: A Low-cost Customizable LLM-based Virtual
						Standardized Patient Simulator </strong> <br>Christian Grévisse</li>
				<li><strong>User Experience Insights from a Virtual Reality
						Application for Second Language Learners </strong> <br>Cristina
					Suarez-Pareja, Alix E. Rojas</li>
			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-info"><strong>Decision Systems &amp; Natural Language
				Processing</strong><br>Session Chair: <strong>Ixent Galpin</strong><br>
			<ul>
				<li><strong>Developing AI-Driven Cross-Platform Geolocation for
						Enhanced Strategic Decision-Making </strong> <br>Angel Fiallos</li>
				<li><strong>From Data to Decisions: Performance Evaluation of Retail
						Recommender Systems </strong> <br>Juan Alberto Blanco-Serrano,
					Ixent Galpin</li>
				<li><strong>Optimizing Fraud Detection in Traffic Accident Insurance
						Claims through AI Models: Strategies and Challenges </strong> <br>Luis
					Miguel Mora-Escobar, Ixent Galpin</li>
				<li><strong>Developing Nigeria Multilingual Languages Speech
						Datasets for Antenatal Orientation </strong> <br>Sunday Adeola
					Ajagbe</li>
				<li><strong>Statistical modeling of discourse genres: the case of
						the opinion column in Spanish </strong> <br>Rogelio Nazar</li>
				<li><strong>Metaphor identification and interpretation in corpora
						with ChatGPT </strong> <br>Eduardo Puraivan, Irene Renau, Nicolás
					Riquelme</li>
			</ul>
			<hr> <strong>Cloud Computing &amp; Software Architectures</strong><br>Session
			Chair: <strong>Maria F. Pollo-Cattaneo</strong><br>
			<ul>
				<li><strong>Migration from On-Premises to Cloud: Challenges and
						Opportunities </strong> <br>Rossana M. C. Andrade, Wilson Castro,
					Leonan Carneiro, Erik Bayerlein, Icaro S. de Oliveira, Pedro A. M.
					Oliveira, Ismayle S. Santos, Tales P. Nogueira, Victória T.
					Oliveira</li>
				<li><strong>A Study of Software Architects’ Cognitive Approaches:
						Kolb’s Learning Styles Inventory in Action </strong> <br>Mauricio
					Hidalgo, Hernán Astudillo, Laura M. Castro</li>
				<li><strong>Implementing Free TLS Certificates for Virtual Services:
						An Experimental Approach in Proxmox VE </strong> <br>Milton
					Escobar, Verónica Tintín, Raul Gallegos</li>
				<li><strong>NOTORIOUS: agNOsTic sOftware aRchItecture fOr aUtomated
						diagnoSis for GPONs </strong> <br>Ronaldo T. P. Milfont, Rubens A.
					S. Sousa, Rossana M. C. Andrade, Danilo R. Vasconcelos</li>
				<li><strong>Performance Analysis of MIMO-OFDM Systems in 5G Wireless
						Networks </strong> <br>Akande Hakeem Babalola, Oloyede Ayopo
					Abdulkarim, Shakirat Aderonke Salihu, Taibat O. Adebakin</li>
				<li><strong>SST: A Tool to Support the Triage of Security Smells in
						Microservice Applications </strong> <br>Francisco Ponce, Andrea
					Malnati, Roberto Negro, Francesca Arcelli Fontana, Hernán
					Astudillo, Antonio Brogi, Jacopo Soldani</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info"><strong>Bioinformatics &amp; Health Care
				Information Systems</strong><br>Session Chair: <strong>Evilasio
				Costa Junior</strong><br>
			<ul>
				<li><strong>Cytosine methylation and demethylation model ---
						equilibrium point and the influence of enzymes on cytosine forms
						levels </strong> <br>Krzysztof Fujarewicz, Karolina Kurasz</li>
				<li><strong>Extraction and selection of multi-omic features for the
						breast cancer survival prediction </strong> <br>Daria Kostka,
					Wiktoria Płonka, Roman Jaksik</li>
				<li><strong>Clustering-based Health Indicators for Health-related
						Quality of Life </strong> <br>Pedro A. M. Oliveira, Rossana M. C.
					Andrade, Pedro A. Santos Neto, Ismayle S. Santos, Evilasio C.
					Junior, Victória T. Oliveira, Nadiana K. N. Mendes</li>
				<li><strong>Comprehensive Monitoring System for High-Risk
						Pregnancies </strong> <br>Santiago Paeres, German Montoya, Carlos
					Andres Lozano Garzon</li>
				<li><strong>Development of a Software Prototype for Assisting People
						with Quadriplegia: An Approach Based on Interface Analysis and
						Computer Vision </strong> <br>Braian F. Ramírez, Daniel E. Torres,
					Lisseth T. Quilindo, Óscar A. Méndez</li>
			</ul>
			<hr> <strong>Social and Behavioral Applications &amp; Software and
				Systems Modeling</strong><br>Session Chair: <strong>Hector Florez</strong><br>
			<ul>
				<li><strong>Development of a Portable Educational Mechatronic Device
						to Improve Attention and Memory in Children with Attention Deficit
						Hyperactivity Disorder (ADHD) from the Age of Three - Nubox </strong>
					<br>Angie Luisa Herrera Poma, Alexander Carlos Mendoza Puris, Jose
					Alexis Del Aguila Ramos</li>
				<li><strong>Emotions, Attitudes, and Challenges in the Perception of
						Artificial Intelligence in Social Research </strong> <br>Simone
					Belli, Marcelo Leon</li>
				<li><strong>Towards an Academic Social Network to Support Students
						Decision Making Processes </strong> <br>Hector Florez, Dana Macias</li>
				<li><strong>Design of a functional block for testing analog and
						mixed-signal integrated circuits </strong> <br>José L.
					Simancas-García, Farid A. Meléndez-Pertuz, Ramón E. R. González,
					César A. Cárdenas, César Mora, Carlos Andrés Collazos Morales</li>
				<li><strong>Events Correlations for Fault Identification in GPON
						Networks </strong> <br>Manoel Lopes Filho, Danilo Vasconcelos,
					Rossana Andrade, Alex Ramos, Ismayle Santos</li>
			</ul></td>
	</tr>
	<tr>
		<td>18:00-19:00</td>
		<td class="table-danger"><strong>Conference Closing </strong></td>

	</tr>
</table>