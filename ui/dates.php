<h3>Important Dates</h3>
<ul>
	<li>Paper Submission:
		<ul>
			<li><strong>June 8, 2025</strong></li>
		</ul>
	</li>
	<li>Paper Notification:
		<ul>			
			<li><strong>July 6, 2025</strong></li>
		</ul>
	</li>
	<li>Camera Ready:
		<ul>
			<li><strong>August 3, 2025</strong></li>
		</ul>
	</li>
	<li>Authors Registration:
		<ul>
			<li><strong>August 3, 2025</strong></li>
		</ul>
	</li>
</ul>