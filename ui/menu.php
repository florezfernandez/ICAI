<br>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
	<div class="container-fluid">
		<a class="navbar-brand" href="index.php"><span class="fas fa-home"
			aria-hidden="true"></span></a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
			data-bs-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
					href="#" role="button" data-bs-toggle="dropdown"
					aria-expanded="false"> Conference </a>
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="index.php?pid=call">Call for
								Papers</a></li>
						<li><a class="dropdown-item" href="index.php?pid=dates">Important
								Dates</a></li>
						<li><a class="dropdown-item" href="index.php?pid=submission">Submission</a></li>
						<li><a class="dropdown-item" href="index.php?pid=call4W">Call for
								Workshops</a></li>
						<li><a class="dropdown-item" href="index.php?pid=acknowledgements">Acknowledgements</a></li>
					</ul></li>
				<li class="nav-item"><a class="nav-link"
					href="index.php?pid=acceptedWorkshops">Workshops</a></li>
				<li class="nav-item"><a class="nav-link" aria-current="page"
					href="index.php?pid=committees">Committees</a></li>
<!-- 				<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" -->
<!-- 					href="#" role="button" data-bs-toggle="dropdown" -->
<!-- 					aria-expanded="false">Program</a> -->
<!-- 					<ul class="dropdown-menu"> -->
<!-- 						<li><a class="dropdown-item" href="index.php?pid=keynotes">Keynotes</a></li> -->
<!-- 						<li><a class="dropdown-item" href="index.php?pid=program">General -->
<!-- 								Program</a></li> -->
<!-- 						<li><a class="dropdown-item" href="index.php?pid=acceptedPapers">Accepted -->
<!-- 								Papers</a></li> -->
<!-- 					</ul></li> -->
				<li class="nav-item"><a class="nav-link"
					href="index.php?pid=proceedings">Proceedings</a></li>
				<li class="nav-item"><a class="nav-link"
					href="index.php?pid=registration">Registration</a></li>
				<li class="nav-item"><a class="nav-link" href="index.php?pid=venue">Venue</a></li>
				<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
					href="#" role="button" data-bs-toggle="dropdown"
					aria-expanded="false"> Stats </a>
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="index.php?pid=statsAllEditions">All
								Editions</a></li>
						<li><a class="dropdown-item" href="index.php?pid=statsIcai">ICAI</a></li>
						<li><a class="dropdown-item"
							href="index.php?pid=statsIcaiWorkshops">ICAI Workshops</a></li>
					</ul></li>
				<li class="nav-item"><a class="nav-link"
					href="index.php?pid=contact">Contact</a></li>

			</ul>
			<ul class="navbar-nav">
				<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
					href="#" role="button" data-bs-toggle="dropdown"
					aria-expanded="false"> Previous Editions </a>
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="./2024/" target="_blank">ICAI
								2024</a></li>
						<li><a class="dropdown-item" href="./2023/" target="_blank">ICAI
								2023</a></li>
						<li><a class="dropdown-item" href="./2022/" target="_blank">ICAI
								2022</a></li>
						<li><a class="dropdown-item" href="./2021/" target="_blank">ICAI
								2021</a></li>
						<li><a class="dropdown-item" href="./2020/" target="_blank">ICAI
								2020</a></li>
						<li><a class="dropdown-item" href="./2019/" target="_blank">ICAI
								2019</a></li>
						<li><a class="dropdown-item" href="./2018/" target="_blank">ICAI
								2018</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
</nav>
<br>