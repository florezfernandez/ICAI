<br>
<div class="row">
	<div class="col-md-8">
		<h3>ICAI 2025</h3>
		<p class="text-justify">
			The 8<sup>th</sup> International Conference on Applied Informatics
			(ICAI) aims to bring together researchers and practitioners working
			in different domains in the field of informatics in order to exchange
			their expertise and discuss the perspectives of development and
			collaboration
		</p>
		<p class="text-justify">
			ICAI 2025 will be held at the <strong>Université Mohammed VI
				Polytechnic</strong> located in <strong>Ben Guerir, Morocco</strong>,
			from 8 to 11 October 2025. It is organized by the Université Mohammed
			VI Polytechnic and the ITI Research Group which belongs to the
			Universidad Distrital Francisco Jose de Caldas.
		</p>
		<p class="text-justify">ICAI 2025 is proudly sponsored by ITI Research
			Group and Springer.</p>

		<p>ICAI has been previously held in:</p>
		<ul>
			<li>2024: Viña del Mar, Chile <img
				src="https://www.worldometers.info/img/flags/ci-flag.gif"
				height="16px"></li>
			<li>2023: Guayaquil, Ecuador <img
				src="https://www.worldometers.info/img/flags/ec-flag.gif"
				height="16px"></li>
			<li>2022: Arequipa, Peru <img
				src="https://www.worldometers.info/img/flags/pe-flag.gif"
				height="16px"></li>
			<li>2021: Buenos Aires, Argentina <img
				src="https://www.worldometers.info/img/flags/ar-flag.gif"
				height="16px"></li>
			<li>2020: Ota, Nigeria <img
				src="https://www.worldometers.info/img/flags/ni-flag.gif"
				height="16px"></li>
			<li>2019: Madrid, Spain <img
				src="https://www.worldometers.info/img/flags/sp-flag.gif"
				height="16px"></li>
			<li>2018: Bogota, Colombia <img
				src="https://www.worldometers.info/img/flags/co-flag.gif"
				height="16px"></li>
		</ul>
		<div class="alert alert-success" role="alert">
			<?php include "ui/scholar.php" ?>
		</div>
	</div>
	<div class="col-md-4">
		<div class="alert alert-success" role="alert"><?php include "ui/dates.php" ?></div>
		<div class="alert alert-success" role="alert">
			<h3>Proceedings</h3>
			ICAI proceedings will be published with <strong>Springer</strong> in
			their <strong><a href="http://www.springer.com/series/7899"
				target="_blank">Communications in Computer and Information Science
					(CCIS)</a></strong> series. <br>
			<div class="text-center">
				<a href="http://www.springer.com/series/7899" target="_blank"><img
					src="img/logos/ccis.jpg" height="100" data-toggle="tooltip"
					data-placement="bottom"
					title="Communications in Computer Information Science"></a> <a
					href="http://www.springer.com" target="_blank"><img
					src="img/logos/springer2.png" height="100" data-toggle="tooltip"
					data-placement="bottom" title="Springer"></a>
			</div>
		</div>
		<div class="alert alert-success" role="alert">
			<h3>Best Papers</h3>
			Selected Best Papers will be invited to publish in <br> <strong><a
				href="https://www.springer.com/journal/42979" target="_blank">SN
					Computer Science</a></strong> by <strong>Springer</strong>.
			<div class="text-center">
				<a href="https://www.springer.com/journal/42979" target="_blank"><img
					src="img/logos/sncs.jpg" height="100" data-toggle="tooltip"
					data-placement="bottom" title="SN Computer Science"></a> <a
					href="http://www.springer.com" target="_blank"><img
					src="img/logos/springer2.png" height="100" data-toggle="tooltip"
					data-placement="bottom" title="Springer"></a>
			</div>
		</div>
		<div class="alert alert-success" role="alert">
			<h3>Submission</h3>
			To submit or upload a paper please go to <a href="https://meteor.springer.com/ICAI2025" target="_blank"><strong>Meteor</strong></a>
			by <strong>Springer</strong>. <span class="text-danger">The first anonymous version
				must be submitted in PDF</span>.
		</div>

	</div>
</div>