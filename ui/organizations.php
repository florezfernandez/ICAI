<hr>
<h3>Organized by</h3>
<div class="text-center">
	<a href="https://www.udistrital.edu.co" target="_blank"><img
		src="img/logos/ud.png" height="120" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Distrital Francisco Jose de Caldas"></a> <a
		href="https://um6p.ma/" target="_blank"><img src="img/logos/um6p.png"
		width="200" data-toggle="tooltip" data-placement="bottom"
		title="Université Mohammed VI Polytechnic"></a>
</div>
<h3>Sponsored by</h3>
<div class="text-center">
	<a href="http://www.itiud.org" target="_blank"><img
		src="img/logos/iti.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="ITI Research Group"></a> <a
		href="http://www.springer.com" target="_blank"><img
		src="img/logos/springer2.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a>
</div>
<hr>