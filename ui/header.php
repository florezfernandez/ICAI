<div class="col-lg-3 col-md-4 text-center">
	<img src="img/logos/icai.png" width="200">
</div>
<div class="col-lg-9 col-md-12">
	<h4>
		<i><strong>8<sup>th</sup> International Conference on Applied
				Informatics
		</strong></i>
	</h4>
	<h5>
		8 to 11 October 2025 <br>Université Mohammed VI Polytechnic<br> Ben Guerir,
		Morocco <img src="https://www.worldometers.info/img/flags/mo-flag.gif"
			height="16px">
	</h5>
</div>