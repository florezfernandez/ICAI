<h3>Registration</h3>
<p class="text-justify">One author of each accepted paper must register
	to ensure its inclusion in the conference proceedings. Each
	registration will allow the publication and presentation of just one
	paper. Authors must pay an extra fee if they wish to publish and
	present one additional paper.</p>

<h3>Registration Fees</h3>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<table class="table table-hover table-striped w-auto">
				<thead>
					<tr>
						<th class="text-center">Registration</th>
						<th class="text-center">Fee</th>
					</tr>
				</thead>
				<tbody class="table-group-divider">
					<tr>
						<td nowrap>Regular Registration</td>
						<td class="text-center">350 USD</td>
					</tr>
					<tr>
						<td nowrap>PC Member</td>
						<td class="text-center">250 USD</td>
					</tr>
					<tr>
						<td nowrap>Student Registration</td>
						<td class="text-center">250 USD</td>
					</tr>
					<tr>
						<td nowrap>Workshop Registration</td>
						<td class="text-center">250 USD</td>
					</tr>
					<tr>
						<td nowrap>One Additional Paper</td>
						<td class="text-center">150 USD</td>
					</tr>
					<tr>
						<td nowrap>Attendee</td>
						<td class="text-center">100 USD</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<p class="text-justify">Registration instructions are sent to <strong>authors of
	accepted papers</strong> in the corresponding acceptance notification.</p>