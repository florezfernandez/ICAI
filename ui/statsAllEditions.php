<?php 
$streamContext = stream_context_create([
    'ssl' => [
        'verify_peer'      => false,
        'verify_peer_name' => false
    ]
]);
$content = file_get_contents("https://icai-s.itiud.org/statsAllEditions.php", false, $streamContext);
echo $content;
?>