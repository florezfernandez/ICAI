<h3>General Chairs</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Diego Peluffo-Ordóñez, Ph.D. Université Mohammed VI Polytechnique, Morocco <a href="https://orcid.org/0000-0002-9045-6997" target="_blank"><img src="img/logos/orcid.png" ></a></li>
</ul>

<h3>Steering Committee</h3>
<ul>
	<li>Hernan Astudillo, Ph.D. Universidad Andrés Bello, Chile <a href="https://orcid.org/0000-0002-6487-5813" target="_blank"><img src="img/logos/orcid.png" ></a></li>	
	<li>Jaime Chavarriaga, Ph.D. Pontificia Universidad Javeriana, Colombia <a href="https://orcid.org/0000-0002-8372-667X" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Cesar Diaz, Ph.D. OMASHU, Spain <a href="https://orcid.org/0000-0002-9132-2747" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0001-7020-6328" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Olmer García, Ph.D. Ekumen Labs, USA <a href="https://orcid.org/0000-0002-6964-3034" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Christian Grévisse, Ph.D. Université du Luxembourg, Luxembourg <a href="https://orcid.org/0000-0002-9585-1160" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Ma Florencia Pollo-Cattaneo, Ph.D. Universidad Tecnológica Nacional Facultad Regional Buenos Aires, Argentina <a href="https://orcid.org/0000-0003-4197-3880" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Fernando Yepes-Calderon, Ph.D. Children's Hospital Los Angeles, United States <a href="https://orcid.org/0000-0001-9184-787X" target="_blank"><img src="img/logos/orcid.png" ></a></li>
</ul>

<h3>Organizing Committee</h3>
<ul>
	<li>Daniel Bonillea Licea, Ph.D. Université Mohammed VI Polytechnique, Morocco <a href="https://orcid.org/0000-0002-1057-816X" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Diego Peluffo-Ordóñez, Ph.D. Université Mohammed VI Polytechnique, Morocco <a href="https://orcid.org/0000-0002-9045-6997" target="_blank"><img src="img/logos/orcid.png" ></a></li>
</ul>

<h3>Workshops Committee</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0001-7020-6328" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Christian Grévisse, Ph.D. Université du Luxembourg, Luxembourg <a href="https://orcid.org/0000-0002-9585-1160" target="_blank"><img src="img/logos/orcid.png" ></a></li>
</ul>

<h3>Program Committee Chairs</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Diego Peluffo-Ordóñez, Ph.D. Université Mohammed VI Polytechnique, Morocco <a href="https://orcid.org/0000-0002-9045-6997" target="_blank"><img src="img/logos/orcid.png" ></a></li>
	<li>Samira Hosseini, Ph.D. Tecnológico de Monterrey, Mexico <a href="https://orcid.org/0000-0001-9190-4782" target="_blank"><img src="img/logos/orcid.png" ></a></li>
</ul>


<h3>Program Committee</h3>
<ol>
<?php 
$pc = fopen("docs/pc.csv", "r");
while(!feof($pc)) {
    $data = explode(";", trim(fgets($pc)));
    if($data[0] != ""){
        echo "<li>" . $data[0] . " " . $data[1] . ", Ph.D. " . $data[3] . ", " . $data[4] . (($data[7]!="")?" <a href='" . $data[7] . "' target='_blank'><img src='img/logos/orcid.png' ></a>":"") . "</li>\n";        
    }
}
?>
</ol>