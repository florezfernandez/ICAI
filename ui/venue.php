<h3>Venue</h3>
<p>
	ICAI 2025 will be held at the <strong>Université Mohammed VI
		Polytechnic</strong> located in <strong>Ben Guerir, Morocco</strong>.
	The Université Mohammed VI Polytechnic (UM6P), situated in the heart of
	Ben Guerir's Green City, stands as a premier venue for conferences and
	events in Morocco. As a hub for research and innovation, UM6P is
	committed to fostering economic and human development, particularly
	across Africa. Its cutting-edge facilities including a world-class
	conference hall designed to accommodate international events, featuring
	a spacious 635-square-meter stage and seating for up to 2000 attendees
	across its main floor and balcony. This dedication to excellence has
	solidified UM6P’s reputation as a top destination for academic and
	professional gatherings, highlighted by its selection as the host of
	the 2024 Paris Peace Forum Spring Meeting, the first time this
	prestigious event has been held on the African continent.

</p>
<iframe
	src="https://www.google.com/maps/d/u/2/embed?mid=1ZoOMUWDb8iA4_FoL6_Y2DfvQ-1sDCI4&ehbc=2E312F"
	width="640" height="480"></iframe>

<h4>Transportation</h4>
<p>Ben Guerir is well-connected to both Marrakech and Casablanca, making
	it easily accessible by train, car, or taxi.</p>
<ul>
	<li>From Marrakech to Benguerir (70 Km aprox | 40–50 min)
		<ul>
			<li>By Train: The ONCF (Moroccan National Railways) operates frequent
				trains from Marrakech Railway Station (Gare de Marrakech) to Ben
				Guerir Railway Station. The journey takes around 40–50 minutes and
				is the most comfortable and reliable option.</li>
			<li>By Car: Take the A7 highway (Marrakech–Casablanca Motorway) north
				towards Ben Guerir. The drive is approximately 45–50 minutes, with
				well-maintained roads.</li>
			<li>By Taxi:
				<ul>
					<li>Grand taxis (shared taxis for intercity travel) are available
						from Bab Doukkala or Sidi Mimoun taxi stations in Marrakech, with
						a travel time of 45–60 minutes.</li>
					<li>Private taxis can also be booked for a direct and more
						comfortable ride.</li>
					<li>By Bus: Some regional buses operate between Marrakech and
						Benguerir, but they are generally slower and less frequent than
						trains or taxis.</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>From Casablanca to Ben Guerir (200 Km aprox | 1h 40min – 2h)
		<ul>
			<li>By Train: Direct ONCF trains run frequently from Casablanca
				Voyageurs Station to Benguerir, taking around 1 hour 40 minutes.
				This is the most convenient and comfortable way to travel.</li>
			<li>By Car: Drive south on the A7 highway, which connects Casablanca
				to Marrakech via Benguerir. The journey takes approximately 2 hours,
				depending on traffic.</li>
			<li>By Taxi: Private taxis can be hired for a direct trip, but they
				are significantly more expensive than other options.</li>
		</ul>
	</li>
</ul>
