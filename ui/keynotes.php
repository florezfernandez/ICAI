<h3>Keynote Speakers</h3>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/cg.jpg" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Christian Grévisse</strong>
		</h5>
		<p>E-learning specialist at the University of Luxembourg. A computer
			scientist by training, he designs, develops and evaluates
			technological solutions for e-learning and e-assessment in computer
			science, medical and nursing education. He provides both pedagogical
			and technological advice to health professionals regarding teaching
			and learning. His current research interests include scaffolding
			support in simulation-based training and the application of Large
			Language Models in Technology Enhanced Learning. He has published
			around 30 papers so far and is member of several academic and
			scientific committees. He teaches technology-related topics to
			computer science, medicine and nursing students. Finally, he has
			accompanied or supervised around 20 students in both Europe and Latin
			America in their Bachelor, Master or PhD theses.</p>
		<h5>
			<strong>Keynote: Technology Enhanced Learning: From Computer Science
				to Simulation in Healthcare</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Technological advancements have
			transformed the way we teach and learn. This has not begun with the
			information age, but the pace in recent years has certainly
			increased. A diverse field of devices, more computational resources,
			advances in networking and artificial intelligence - all these
			factors enable new forms of learning, independently of the age,
			location or time. In this talk, we will accompany a millennial in his
			journey through the e-learning landscape, first as a user, then as a
			developer and researcher. We will cover different developments in
			Technology Enhanced Learning (TEL), starting in computer science
			education, followed by simulation in healthcare education. We will
			also have a look at the future of TEL in light of recent advancements
			in generative AI.
		</p>


	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/sb.jpg" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Sven von Brand</strong>
		</h5>
		<p>Software Engineer with a Masters Degree in Informatics and
			Telecommunications with 10 years of experience teaching and
			developing video games. Co founder of the game company Abstract
			Digital, current president of the game developers association of
			Chile, VG Chile and college professor of game development courses for
			over 10 years.</p>
		<h5>
			<strong>Keynote: Empowering Game Design though Software Architecture
				in Tormented Souls a Case Study</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Tormented Souls is a game developed in
			Chile that has sold over 400 thousand copies worldwide through Steam,
			Playstation, Xbox and Nintendo. During development our company was in
			charge of all software development and to be able to make the game in
			the time and budget we designed the software architecture to improve
			the speed and options for the gameplay developers and game designers
			to make content faster, while still being flexible and interesting.
			The most important aspect of this development is that both the
			systems and the content were being developed simultaneously, so
			initial content was slower, but guided the tools, while later content
			was faster to use and was guided by features of the tools.
		</p>


	</div>
</div>

