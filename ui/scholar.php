<?php
$id = "O5B96doAAAAJ";
$curl = curl_init();
$url = "http://scholar.google.com/citations?user=" . $id . "&hl=en";
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
filter_content(curl_exec($curl), $url, $id);

function filter_content($content, $url, $id){
    $matches = array();
    preg_match_all('/<div class="gsc_rsb_s gsc_prf_pnl" id="gsc_rsb_cit" role="region" aria-labelledby="gsc_prf_t-cit">(.*)<\/div><div class="gsc_lcl" role="main" id="gsc_prf_w">/is', $content, $matches);
    $pos = strpos($matches[0][0], "<style>", 0);
    $graph = substr($matches[0][0], $pos, strlen($matches[0][0])-$pos-60);
    $table = substr($matches[0][0], 0, $pos);
    $data = array();
    preg_match_all('/<td class="gsc_rsb_std">(.*?)<\/td>/is', $table, $data);
    echo "<div class='row'>";
    echo "<div class='col-md-6 col-12'>";
    echo "<h6><strong>Google Scholar Profile</strong> <a href='https://scholar.google.com/citations?user=" . $id . "' target='_blank'><span class='fas fa-external-link-alt'></span></a></h6>";    
    echo "<table class='table table-sm'>";
    echo "<thead><tr><th width='100%'></th><th class='text-right'>All</th><th class='text-right'nowrap>Since " . (intval(date("Y"))-5) . "</th></tr></thead>";
    echo "<tbody>";
    echo "<tr><th>Citations</th><td class='text-right'>" . $data[1][0] . "</td><td class='text-right'>" . $data[1][1] . "</td></tr>";
    echo "<tr><th>h-index</th><td class='text-right'>" . $data[1][2] . "</td><td class='text-right'>" . $data[1][3] . "</td></tr>";
    echo "<tr><th>i10-index</th><td class='text-right'>" . $data[1][4] . "</td><td class='text-right'>" . $data[1][5] . "</td></tr>";
    echo "</tbody>";    
    echo "</table>";
    echo "</div>";
    echo "<div class='col-md-6 col-12'>";
    echo $graph;
    echo "</div>";
    echo "</div>";
}
?>