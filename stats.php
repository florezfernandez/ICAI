<?php 
$type = $_GET["t"];
$edition = $_GET["e"];
$streamContext = stream_context_create([
    'ssl' => [
        'verify_peer'      => false,
        'verify_peer_name' => false
    ]
]);
if($type == "icai"){
    $content = file_get_contents("https://icai-s.itiud.org/statsIcai.php?edition=" . $edition, false , $streamContext);
}else if($type == "icaiw"){
    $content = file_get_contents("https://icai-s.itiud.org/statsIcaiWorkshops.php?edition=" . $edition, false, $streamContext);
}
echo $content;
?>