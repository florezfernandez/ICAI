<br>
<nav class="navbar navbar-expand-lg navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="icon-bar"></span><span class="icon-bar"></span><span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php"><span
				class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Conference <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.php?pid=call">Call for Papers</a></li>
						<li><a href="index.php?pid=dates">Important Dates</a></li>
						<li><a href="index.php?pid=submission">Submission</a></li>
						<li><a href="index.php?pid=call4W">Call for Workshops</a></li>
						<li><a href="index.php?pid=acceptedWorkshops">Accepted Workshops</a></li>
					</ul></li>
				<li><a href="index.php?pid=committees">Committees</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Program<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.php?pid=keynote">Keynote Speakers</a></li>
						<li><a href="index.php?pid=acceptedPapers">Accepted Papers</a></li>
						<li><a href="index.php?pid=program">General Program</a></li>
					</ul></li>
				<li><a href="index.php?pid=registration">Registration</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Venue <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.php?pid=venue">Conference Venue</a></li>
						<li><a href="index.php?pid=accommodation">Accommodation</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
</nav>
