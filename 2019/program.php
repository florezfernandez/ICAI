<h3>Program*</h3>

<table class="table">
	<tr>
		<th width="8%" class="text-center">Time</th>
		<th width="23%" class="text-center">Wednesday 6 November</th>
		<th width="23%" class="text-center">Thursday 7 November</th>
		<th width="23%" class="text-center">Friday 8 November</th>
		<th width="23%" class="text-center">Saturday 9 November</th>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td rowspan="6"></td>
		<td class="active">Registration<br> <i>Registration Desk (Ground Floor)</i> <br>
			Conference Opening<br> <i>Salón de Juntas (3<sup>rd</sup> Floor)</i></td>
		<td class="success">Keynote: Ixent Galpin<br> <i>Salón de Juntas (3<sup>rd</sup> Floor)</i>
		</td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="success">Keynote: Wolfgang Hofkirchner<br> <i>Salón de
				Juntas (3<sup>rd</sup> Floor)</i></td>
		<td class="success">Keynote: Varun Gupta <br> <i>Salón de Juntas (3<sup>rd</sup> Floor)</i>
		</td>
	</tr>
	<tr>
		<td>11:00-11:30</td>
		<td class="active text-center">Coffee Break</td>
		<td class="active text-center">Coffee Break</td>
		<td class="active" rowspan="3">Cultural Activity</td>
	</tr>
	<tr>
		<td>11:30-12:30</td>
		<td class="success">Keynote: Luis de-la-Fuente-Valentín<br> <i>Salón
				de Juntas (3<sup>rd</sup> Floor)</i>
		</td>
		<td rowspan="2" class="info">ICAI IT Architectures Session<br> <i>Sala
				Ramón Torregrosa (1<sup>st</sup>  Floor)</i><br> <br>ICAI Health Care Information Systems
			&amp; Learning Management Systems Sessions<br> <i>Salon de
				Antropología Social (1<sup>st</sup> Floor)</i><br> <br>ICAI Decision Systems Session<br> <i>Salón
				de Juntas (3<sup>rd</sup> Floor)</i></td>
	</tr>
	<tr>
		<td>12:30-13:30</td>
		<td class="success">Keynote: Florencia Pollo-Cattaneo<br> <i>Salón de
				Juntas (3<sup>rd</sup> Floor)</i>
		</td>

	</tr>
	<tr>
		<td>13:30-14:30</td>
		<td class="active text-center">Lunch</td>
		<td class="active text-center">Lunch</td>
	</tr>
	<tr>
		<td>14:30-15:30</td>
		<td class="active">Registration <br> <i>Registration Desk (Ground Floor)**</i><br>Conference Welcome<br> <i>Salón de Actos
				(B1 Ground Floor)**</i> </td>
		<td class="warning" rowspan="2">WDEA<br> <i>Sala Ramón Torregrosa (1<sup>st</sup>  Floor)</i><br>
			<br>IKIT &amp; ISTIHMR<br> <i>Salon de Antropología Social (1<sup>st</sup>  Floor)</i><br> <br>AIESD
			&amp; VGameEdu<br> <i>Salón de Juntas (3<sup>rd</sup> Floor)</i></td>
		<td class="info" rowspan="2">ICAI Data Analysis Session<br> <i>Sala
				Ramón Torregrosa (1<sup>st</sup>  Floor)</i><br> <br>ICAI Software Design Engineering
			Sessions<br> <i>Salon de Antropología Social (1<sup>st</sup>  Floor)</i><br> <br>ICAI
			Socio-technical Systems Session<br> <i>Salón de Juntas (3<sup>rd</sup> Floor)</i></td>
	</tr>
	<tr>
		<td>15:30-16:30</td>
		<td class="success">Keynote: Modestos Stavrakis<br> <i>Salón de Actos
				(B1 Bround Floor)**</i>
		</td>
	</tr>
	<tr>
		<td>16:30-17:00</td>
		<td class="active text-center">Coffee Break</td>
		<td class="active text-center">Coffee Break</td>
		<td class="active text-center">Coffee Break</td>
	</tr>
	<tr>
		<td>17:00-18:00</td>
		<td class="warning" rowspan="2">EduSynergies<br> <i>Salón de Actos
				(B1 Ground Floor)**</i></td>
		<td class="info" rowspan="2">ICAI Data Analysis Session<br> <i>Sala
				Ramón Torregrosa (1<sup>st</sup>  Floor)</i><br> <br>ICAI Robotic Autonomy &amp; Security
			Services Sessions<br> <i>Salon de Antropología Social (1<sup>st</sup>  Floor)</i></td>
		<td class="success">Keynote: Mark Burgin<br> <i>Salón de Juntas (3<sup>rd</sup>  Floor)</i></td>

	</tr>
	<tr>
		<td>18:00-19:00</td>
		<td class="warning">glossaLAB Seminar<br> <i>Salón de Juntas (3<sup>rd</sup>  Floor)</i></td>
	</tr>
	<tr>
		<td>19:00-20:00</td>
		<td class="success">Keynote: Mario Pérez-Montoro <br> <i>Salón de
				Actos (B1 Ground Floor)**</i></td>
		<td class="danger text-center">City-Orientation Game<br> <i>Registration Desk (Ground Floor)</i>
		
		<td class="active">Conference Closing<br> <i>Salón de Juntas (3<sup>rd</sup>  Floor)</i>
		</td>


	</tr>
	<tr>
		<td>20:00-21:00</td>
		<td class="danger text-center">Cocktail<br> <i>Hall (Ground Floor)**</i>
		</td>

	</tr>
</table>

<h4>Leyend</h4>
<table class="table">
  <tr>
    <td class="info" width="5%"></td><td width="45%">ICAI Sessions (Presentations of papers in CCIS)</td><td class="success" width="5%"></td><td width="45%">Keynotes</td>
  </tr>
  <tr>
    <td class="warning" width="5%"></td><td width="45%">ICAI Workshops Sessions (Presentations of papers in CEUR-WS)</td><td class="danger" width="5%"></td><td width="45%">Social and Cultural Activities</td>
  </tr>
</table>


<p>
	*Since 9<sup>th</sup> November is The Virgin of Almudena's day, we had
	to move one workshop to 6<sup>th</sup> November at UDIMA Campus in
	Madrid downtown. In addition, In September 2019 a second round of the
	Spanish general elections were convened for 10<sup>th</sup>, which
	automatically makes the previous day an official <i>reflection day</i><br>
	**UDIMA-CEF Headquarters, Madrid Center.
</p>

<h3>Detailed Program</h3>

<table class="table">
	<tr>
		<th width="8%" class="text-center">Time</th>
		<th class="text-center">Wednesday 6 November <br><i>(in UDIMA-CEF Headquarters, Madrid Center)</i></th>
	</tr>
	<tr>
		<td>14:30-15:30</td>
		<td class="active"><strong>Registration<br></strong> <i>Registration
				Desk (Ground Floor)**</i><br>Conference Welcome<br> <i>Salón de Actos
				(B1)**</i></td>
	</tr>
	<tr>
		<td>15:30-16:30</td>
		<td class="success"><strong>Keynote: Modestos Stavrakis: Designing
				interactive technologies for smart learning environments</strong><br>
			<i>Salón de Actos (B1)** </i></td>
	</tr>
	<tr>
		<td>16:30-17:00</td>
		<td class="active">Coffee Break</td>
	</tr>
	<tr>
		<td>17:00-19:00</td>
		<td class="warning"><strong>EduSynergies 2019: First International
				Workshop on Education and Technology Synergies for Current and
				Future Societies</strong><br> <i>Salón de Actos (B1)**</i><br>Session
			Chair: Iris Celorrio
			<ul>
				<li><strong>Degrowth and Educative Deconstruction of the Neoliberal
						Subject: Alternatives to Build up a Sustainable Society</strong><br>Enrique
					Javier Díez-Gutiérrez, José María Díaz-Nafría</li>
				<li><strong>How Innovation Emerges in Collectivities in Online
						Collaborative Learning</strong><br>Simone Belli</li>
				<li><strong>Open Science and Open Access, a Scientific Practice for
						Sharing Knowledge</strong><br>Simone Belli, Ronald Cardenas,
					Martin Velez, Ariana Rivera, Valeria Santoro</li>
				<li><strong>PRIMER Initiative: PRomoting Interdisciplinary
						Methodologies in Education and Research</strong> <br>José María
					Díaz-Nafría, Enrique Díez-Gutiérrez, Wolfgang Hofkirchner, Rainer
					E. Zimmermann, Simone Belli, Gustavo Martínez-Mekler, Markus
					Müller, Luis Tobar, Fernando Martín-Mayoral, Modestos Stavrakis,
					Teresa Guarda, Edgardo Ugalde</li>
				<li><strong>Repercussions of International Migration - Understanding
						the Consequences for Social Network</strong><br>Yovany Salazar,
					Marcelo León</li>
				<li><strong>Students' Perception of a Postgraduate Course in Agile
						Project Management Aimed at Developing Soft Skills</strong><br>Alix
					E. Rojas, Camilo Mejı́a-Moncayo</li>
			</ul></td>
	</tr>
	<tr>
		<td>19:00-20:00</td>
		<td class="success"><strong>Keynote: Mario Pérez-Montoro. Information
				Visualization as Knowledge Analysis</strong><br> <i>Salón de Actos
				(B1)** </i></td>
	</tr>
	<tr>
		<td>20:00-21:00</td>
		<td class="danger">Cocktail<br> <i> Room Pending</i></td>
	</tr>
</table>



<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th colspan="3" class="text-center">Thursday 7 November <br><i>(in Universidad Complutense de Madrid, Campus Somosaguas)</i></th>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td colspan="3" class="active"><strong>Registration<br></strong> <i>Registration
				Desk</i><br> <strong>Conference Opening </strong> <br> <i>Salón de
				Juntas</i></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td colspan="3" class="success"><strong>Keynote: Wolfgang Hofkirchner.
				Designing for a Global Sustainable Information Society</strong><br>
			<i>Salón de Juntas </i></td>
	</tr>
	<tr>
		<td>11:00-11:30</td>
		<td colspan="3" class="active">Coffee Break</td>
	</tr>
	<tr>
		<td>11:30-12:30</td>
		<td colspan="3" class="success"><strong>Keynote: Luis
				de-la-Fuente-Valentín. Learning Analytics: the use of Big Data in
				the classroom to improve the teaching/learning experience</strong><br>
			<i>Salón de Juntas</i></td>
	</tr>
	<tr>
		<td>12:30-13:30</td>
		<td colspan="3" class="success"><strong>Keynote: Florencia
				Pollo-Cattaneo. Artificial Intelligence for the Solution of Real
				Problems</strong><br> <i>Salón de Juntas</i></td>
	</tr>
	<tr>
		<td>13:30-14:30</td>
		<td colspan="3">Lunch</td>
	</tr>
	<tr>
		<td width="10%">14:30-16:30</td>
		<td width="30%" class="warning"><strong>WDEA: Second International
				Workshop on Data Engineering and Analytics</strong><br> <i>Sala
				Ramón Torregrosa </i><br>Session Chair: Cesar Diaz
			<ul>
				<li><strong>A Generic Data Management Framework for the Internet of
						Things</strong><br>JDiego Fernando Núñez-Sánchez, Juan Sebastián
					Cantor, Ixent Galpin</li>
				<li><strong>A Preliminary Assessment of the Traffic Measures in
						Madrid City</strong><br>Pilar Rey del Castillo</li>
				<li><strong>Alpha-Beta vs Scout Algorithms for the Othello Game</strong><br>Jorge
					Hernandez, Karen Daza, Hector Florez</li>
				<li><strong>ConIText: An Improved Approach for Contextual Indexation
						of Text Applied to Classification of Large Unstructured Data</strong>
					<br>Mohamed Salim El Bazzi, Abdelatif Ennaji, Driss Mammass</li>
				<li><strong>Demand Forecasting and Material Requirement Planning
						Optimization using Open Source Tools</strong><br>Jorge Ivan
					Romero-Gelvez, Edisson Alexander Delgado-Sierra, Jorge Aurelio
					Herrera-Cuartas, Olmer Garcia-Bedoya</li>
				<li><strong>Modeling Air Quality and Cancer Incidences in Proximity
						to Hazardous Waste and Incineration Treatment Areas</strong><br>Miriam
					Ugarte Querejeta, Ricardo S. Alonso</li>

			</ul></td>
		<td width="30%" class="warning"><strong>IKIT: Third International
				Workshop on Information and Knowledge in Internet of Things &amp;
				ISTIHMR: Second International Workshop on Intelligent Systems and
				Technologies for Interactive Human-Machine Relationships</strong><br>
			<i>Salon de Antropología Social </i><br>Session Chair: Teresa Guarda
			<ul>
				<li><strong>Geographic Marketing Intelligence: GMI Model</strong><br>Teresa
					Guarda, Maria Fernanda Augusto</li>
				<li><strong>Hindcasting with Multistations Using Analog Ensembles</strong><br>Alexandre
					Chesneau, Carlos Balsa, Carlos Veiga Rodrigues, Isabel Lopes</li>
				<li><strong>Numerical Simulation of the Biological Control of the
						Chestnut Gall Wasp with T. Sinensis</strong><br>Carlos Balsa,
					Margaux Citerici, Isabel Lopes, José Rufino</li>
				<li><strong>Towards a Secure Behavior Modeling for IoT Networks
						Using Blockchain</strong><br>Jawad Ali, Ahmad Sharafidz Khalid,
					Eiad Yafi, Shahrulniza Musa, Waqas Ahmed</li>
				<li><strong>Use of Information Technology in Educational Contexts:
						Case study of CCMI Digital</strong><br>Filipe Mota Pinto, Teresa
					Guarda, Maria Fernanda Augusto, Washington Torres, Samuel Bustos,
					Freddy Villao, Luis Mazon</li>
				<li><strong>Determination of the Speed of Sound to Calculate
						Distances Through Firing a Rifle at 22 C at the Sea Level</strong><br>Luis
					Palacios Aguirre, Rosalba Rodrı́guez Reyes</li>
				<li><strong>Influence Networks based Methodology for Consensus
						Reaching in Group-Decision-Making Problems</strong><br>Jorge Ivan
					Romero-Gelvez, Felix Antonio Cortes-Aldana, Monica Garcia-Melon,
					Jorge Aurelio Herrera Cuartas, Olmer Garcia Bedoya</li>
				<li><strong>Multimedia System Kinect-Based. Learning Experience for
						Children of Primary School</strong><br>Lidice Haz, Yolanda
					Molineros, Estefania Vargas, Araceli Davila</li>
				<li><strong>Pilot Training Program at the Military Aviation School
						in Salinas</strong><br>Marisol Gutiérrez Santos, Hernán Soberón
					Villacrés, Miguel Intriago Jaya, Anthony España Taco</li>
				<li><strong>Use of e-Health as a Mobility and Accessibility Strategy
						within Health Centers in Ecuador with the Aim of Reducing
						Absenteeism to Medical Consultations</strong><br>Diego Terán,
					Freddy Tapia, Joel Rivera, Hernán Aules</li>
			</ul></td>
		<td width="30%" class="warning"><strong>AIESD: First International
				Workshop on Applied Informatics for Economics, Society and
				Development &amp; VGameEdu: First International Workshop on Video
				Games, Gamification and Educational Innovation</strong><br> <i>Salón
				de Juntas </i><br>Session Chair: Simone Belli
			<ul>
				<li><strong>A Novel Approach to Address Process Plant Layout based
						on a Bacterial Genetic Optimization Algorithm</strong><br>TBrandon
					Y. Morales-Calvache, Santiago Vásquez-Méndez, Fabian C.
					Prada-Ariza, Camilo Mejı́a-Moncayo</li>
				<li><strong>Artificial Intelligence in Mobile Applications of
						Dermatology: A Systematic Mapping Study</strong><br>Verónica
					Tintı́n, José Caiza, Hebert Atencio, Fernando Caicedo</li>
				<li><strong>The Internal Migration in the Ecuadorian Novel: A
						Contribution to the Educational Technology</strong><br>Yovany
					Salazar, Marcelo León</li>
				<li><strong>An Interactive Multimedia Game "Let's Save the Water"
						for the Communities of Ecuador and Bolivia</strong><br>Marcelo
					León, Valeria Burgos, Leidy Lozano Jacome</li>
				<li><strong>Game Balancing - A Semantical Analysis</strong><br>Alexander
					Becker, Daniel Görlich</li>
				<li><strong>Psychological Outlook on Video Games: an Example of the
						Long Road to the Invention of Disorders not Associated with
						Substances</strong><br>Cristian López Raventós, Simone Belli</li>
				<li><strong>Statistical Determination of Parents’ Use of
						Gamifica-tion Applications According to Technology Acceptance
						Model</strong><br>Huseyin Bicen, Senay Kocakoyun Aydogan</li>
				<li><strong>Video Games and Advertising. Examples Through the Press
						in the City Morelia (Mexico) Between 1985-2000</strong><br>Guillermo
					Fernando Rodríguez Herrejón</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:30-17:00</td>
		<td colspan="3" class="active">Coffee Break</td>
	</tr>
	<tr>
		<td>17:00-19:00</td>
		<td colspan="3" class="info">
			<table class="table">
				<tr>
					<td width="50%" class="info"><strong>ICAI Data Analysis Session</strong><br>
						<i>Sala Ramón Torregrosa </i><br>Session Chair: Almudena Sánchez
						<ul>
							<li><strong>Academic behavior analysis in virtual courses using a
									data mining approach</strong><br>Dario Delgado-Quintero, Olmer
								Garcia, Pablo Munevar-Garcia, Diego Aranda-Lozano, Cesar Diaz</li>
							<li><strong>Analysis of Usability of Various Geosocial Network
									POI in Tourism </strong>Jiri Kysela</li>
							<li><strong>Application of the Requirements Elicitation Process
									for the Construction of Intelligent System-based Predictive
									Models in the Education Area</strong><br>Cinthia Vegega, Pablo
								Pytel, Maria Florencia Pollo-Cattaneo</li>
							<li><strong>Evaluating Student Learning Effect based on Process
									Mining </strong><br>Yu Wang, Tong Li, Congkai Geng, Yihan Wang</li>
							<li><strong>Evalu@: an Agnostic Web-based Tool for Consistent and
									Constant Evaluation Used as a Data Gatherer for Artificial
									Intelligence Implementations</strong><br>Fernando
								Yepes-Calderon, Eduardo Yepes-Calderon, Juan Fernando
								Yepes-Zuluaga</li>
						</ul></td>
					<td width="50%" class="info"><strong>ICAI Robotic Autonomy &amp;
							Security Services Sessions</strong><br> <i>Salon de Antropología
							Social </i><br>Session Chair: Hector Florez
						<ul>
							<li><strong>Comparative Analysis of Three Obstacle Detection And
									Avoidance Algorithms For A Compact Differential Drive Robot I N
									V-Rep</strong><br> Chika Yinka-Banjo, Obawole Daniel, Sanjay
								Misra, Hector Florez</li>
							<li><strong>A Secured Private-Cloud Computing System</strong><br>Modebola
								Olowu, Chika Yinka-Banjo, Hector Florez, Sanjay Misra</li>
							<li><strong>FComparative Evaluation of Techniques for Detection
									of Phishing URLs </strong><br>Oluwafemi Osho, Ayanfeoluwa
								Oluyomi, Sanjay Misra, Ravin Ahuja, Robertas Damasevicius, Rytis
								Maskeliunas</li>
						</ul></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>19:00-20:00</td>
		<td colspan="3" class="danger"><strong>City-Orientation Game </strong><br>Room
			Pending</td>
	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%">Time</th>
		<th colspan="3" class="text-center">Friday 8 November <br><i>(in Universidad Complutense de Madrid, Campus Somosaguas)</i></th>
	</tr>
	<tr>
		<td>9:00-10:00</td>
		<td colspan="3" class="success"><strong>Keynote: Ixent Galpin.
				Usability Issues in Data Science</strong> <br> <i>Salón de Juntas</i></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td colspan="3" class="success"><strong>Keynote: Varun Gupta: Software
				Evolution strategies of Startups: Do product category have a role to
				play?</strong><br> <i>Salón de Juntas</i></td>
	</tr>
	<tr>
		<td>11:00-11:30</td>
		<td colspan="3" class="active">Coffee Break</td>
	</tr>
	<tr>
		<td>11:30-13:30</td>
		<td width="30%" class="info"><strong>ICAI IT Architectures Session</strong><br>
			<i>Sala Ramón Torregrosa </i><br>Session Chair: Oscar Avila

			<ul>
				<li><strong>A proposal model based on Blockchain technology to
						support traceability of Colombian scholar alimentation program
						(PAE) </strong><br> Alejandro Guzmán, Camilo Rincón, Carol Cortés,
					Catherine Torres, Camilo Mejía-Moncayo</li>
			</ul>
			<ul>
				<li><strong>Capacity of Desktop Clouds for running HPC applications:
						a revisited analysis </strong><br> Jaime Chavarriaga, Carlos
					Gomez, Harold Castro, David Camilo Bonilla</li>
			</ul>
			<ul>
				<li><strong>Migration to Microservices: Barriers and Solutions</strong><br>
					Javad Ghofrani, Arezoo Bozorgmehr</li>
			</ul>
			<ul>
				<li><strong>Towards a maturity model for cloud service customizing</strong><br>
					Oscar Avila, Cristian Paez, Dario Correal</li>
			</ul>
			<ul>
				<li><strong>Towards an architecture framework for the implementation
						of omnichannel strategies </strong><br> Nestor Suarez, Oscar Avila
				</li>
			</ul></td>
		<td width="30%" class="info"><strong>ICAI Health Care Information
				Systems &amp; Learning Management Systems Sessions</strong><br> <i>Salon
				de Antropología Social </i><br>Session Chair: Manuela Cañizares

			<ul>
				<li><strong>Hyperthermia Study in Breast Cancer Treatment using a
						new Applicator</strong><br> Hector Fabian Guarnizo Mendez,
					Mauricio Andrés Poloché Arango, Juan Fernando Coronel Rico, Tatiana
					Angelica Rubiano Suazo</li>
			</ul>
			<ul>
				<li><strong>Manual Segmentation Associated Errors in Medical
						Imaging. Proposing a more reliable Gold Standard </strong><br>
					Fernando Yepes-Calderon, J. Gordon McComb</li>
			</ul>
			<ul>
				<li><strong>Connecting CS1 with student's careers through
						multidisciplinary projects. Case of study: Material selection
						following the Ashby Methodology</strong><br> Miguel Realpe, Bruno
					Paucar, Giovanny Eduardo Chunga Santos, Clotario Tapia, Natalia
					Lopez</li>
			</ul>
			<ul>
				<li><strong>Development of Online Clearance System for an
						Educational Institution </strong><br> Oluranti Jonathan, Sanjay
					Misra, Funmilayo Makinde, Robertas Damasevicius, Rytis Maskeliunas,
					Marcelo Leon</li>
			</ul>
			<ul>
				<li><strong>Enhanced Sketchnoting through Semantic Integration of
						Learning Material </strong><br> Aryobarzan Atashpendar, Christian
					Grévisse, Steffen Rothkugel</li>
			</ul></td>
		<td width="30%" class="info"><strong>ICAI Decision Systems Session</strong><br>
			<i>Salón de Juntas </i><br>Session Chair: Wolfgang Hofkirchner

			<ul>
				<li><strong>Algorithmic Discrimination and Responsibility: Case
						Studies from USA and South America </strong><br> Musonda
					Kapatamoyo, Yalitza Ramos-Gil, Carmelo Márquez-Domínguez</li>
			</ul>
			<ul>
				<li><strong>Continuous Variable Binning Algorithm to Maximize
						Information Value using Genetic Algorithm </strong><br> Nattawut
					Vejkanchana, Pramote Kucharoen</li>
			</ul>
			<ul>
				<li><strong>CVRPTW model for cargo collection with heterogeneous
						capacity fleet </strong><br> Jorge Ivan Romero Gelvez, William
					Camilo Gonzalez Cogua, Jorge Aurelio Herrera Cuartas</li>
			</ul>
			<ul>
				<li><strong>Evaluation of Transfer Learning Techniques with
						Convolutional Neural Networks (CNNs) to Detect the Existence of
						Roads in High-Resolution Aerial Imagery </strong><br>
					Calimanut-Ionut Cira, Ramon Alcarria, Miguel-Ángel Manso-Callejo,
					Francisco Serradilla</li>
			</ul>
			<ul>
				<li><strong>Predicting stock prices using LSTM </strong><br>Vu
					Nguyen, Duc Nguyen Huu Dat, Loc Tran Phuoc</li>
			</ul></td>
	</tr>
	<tr>
		<td>13:30-14:30</td>
		<td colspan="3" class="active">Lunch</td>
	</tr>
	<tr>
		<td>14:30-16:30</td>
		<td width="30%" class="info"><strong>ICAI Data Analysis Session</strong>
			<br> <i>Sala Ramón Torregrosa </i><br>Session Chair: Lorena Lobo
			<ul>
				<li><strong>Model for resource allocation in decentralized networks
						using Interaction Nets </strong><br> Joaquín Sanchez, Juan P
					Ospina, Carlos Andrés Collazos Morales, Henry Avendaño, Paola Ariza
					Colpas, Vanesa Landero</li>
				<li><strong>RefDataCleaner: A Usable Data Cleaning Tool </strong><br>
					Juan Carlos Leon Medina, Ixent Galpin</li>
				<li><strong>Study of the crime status in Colombia and Development of
						a citizen security App </strong><br> Raquel Canon, Cesar Diaz,
					Olmer Garcia-Bedoya, Holman Diego Bolivar Baron</li>
				<li><strong>University Quality Measurement Model Based On Balanced
						Scorecard </strong><br> Thalia Obredor Baldovino, Harold Combita
					Niño, Tito J. Crissien Borrero, Emiro De La Hoz Franco, Diego
					Beltrán, Iván Ruiz, Carlos Andrés Collazos Morales, Joaquín Sanchez</li>
			</ul></td>
		<td width="30%" class="info"><strong>ICAI Software Design Engineering
				Sessions</strong> <br> <i>Salon de Antropología Social </i><br>Session
			Chair: Iris Celorrio
			<ul>
				<li><strong>Dynamic Interface and Access Model by Dead Token for IoT
						Systems </strong><br> Karen Daza, Jorge Hernandez, Hector Florez,
					Sanjay Misra</li>
				<li><strong>Evaluation of the Performance of Message Routing
						Protocols in Delay Tolerant Networks (DTN) in Juan de Acosta –
						Barranquilla Scenario </strong><br> Carlos Andrés Collazos
					Morales, Farid Meléndez Pertuz, Hermes Castellanos, Nazhir Amaya
					Tejera, Rubén Sánchez Dams, José Simancas-García, Iván Ruiz, Fredy
					Sanz, César A Cárdenas</li>
				<li><strong>Recovering Fine Grained Traceability Links Between
						Software Mandatory Constraints and Source Code </strong><br>
					Alejandro Velasco, Jairo Aponte</li>
				<li><strong>Using Graph Embedding to Improve Requirements
						Traceability Recovery </strong><br> Shiheng Wang, Tong Li, Zhen
					Yang,</li>
			</ul></td>
		<td width="30%" class="info"><strong>ICAI Socio-technical Systems
				Session</strong><br> <i>Salón de Juntas </i><br>Session Chair: Francisco Álvarez
			<ul>
				<li><strong>Cultural Archaeology of video games: between nostalgic
						discourse, gamer experience and technological innovation</strong><br>
					Cristian López Raventós, Simone Belli</li>
			</ul>
			<ul>
				<li><strong>Effects of digital transformation in scientific
						collaborations</strong><br> Simone Belli</li>
			</ul>
			<ul>
				<li><strong>glossaLAB: Co-Creating Interdisciplinary Knowledge</strong><br>
					José María Díaz-Nafría, Teresa Guarda, Mark Burgin, Wolfgang
					Hofkirchner, Rainer Zimmermann, Gerhard Chroust, Simone Belli</li>
			</ul>
			<ul>
				<li><strong>ICT and Science: How applied informatics change
						scientific networks</strong><br> Simone Belli, Ernesto Ponsot</li>
			</ul>
			<ul>
				<li><strong>ICTs connecting global citizens, global dialogue and
						global governance. A call for needful designs</strong><br>
					Wolfgang Hofkirchner, Peter Crowley, José María Díaz-Nafría,
					Wilfried Graf, Gudrun Kramer, Hans-Jörg Kreowski, Werner
					Wintersteiner</li>
			</ul>
			<ul>
				<li><strong>Introduction to the mathematical theory of knowledge
						conceptualization: Conceptual systems and structures</strong><br>
					Mark Burgin, José María Díaz-Nafría</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:30-17:00</td>
		<td colspan="3" class="active">Coffee Break</td>
	</tr>
	<tr>
		<td>17:00-18:00</td>
		<td colspan="3" class="success"><strong>Keynote: Mark Burgin:
				Introduction to the mathematical theory of knowledge
				conceptualization: Conceptual systems and structures</strong><br> <i>Salón
				de Juntas</i></td>
	</tr>
	<tr>
		<td>18:00-19:00</td>
		<td colspan="3" class="warning"><strong>glossaLAB Seminar </strong><br>
			<i>Salón de Juntas </i><br>Session Chair: Teresa Guarda</td>
	</tr>
	<tr>
	
	
	<tr>
		<td>19:00-20:00</td>
		<td colspan="3" class="active"><strong>Conference Closing</strong><br>
			<i>Salón de Juntas</i></td>
	</tr>
</table>
<table class="table">
	<tr>
		<th width="10%">Time</th>
		<th width="90%" class="text-center">Saturday 9 November</th>
	</tr>
	<tr>
		<td>11:00-13:30</td>
		<td class="active">Cultural Activity</td>
	</tr>
</table>