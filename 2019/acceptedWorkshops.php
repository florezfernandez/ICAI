<h3>Accepted Workshops</h3>

<h4>
	<strong>First International Workshop on Applied Informatics for Economics, Society
		and Development (AIESD 2019)
	</strong>
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Informatics for
	Economy, Society and Development organized by the Faculty of Economics
	of the National University of Loja (Ecuador) seeks to promote
	discussion and dissemination of current economics research, systems for
	territorial intelligence, e-participation, e-democracy and territorial
	economic development; with special emphasis on social and environmental
	development. The objective of this first edition of AIESD 2019 is to
	discuss aspects related to computer science applied in society. The
	idea is to learn the different techniques and tools that are used to
	study social phenomena, economic growth, the interaction between
	economic analysis and decision-making. The workshop seeks to promote an
	atmosphere of dialogue among the community of professionals working on
	issues related to technology and its application in the economy as well
	as society..
</p>
<br>
<h4>
	<strong>Second International Workshop on Data Engineering and Analytics (WDEA 2019)</strong>
</h4>
<p>
	<strong>Abstract:</strong> Data is everywhere. Data Analytics, Data
	Mining, Data Science or more generally the practice of identifying
	patterns or constructing mathematical models based on data is at the
	heart of most new business models or projects. This together with what
	has been termed as the Big Data phenomena, associated to data sets too
	big to be handled as usual, with non structured contents, data streams
	or all of these, has led to a whole new series of rapidly evolving
	tools, algorithms and methods which combine data engineering, computer
	science, statistics and mathematics. The aim of this Workshop is to
	present recent results in this area, discussing new methods or
	algorithms, or applications including Machine Learning, Databases,
	optimization or any type of algorithms which consider managing or
	obtaining value from data.
</p>
<br>
<h4>
	<strong>First International Workshop on Education and Technology Synergies for
		Current and Future Societies (EduSynergies 2019)
	</strong>
</h4>
<p>
	<strong>Abstract:</strong> A global sustainable information society
	needs, in the first place, the boosting of new behaviours adapted to
	the sustainability requirements at planetary scale, from the community
	level up to the global level. The traditional educational settings are
	not enough to cope with the multidimensional challenges we are facing
	nowadays. At the same time, the information technologies offer a
	panoply of learning pathways we should explore to deploy the education
	we need to build up a global sustainable future. During last two
	decades we have been able to see how digital transformation has change
	our societies as well as our educational systems. Learning and teaching
	meth- ods have evolved in combination with new technologies. The
	expectations of students from all educational levels, subjects of study
	and age grow along with the increase of their own daily life technology
	use. Therefore, we propose a workshop where exchange knowledge about
	digital transformation in learning and education.
</p>
<br>
<h4>
	<strong>Third International Workshop on Information and Knowledge in
		Internet of Things (IKIT 2019)
	</strong>
</h4>
<p>
	<strong>Abstract:</strong> Internet of Things (IoT) is currently one of
	the most challenging areas of the Internet, enabling ubiquitous
	computing between global networked machines and physical objects,
	providing a promising vision of the future integrating the real world
	of things with the virtual world of information. IoT is seen as a
	network of trillions of machines that communicate with each other,
	being a profound technological revolution, which is the current reality
	and the future of computing and communications, supported by a dynamics
	technological evolution in many fields, from wireless sensors, wireless
	sensor networks, to nanotechnology, and which rapidly gained global
	attention from academia, governments and industry. This workshop
	explores the information and knowledge in the IoT, in particularly
	investigate data management and processing, information extraction,
	technology, knowledge management, knowledge sharing, and the
	development of new intelligent services available anytime, anywhere, by
	anyone and anything.
</p>
<br>
<h4>
	<strong>Second International Workshop on Intelligent Systems and Technologies for
		Interactive Human-Machine Relationships (ISTIHMR 2019)
	</strong>
</h4>
<p>
	<strong>Abstract:</strong> Intelligent Technologies for Interactive
	Human-Machine Relations will change our lives in the near future,
	creating new human-machine relationships that will lead to a new
	Intelligent Technologies society. ITIHMR is focusing on advanced
	technological innovations as well as on social aspects. We invite
	researchers and practitioners to report on up-to-date innovation and
	development, summarize the state of the art, ideas and advances in all
	aspects of Intelligent Systems and Technologies for Interactive
	Human-Machine Relationships.
</p>
<br>
<h4>
	<strong>Second International WorkShop on Smart Sustainable Cities (WSSC 2019)</strong>
</h4>
<p>
	<strong>Abstract:</strong> The International Telecommunication Union
	ITU define a smart sustainable city as : “An innovative city that uses
	information and communication technologies (ICTs) and other means to
	improve quality of life, efficiency of urban operation and services,
	and competitiveness, while ensuring that it meets the needs of present
	and future generations with respect to economic, social, environmental
	as well as cultural aspects”. The aim of this Workshop is to present
	recent results in this area, discussing new methods or algorithms, or
	applications in different areas including architecture, cultural,
	tourism, mobility, among others.
</p>
<br>
<h4>
	<strong>First International Workshop on Video Games, Gamification and Educational
		Innovation (VGameEdu 2019)
	</strong>
</h4>
<p>
	<strong>Abstract:</strong> The subject is of great interest every time,
	that the introduction of video games in education has raised a series
	of possibilities but also problems and doubts about the true
	potentialities. This process integrated into the progressive
	digitalization of the classrooms has put on the table a series of
	problematizations about how and for what videogames can be used to
	teach. The concepts of gamification (or ludification) are increasingly
	common, especially in the fields of marketing, but starting to see
	their advantages also for The learning; and the bibliography on the
	subject is growing. Simply put the word gamification in the search
	engine, so that thousands of examples of the impact it is having on the
	education and other areas. Currently, the development of games applied
	to the teaching of all types of subjects, in formal and informal
	education, not only is it very useful for learning, but it is essential
	to attract the interest of the people of the XXI century to the
	knowledge of different issues.
</p>