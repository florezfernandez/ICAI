<h3>Important Dates</h3>
<ul>
<li>Paper Submission: <del>June 9<sup>th</sup> 2019</del> June 16<sup>th</sup> 2019</li>
<li>Paper Notification: <del>July 7<sup>th</sup> 2019</del> July 14<sup>th</sup> 2019</li>
<li>Camera Ready: <del>August 4<sup>th</sup> 2019</del> August 11<sup>th</sup> 2019</li>
<li>Authors Registration: <del>August 4<sup>th</sup> 2019</del> August 11<sup>th</sup> 2019</li>
</ul>
