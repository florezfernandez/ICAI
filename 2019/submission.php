<h3>Submission Guidelines</h3>
<p>
	Authors must submit an original full paper (12 to 15 pages) that has
	not previously been published. All contributions must be written in
	english. Authors should consult Springer's authors' guidelines and use
	their proceedings templates, either for LaTeX or for MSWord (<a
		href="http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines"
		target="_blank">http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines</a>),
	for the preparation of their papers. Springer encourages authors to
	include their ORCIDs in their papers. In addition, the corresponding
	author of each accepted paper, acting on behalf of all of the authors
	of that paper, must complete and sign a <a href="CCIS_ICAI_2019.pdf"
		target="_blank">Consent-to-Publish</a> form, through which the
	copyright for their paper is transferred to Springer.
</p>

<p>
	We encourage Overleaf users to use Springer's proceedings template for
	LaTeX <a
		href="https://www.overleaf.com/latex/templates/springer-lecture-notes-in-computer-science/kzwwpvhwnvfj"
		target="_blank"><span class="glyphicon glyphicon-new-window"
		aria-hidden="true"></span></a>
</p>
<div class="text-center">
	<a href="https://www.overleaf.com" target="_blank"><img
		src="../img/logos/overleaf.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="Overleaf"></a>
</div>

In addition, Overleaf users can easily download and submit papers by
using the Overleaf menu
<i>Submit</i>
. By using this service, users can search the conference typing "applied
informatics" By clicking
<i>Submit to ICAI</i>
, users can download the LaTeX files and PDF file, as well as submit the
paper using the corresponding buttoms.

<h3>Review Process</h3>
<p>
	All submissions will be reviewed by 3 experts. <strong>Authors must
		remove personal details, acknowledgments section and any other
		information related to the authors' identity</strong>. In addition,
	all submissions will be screened by iThenticate.
</p>
<div class="text-center">
	<a href="http://www.ithenticate.com/" target="_blank"><img
		src="../img/logos/ithenticate.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="iThenticate"></a>
</div>

<h3>Submission Process</h3>

<p>
	To submit or upload a paper please go to <a
		href="https://easychair.org/conferences/?conf=icai2019"
		target="_blank">https://easychair.org/conferences/?conf=icai2019</a>.
	<strong>The first anonymous version must be submitted in PDF</strong>
</p>
<div class="text-center">
	<a href="https://easychair.org" target="_blank"><img
		src="../img/logos/easychair.png" width="150" data-toggle="tooltip"
		data-placement="bottom" title="Easychair"></a>
</div>

<h3>Proceedings</h3>

<p>
	ICAI 2019 proceedings will be published with Springer in their <strong><a
		href="http://www.springer.com/series/7899" target="_blank">Communications
			in Computer and Information Science (CCIS) series</a></strong>.
</p>

<div class="text-center">
	<a href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer.jpg" width="250" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="http://www.springer.com/series/7899" target="_blank"><img
		src="../img/logos/ccis.jpg" width="150" data-toggle="tooltip"
		data-placement="bottom"
		title="Communications in Computer Information Science"></a><br> <a
		href="https://www.scimagojr.com/journalsearch.php?q=17700155007&amp;tip=sid&amp;exact=no"
		title="SCImago Journal &amp; Country Rank" target="_blank"><img
		border="0"
		src="https://www.scimagojr.com/journal_img.php?id=17700155007"
		alt="SCImago Journal &amp; Country Rank" /></a>
</div>


<h3>Abstracting/Indexing</h3>
<p>CCIS is abstracted/indexed in DBLP, Google Scholar, EI-Compendex,
	SCImago, Scopus. CCIS volumes are also submitted for the inclusion in
	ISI Proceedings.</p>

<div class="text-center">
	<a href="http://dblp.org/" target="_blank"><img
		src="../img/logos/dbpl.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="DBPL"></a> <a
		href="https://scholar.google.com" target="_blank"><img
		src="../img/logos/googlescholar.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Google Scholar"></a> <a
		href="https://www.elsevier.com/solutions/engineering-village/content/compendex"
		target="_blank"><img src="../img/logos/ei.png" height="100"
		data-toggle="tooltip" data-placement="bottom" title="Ei Compendex"></a>
	<a href="http://www.scimagojr.com/" target="_blank"><img
		src="../img/logos/scimago.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="SCImago"></a> <a
		href="https://www.scopus.com/" target="_blank"><img
		src="../img/logos/scopus.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Scopus"></a> <a
		href="https://webofknowledge.com/" target="_blank"><img
		src="../img/logos/isi.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="ISI Web of Knowledge"></a>
</div>

<h3>Conditions</h3>
<ul>
	<li><strong>All accepted papers must be presented by an author, who must be
		registered</strong></li>
	<li><strong>Authors of accepted papers <span class="text-danger">cannot be
			changed</span></strong>.
	</li>
</ul>

<h3>Associated Journals</h3>
<p>Extended version of selected papers will be invited to publish in:</p>
<ul>
	<li>International Journal of Human Capital and Information Technology
		Professionals <a
		href="https://www.igi-global.com/journal/international-journal-human-capital-information/1152"
		target="_blank"><span class="glyphicon glyphicon-new-window"
			aria-hidden="true"></span></a>
		<ul>
			<li>Indexed In: ACM Digital Library, Cabell's Directories, DBLP,
				Google Scholar, INSPEC, JornalITOCs, MediaFinder, The Standard
				Periodical Directory, Ulrich's Periodicals Directory, Scopus, Web of
				Science</li>
			<li><a
				href="https://www.scimagojr.com/journalsearch.php?q=21100223336&amp;tip=sid&amp;exact=no"
				title="SCImago Journal &amp; Country Rank" target="_blank"><img
					border="0"
					src="https://www.scimagojr.com/journal_img.php?id=21100223336"
					alt="SCImago Journal &amp; Country Rank" /></a></li>
		</ul>
	</li>
	<li>Covenant Journal of Informatics and Communication Technology <a
		href="https://journals.covenantuniversity.edu.ng/index.php/cjict/index"
		target="_blank"><span class="glyphicon glyphicon-new-window"
			aria-hidden="true"></span></a>
		<ul>
			<li>Indexed In: Google Scholar</li>
			<li>Submited for the inclusion in Scopus</li>
		</ul>


	</li>
	<li>Smart Learning Environments <a
		href="https://slejournal.springeropen.com/" target="_blank"><span
			class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
		<ul>
			<li>Submited for the inclusion in Web of Science and Scopus</li>
		</ul>
	</li>
</ul>
<h3>Associated Books</h3>
<p>Extended version of selected papers will be invited to publish in:</p>
<ul>
	<li>Advances in IT Personnel and Project Management <a
		href="https://www.igi-global.com/book-series/advances-personnel-project-management/77666"
		target="_blank"><span class="glyphicon glyphicon-new-window"
			aria-hidden="true"></span></a>. IGI Global
	</li>
</ul>