<div class="col-md-3 text-center">
<img src="../img/logos/icai.png" width="200">
</div>
<div class="col-md-7">
<h3><i><strong>Second International Conference on Applied Informatics</strong></i></h3>
<h4>6 to 9 November 2019, Madrid, Spain</h4>
<h4><i class="text-success">Building up a Global Sustainable Information Society</i></h4>
</div>
<div class="col-md-2 text-center">
<img src="../img/logos/icaimadrid.png" width="180">
</div>