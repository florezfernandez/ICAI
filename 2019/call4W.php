<h3>Call for Workshops Proposals</h3>
<p>ICAI 2019 will host selected workshops during the main conference.
	The workshops will provide a collaborative forum for participants to
	exchange recent or preliminary results, to conduct intensive
	discussions on a particular topic related to applied informatics, and
	to coordinate efforts between representatives of a technical community.
	Workshops are intended as a forum for lively discussion of innovative
	ideas, recent progress, and practical approaches. We invite prospective
	workshop organizers to submit proposals for highly interactive
	workshops focusing on areas related to applied informatics. Both
	research oriented and applied topics are welcome.</p>

<h3>Important Dates</h3>
<ul>
	<li>Workshop Proposal Submission: June 1<sup>st</sup> 2019
	</li>
	<li>Workshop Proposal Notification: June 15<sup>th</sup> 2019
	</li>
</ul>

<h3>Workshop Proposal Guidelines</h3>

<p>Workshop proposal must contain the following information:
<ul>
	<li>Workshop title and acronym</li>
	<li>Name, affiliation, and e-mail addresses of the workshop chairs</li>
	<li>Abstract (200 words) for the ICAI Workshops web site.</li>
	<li>Motivation</li>
	<li>Objectives</li>
	<li>Topics of interest</li>
	<li>Intended audience</li>
	<li>Relevance to ICAI 2018 conference</li>
	<li>Information about previous workshop editions</li>
	<li>Call for Papers draft</li>
	<li>URL of the workshop web site (or a draft)</li>
</ul>

<h3>Submission Guidelines</h3>
<p>
	Please follow the workshop proposal guidelines and provide all
	requested information using at most 7 pages, including the CfP draft.
	Submissions must adhere to the Springer template (<a
		href="http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines"
		target="_blank">http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines</a>)
</p>
<p>The proposal must be submitted as a PDF file to the workshop chairs:
	Jaime Chavarriaga &lt;ja.chavarriaga908@uniandes.edu.co&gt;, Cesar Diaz
	&lt;cesaro.diazb@utadeo.edu.co&gt;, and Olmer Garcia
	&lt;olmer.garciab@utadeo.edu.co&gt;</p>

<h3>Proceedings</h3>
<p>Proceedings shall be submitted to CEUR-WS.org for online publication</p>	
