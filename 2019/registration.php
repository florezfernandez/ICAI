<h3>Registration</h3>
<p>
	At least one author of each accepted paper must register by August 11<sup>th</sup>
	2019 to ensure its inclusion in the conference proceedings. Each
	registration will allow the publication and presentation of just one
	paper. Authors must pay an extra fee if they wish to publish and
	present more than one paper. One registration permits only the
	participation of one author in the conference. In addition, the
	registration fee includes:
</p>
<ul>
	<li>Conference material</li>
	<li>Lunch on working days of the conference</li>
	<li>Coffee breaks</li>
</ul>

<h3>Registration Fees</h3>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<table class="table table-hover table-bordered">
				<tr class="info">
					<th class="text-center">Registration</th>
					<th class="text-center">Fee</th>
				</tr>
				<tr>
					<td>Regular Author</td>
					<td class="text-center">350 EUR</td>
				</tr>
				<tr>
					<td>Student Author <br>Workshop Author
					</td>
					<td class="text-center">250 EUR</td>
				</tr>
				<tr>
					<td>One Additional Paper</td>
					<td class="text-center">150 EUR</td>
				</tr>
				<tr>
					<td>Poster Author</td>
					<td class="text-center">150 EUR</td>
				</tr>
				<tr>
					<td>Attendee</td>
					<td class="text-center">100 EUR</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<p>Registration instructions are sent to authors of accepted papers in
	the corresponding acceptance notification.</p>
<p>
	People interested in registering as Attendee, please send a message
	through the <a href="http://icai.itiud.org/index.php?pid=contact">Contact</a>
	form.

</p>
