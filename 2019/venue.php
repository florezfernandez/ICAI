<h3>Venue</h3>
<p>
	ICAI 2019 will be held at the <strong>Universidad Abierta de Madrid
		UDIMA and Universidad Complutense de Madrid, Somosaguas Campus</strong>
	located in <strong>Madrid, Spain</strong>.
</p>
<iframe
	src="https://www.google.com/maps/d/embed?mid=1JfhW9pk2rYRxJlkogmN2rBX1rqJPlLb9"
	width="640" height="480"></iframe>

<h3>Transportation</h3>
<h4>
	From the Madrid's international airport <i>Barajas</i> to Campus
</h4>
<p>
	From the Madrid's international airport <strong>Barajas</strong>, there
	is the following option to go to the <i>Universidad Complutence de
		Madrid</i>.
</p>
<ul>
	<li><strong>Consorcio Regional de Transportes de Madrid</strong>. It is
		the public transportation system in Madrid. If you have a phone in
		your hands with data access, we recommend you to look for a suited
		connection using some route-finder (e.g. google-maps), which may vary
		depending on several circumstances. Nevertheless the fastest
		combinations are usually:
		<ul>
			<li>Subway-Line 8 until Nuevos Ministerios > Subway-Line 6 until
				Moncloa > Blue Bus (EMT) route A until Campus Somosaguas of
				Universidad Complutense (aprox. transfer duration less than 1h 20
				min. You need to buy a Subway ticket in Barajas and a bus ticket in
				Moncloa, direct in the )</li>
			<li>If you arrive at Terminal 4 (T4) it may be convenient to take:
				Local train (Cercanias Renfe) C1 until Chamartin > Subway-Line 10
				until Colonia Jardín > Light train (Metro ligero) ML2 until Campus
				de Somosaguas (prox. transfer duration less than 1h 20 min, but the
				frequency of the local train is 15-20 min. You need to buy a combi
				ticket at Barajas airport, which cover the train and subway
				connections).</li>
		</ul></li>
	<li>In case you are traveling with some partner it may be convenient to
		use some transfer service (as it can be <a
		href="https://www.mytransfers.com/es/?gclid=EAIaIQobChMI-ef_uI6w5QIVWp3VCh0YMQUdEAAYASAAEgLm9_D_BwE"
		target="_blank">my-transfers</a> or <a href="https://gettransfer.com"
		target="_blank">get-transfers</a>) through which you may pre-arrange
		the transfer for the day of your arrival and the day of return. In
		case the transfer is arranged from Airport to Campus Somosaguas in
		both directions, the cost may be below 80 eur for 4 passengers and the
		duration of the transfer below 30 min. If the round-trip transfer is
		pre-arranged to Moncloa (nearby is where the recommended
		accommodations are located) the duration may be similar.
	</li>
	<li>Of course, you can also use a regular taxi or other shared
		transport options (Uber, Cabify). The cost may be around 35-40 eur and
		the transfer duration is below 30 min.</li>
</ul>

<h4>From Madrid city center to campus</h4>


<p>From the city quartier of Moncloa, where the recommended
	accommodations are located, the best option to the conference venue is
	using Blue Bus (EMT) route A until Campus Somosaguas of Universidad
	Complutense (the transfer last about 15 min, for which you need a bus
	ticket). If you are lodged in other quartiers in city center, the more
	convenient will usually be arriving to Moncloa (using bus or subway).</p>

<p>If you wish a more adventurous arrival, you can ask about how to
	cross the river and get across the large and beautiful city park of
	Casa de Campo, painted in the famous Goya’s masterpiece “La Pradera de
	San Isidro”. We can provide you information during the conference.</p>

<h4>Moving in the Campus</h4>
<p>When you are in Campus Somosaguas of Universidad Complutense de
	Madrid, you may go to the Faculty of Politics.</p>

<h4>Schedules of useful transfer lines</h4>
<ul>
	<li>Blue Buses: Route A (Moncloa - Canpus Somosaguas): Monday to Friday
		7:30 – 21:53. Saturday 8:00 14:40. (Autobuses urbanos de Madrid).</li>
	<li>Subway: Line 8/Pink (Nuevos Ministerios - Aeropuerto T4): Monday to
		Sunday 6:05 – 2:00. (Metro)</li>
	<li>Subway: Line 10/dark blue (Hospital Infanta Sofia - Puerta del
		Sur): Monday to Sunday 6:05 – 2:00. (Metro)</li>
	<li>Subway: Linea 2 (Colonia Jardin - Estación Aravaca): Monday to
		Sunday 6:00 – 2:00. (Metro Ligero)</li>
	<li>Subway: Line 3/Yellow (Villaverde Alto - Moncloa): Monday to Sunday
		6:05 – 2:00. (Metro)</li>

</ul>
