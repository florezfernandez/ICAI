<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ICAI 2019</title>
<link rel="icon" type="image/png" href="../img/logos/icai2.png" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/chartkick/2.3.0/chartkick.min.js"></script>
<script src="https://www.gstatic.com/charts/loader.js"></script>

<script charset="utf-8">
      $(function () { 
        $("[data-toggle='tooltip']").tooltip(); 
      });
</script>
</head>
<body>
	<br>
	<div class="container">
		<div class="row">
		<?php include 'header.php';?>		
		</div>
		<div class="row">
		<?php include 'menu.php';?>		
		</div>
<?php
if (empty($_GET['pid'])) {
    include 'home.php';
} else {
    include $_GET['pid'] . '.php';
}
?>
		<?php include 'sponsors.php' ?>
		<?php include '../ui/visitors.php' ?>
		
		<div class="text-center text-muted">
			&copy; ITI Research Group<br><?php echo date("Y")?> All rights reserved
    	</div>
	</div>
	<br>
</body>
</html>