
<h3>Accepted Papers</h3>
<h4>Bioinformatics</h4>
<ul>
    <li><strong>Bioinformatics Methods to discover antivirals against Zika virus</strong>
        <ul>
            <li>Karina Salvatierra, Universidad Nacional de Misiones, Argentina</li>
            <li>Marcos Vera, Universidad Nacional de Misiones, Argentina</li>
            <li>Hector Florez, Universidad Distrital Francisco Jose de Caldas, Colombia</li>
        </ul></li>
</ul>

<h4>Data Analysis</h4>
<ul>
    <li><strong>Academic behavior analysis in virtual courses using a data mining approach</strong>
        <ul>
            <li>Dario Delgado-Quintero, Universidad Nacional Abierta y a Distancia, Colombia</li>
            <li>Olmer Garcia, Universidad Jorge tadeo Lozano, Colombia</li>
            <li>Pablo Munevar-Garcia, Universidad Nacional Abierta y a Distancia, Colombia</li>
            <li>Diego Aranda-Lozano, Universidad Nacional Abierta y a Distancia, Colombia</li>
            <li>Cesar Diaz, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
        </ul></li>

    <li><strong>Analysis of Usability of Various Geosocial Network POI in Tourism</strong>
        <ul>
            <li>Jiri Kysela, Metropolitan University Prague, Czechia</li>
        </ul></li>

    <li><strong>Application of the Requirements Elicitation Process for the Construction of Intelligent 
            System-based Predictive Models in the Education Area</strong>
        <ul>
            <li>Cinthia Vegega, Universidad Tecnologica Nacional Facultad Regional Buenos Aires, Argentina</li>
            <li>Pablo Pytel, Universidad Tecnologica Nacional Facultad Regional Buenos Aires, Argentina</li>
            <li>Maria Florencia Pollo-Cattaneo, Universidad Tecnologica Nacional Facultad Regional Buenos Aires, Argentina</li>
        </ul></li>

    <li><strong>Evaluating Student Learning Effect based on Process Mining</strong>
        <ul> 
            <li>Yu Wang,Beijing University of Technology, China</li>
            <li>Tong Li, Beijing University of Technology, China</li>
            <li>Congkai Geng, Beijing University of Technology, China</li>
            <li>Yihan Wang, Beijing University of Technology, China</li>           
        </ul></li>

    <li><strong>Evalu@: an Agnostic Web-based Tool for Consistent and Constant Evaluation Used as a Data Gatherer 
            for Artificial Intelligence Implementations</strong>
        <ul> 
            <li>Fernando Yepes-Calderon, GYM Group SA, United States</li>
            <li>Eduardo Yepes-Calderon, GYM Group SA, Colombia</li>
            <li>Juan Fernando Yepes-Zuluaga, Strategic Business Platforms, United States</li>         
        </ul></li>

    <li><strong>Model for resource allocation in decentralized networks using Interaction Nets</strong>
        <ul> 
            <li>Joaquín Sanchez, Universidad Manuela Beltrán, Colombia</li>
            <li>Juan P Ospina, Fundación Universitaría San Mateo, Colombia</li>
            <li>Carlos Andrés Collazos Morales, Universidad Manuela Beltrán, Colombia</li>       
            <li>Henry Avendaño, Universidad Manuela Beltrán, Colombia</li>
            <li>Paola Ariza Colpas, Universidad de la Costa, Colombia</li>    
            <li>Vanesa Landero, Universidad de Apodaca, Mexico</li>    
        </ul></li>

    <li><strong>RefDataCleaner: A Usable Data Cleaning Tool</strong>
        <ul> 
            <li>Juan Carlos Leon Medina, Universidad Jorge Tadeo Lozano, Colombia</li>
            <li>Ixent Galpin, Universidad Jorge Tadeo Lozano, Colombia</li>
        </ul></li>

    <li><strong>Study of the crime status in Colombia and Development of a citizen security App</strong>
        <ul> 
            <li>Raquel Canon, firefly-e, Colombia</li>
            <li>Cesar Diaz, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
            <li>Olmer Garcia-Bedoya, Universidad Jorge tadeo Lozano, Colombia</li>
            <li>Holman Diego Bolivar Baron, Universidad Catolica de Colombia, Colombia</li>
        </ul></li>

    <li><strong>University Quality Measurement Model Based On Balanced Scorecard</strong>
        <ul> 
            <li>Thalia Obredor Baldovino, Universidad de la Costa, Colombia</li>
            <li>Harold Combita Niño, Universidad de la Costa, Colombia</li>
            <li>Tito J. Crissien Borrero, Universidad de la Costa, Colombia</li>
            <li>Emiro De La Hoz Franco, Universidad de la Costa, Colombia</li>
            <li>Diego Beltrán, Escuela Colombiana de Ingeniería, Colombia</li>
            <li>Iván Ruiz, Universidad Manuela Beltrán, Colombia</li>
            <li>Carlos Andrés Collazos Morales, Universidad Manuela Beltrán, Colombia</li>
            <li>Joaquín Sanchez, Universidad Manuela Beltrán, Colombia</li>
        </ul></li>
</ul>

<h4>Decision Systems</h4>
<ul>
    <li><strong>Algorithmic Discrimination and Responsibility: Case Studies from USA and South America</strong>
        <ul>
            <li>Musonda Kapatamoyo, Southern Illinois University-Edwardsville, United States</li>
            <li>Yalitza Ramos-Gil, Pontificia Universidad Católica, Ecuador</li>
            <li>Carmelo Márquez-Domínguez, Pontificia Universidad Católica del Ecuador, Ecuador</li>
        </ul></li>

    <li><strong>Continuous Variable Binning Algorithm to Maximize Information Value using Genetic Algorithm</strong>
        <ul>
            <li>Nattawut Vejkanchana, National Institute of Development Administration, Thailand</li>
            <li>Pramote Kucharoen, National Institute of Development Administration, Thailand</li>
        </ul></li>

    <li><strong>CVRPTW model for cargo collection with heterogeneous capacity fleet</strong>
        <ul>
            <li>Jorge Ivan Romero Gelvez, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
            <li>William Camilo Gonzalez Cogua, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
            <li>Jorge Aurelio Herrera Cuartas, Universidad de Bogota Jorge Tadeo Lozano, Colombia</li>
        </ul></li>

    <li><strong>Evaluation of Transfer Learning Techniques with Convolutional Neural Networks (CNNs) to Detect 
            the Existence of Roads in High-Resolution Aerial Imagery</strong>
        <ul>
            <li>Calimanut-Ionut Cira, Universidad Politécnica de Madrid, Spain</li>
            <li>Ramon Alcarria, Universidad Politécnica de Madrid, Spain</li>
            <li>Miguel-Ángel Manso-Callejo, Universidad Politécnica de Madrid, Spain</li>
            <li>Francisco Serradilla, Universidad Politécnica de Madrid, Spain</li>
        </ul></li>

    <li><strong>Predicting stock prices using LSTM</strong>
        <ul>
            <li>Vu Nguyen, University of Science, Vietnam National University, Viet Nam</li>
            <li>Duc Nguyen Huu Dat, University of Science, Vietnam National University, Viet Nam</li>
            <li>Loc Tran Phuoc, University of Science, Vietnam National University, Viet Nam</li>
        </ul></li>
</ul>

<h4>Health Care Information Systems</h4>
<ul>
    <li><strong>Hyperthermia Study in Breast Cancer Treatment using a new Applicator</strong>
        <ul>
            <li>Hector Fabian Guarnizo Mendez, Universidad El Bosque, Colombia</li>
            <li>Mauricio Andrés Poloché Arango, Universidad de San Buenaventura, Colombia</li>
            <li>Juan Fernando Coronel Rico, Universidad El Bosque, Colombia</li>
            <li>Tatiana Angelica Rubiano Suazo, Universidad El Bosque, Colombia</li>
        </ul></li>

    <li><strong>Manual Segmentation Associated Errors in Medical Imaging. Proposing a more reliable Gold Standard</strong>
        <ul>
            <li>Fernando Yepes-Calderon, Children's Hospital, United States</li>
            <li>J. Gordon McComb, Children's Hospital, United States</li>
        </ul></li>
</ul>

<h4>IT Architectures</h4>
<ul>
    <li><strong>A proposal model based on Blockchain technology to support traceability of 
            Colombian scholar alimentation program (PAE)</strong>
        <ul>
            <li>Alejandro Guzmán, Universidad EAN, Colombia</li>
            <li>Camilo Rincón, Universidad EAN, Colombia</li>
            <li>Carol Cortés, Universidad EAN, Colombia</li>
            <li>Catherine Torres, Universidad EAN, Colombia</li>
            <li>Camilo Mejía-Moncayo, Universidad EAN, Colombia</li>
        </ul></li>

    <li><strong>Capacity of Desktop Clouds for running HPC applications: a revisited analysis</strong>
        <ul>
            <li>Jaime Chavarriaga, Universidad de Los Andes, Colombia</li>
            <li>Carlos Gomez, Universidad del Quindio, Colombia</li>
            <li>Harold Castro, Universidad de Los Andes, Colombia</li>
            <li>David Camilo Bonilla, Universidad de los Andes, Colombia</li>
        </ul></li>

    <li><strong>Migration to Microservices: Barriers and Solutions</strong>
        <ul>
            <li>Javad Ghofrani, HTW Dresden University of Applied Sciences, Germany</li>
            <li>Arezoo Bozorgmehr, Uni Klinkum Bonn, Germany</li>
        </ul></li>

    <li><strong>Towards a maturity model for cloud service customizing</strong>
        <ul>
            <li>Oscar Avila, Universidad de Los Andes, Colombia</li>
            <li>Cristian Paez, Universidad de Los Andes, Colombia</li>
            <li>Dario Correal, Universidad de Los Andes, Colombia</li>
        </ul></li>

    <li><strong>Towards an architecture framework for the implementation of omnichannel strategies</strong>
        <ul>
            <li>Nestor Suarez, Universidad de Los Andes, Colombia</li>
            <li>Oscar Avila, Universidad de Los Andes, Colombia</li>
        </ul></li>
</ul>

<h4>Learning Management Systems</h4>
<ul>
    <li><strong>Connecting CS1 with student's careers through multidisciplinary projects. 
            Case of study: Material selection following the Ashby Methodology</strong>
        <ul>
            <li>Miguel Realpe, ESPOL, Ecuador</li>
            <li>Bruno Paucar, Escuela Superior Politénica del Litoral, Ecuador</li>
            <li>Giovanny Eduardo Chunga Santos, Escuela Superior Politénica del Litoral, Ecuador</li>
            <li>Clotario Tapia, Escuela Superior Politénica del Litoral, Ecuador</li>
            <li>Natalia Lopez, Escuela Superior Politénica del Litoral, Ecuador</li>
        </ul></li>

    <li><strong>Development of Online Clearance System for an Educational Institution</strong>
        <ul>
            <li>Oluranti Jonathan, Covenant University, Nigeria</li>
            <li>Sanjay Misra, Covenant University, Nigeria</li>
            <li>Funmilayo Makinde, Covenant University, Nigeria</li>
            <li>Robertas Damasevicius, Kaunas University of Technology, Lithuania</li>
            <li>Rytis Maskeliunas, Kaunas University of Technology, Lithuania</li>
            <li>Marcelo Leon, Universidad Nacional de Loja, Ecuador</li>
        </ul></li>

    <li><strong>Enhanced Sketchnoting through Semantic Integration of Learning Material</strong>
        <ul>
            <li>Aryobarzan Atashpendar, University of Luxembourg, Luxembourg</li>
            <li>Christian Grévisse, University of Luxembourg, Luxembourg</li>
            <li>Steffen Rothkugel, University of Luxembourg, Luxembourg</li>
        </ul></li>
</ul>

<h4>Robotic Autonomy</h4>
<ul>
    <li><strong>Comparative Analysis of Three Obstacle Detection And Avoidance Algorithms 
            For A Compact Differential Drive Robot I N V-Rep</strong>
        <ul>
            <li>Chika Yinka-Banjo, University of Cape Town, South Africa</li>
            <li>Obawole Daniel, Uni of Lagos, Nigeria</li>
            <li>Sanjay Misra, Covenant University, Nigeria</li>
            <li>Hector Florez, Universidad Distrital Francisco Jose de Caldas, Colombia</li>
        </ul></li>
</ul>

<h4>Security Services</h4>
<ul>
    <li><strong>A Secured Private-Cloud Computing System</strong>
        <ul>
            <li>Modebola Olowu, Union Bank of Nigeria. Plc, Nigeria</li>
            <li>Chika Yinka-Banjo, University of Cape Town, South Africa</li>
            <li>Hector Florez, Universidad Distrital Francisco Jose de Caldas, Colombia</li>
            <li>Sanjay Misra, Covenant University, Nigeria</li>
        </ul></li>

    <li><strong>Comparative Evaluation of Techniques for Detection of Phishing URLs</strong>
        <ul>
            <li>Oluwafemi Osho, FUT Minna, Nigeria</li>
            <li>Ayanfeoluwa Oluyomi, FUT MINNA, Nigeria</li>
            <li>Sanjay Misra, Covenant University, Nigeria</li>
            <li>Dr.Ravin Ahuja, Save Life Engineering, India</li>
            <li>Robertas Damasevicius, Kaunas University of Technology, Lithuania</li>
            <li>Rytis Maskeliunas, Kaunas University of Technology, Lithuania</li>
        </ul></li>
</ul>

<h4>Socio-technical Systems</h4>
<ul>
    <li><strong>Cultural Archaeology of video games: between nostalgic discourse, 
            gamer experience and technological innovation</strong>
        <ul>
            <li>Cristian López Raventós, Escuela Nacionald de Estudios Superiores, Unidad Morelia (UNAM), Mexico</li>
            <li>Simone Belli, Universidad Complutense de Madrid, Spain</li>
        </ul></li>

    <li><strong>Effects of digital transformation in scientific collaborations</strong>
        <ul>
            <li>Simone Belli, Universidad Complutense de Madrid, Spain</li>
        </ul></li>

    <li><strong>glossaLAB: Co-Creating Interdisciplinary Knowledge </strong>
        <ul>
            <li>José María Díaz-Nafría, Madrid Open University, Spain</li>
            <li>Teresa Guarda, Universidad Estatal Península de Santa Elena, Ecuador</li>
            <li>Mark Burgin, University of California, Los Angeles, United States</li>
            <li>Wolfgang Hofkirchner, Institute for a Global Sustainable Information Society, Austria</li>
            <li>Rainer Zimmermann, Institute for Design Science, Germany</li>
            <li>Gerhard Chroust, Institute for a Global Sustainable Information Society, Austria</li>
            <li>Simone Belli, Universidad Complutense de Madrid, Spain</li>
        </ul></li>

    <li><strong>ICT and Science: How applied informatics change scientific networks</strong>
        <ul>
            <li>Simone Belli, Universidad Complutense de Madrid, Spain</li>
            <li>Ernesto Ponsot, Yachay Tech, Ecuador</li>
        </ul></li>

    <li><strong>ICTs connecting global citizens, global dialogue and global governance. 
            A call for needful designs </strong>
        <ul>
            <li>Wolfgang Hofkirchner, Vienna University of Technology, Spain</li>
            <li>Peter Crowley, Paris Lodron University Salzburg, Austria</li>
            <li>José María Díaz-Nafría, University of Applied Sciences, Germany</li>
            <li>Wilfried Graf, Herbert C. Kelman Institute for Interactive Conflict Transformation, Austria</li>
            <li>Gudrun Kramer, Austrian Study Centre for Peace and Conflict Resolution, Austria</li>
            <li>Hans-Jörg Kreowski, University Bremen, Germany</li>
            <li>Werner Wintersteiner, Alpen-Adria Universität, Austria</li>
        </ul></li>

    <li><strong>Introduction to the mathematical theory of knowledge conceptualization: 
            Conceptual systems and structures </strong>
        <ul>
            <li>Mark Burgin, University of California, United States</li>
            <li>José María Díaz-Nafría, Madrid Open University, Spain</li>
        </ul></li>
</ul>

<h4>Software Design Engineering</h4>
<ul>
    <li><strong>Dynamic Interface and Access Model by Dead Token for IoT Systems</strong>
        <ul>
            <li>Karen Daza, Universidad Distrital Francisco Jose de Caldas, Colombia</li>
            <li>Jorge Hernandez, Universidad Distrital Francisco Jose de Caldas, Colombia</li>
            <li>Hector Florez, Universidad Distrital Francisco Jose de Caldas, Colombia</li>
            <li>Sanjay Misra, Covenant University, Nigeria</li>
        </ul></li>

    <li><strong>Evaluation of the Performance of Message Routing Protocols in Delay Tolerant 
            Networks (DTN) in Juan de Acosta – Barranquilla Scenario</strong>
        <ul>
            <li>Carlos Andrés Collazos Morales, Universidad Manuela Beltrán, Colombia</li>
            <li>Farid Meléndez Pertuz, Universidad de la Costa, Colombia</li>
            <li>Hermes Castellanos, Universidad Manuela Beltrán, Colombia</li>
            <li>Nazhir Amaya Tejera, Universidad de la Costa, Colombia</li>
            <li>Rubén Sánchez Dams, Universidad Nacional de Colombia</li>
            <li>José Simancas-García, Universidad de la Costa, Colombia</li>
            <li>Iván Ruiz, Universidad Manuela Beltrán, Colombia</li>
            <li>Fredy Sanz, Universidad Manuela Beltrán, Colombia</li>
            <li>César A Cárdenas, Universidad Manuela Beltrán, Colombia</li>
        </ul></li>

    <li><strong>Recovering Fine Grained Traceability Links Between Software Mandatory 
            Constraints and Source Code</strong>
        <ul>
            <li>Alejandro Velasco, Universidad Nacional de Colombia</li>
            <li>Jairo Aponte, Universidad Nacional de Colombia</li>
        </ul></li>

    <li><strong>Using Graph Embedding to Improve Requirements Traceability Recovery</strong>
        <ul>
            <li>Shiheng Wang, Beijing University of Technology, China</li>
            <li>Tong Li, Beijing University of Technology, China</li>
            <li>Zhen Yang, Beijing University of Technology, China</li>
        </ul></li>
</ul>