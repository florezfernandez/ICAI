<hr>
<h3>Organized by</h3>
<div class="text-center">
	<a href="https://www.udistrital.edu.co" target="_blank"><img
		src="../img/logos/ud.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Distrital Francisco Jose de Caldas"></a> <a
		href="http://www.utadeo.edu.co/" target="_blank"><img
		src="../img/logos/utadeo.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad de Bogota Jorge Tadeo Lozano"></a> <a
		href="https://www.ucm.es/" target="_blank"><img
		src="../img/logos/ucm.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Universidad Complutense de Madrid"></a>
	<a href="https://unl.edu.ec/" target="_blank"><img
		src="../img/logos/unl.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Universidad Nacional de Loja"></a> <a
		href="https://www.udima.es/" target="_blank"><img
		src="../img/logos/udima.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Universidad a Distancia de Madrid"></a>
	<a href="http://bitrum.unileon.es/" target="_blank"><img
		src="../img/logos/bitrum.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="BITrum Research Group"></a> <a
		href="https://www.upse.edu.ec/" target="_blank"><img
		src="../img/logos/upse.jpeg" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Estatal Peninsula de Santa Elena"></a>

</div>
<h3>Sponsored by</h3>
<div class="text-center">
	<a href="http://www.itiud.org" target="_blank"><img
		src="../img/logos/iti.png" height="100" data-toggle="tooltip"
		data-placement="bottom"
		title="Information Technologies Innovation Research Group"></a> <a
		href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer2.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="https://ec.europa.eu/programmes/erasmus-plus/node_en"
		target="_blank"><img src="../img/logos/erasmus.jpg" height="100"
		data-toggle="tooltip" data-placement="bottom" title="Erasmus+"></a> <a
		href="https://gsis.at/" target="_blank"><img src="../img/logos/gsis.png"
		height="100" data-toggle="tooltip" data-placement="bottom"
		title="The Institute for a Global Sustainable Information Society"></a>
</div>
<hr>