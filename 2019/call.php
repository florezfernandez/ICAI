<h3>Call for Papers</h3>
<p>ICAI aims to bring together researchers and practitioners interested
	in all topics related to informatics. Submitted papers should be
	related to one or more of the main topics proposed for the
	conference:</p>
<ul>
	<li>Applied Informatics for Less Developed Countries</li>
	<li>Artificial Intelligence</li>
	<li>Bioinformatics</li>
	<li>Business Analytics</li>
	<li>Cloud Computing</li>
	<li>Data Analysis</li>
	<li>Decision Systems</li>
	<li>Digital Transformation of Society</li>
	<li>e-Participation</li>
	<li>e-Democracy</li>
	<li>Educational Technology</li>	
	<li>Enterprise Information Systems Applications</li>
	<li>Ethic and Policies for a Digitalized World</li>
	<li>Gamification and Serious Games</li>
	<li>Geoinformatics</li>
	<li>Health Care Information Systems</li>
	<li>ICT-Enabled Social Innovation</li>
	<li>ICTs and Society</li>
	<li>ICTs in Education</li>
	<li>Information Ethics</li>	
	<li>Information Management for Sustainable Organizations</li>
	<li>Information Society</li>
	<li>Interdisciplinary Information Studies</li>
	<li>IT Architectures</li>
	<li>Learning Management Systems</li>
	<li>Management Cybernetics</li>
	<li>Mobile Information Processing Systems</li>
	<li>Robotic Autonomy</li>	
	<li>Social and Behavioral Applications</li>
	<li>Software and Systems Modeling</li>
	<li>Software Architectures</li>
	<li>Software Design Engineering</li>
	<li>Territorial Intelligence Systems</li>
</ul>

<?php 
include 'dates.php';
include 'submission.php';
?>