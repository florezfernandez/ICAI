<h3>Keynote Speakers for ICAI 2019</h3>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/lf.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Luis de-la-Fuente-Valentín</strong> <a
				href="https://www.unir.net/profesores/luis-de-la-fuente-valentin/"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a>
		</h4>
		<p>Luis de-la-Fuente-Valentín holds a PhD in Telematics Engineering,
			obtained at Universidad Carlos III de Madrid. He currently teachs at
			Universidad Internacional de La Rioja (UNIR), where he is also
			director of the Data Driven Science research group. He has
			participated in European and national funded projects related to the
			field of Technology Enhanced Learning and Analytics. His research
			areas include supporting systems for instructional desing, with the
			proposal of a generic service integration layer for the IMS-LD
			specification, and the use of such services to improve the workflow
			in complex learning settings. He lead the A4Learning Project that
			uses Learning Anatytics techniques to support the teacher in tutoring
			tasks by providing information of the estimated student progress.</p>
		<h4>
			<strong>Keynote: Learning Analytics: the use of Big Data in the
				classroom to improve the teaching/learning experience</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> The rise of Big Data and Artificial
			Intelligence techniques has impacted many áreas, and education is not
			out of the scope of such techonolgy. One result is the Learning
			Analytics field of study, whose aim is to provide methods and tools
			for the better understanding and improvement of the teaching/learning
			process. This talk will present the most relevant challenges in the
			way to adopt Learning Analytics as a efficient tool in the actual
			classroom, as well as some examples and ideas that show the potential
			of this research field.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/ig.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Ixent Galpin</strong> <a
				href="https://www.utadeo.edu.co/es/person/10546/Maestr%C3%ADa-en-Ingenier%C3%ADa-y-Anal%C3%ADtica-de-Datos"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a>
		</h4>
		<p>Ixent Galpin is full profesor at the Department of Engineering of
			Universidad de Bogota Jorge Tadeo Lozano, Colombia. He is head of the
			Masters in Data Engineering and Analytics programme and teaches
			Advanced Database Technologies at postgraduate level. Ixent holds a
			Ph.D. and in Computer Science, and a M.Sc. in Advanced Computer
			Science with Distinction, from the University of Manchester. He
			worked as Research Associate in the Information Management Group at
			the University of Manchester in the field of sensor network query
			processing. Previously, he worked as software engineer at Thomson
			Reuters developing financial data feeds. His research interests
			include data integration, usability issues in data management,
			recommender systems, and distributed query processing.</p>
		<h4>
			<strong>Keynote: Usability Issues in Data Science</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> Traditionally, data management benchmarks
			focus on measures such a throughput, pricing, energy consumption or
			scalability. Usability tends to be an afterthought. However, every
			data management system needs to interact with humans, and in order to
			be effective it is imperative that user requirements are suitably
			captured. In this talk, I will survey recent developments in this
			area and outline future research challenges.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/wh.png" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Wolfgang Hofkirchner</strong> <a
				href="http://www.hofkirchner.uti.at/about/" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
		</h4>
		<p>Wolfgang Hofkirchner is retired associate professor of technology
			assessment at the Technische Universität Wien, Austria. He was
			professor of Internet and society at the University of Salzburg,
			Austria. He has been visiting professor at the Universitat Oberta
			Catalunya, Spain; Universidad de León, Spain; and Universidade
			Federal da Bahía, Brazil. Wolfgang holds a PhD in Political Science
			and Psychology from the University of Salzburg. He is elected member
			of the Leibniz Society of Scholars in Berlin, of the International
			Academy of Systems and Cybernetic Sciences in Brussels, and
			distinguished researcher of the International Center for Philosophy
			of Information at the Xi’an Jiaotong University, Xi’an. He has
			expertise in ICTs and Society, Science of Information, and Complexity
			Thinking.</p>
		<h4>
			<strong>Keynote: Designing for a Global Sustainable Information
				Society</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> The Information Age is at the same time an
			Age of Global Challenges that might cause the extermination of
			mankind. However, Information Society can provide the means for
			coping with those challenges. A Global Sustainable Information
			Society represents a framework of conditions that guarantee thriving
			and surviving instead of running the risk of a self-inflicted
			breakdown of society. Information Technology needs to be questioned
			not only with regard to its efficacy and efficiency as a means but
			also, if not foremost, with regard to the justification of the
			diverse ends to which it serves as a means. Informatisation,
			digitalisation, automatisation need to be harnessed to the purpose of
			building up a “good society” and not of designing things we really
			don’t need.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/mp.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Mario Pérez-Montoro</strong> <a
				href="https://fbd.ub.edu/directori/ficha3" target="_blank"><span
				class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
		</h4>
		<p>Dr. Mario Pérez-Montoro is a Full Professor in the Faculty of
			Information and Media at University of Barcelona (Spain), where he
			researches and lectures in the areas of interaction design and
			information visualization. He is currently the Director of the PhD in
			Information and Communication Program at this University. He holds a
			PhD in Philosophy and Education from the University of Barcelona,
			Spain, and a Master in Information Management and Systems from the
			Polytechnic University of Catalonia, Spain. He studied at the
			Istituto di Discipline della Comunicazione at the University of
			Bologna (Italy), he has been a professor at different universities in
			Spain (Complutense University of Madrid, Open University of
			Catalonia, and Autonomous University of Barcelona), and he has been
			visiting scholar at the Center for the Study of Language and
			Information (CSLI) at Stanford University (California, USA) and at
			the School of Information at UC Berkeley (California, USA).</p>
		<h4>
			<strong>Keynote: Information Visualization as Knowledge Analysis</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> An important part of a researcher’s tasks
			focuses on searching, obtaining and refining a set of data that allow
			analyzing a phenomenon and finding the patterns that govern it. But
			the data does not speak for themselves. They need to be analyzed
			using methodological tools that allow conclusions to be drawn. Among
			these methodologies, the visualization of information stands out as a
			complementary strategy for the analysis of the phenomena that involve
			data. Information Visualization is the discipline that deals with the
			visual representation of propositional content by using charts,
			graphs and diagrams in order to facilitate the apprehension,
			interpretation, transformation and communication of those contents
			through these visual representations. For the correct use of
			information visualization as knowledge analysis, it is necessary to
			know the logical and perceptual principles that guarantee its
			explanatory efficiency.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/fp.jpg" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Florencia Pollo-Cattaneo</strong> <a
				href="http://www.posgrado.frba.utn.edu.ar/investigacion/profesores/pollo/cv-fpc.htm"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a>
		</h4>
		<p>Florencia Pollo-Cattaneo has been professor of Computer Science -
			Artificial Intelligence within the Degree Program in Information
			Systems Engineering, since 1996, at the National Technological
			University - Regional School of Buenos Aires (Argentina). She is
			currently also the director of the GEMIS research group and the
			Master in Information Systems Engineering at the same university. She
			has been an Information Systems Engineer since 1992, then specialized
			in the construction of expert systems at the Technological Institute
			of Buenos Aires (ITBA). She also obtained a Masters Degree in
			Software Engineering from the Technological Institute of Buenos Aires
			(ITBA) and in the Polytechnic University of Madrid (Spain). Finally,
			she graduated as Doctor of Computer Science at the National
			University of La Plata (Argentina).She has published over 60 academic
			and industrial papers in noteworthy peer-reviewed publications. She
			has participated in numerous national and foreign congresses, courses
			and workshops, as a panellist and organizer. Her concern for the
			socio-cultural projection of the discipline has earned her the
			recognition of her opinion as a reference in the specialized
			scientific press.</p>
		<h4>
			<strong>Keynote: Artificial Intelligence for the Solution of Real
				Problems</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> Given the success obtained by the
			application of Artificial Intelligence (AI) in large corporations
			such as Google, Facebook and Microsoft (to offer new and better
			services), other companies are imitating them and investing more and
			more in research and development within that discipline. This
			situation has led institutions to highlight the importance of the AI
			in the near future. It should be noted that these benefits are not
			limited to the existence of 'thinking computers', but are about
			software systems that exhibit some degree of intelligent behavior.
			Thanks to the latest advances in Machine Learning and Artificial
			Intelligence, it is easy to let imagination run wild and think about
			how incredible the future will be. Automatic cars, much more precise
			medical diagnoses, online purchases completely automated through
			intelligent chatbots. But, what do we really know about it? We all
			have our own idea of Artificial Intelligence conceived from news,
			books, movies or science fiction series. However, this has also given
			rise to certain myths that need to be dismantled in order to
			understand what Artificial Intelligence really is and what it really
			means. In this context, the aim of the talk is to provide a complete
			review of the evolution of AI and its main applications, as well as
			the myths and realities of what will come with it in the future.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/ms.png" class="img-circle" width="180px">
	</div>
	<div class="col-md-10">
		<h4>
			<strong>Modestos Stavrakis</strong> <a
				href="https://www.syros.aegean.gr/en/staff/professors-and-lecturers/lecturers/modestos-stavrakis"
				target="_blank"><span class="glyphicon glyphicon-new-window"
				aria-hidden="true"></span></a>
		</h4>
		<p>Modestos Stavrakis is currently a Lecturer in Interaction Design at
			the Department of Product and Systems Design Engineering at the
			University of the Aegean, Greece, and Adjunct Professor in Hellenic
			Open University. He holds a PhD in Design from the same university,
			an MSc in Computer Aided Graphical Technology Applications and a BA
			(first class honours) in Creative Visualisation from the University
			of Teesside (UK). Over the past few years he has conducted research
			in the fields of Interaction Design and User Experience, HCI,
			computer aided collaborative design, web information systems,
			computer assistive technologies and distance/e-learning.</p>
		<h4>
			<strong>Keynote: Designing interactive technologies for smart
				learning environments</strong>
		</h4>
		<p>
			<strong>Abstract:</strong> Interactivity plays an important role in
			the way we experience information in the era of ICTs. The same
			applies in educational contexts. The way we design learning
			environments based on interactive systems and technologies heavily
			influence the experience of both the educator and the learner.
			Digital technologies and recent developments in the design of
			interactive systems alter the way we understand and perform the
			learning and teaching processes in various educational contexts
			(classroom, online, virtual and augmented environments). This speech
			will discuss the most current developments in the area and a number
			of relevant challenges in designing interactive learning
			environments. It will outline examples and projects that show the
			potential of this research field, but also point a number of future
			research and design challenges.
		</p>
	</div>
</div>