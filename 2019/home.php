<?php
$dir = "img/city/";
$dirObj = opendir($dir);
$pictures = array();
while (($file = readdir($dirObj)) != null) {
    if ($file != ".." && $file != ".") {
        array_push($pictures, $file);
    }
}
closedir($dirObj);
sort($pictures);
?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
<?php
for ($i = 0; $i < count($pictures); $i ++) {
    echo "<li data-target='#myCarousel' data-slide-to='" . $i . "'", ($i == 0) ? "class='active'" : "", "></li>";
}
?>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
<?php
for ($i = 0; $i < count($pictures); $i ++) {
    echo "<div class='", ($i == 0) ? "item active" : "item", "'>\n";
    echo "<img src='" . $dir . $pictures[$i] . "' width='100%'>\n";
    echo "</div>\n";
}
?>
	</div>

	<!-- Left and right controls -->
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span> <span
		class="sr-only">Previous</span>
	</a> <a class="right carousel-control" href="#myCarousel"
		data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<br>
<div class="row">

	<h3>ICAI 2019</h3>
	<p>The Second International Conference on Applied Informatics (ICAI)
		aims to bring together researchers and practitioners working in
		different domains in the field of informatics in order to exchange
		their expertise and to discuss the perspectives of development and
		collaboration</p>
	<p>
		ICAI 2019 was held at the <strong>Universidad a Distancia de Madrid
			and Universidad Complutense de Madrid</strong> located in <strong>Madrid,
			Spain</strong>, from 6 to 9 November 2019. It was organized by the
		Universidad Distrital Francisco Jose de Caldas, Universidad de Bogota
		Jorge Tadeo Lozano, Universidad Complutense de Madrid, Universidad
		Nacional de Loja, Universidad a Distancia de Madrid, BITrum Research
		Group, and Universidad Peninsula de Santa Elena.
	</p>
	<p>ICAI 2019 was proudly sponsored by: Information Technologies
		Innovation Research Group, Springer, and Erasmus+.</p>

	<h4>Building Up a Global Sustainable Information Society</h4>
	<p>The information technologies have shown an ever growing capacity to
		perform operations we could not even dream of some decades ago. During
		this time, we have witnessed a constant growth of the
		interconnectivity of agents in a thriving global network in which the
		algorithmic capacity of the nodes and the intensity of the interaction
		have continuously increased. As a consequence, this global network
		evolves as an ecosystem in which our lives are more and more immersed,
		and our destinies are increasingly entangled, as it happens with the
		lives of the organisms evolving in a dense ecosystem. The question is
		whether the conditions of this global life are given to evolve towards
		a sustainable future. Namely a future in which the needs of the so
		called information society can be met, a sustainable future without
		exclusion and oppression, a sustainable future in which we give back
		to nature more than we take. The design, development and
		implementation of the information technologies has a profound
		consequence on these dynamics. Technological development is not an end
		in itself, it has to prove its applicability in the solution of human
		needs, in the building up of a sustainable information society at a
		global scale</p>

	<p>Under this broad focus, we call for contributions to ICAI 2019. The
		tasks that need to be accomplished in the endeavour of building a
		global sustainable information society are multifarious, ranging from
		artificial intelligence to information ethics, from bioinformatics to
		education, from data science to co‐creation of knowledge.</p>

	<div class="alert alert-success" role="alert">
		<h4>
			<strong>Best Paper Award</strong>
		</h4>
		<p>
			The paper <strong>Enhanced Sketchnoting Through Semantic Integration
				of Learning Material <a
				href="https://link.springer.com/chapter/10.1007/978-3-030-32475-9_25"
				target="_blank"><span class="glyphicon glyphicon-new-window"
					aria-hidden="true"></span></a>
			</strong> authored by <strong><i>Aryobarzan Atashpendar, Christian
					Grévisse, Steffen Rothkugel</i></strong> from the <strong>University
				of Luxembourg, Luxembourg</strong> has been selected as Best Paper
			of ICAI 2019.


		</p>
	</div>

	<div class="alert alert-success" role="alert">
		<h4>
			<strong>ICAI 2019 Proceedings in CCIS</strong>
		</h4>
		<p>
			ICAI 2019 proceedings in Communications in Computer and Information
			Science (CCIS) Volume 1051 by Springer are now available at <a
				href="https://link.springer.com/book/10.1007/978-3-030-32475-9"
				target="_blank">https://link.springer.com/book/10.1007/978-3-030-32475-9
			</a>
		</p>
	</div>

	<div class="alert alert-success" role="alert">
		<h4>
			<strong>ICAI Workshops Proceedings in CEUR-WS</strong>
		</h4>
		<p>
			ICAI Workshops 2019 proceedings in CEUR Workshop Proceedings
			(CEUR-WS) Volume 2486 are now available at <a
				href="http://ceur-ws.org/Vol-2486" target="_blank">http://ceur-ws.org/Vol-2486</a>
		</p>
	</div>
</div>