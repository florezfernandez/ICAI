<h3>General Chairs</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Marcelo Leon, Ph.D. Universidad Nacional de Loja, Ecuador <a href="https://orcid.org/0000-0001-6303-6615" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Jose Maria Diaz, Ph.D. Universidad a Distancia de Madrid, Spain <a href="https://orcid.org/0000-0001-5383-6037" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Simone Belli, Ph.D. Universidad Complutense de Madrid, Spain <a href="https://orcid.org/0000-0001-8934-7569" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Organizing Committee</h3>
<ul>
	<li>Simone Belli, Ph.D. Universidad Complutense de Madrid, Spain <a href="https://orcid.org/0000-0001-8934-7569" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Manuela Cañizares, Ph.D. Universidad a Distancia de Madrid, Spain <a href="https://orcid.org/0000-0002-8164-3694" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Iris Celorrio, Ph.D. Universidad a Distancia de Madrid, Spain <a href="https://orcid.org/0000-0002-2189-2154" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Jose Maria Diaz, Ph.D. Universidad a Distancia de Madrid, Spain <a href="https://orcid.org/0000-0001-5383-6037" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Teresa Guarda, Ph.D. BITrum-Research Group, Spain <a href="https://orcid.org/0000-0002-9602-0692" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Wolfgang Hofkirchner, Ph.D. Institute for a Global Sustainable Information Society (GSIS), Austria <a href="https://orcid.org/0000-0003-1267-3504" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Juan Carlos Revilla, Ph.D. Universidad Complutense de Madrid, Spain <a href="https://orcid.org/0000-0001-6828-161X" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Isaac Seoane, Ph.D. Universidad a Distancia de Madrid, Spain <a href="https://orcid.org/0000-0003-4678-8019" target="_blank"><img src="../img/logos/orcid.png" ></a> </li>	
</ul>

<h3>Steering Committee</h3>
<ul>
	<li>Jaime Chavarriaga, Ph.D. Universidad de los Andes, Colombia <a href="https://orcid.org/0000-0002-8372-667X" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Cesar Diaz, Ph.D. OCOX AI, Colombia <a href="https://orcid.org/0000-0002-9132-2747" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Ixent Galpin, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0001-7020-6328" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Olmer García, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0002-6964-3034" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Workshops Committee</h3>
<ul>
	<li>Jaime Chavarriaga, Ph.D. Universidad de los Andes, Colombia <a href="https://orcid.org/0000-0002-8372-667X" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Cesar Diaz, Ph.D. OCOX AI, Colombia <a href="https://orcid.org/0000-0002-9132-2747" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Olmer García, Ph.D. Universidad de Bogotá Jorge Tadeo Lozano, Colombia <a href="https://orcid.org/0000-0002-6964-3034" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Teresa Guarda, Ph.D. Universidad Estatal Península de Santa Elena, Ecuador <a href="https://orcid.org/0000-0002-9602-0692" target="_blank"><img src="../img/logos/orcid.png" ></a></li>	
</ul>

<h3>Publication Chairs</h3>
<ul>
	<li>Hector Florez, Ph.D. Universidad Distrital Francisco José de Caldas, Colombia <a href="https://orcid.org/0000-0002-5339-4459" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
	<li>Sanjay Misra, Ph.D. Covenant University, Nigeria <a href="https://orcid.org/0000-0002-3556-9331" target="_blank"><img src="../img/logos/orcid.png" ></a></li>
</ul>

<h3>Program Committee</h3>
<ol>
<?php 
$pc = fopen("docs/pc.csv", "r");
while(!feof($pc)) {
    $data = explode(";", trim(fgets($pc)));
    if($data[0] != ""){
        echo "<li>" . $data[0] . " " . $data[1] . ", Ph.D. " . $data[3] . ", " . $data[4] . (($data[7]!="")?" <a href='" . $data[7] . "' target='_blank'><img src='../img/logos/orcid.png' ></a>":"") . "</li>\n";        
    }
}
?>
</ol>