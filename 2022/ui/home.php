<br>
<div class="row">
	<div class="col">
		<h3>ICAI 2022</h3>
		<p class="text-justify">
			The 5<sup>th</sup> International Conference on Applied Informatics
			(ICAI) aims to bring together researchers and practitioners working
			in different domains in the field of informatics in order to exchange
			their expertise and to discuss the perspectives of development and
			collaboration
		</p>
		<p class="text-justify">
			ICAI 2022 was held at the <strong>Universidad Continental</strong>
			located in <strong>Arequipa, Peru</strong>, from 27 to 29 October
			2022 in <strong class="text-danger">BLENDED</strong> form (i.e., with
			the option to participate remotely online). It is organized by the
			ITI Research Group that belongs to the Universidad Distrital
			Francisco Jose de Caldas and the Universidad Continental.
		</p>
		<p class="text-justify">ICAI 2022 is proudly sponsored by: ITI
			Research Group, Science Based Platforms, and CEUR Workshop
			Proceedings.</p>
		<p>ICAI has been previously held in:</p>
		<ul>
			<li>2021: Buenos Aires, Argentina <img
				src="https://www.worldometers.info/img/flags/ar-flag.gif"
				height="16px"></li>
			<li>2020: Ota, Nigeria <img
				src="https://www.worldometers.info/img/flags/ni-flag.gif"
				height="16px"></li>
			<li>2019: Madrid, Spain <img
				src="https://www.worldometers.info/img/flags/sp-flag.gif"
				height="16px"></li>
			<li>2018: Bogota, Colombia <img
				src="https://www.worldometers.info/img/flags/co-flag.gif"
				height="16px"></li>
		</ul>

		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2022 Proceedings in CCIS </strong>
			</h5>
			ICAI 2022 proceedings in Communications in Computer and Information
			Science (CCIS) Volume 1643 by Springer are now available at <a
				href="https://link.springer.com/book/10.1007/978-3-031-19647-8"
				target="_blank">https://link.springer.com/book/10.1007/978-3-031-19647-8</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Workshops 2022 Proceedings in CEUR-WS</strong>
			</h5>
			ICAI Workshops 2022 proceedings in CEUR Workshop Proceedgins
			(CEUR-WS) Volume 3282 are now available at <a
				href="https://ceur-ws.org/Vol-3282/" target="_blank">https://ceur-ws.org/Vol-3282/</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Certificates</strong>
			</h5>
			ICAI 2022 certificates are available at: <a
				href="https://icai-c.itiud.org/certificate.php?edition=2022"
				target="_blank">ICAI-C</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>Best Paper Award</strong>
			</h5>
			The paper <strong>Preliminary Study for Impact of Social Media
				Networks on Traffic Prediction</strong> <a
				href="https://link.springer.com/chapter/10.1007/978-3-031-19647-8_15"
				target="_blank"><span class="fas fa-external-link-alt"
				aria-hidden="true"></span></a> authored by <strong><i>Valeria Laynes
					Fiascunari and Luis Rabelo</i></strong> from the <strong>University
				of Central Florida, United States</strong> has been selected as <strong>Best
				Paper</strong> of ICAI 2022.
		</div>

		<div class="alert alert-success" role="alert">
			<h5>
				<strong>Best Reviewers Acknowledgement</strong>
			</h5>
			ICAI 2022 acknowledges the remarkable work of the following reviewers
			<ul>
				<li>Victor Darriba, Universidade de Vigo, Spain</li>
				<li>Fernando Yepes-Calderon, Children's Hospital Los Angeles, United
					States</li>
				<li>Alber Sanchez, Instituto Nacional de Pesquisas Espaciais, Brazil</li>
				<li>Jorge Bacca, Fundacion Universitaria Konrad Lorenz, Colombia</li>
				<li>Jens Gulden, Universiteit Utrecht, Netherlands</li>
				<li>Alexander Bock, Universität Duisburg Essen, Germany</li>
				<li>Ivan Mura, Duke Kunshan University, China</li>
				<li>Filipe Portela, Universidade do Minho, Portugal</li>
				<li>Diego Peluffo-Ordóñez, Mohammed VI Polytechnic University,
					Morocco</li>
				<li>Christian Grévisse, Université du Luxembourg, Luxembourg</li>
			</ul>
		</div>
	</div>
</div>