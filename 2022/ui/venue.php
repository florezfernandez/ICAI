<h3>Venue</h3>
<p>
	ICAI 2022 will be held at the <strong>Universidad Continental</strong>
	located in <strong>Arequipa, Peru</strong>.
</p>
<iframe
	src="https://www.google.com/maps/d/embed?mid=1n7i3OTwd6tM-yPDuvzXFt4NNJJFtpd4&ehbc=2E312F"
	width="640" height="480"></iframe>
<h3>Transportation</h3>
<h4>
	From the Arequipas's international airport <i>Alfredo Rodríguez Ballón</i>
	to the Campus
</h4>
<p>
	The trip from the Arequipas's international airport <strong>Alfredo
		Rodríguez Ballón</strong> to the <i>Universidad Continental</i> takes
	approximately 40 minutes. It is recommended to use the taxis found in
	the airport parking lot (Turismo Arequipa, Taxitel, Alo45, etc.). On
	the way, the taxi will cross the Chili River through the Chilina Bridge
	or through the Grau Bridge. On the left side, you will be able to
	appreciate the Chachani, the Misti, and the Pichupichu. They are the
	tutelary volcanoes that protect the city of Arequipa.
</p>

<h4>Campus</h4>
<p>The Arequipa campus is located on a 10,000 square meter land on
	Avenida Los Incas s/n in the José Luis Bustamante and Rivero district.
	It is a modern sustainable campus with advanced educational technology
	that at the same time maintains harmony with nature. The main source of
	energy for outdoor lighting is solar energy, the water from sinks is
	recycled for irrigation of gardens after primary treatment. It has
	innovative classrooms that promote collaborative learning, a library
	and a documentation center, a cafeteria, coworking spaces, computer
	labs, multipurpose and specialty labs, FabLab, an open studio or
	innovation classroom, lounges, and study environments, and student
	interaction such as an auditorium, sports tiles, and social areas.</p>

<h4>Tourism in Arequipa</h4>
<p>Arequipa is an important industrial, commercial and tourist center of
	Peru. It is considered the second most industrialized city with the
	greatest economic activity in the country. It has been declared by
	UNESCO as a cultural heritage of humanity, due to its historical past
	and colonial religious architecture. It also has one of the most
	exquisite gastronomic schools in the region.</p>

<p>There are the following important turistic places:</p>
<ul>
	<li><strong>St. Catalina's Monastery</strong>. In the heart of the
		city, it is one of the great architectural works of colonial Peru. it
		is a cloistered convent built in the 16th century and has more than
		20,000 square meters that you can explore while reliving its
		historical past.</li>
	<li><strong>Yanahuara City Lookout</strong>. Located in the picturesque
		district of the same name, the Yanahuara offers one of the most
		spectacular views of the three most representative volcanoes: Misti,
		Chachani, and Pichupichu. In addition, other attractions of this place
		is its characteristic white ashlar arches, in which you can read
		phrases from famous people of Peru.</li>
	<li><strong>Sillar Route</strong>. One of the main characteristics of
		Arequipa is the white of the walls. If you want to know how is the
		extraction process of the stone of volcanic origin, we recommend you
		the Sillar Route (Ruta del Sillar).</li>
	<li><strong>Carmen Alto Lookout</strong>. It is a traditional town of
		Cayma located on the right side of the Chili River and 4 km from the
		Plaza de Armas. With quiet streets and traditional people, it has
		beautiful and well-preserved platforms with stone walls and is
		surrounded by old paths that are still used today.</li>
	<li><strong>Colca Canyon</strong>. This impressive jewel of the
		Peruvian nature is the main tourist attraction in the Arequipa region,
		and with good reason! At 3.600 meters above sea level, the Colca is
		one of the deepest and narrowest canyons in the world.</li>

</ul>



