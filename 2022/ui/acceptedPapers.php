<h3>Accepted Papers</h3>
<h4>Artificial Intelligence</h4>
<ul>
	<li><strong>A genetic algorithm for scheduling laboratory rooms: A case
			study</strong>
		<ul>
			<li>Rafael Fuenmayor, Universidad Internacional del Ecuador, Ecuador</li>
			<li>Martín Larrea, Universidad Internacional del Ecuador, Ecuador</li>
			<li>Mario Moncayo, Universidad Internacional del Ecuador, Ecuador</li>
			<li>Esteban Moya, Universidad Internacional del Ecuador, Ecuador</li>
			<li>Sebastián Trujillo, Universidad Internacional del Ecuador,
				Ecuador</li>
			<li>Juan-Diego Terneus, Universidad Internacional del Ecuador,
				Ecuador</li>
			<li>Robinson Guachi, Universidad Internacional del Ecuador, Ecuador</li>
			<li>Diego H. Peluffo-Ordoñez, Mohammed VI Polytechnic University,
				Morocco</li>
			<li>Lorena Guachi-Guachi, Universidad Internacional del Ecuador,
				Ecuador</li>
		</ul></li>

	<li><strong>COVID-19 Article Classification using Word-Embedding and
			Different Variants of Deep-Learning Approach</strong>
		<ul>
			<li>Sanidhya Vijayvargiya, BITS-Pilani Hyderabad, India</li>
			<li>Lov Kumar, BITS-Pilani Hyderabad, India</li>
			<li>Lalita Bhanu Murthy, BITS-Pilani Hyderabad, India</li>
			<li>Sanjay Misra, Østfold University College, Norway</li>
		</ul></li>

	<li><strong>Crop classification using Deep Learning: A quick
			comparative study of modern approaches</strong>
		<ul>
			<li>Hind Raki, Mohammed VI Polytechnic University, Morocco</li>
			<li>Juan González-Vergara, Smart Data Analysis Systems Group, Morocco</li>
			<li>Yahya Aalaila, Mohammed VI Polytechnic University, Morocco</li>
			<li>Mouad Elhamdi, Mohammed VI Polytechnic University, Morocco</li>
			<li>Sami Bamansour, University Cadi Ayyad, Morocco</li>
			<li>Lorena Guachi-Guachi, Universidad Internacional del Ecuador,
				Ecuador</li>
			<li>Diego H. Peluffo-Ordoñez, Mohammed VI Polytechnic University,
				Morocco</li>
		</ul></li>

	<li><strong>Internet of Things (IoT) for Secure and Sustainable
			Healthcare Intelligence: Analysis and Challenges</strong>
		<ul>
			<li>Sunday Adeola Ajagbe, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Sanjay Misra, Østfold University College, Norway</li>
			<li>Oluwaseyi F. Afe, Lead City University, Nigeria</li>
			<li>Kikelomo I. Okesola, Lead City University, Nigeria</li>
		</ul></li>

	<li><strong>Multiple Colour Detection of RGB Images using Machine
			Learning Algorithm</strong>
		<ul>
			<li>Joseph Bamidele Awotunde, University of Ilorin, Nigeria</li>
			<li>Sanjay Misra, Østfold University College, Norway</li>
			<li>David Obagwu, University of Ilorin, Nigeria</li>
			<li>Hector Florez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
		</ul></li>

	<li><strong>Neural model-based similarity prediction for compounds with
			unknown structures</strong>
		<ul>
			<li>Eugenio Borzone, Universidad Nacional del Litoral, Argentina</li>
			<li>Leandro Ezequiel Di Persia, Universidad Nacional del Litoral,
				Argentina</li>
			<li>Matias Gerard, Universidad Nacional del Litoral, Argentina</li>
		</ul></li>
</ul>

<h4>Data Analysis</h4>
<ul>
	<li><strong>Classifying Incoming Customer Messages for an E-Commerce
			Site using Supervised Learning</strong>
		<ul>
			<li>Misael Andrey Albañil Sánchez, Universidad de Bogotá Jorge Tadeo
				Lozano, Colombia</li>
			<li>Ixent Galpin, Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
		</ul></li>

	<li><strong>Comparison of Machine Learning Models for Predicting
			Rainfall in Tropical Regions: The Colombian Case</strong>
		<ul>
			<li>Carlos Andrés Rocha-Ruiz, Universidad de Bogotá Jorge Tadeo
				Lozano, Colombia</li>
			<li>Olmer García-Bedoya, Universidad de Bogotá Jorge Tadeo Lozano,
				Colombia</li>
			<li>Ixent Galpin, Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
		</ul></li>

	<li><strong>Deep Mining Covid-19 Literature</strong>
		<ul>
			<li>Joshgun Sirajzade, University of Luxembourg, Luxembourg</li>
			<li>Pascal Bouvry, University of Luxembourg, Luxembourg</li>
			<li>Christoph Schommer, University of Luxembourg, Luxembourg</li>
		</ul></li>

	<li><strong>Keyword-based Processing for Assessing Short Answers in the
			Educational Field</strong>
		<ul>
			<li>Javier Sanz-Fayos, Universidad Internacional de La Rioja, Spain</li>
			<li>Luis de-la-Fuente-Valentín, Universidad Internacional de La
				Rioja, Spain</li>
			<li>Elena Verdú, Universidad Internacional de La Rioja, Spain</li>
		</ul></li>

	<li><strong>Statistical Characterization of Image Intensities in
			Regions Inside Arteries to Facilitate the Extraction of Center Lines
			in Atherosclerosis Frameworks</strong>
		<ul>
			<li>Fernando Yepes-Calderon, Science Based Platforms, United States</li>
		</ul></li>

	<li><strong>Text Encryption with Advanced Encryption Standard (AES) for
			Near Field Communication (NFC) using Huffman Compression</strong>
		<ul>
			<li>Oluwashola David Adeniji, University of Ibadan, Nigeria</li>
			<li>Olaniyan Eliais Akinola, University of Ibadan, Nigeria</li>
			<li>Ademola Olusola Adesina, Olabisi Onabanjo University, Nigeria</li>
			<li>Olamide Afolabi, University of Ibadan, Nigeria</li>
		</ul></li>

</ul>

<h4>Decision Systems</h4>
<ul>
	<li><strong>A systematic review on phishing detection: A perspective
			beyond a high accuracy in phishing detection</strong>
		<ul>
			<li>Daniel Alejandro Barreiro Herrera, Universidad Nacional de
				Colombia, Colombia</li>
			<li>Jorge Eliecer Camargo Mendoza, Universidad Nacional de Colombia,
				Colombia</li>
		</ul></li>

	<li><strong>Application of Duality Properties of Renyi Entropy for
			Parameter Tuning in an Unsupervised Machine Learning Task</strong>
		<ul>
			<li>Sergei Koltcov, National Research University Higher School of
				Economics, Russia</li>
		</ul></li>

	<li><strong>Preliminary Study for Impact of Social Media Networks on
			Traffic Prediction</strong>
		<ul>
			<li>Valeria Laynes Fiascunari, University of Central Florida, United
				States</li>
			<li>Luis Rabelo, University of Central Florida, United States</li>
		</ul></li>


	<li><strong>Website Phishing Detection Using Machine Learning
			Classification Algorithms</strong>
		<ul>
			<li>Mukta Mithra Raj, Dubai International Academic City, United Arab
				Emirates</li>
			<li>J. Angel Arul Jothi, Dubai International Academic City, United
				Arab Emirates</li>
		</ul></li>

</ul>

<h4>Health Care Information Systems</h4>
<ul>
	<li><strong>AESRSA: A New Cryptography Key for Electronic Health Record
			Security</strong>
		<ul>
			<li>Sunday Adeola Ajagbe, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Hector Florez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
			<li>Joseph Bamidele Awotunde, University of Ilorin, Nigeria</li>
		</ul></li>

	<li><strong>Detection of COVID-19 Using Denoising Autoencoders and
			Gabor Filters</strong>
		<ul>
			<li>Jayalakshmi Saravanan, IFET College of Engineering, India</li>
			<li>T. Ananth Kumar, IFET College of Engineering, India</li>
			<li>Andrew C. Nwanakwaugwu, University of Salford, United Kingdom</li>
			<li>Sunday Adeola Ajagbe, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Ademola T. Opadotun, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Deborah D. Afolayan, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Oluwafemi O. Olawoyin, Ladoke Akintola University of Technology,
				Nigeria</li>
		</ul></li>

	<li><strong>Development of Mobile Applications to Save Lives on
			Catastrophic Situations</strong>
		<ul>
			<li>Rocío Rodriguez-Guerrero, Universidad Distrital Francisco Jose de
				Caldas, Colombia</li>
			<li>Alvaro A. Acosta, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
			<li>Carlos A. Vanegas, Universidad Distrital Francisco Jose de
				Caldas, Colombia</li>
		</ul></li>

	<li><strong>Internet of Things with Wearable Devices and Artificial
			Intelligence for Elderly Uninterrupted Healthcare Monitoring Systems</strong>
		<ul>
			<li>Joseph Bamidele Awotunde, University of Ilorin, Nigeria</li>
			<li>Sunday Adeola Ajagbe, Ladoke Akintola University of Technology,
				Nigeria</li>
			<li>Hector Florez, Universidad Distrital Francisco Jose de Caldas,
				Colombia</li>
		</ul></li>
</ul>

<h4>ICT-Enabled Social Innovation</h4>
<ul>
	<li><strong>A Wayuunaiki Translator to Promote Ethno-tourism using Deep
			Learning Approaches</strong>
		<ul>
			<li>Rafael Negrette, Universidad de Bogotá Jorge Tadeo Lozano,
				Colombia</li>
			<li>Ixent Galpin, Universidad de Bogotá Jorge Tadeo Lozano, Colombia</li>
			<li>Olmer García-Bedoya, Universidad de Bogotá Jorge Tadeo Lozano,
				Colombia</li>
		</ul></li>

	<li><strong>Rating the acquisition of pre-writing skills in children:
			an analysis based on computer vision and data mining techniques in
			the Ecuadorian context</strong>
		<ul>
			<li>Adolfo Jara-Gavilanes, Universidad Politécnica Salesiana, Ecuador</li>
			<li>Romel Ávila-Faicán, Universidad Politécnica Salesiana, Ecuador</li>
			<li>Vladimir Robles-Bykbaev, Universidad Politécnica Salesiana,
				Ecuador</li>
			<li>Luis Serpa-Andrade, Universidad Politécnica Salesiana, Ecuador</li>
		</ul></li>

	<li><strong>Study techniques: procedure to promote the comprehension of
			texts in university students</strong>
		<ul>
			<li>Eilen Lorena Pérez Montero, Corporación Universitaria del Huila
				CORHUILA, Colombia</li>
			<li>Yolanda Díaz Rosero, Corporación Universitaria del Huila
				CORHUILA, Colombia</li>
		</ul></li>

	<li><strong>TPACK and Toondoo Digital Storytelling tool transform
			Teaching and Learning</strong>
		<ul>
			<li>Serafeim A. Triantafyllou, Greek Ministry of Education and
				Religious Affairs, Greece</li>
		</ul></li>
</ul>


<h4>Image Processing</h4>
<ul>
	<li><strong>Fully Automated Lumen Segmentation Method and BVS Stent
			Struts Detection in OCT images</strong>
		<ul>
			<li>Julia Duda, AGH University of Science and Technology, Poland</li>
			<li>Izabela Cywińska, AGH University of Science and Technology,
				Poland</li>
			<li>Elżbieta Pociask, AGH University of Science and Technology,
				Poland</li>
		</ul></li>

	<li><strong>Predictive Modeling Toward the Design of a Forensic
			Decision Support System using Cheiloscopy for Identification from Lip
			Prints</strong>
		<ul>
			<li>Agustin Sabelli, Universidad Tecnológica Nacional Facultad
				Regional Buenos Aires, Argentina</li>
			<li>Parag Chatterjee, Universidad Tecnológica Nacional Facultad
				Regional Buenos Aires, Argentina</li>
			<li>María F. Pollo-Cattaneo, Universidad Tecnológica Nacional
				Facultad Regional Buenos Aires, Argentina</li>
		</ul></li>
</ul>

<h4>Robotic Autonomy</h4>
<ul>
	<li><strong>Automotive industry applications based on Industrial
			Internet of Things (IIoT). A review</strong>
		<ul>
			<li>Luis Carlos Guzman Mendoza, Universidad Manuela Beltrán, Colombia</li>
			<li>Juan Carlos Amaya, Universidad Manuela Beltrán, Colombia</li>
			<li>César A. Cárdenas, Universidad Manuela Beltrán, Colombia</li>
			<li>Carlos Andrés Collazos Morales, Universidad Manuela Beltrán,
				Colombia</li>
		</ul></li>
	<li><strong>Implementation of an IoT Architecture for Automated Garbage
			Segregation</strong>
		<ul>
			<li>Angel Antonio Rodríguez Varela, Universidad de Los Andes,
				Colombia</li>
			<li>Germán A. Montoya, Universidad de Los Andes, Colombia</li>
			<li>Carlos Lozano-Garzón, Universidad de Los Andes, Colombia</li>
		</ul></li>
</ul>

<h4>Software Architectures</h4>
<ul>
	<li><strong>Architecture on Knowledge Management Systems: its presence
			in the academic literature</strong>
		<ul>
			<li>Luciano Straccia, Universidad Tecnológica Nacional Facultad
				Regional Buenos Aires, Argentina</li>
			<li>María F Pollo-Cattáneo, Universidad Tecnológica Nacional Facultad
				Regional Buenos Aires, Argentina</li>
			<li>Matías Giorda, Universidad Tecnológica Nacional Facultad Regional
				Buenos Aires, Argentina</li>
			<li>M. Gracia Bongiorno, Universidad Tecnológica Nacional Facultad
				Regional Buenos Aires, Argentina</li>
			<li>Adriana Maulini, Universidad Tecnológica Nacional Facultad
				Regional Buenos Aires, Argentina</li>
		</ul></li>
</ul>

<h4>Software Design Engineering</h4>
<ul>
	<li><strong>Automatic Location and Suppression of Calcifications and
			Atheromas by Gradient Change in the Pattern of Intensities Inside the
			Carotid Artery</strong>
		<ul>
			<li>Fernando Yepes-Calderon, Science Based Platforms, United States</li>
		</ul></li>
	<li><strong>Introducing Planet-Saving Strategies to Measure, Control,
			and Mitigate Greenhouse Gas Emissions and Water Misuse in Household
			Residences</strong>
		<ul>
			<li>Ronald S. Marin Cifuentes, Institución Universitaria Antonio José
				Camacho, Colombia</li>
			<li>Adriana M. Florez Laiseca, Universidad del Quindio, Colombia</li>
			<li>Fernando Yepes-Calderon, Science Based Platforms, United States</li>
		</ul></li>
	<li><strong>Team Productivity in Agile Software Development: A
			Systematic Mapping Study</strong>
		<ul>
			<li>Marcela Guerrero-Calvache, Universidad de Nariño, Colombia</li>
			<li>Giovanni Hernández, Universidad Mariana, Colombia</li>
		</ul></li>
</ul>