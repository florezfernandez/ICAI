<div class="col-lg-3 col-md-4 text-center">
	<img src="../img/logos/icai.png" width="200">
</div>
<div class="col-lg-9 col-md-12">
	<h4>
		<i><strong>5th International Conference on Applied Informatics</strong></i>
	</h4>
	<h5>
		27 to 29 October 2022 <br>Universidad Continental <br> Arequipa, Peru
		<img src="https://www.worldometers.info/img/flags/pe-flag.gif"
			height="16px"> <i class="text-danger">BLENDED</i>
	</h5>
</div>