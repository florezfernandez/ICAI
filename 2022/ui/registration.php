<h3>Registration</h3>
<p class="text-justify">At least one author of each accepted paper must
	register to ensure its inclusion in the conference proceedings. Each
	registration will allow the publication and presentation of just one
	paper. Authors must pay an extra fee if they wish to publish and
	present more than one paper.</p>

<h3>Registration Fees</h3>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<table class="table table-hover table-striped">
				<thead>
				<tr class="info">
					<th class="text-center" >Registration</th>
					<th class="text-center" >Fee</th>
				</tr>
				</thead>
				<tbody class="table-group-divider">
				<tr>
					<td nowrap>Regular Registration</td>
					<td class="text-center">350 USD</td>
				</tr>
				<tr>
					<td nowrap>Student Registration</td>
					<td class="text-center">250 USD</td>
				</tr>
				<tr>
					<td nowrap>Workshop Registration</td>
					<td class="text-center">250 USD</td>
				</tr>
				<tr>
					<td nowrap>Virtual Registration</td>
					<td class="text-center">150 USD</td>
				</tr>
				<tr>
					<td nowrap>Additional Paper (up to 2)</td>
					<td class="text-center">100 USD</td>
				</tr>
				<tr>
					<td nowrap>Attendee</td>
					<td class="text-center">100 USD</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<p class="text-justify">Registration instructions are sent to authors of
	accepted papers in the corresponding acceptance notification.</p>