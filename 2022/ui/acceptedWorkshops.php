<h3>Accepted Workshops</h3>

<h4>
	3rd International Workshop on Applied Artificial Intelligence (WAAI)	
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Artificial
	Intelligence is an annual event intended to be an important forum of
	the Artificial Intelligence (AI) community, organized by the GRUPO
	GEMIS of the Universidad Tecnológica Nacional Facultad Regional Buenos
	Aires. The workshop aims to provide a forum for researchers and AI
	community members to discuss and exchange ideas and experiences on
	diverse topics of AI. It also seeks to strengthen a space for
	discussion of innovative ideas as well as development of emerging
	technologies and practical experiences related to Artificial
	Intelligence in general and each of its technologies in particular.
	Submissions are invited on both applications of AI and new tools and
	foundations. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=waai2022"
		target="_blank">https://easychair.org/conferences/?conf=waai2022</a> <br>
	<strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/utn.png" height="100px">
</div>
<hr>

<h4>
	4th International Workshop on Applied Informatics for Economy,
	Society, and Development (AIESD) 
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Informatics for
	Economy, Society and Development organized by the Universidad Ecotec
	(Ecuador) seeks to promote discussion and dissemination of current
	economics research, systems for territorial intelligence, e-
	participation, e-democracy and territorial economic development; with
	special emphasis on social and environmental development. The objective
	of this third edition of AIESD 2021 is to discuss aspects related to
	computer science applied in society. The idea is to learn the different
	techniques and tools that are used to study social phenomena, economic
	growth, the interaction between economic analysis and decision-making.
	The workshop seeks to promote an atmosphere of dialogue among the
	community of professionals working on issues related to technology and
	its application in the economy as well as society. <br>Submission URL:
	<a href="https://easychair.org/conferences/?conf=aiesd2022"
		target="_blank">https://easychair.org/conferences/?conf=aiesd2022</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/ecotec.png" height="90px">
</div>
<hr>

<h4>
	5th International Workshop on Data Engineering and Analytics (WDEA) 
</h4>
<p>
	<strong>Abstract:</strong> Data has become pervasive. Data Analytics,
	Data Mining, Data Science, or more generally the practice of
	identifying patterns or constructing mathematical models based on data,
	is at the heart of most new business models or projects. This, coupled
	with what has been termed the Big Data phenomenon, used to describe
	challenging scenarios involving either datasets that are too big to be
	handled using traditional approaches, datasets with diverse formats and
	semantics, data streams that need to be actioned in real time, or a
	combination of these, has led to a whole new series of rapidly evolving
	tools, algorithms and methods which combine data engineering, computer
	science, statistics and mathematics. The aim of this workshop is to
	present recent results at the intersection of these areas, discussing
	new methods or algorithms, or applications including Machine Learning,
	Databases, optimization or any type of algorithms which consider
	managing or obtaining value from data. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=wdea2022"
		target="_blank">https://easychair.org/conferences/?conf=wdea2022</a> <br>
	<strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/utadeo.png" height="80px">
</div>
<hr>

<h4>
	1st International Workshop on Intelligent Transportation Systems and
	Smart Mobility Technology (WITS) 
</h4>
<p>
	<strong>Abstract:</strong> The digital transformation of traffic
	monitoring systems has led to an increased interest from the academic
	community, governments and the general public on the concept of smart
	mobility, consisting of the optimized use of transport networks through
	the automated analysis of traffic data and, especially, the
	implementation of intelligent transportation systems. This type of
	technology focuses on optimizing how people travel, decreasing traffic
	congestion, improving road safety, and enhancing governmental
	decision-making during the construction or expansion of infrastructure.
	The International Workshop on Intelligent Transportation Systems and
	Smart Mobility Technology (WITS), part of the International Confer-
	ence on Applied Informatics (ICAI), is an annual event organized by
	Universidad Antonio Nariño in Bogotá, Colombia, and intended to be an
	important forum for the discussion and dissemination of contemporary
	research on the study and design of software providing integrated
	information in order to allow drivers, commuters and pedestrians to
	make a safer and more coordinated use of urban transport networks.
	Partic- ipants in the workshop will find a space for the exchange of
	innovative ideas and practical experiences on the use of this type of
	technology as well as for the establishment of collaborative networks.
	The work- shop welcomes submissions on both practical and theoretical
	proposals on ITSs and related technological solutions. <br>Submission
	URL: <a href="https://easychair.org/conferences/?conf=wits20220"
		target="_blank">https://easychair.org/conferences/?conf=wits20220</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/uan.png" height="80px"> <img
		src="../img/logos/rumbo.png" height="80px">
</div>
<hr>

<h4>
	2nd International Workshop on Knowledge Management and Information
	Technologies (WKMIT) 
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Knowledge Management and
	Information Technologies is an annual event intended to be an important
	forum of the Knowledge Management (KM) community, organized by the
	GRUPO GEMIS of the Universidad Tecnológica Nacional Facultad Regional
	Buenos Aires. The knowledge is a strategic resource to obtain better
	results in the organizations and its management promotes continuous
	improvement practices and innovation. A complete and integral knowledge
	management requires an approach from different perspectives: people,
	organizational culture, processes, tools and technologies, among
	others. The workshop aims at providing a forum for researchers and KM
	community members to discuss and exchange ideas and experiences on
	diverse topics of KM. It also seeks to strengthen a space for
	discussion of innovative ideas, development of emerging technologies
	and practical experiences related to Knowledge Management in general
	and each of its technologies in particular. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=wkmit2022"
		target="_blank">https://easychair.org/conferences/?conf=wkmit2022</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/utn.png" height="100px">
</div>
<hr>

<h4>
	1st International Workshop on Systems Modeling (WSM) 
</h4>
<p>
	<strong>Abstract:</strong> International Workshop on Systems Modeling
	(WSM) aims to bring together researchers and practitioners working in
	different areas of systems modeling to provide an opportunity for the
	modeling community to further advance the foundations of modeling and
	modeling in emerging areas. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=wsm2022" target="_blank">https://easychair.org/conferences/?conf=wsm2022</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/ud.png" height="120px"> <img
		src="../img/logos/ucauca.jpg" height="120px">
</div>
<hr>

<h3>Important Dates</h3>
<ul>
	<li>Paper Submission: September 5, 2022</li>
	<li>Paper Notification: September 19, 2022</li>
	<li>Camera Ready: October 3, 2022</li>
	<li>Authors Registration: October 3, 2022</li>
</ul>

<h3>Submission Guidelines</h3>
<p class="text-justify">
	Authors must submit an original full paper (12 to 15 pages) that has
	not previously been published. All contributions must be written in
	English. Authors must follow CEUR styles either for LaTeX or for MSWord
	(<a href="http://ceur-ws.org/Vol-XXX/CEURART.zip" target="_blank">http://ceur-ws.org/Vol-XXX/CEURART.zip</a>),
	for the preparation of their papers. We encourage authors to include
	their ORCIDs in their papers. In addition, the corresponding author of
	each accepted paper, acting on behalf of all of the authors of that
	paper, must complete and sign an <a href="docs/CEUR_ICAIW_2022.pdf"
		target="_blank">Author Agreement</a> form.
</p>

<h3>Proceedings</h3>
<p>Proceedings will be submitted to CEUR Workshop Proceedings for online
	publication.</p>