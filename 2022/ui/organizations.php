<hr>
<h3>Organized by</h3>
<div class="text-center">
	<a href="https://www.udistrital.edu.co" target="_blank"><img
		src="../img/logos/ud.png" height="120" data-toggle="tooltip"
		data-placement="bottom"
		title="Universidad Distrital Francisco Jose de Caldas"></a> <a
		href="https://www.continental.edu.pe/" target="_blank"><img
		src="../img/logos/uc.png" height="70" data-toggle="tooltip"
		data-placement="bottom" title="Universidad Continental"></a>
</div>
<h3>Sponsored by</h3>
<div class="text-center">
	<a href="http://www.itiud.org" target="_blank"><img
		src="../img/logos/iti.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="ITI Research Group"></a> <a
		href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer2.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="https://www.strategicbp.net/" target="_blank"><img
		src="../img/logos/sbp.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Science Based Platforms"></a> <a
		href="https://ceur-ws.org/" target="_blank"><img
		src="../img/logos/ceur.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="CEUR Workshop Proceedigns"></a>
</div>
<hr>