<h3>Keynote Speakers for ICAI 2022</h3>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/wc.jpg" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Walter H. Curioso <a href="https://www.waltercurioso.com/"
		target="_blank"><span class="fas fa-external-link-alt"></span></a></strong>
		</h5>
		<p>Ph.D. in Biomedical Informatics from the University of Washington
			(UW), Seattle, United States and Medical Doctor from Universidad
			Peruana Cayetano Heredia (UPCH) in Lima, Peru. Master’s in Public
			Health (MPH) from UW. Over the last 20 years, Dr. Curioso has held
			numerous leadership positions at national and international levels,
			both at the public and private sector. In academia, he is currently
			the Vice Provost of Research at Universidad Continental in Lima,
			Peru. In addition, Dr. Curioso is Affiliate Associate Professor in
			the Department of Biomedical Informatics and Medical Education in the
			School of Medicine at the University of Washington, Seattle,
			Washington, USA. Dr. Curioso has more than 140 publications,
			including peer-reviewed articles in indexed journals and book
			chapters related to health information systems, biomedical
			informatics and global health. Dr. Curioso is a Member of the World
			Health Organization Digital Health Roster of Experts. He has led the
			planning, implementation, monitoring and evaluation of projects at
			the national level related to strengthening health systems and health
			information systems.</p>
		<h5>
			<strong>Training the biomedical and health informatics workforce in
				the context of COVID-19 in Peru</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> Biomedical informatics could significantly
			improve health care access, use, quality, and outcomes, realizing
			this possibility requires personnel trained in digital health.
			However, significant gaps remain in Latin America. Technological and
			socio-cultural disparities between different regions or between
			provinces within the same country are prevalent. Rural areas, where
			the promise and need are highest, are particularly deprived. There is
			an unmet need for training and building the capacity of professionals
			in biomedical informatics not only in Peru but also in Latin America.
			This Keynote aims to present a selection of experiences in building
			biomedical informatics capacity in Peru to illustrate a series of
			challenges and opportunities for strengthening digital health
			training programs in the Region. A successful biomedical informatics
			ecosystem for Peru requires culturally relevant and collaborative
			research and training programs in digital health. These programs
			should be responsive to the needs of all relevant regional
			stakeholders, including government agencies, non–governmental
			organizations, industry, academic or research entities, professional
			societies, and communities.
		</p>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-2">
		<img src="img/pictures/rl.png" class="rounded img-thumbnail"
			width="180px">
	</div>
	<div class="col-md-10">
		<h5>
			<strong>Robert Laurini <a href="http://www.laurini.net/robert/"
		target="_blank"><span class="fas fa-external-link-alt"></span></a></strong>
		</h5>
		<p>
			Prof. Robert Laurini, (aka Roberto) after having been distinguished
			professor of information technology at INSA/University of Lyon,
			France, was appointed emeritus professor at KSI, United States.
			During his career, he has supervised 44 doctoral students and was a
			member of PhD committees in 17 countries. He worked in the United
			Kingdom, the United States, Italy, Argentina and Mexico. After
			carrying out research in geographic information systems for urban and
			environmental planning, he turned to artificial intelligence and
			geographic knowledge modelling for smart cities and territorial
			intelligence. On these subjects, he wrote 8 books and more than 250
			articles. In 2009, he founded "Universitaires Sans
			Frontières/Academics Without Borders", an NGO dedicated to the
			modernization of universities in which he is in charge of Latin
			America. He speaks French, English, Italian and Spanish. See <a
				href="http://www.laurini.net/robert" target="_blank">www.laurini.net/robert</a>
		</p>
		<h5>
			<strong>A Research Agenda on Knowledge Management for Regional
				Policies</strong>
		</h5>
		<p>
			<strong>Abstract:</strong> A few months ago, there was an expert
			brainstorm worshop whose aim was to create a research agenda
			dedicated to knowledge engineering and management for regional
			planning and policy making. After having analysed the Sustainable
			Development Goals and the possible regional levers, several
			directions and lines of research have been identified and detailed.
			In total, 39 lines of research were proposed, leading to more than
			100 doctorate subjects. The first set was to reveal the
			characteristics of regional knowledge in contrast to conventional
			business knowledge. The other lines of research range from the
			modeling of space-time knowledge to fuzzy rules, from gazetteers to
			ontologies, from scalability to the supersiding of geographic rules,
			etc This research agenda addresses also related to border effects,
			semantic and seamless interoperability of geographic knowledge
			systems, and the needs for curating of knowledge and elimination of
			false knowledge have also been identified. Without forgetting the
			integration of collective intelligence, the use of knowledge
			(case-based reasoning) can also boost the economy/innovation and
			technological and sociological watching.
		</p>
	</div>
</div>
