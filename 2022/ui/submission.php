<h3>Submission Guidelines</h3>
<p class="text-justify">
	Authors must submit an original full paper (12 to 16 pages) that has
	not previously been published. All contributions must be written in
	english. Authors should consult Springer's authors' guidelines and use
	their proceedings templates, either for LaTeX or for MSWord (<a
		href="http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines"
		target="_blank">http://www.springer.com/la/computer-science/lncs/conference-proceedings-guidelines</a>),
	for the preparation of their papers. Springer encourages authors to
	include their ORCIDs in their papers. In addition, the corresponding
	author of each accepted paper, acting on behalf of all of the authors
	of that paper, must complete and sign a <a
		href="https://icai.itiud.org/docs/CCIS_ICAI_2022_LtP.pdf"
		target="_blank">Licence-to-Publish</a> form, through which the
	copyright for their paper is transferred to Springer.
</p>

<p class="text-justify">
	We encourage Overleaf users to use Springer's proceedings template for
	LaTeX <a
		href="https://www.overleaf.com/latex/templates/springer-lecture-notes-in-computer-science/kzwwpvhwnvfj"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>.
</p>
<div class="text-center">
	<a href="https://www.overleaf.com" target="_blank"><img
		src="../img/logos/overleaf.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="Overleaf"></a>
</div>

<h3>Review Process</h3>
<p class="text-justify">
	All submissions will be reviewed by at least 2 experts. <strong>Authors
		must remove personal details, acknowledgments section and any other
		information related to the authors' identity</strong>. In addition,
	all submissions will be screened by Turnitin. <strong><span
		class="text-danger">Papers with coincidence rate over 25% will be
			rejected without revision</span></strong>.
</p>
<div class="text-center">
	<a href="https://www.turnitin.com/" target="_blank"><img
		src="../img/logos/turnitin.png" height="80" data-toggle="tooltip"
		data-placement="bottom" title="Turnitin"></a>
</div>

<h3>Submission Process</h3>

<p class="text-justify">
	To submit or upload a paper please go to <a
		href="https://easychair.org/conferences/?conf=icai_2022"
		target="_blank">https://easychair.org/conferences/?conf=icai_2022</a>.
	<strong>The first anonymous version must be submitted in PDF</strong>.
</p>
<div class="text-center">
	<a href="https://easychair.org" target="_blank"><img
		src="../img/logos/easychair.png" width="150" data-toggle="tooltip"
		data-placement="bottom" title="Easychair"></a>
</div>

<h3>Proceedings</h3>

<p>
	ICAI 2022 proceedings will be published with Springer in their <strong><a
		href="http://www.springer.com/series/7899" target="_blank">Communications
			in Computer and Information Science (CCIS) series</a></strong>. The
	proceedings of all ICAI editios have been published with Springer CCIS
	series <a href="https://link.springer.com/conference/icai2"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
</p>

<div class="text-center">
	<a href="http://www.springer.com" target="_blank"><img
		src="../img/logos/springer.jpg" width="250" data-toggle="tooltip"
		data-placement="bottom" title="Springer"></a> <a
		href="http://www.springer.com/series/7899" target="_blank"><img
		src="../img/logos/ccis.jpg" width="150" data-toggle="tooltip"
		data-placement="bottom"
		title="Communications in Computer Information Science"></a><br> <a
		href="https://www.scimagojr.com/journalsearch.php?q=17700155007&amp;tip=sid&amp;exact=no"
		title="SCImago Journal &amp; Country Rank" target="_blank"><img
		border="0"
		src="https://www.scimagojr.com/journal_img.php?id=17700155007"
		alt="SCImago Journal &amp; Country Rank" /></a>
</div>


<h3>Abstracting/Indexing</h3>
<p>CCIS is abstracted/indexed in DBLP, Google Scholar, EI-Compendex,
	SCImago, Scopus. CCIS volumes are also submitted for the inclusion in
	ISI Proceedings.</p>

<div class="text-center">
	<a href="http://dblp.org/" target="_blank"><img
		src="../img/logos/dbpl.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="DBPL"></a> <a
		href="https://scholar.google.com" target="_blank"><img
		src="../img/logos/googlescholar.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Google Scholar"></a> <a
		href="https://www.elsevier.com/solutions/engineering-village/content/compendex"
		target="_blank"><img src="../img/logos/ei.png" height="100"
		data-toggle="tooltip" data-placement="bottom" title="Ei Compendex"></a>
	<a href="http://www.scimagojr.com/" target="_blank"><img
		src="../img/logos/scimago.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="SCImago"></a> <a
		href="https://www.scopus.com/" target="_blank"><img
		src="../img/logos/scopus.png" height="100" data-toggle="tooltip"
		data-placement="bottom" title="Scopus"></a> <a
		href="https://webofknowledge.com/" target="_blank"><img
		src="../img/logos/isi.jpg" height="100" data-toggle="tooltip"
		data-placement="bottom" title="ISI Web of Knowledge"></a>
</div>
<h3>Conditions</h3>
<ul>
	<li><strong>Papers that do not comply with the LNCS template or number
			of pages will be rejected without revision</strong>.</li>
	<li><strong>Papers out of scope will be rejected without revision</strong>.</li>
	<li><strong>All accepted papers should be presented by an author, who
			must be registered. <span class="text-danger">Virtual presentations
				will be available</span>.
	</strong></li>
	<li><strong>Authors of accepted papers <span class="text-danger">cannot
				be changed</span></strong>.</li>
</ul>

<h3>Associated Journals</h3>
<p>Extended version of selected papers will be invited to publish in:</p>
<ul>
	<li>ParadigmPlus <a
		href="https://journals.itiud.org/index.php/paradigmplus/index"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
		<ul>
			<li>Indexed In: Google Scholar</li>
		</ul>
	</li>
</ul>