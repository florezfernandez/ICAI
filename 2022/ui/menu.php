<br>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="index.php"><span class="fas fa-home"
		aria-hidden="true"></span></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false"> Conference </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=call">Call for Papers</a>
					<a class="dropdown-item" href="index.php?pid=dates">Important Dates</a>
					<a class="dropdown-item" href="index.php?pid=submission">Submission</a>
					<a class="dropdown-item" href="index.php?pid=call4W">Call for
						Workshops</a>
				</div></li>
			<li class="nav-item"><a class="nav-link"
				href="index.php?pid=acceptedWorkshops">Workshops</a></li>
			<li class="nav-item"><a class="nav-link"
				href="index.php?pid=committees">Committees</a></li>
			<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false">Program</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=keynotes">Keynote
						Speakers</a> <a class="dropdown-item"
						href="index.php?pid=acceptedPapers">Accepted Papers</a> <a
						class="dropdown-item" href="index.php?pid=program">General Program</a>

				</div></li>
			<li class="nav-item"><a class="nav-link"
				href="index.php?pid=registration">Registration</a></li>
			<li class="nav-item"><a class="nav-link" href="index.php?pid=venue">Venue</a></li>
		</ul>
	</div>
</nav>
<br>
