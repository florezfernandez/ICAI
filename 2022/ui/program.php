<h3>Program</h3>
<div class="alert alert-success" role="alert">
	<strong>The program is fixed to Peruvian Local time (GMT-5)</strong><br>
	Pre-recorded presentations are available.
</div>
<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th width="30%" class="text-center">Thursday 27 October</th>
		<th width="30%" class="text-center">Friday 28 October</th>
		<th width="30%" class="text-center">Saturday 29 October</th>
	</tr>
	<tr>
		<td>9:30-10:00</td>
		<td class="table-danger"><strong>Conference Opening</strong><br> <i>Room:
				<strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success"><strong>Keynote: Walter H. Curioso</strong><br>
			<i>Room: <strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i></td>
		<td class="table-success"><strong>Keynote: Robert Laurini</strong><br>
			<i>Room: <strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i>
		
		<td class="table-danger"><strong>City Sightseeing Arequipa</strong><br>
			<i>Location: <strong>Cathedral Museum <a
					href="https://www.google.com/maps/place/Cathedral+Museum/@-16.3986522,-71.537447,18.51z/data=!4m5!3m4!1s0x91424a5a064f9d9f:0x11012c2fd1629681!8m2!3d-16.3983085!4d-71.5365092"
					target="_blank"><span class="fas fa-external-link-alt"></span></a></strong>
		</i></td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td class="table-info"><strong>Artificial Intelligence </strong><br> <i>Room:
				<strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i><br> <br> <strong>Decision Systems </strong><br> <i>Room: <strong>201</strong><br>URL:
				<a
				href="https://us02web.zoom.us/meeting/register/tZclc--trj0uGdOGn39BF1YEDKTppOXyRgOl"
				target="_blank">Zoom</a>, Access Code: 332981
		</i></td>
		<td class="table-info"><strong>Data Analysis </strong><br> <i>Room: <strong>Auditorium</strong><br>
				URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i><br> <br> <strong>Health Care Information Systems </strong><br> <i>Room:
				<strong>201</strong><br>URL: <a
				href="https://us02web.zoom.us/meeting/register/tZclc--trj0uGdOGn39BF1YEDKTppOXyRgOl"
				target="_blank">Zoom</a>, Access Code: 332981
		</i></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td class="table-warning"><strong>Workshops WAAI &amp; WDEA</strong> <br>
			<i>Room: <strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i> <br> <br> <strong>Workshops AIESD</strong> <br> <i>Room: <strong>201</strong><br>URL:
				<a
				href="https://us02web.zoom.us/meeting/register/tZclc--trj0uGdOGn39BF1YEDKTppOXyRgOl"
				target="_blank">Zoom</a>, Access Code: 332981

		</i></td>
		<td class="table-info"><strong>ICT-Enabled Social Innovation </strong>
			<br> <i>Room: <strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i> <br> <br> <strong>Image Processing &amp; Robotic Autonomy</strong>
			<br> <i>Room: <strong>201</strong><br>URL: <a
				href="https://us02web.zoom.us/meeting/register/tZclc--trj0uGdOGn39BF1YEDKTppOXyRgOl"
				target="_blank">Zoom</a>, Access Code: 332981

		</i></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-warning"><strong>Workshops WITS &amp; WKMIT &amp; WSM</strong><br>
			<i>Room: <strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i> <br> <br> <strong>Workshop AIESD</strong><br> <i>Room: <strong>201</strong><br>URL:
				<a
				href="https://us02web.zoom.us/meeting/register/tZclc--trj0uGdOGn39BF1YEDKTppOXyRgOl"
				target="_blank">Zoom</a>, Access Code: 332981

		</i></td>
		<td class="table-info"><strong>Software Architectures &amp; Software
				Design Engineering</strong> <br> <i>Room: <strong>Auditorium</strong><br>
				URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i></td>
	</tr>
	<tr>
		<td>18:00-20:00</td>
		<td class="table-danger"><strong>Cocktail and Cultural Activity</strong><br>
			<i>Room: <strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i></td>
		<td class="table-danger"><strong>Conference Closing</strong><br> <i>Room:
				<strong>Auditorium</strong><br> URL: <a
				href="https://us02web.zoom.us/meeting/register/tZAkd-GuqjkiGt3pcWkst0TMsT8tr4U6JQxK"
				target="_blank">Zoom</a>, Access Code: 931999
		</i></td>
	</tr>

</table>

<h4>Leyend</h4>
<table class="table">
	<tr>
		<td class="table-info" width="5%"></td>
		<td width="45%">ICAI Sessions (Presentations of papers in CCIS)</td>
		<td class="table-success" width="5%"></td>
		<td width="45%">Keynotes</td>
	</tr>
	<tr>
		<td class="table-warning" width="5%"></td>
		<td width="45%">ICAI Workshops Sessions (Presentations of papers in
			CEUR-WS)</td>
		<td class="table-danger" width="5%"></td>
		<td width="45%">Social Activities</td>
	</tr>
</table>

<h3>Detailed Program</h3>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center" colspan="2">Thursday 27 October</th>
	</tr>
	<tr>
		<td>9:30-10:00</td>
		<td class="table-danger" colspan="2"><strong>Conference Opening</strong></td>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success" colspan="2"><strong>Keynote: Walter H.
				Curioso. Training the biomedical and health informatics workforce in
				the context of COVID-19 in Peru</strong><br> Session Chair: Henry
			Gomez</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td width="45%" class="table-info"><strong>Artificial Intelligence</strong><br>
			Session Chair: Fernando Yepes-Calderon
			<ul>
				<li><strong>A genetic algorithm for scheduling laboratory rooms: A
						case study</strong><br> Rafael Fuenmayor, Martín Larrea, Mario
					Moncayo, Esteban Moya, Sebastián Trujillo, Juan-Diego Terneus,
					Robinson Guachi, Diego H. Peluffo-Ordoñez, Lorena Guachi-Guachi</li>
				<li><strong>COVID-19 Article Classification using Word-Embedding and
						Different Variants of Deep-Learning Approach</strong><br> Sanidhya
					Vijayvargiya, Lov Kumar, Lalita Bhanu Murthy, Sanjay Misra</li>
				<li><strong>Crop classification using Deep Learning: A quick
						comparative study of modern approaches </strong><br> Hind Raki,
					Juan González-Vergara, Yahya Aalaila, Mouad Elhamdi, Sami
					Bamansour, Lorena Guachi-Guachi, Diego H. Peluffo-Ordoñez</li>
				<li><strong>Internet of Things (IoT) for Secure and Sustainable
						Healthcare Intelligence: Analysis and Challenges </strong><br>
					Sunday Adeola Ajagbe, Sanjay Misra, Oluwaseyi F. Afe, Kikelomo I.
					Okesola</li>
				<li><strong>Multiple Colour Detection of RGB Images using Machine
						Learning Algorithm </strong><br> Joseph Bamidele Awotunde, Sanjay
					Misra, David Obagwu, Hector Florez</li>
				<li><strong>Neural model-based similarity prediction for compounds
						with unknown structures </strong><br> Eugenio Borzone, Leandro
					Ezequiel Di Persia, Matias Gerard</li>
			</ul></td>
		<td width="45%" class="table-info"><strong>Decision Systems</strong><br>
			Session Chair: Hector Florez
			<ul>
				<li><strong>A systematic review on phishing detection: A perspective
						beyond a high accuracy in phishing detection </strong><br> Daniel
					Alejandro Barreiro Herrera, Jorge Eliecer Camargo Mendoza</li>
				<li><strong>Application of Duality Properties of Renyi Entropy for
						Parameter Tuning in an Unsupervised Machine Learning Task </strong><br>
					Sergei Koltcov</li>
				<li><strong>Preliminary Study for Impact of Social Media Networks on
						Traffic Prediction </strong><br> Valeria Laynes Fiascunari, Luis
					Rabelo</li>
				<li><strong>Website Phishing Detection Using Machine Learning
						Classification Algorithms </strong><br> Mukta Mithra Raj, J. Angel
					Arul Jothi</li>

			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td width="45%" class="table-warning"><strong>Workshops WAAI &amp;
				WDEA</strong><br>Session Chair: María F. Pollo-Cattaneo<br>
			<ul>
				<li><strong>Analysis on early dropouts in Engineering careers </strong><br>Ignacio
					Manes, Tomas Lubertino, Jorge Anca, Karen Roberts, Hernán Merlino</li>
				<li><strong>Depression diagnosis using text-based AI methods — A
						Systematic Review </strong><br>Martín Di Felice, Parag Chatterjee,
					María F. Pollo-Cattaneo</li>
				<li><strong>Satellite Orbit Prediction Using Machine Learning
						Approach </strong><br>Giridhar Jadala, Gowri Namratha Meedinti, R.
					Delhi Babu</li>
				<li><strong>Maximizing Student Retention using Supervised Models
						Informed by Student Counseling Data</strong><br>John Anderson
					Rodriguez Ramirez, Olmer García-Bedoya, Ixent Galpin</li>
				<li><strong>Shareholder Structure of Major Technology Companies: A
						Graph Analytics Study during COVID-19 and Beyond </strong><br>Julio
					C. Esquivel, Ixent Galpin, Oscar M. Granados</li>
			</ul></td>

		<td width="45%" class="table-warning"><strong>Workshop AIESD</strong><br>Session
			Chair: Marcelo Leon<br>
			<ul>
				<li><strong>Bibliometric software: The most commonly used in
						research </strong><br> Alejandra Colina Vargas, Marcos
					Espinoza-Mina, Diana López Alvarez, Johanna Navarro Espinosa</li>
				<li><strong>Bio-Business of polymer derived from cassava waste for
						the industrial plastic sector</strong><br> Mercy Agila, Larry
					Yumibanda, Diana Merchán</li>
				<li><strong>Cost of capital and finance evaluation of a family SME
						enterprise of an Ecuadorian ecotourism resort in the San Carlos
						Parish, Los Ríos Province </strong><br> Carlos Arturo San Andrés
					Fuentes, Pablo Ricardo San Andrés Reyes, Ivonne Elizabeth Paredes
					Chévez, César Santana Moncayo</li>
				<li><strong>Dynamic Models to Determine External Factors and their
						Impact on the Shrimp Price Forecast </strong><br> David
					Zaldumbide, Alejandro Pacheco</li>
				<li><strong>How to use Instagram to travel the world? An approach to
						discovering relevant insights from tourist’s media content</strong><br>
					Angel Fiallos</li>
				<li><strong>Public safety perception in Ecuador: An approach from
						social networks over data analytics </strong><br>Maria de Lourdes
					Díaz, Jorge Berrezueta, Gonzalo Albán Molestina, Andres Ortega</li>
				<li><strong>Relationship between Corporate Social Responsibility,
						Assets and Income of Companies in Ecuador </strong><br>Arnaldo
					Vergara-Romero, Lisette Garnica-Jarrin, Yadira Armas-Ortega, César
					Pozo-Estupiñan</li>

			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td width="45%" class="table-warning"><strong>Workshops WITS &amp;
				WKMIT &amp; WSM</strong><br>Session Chair: Juan Camilo Ramirez<br>
			<ul>
				<li><strong>Mobility studies in Villavicencio: Situation and
						prospects towards a smart mobility </strong><br>Carlos Alberto
					Díaz Riveros, Alejandra Baena, Juan Camilo Ramirez</li>
				<li><strong>Knowledge Representation and Technologies in the Latin
						American Academic literature </strong><br>Luciano Straccia,
					Adriana Maulini, María Gracia Bongiorno, Matías Giorda, María
					Florencia Pollo-Cattaneo</li>
				<li><strong>The SECI knowledge creation model: A look through
						sociology</strong><br>Patricia Gerlero</li>
				<li><strong>Construction of a Domain Metamodel using EMF for
						Semiautomatic Generation Of Web Applications</strong><br>Lady
					Viviana Garay González</li>
				<li><strong>Implementation of a Domain Metamodel for the Generation
						of UML Documentation Through Model Transformation Chains</strong><br>Jeyson
					Stith Arévalo Sandoval</li>
			</ul></td>

		<td width="45%" class="table-warning"><strong>Workshop AIESD</strong><br>Session
			Chair: Marcelo Leon<br>
			<ul>
				<li><strong>Retrospective of the scientific production on
						e-democracy </strong><br> Alejandra Colina Vargas, Marcos
					Espinoza-Mina</li>
				<li><strong>Smart Contracts: An Opportunity for Companies
						Modernization in a Post-COVID-19 World </strong><br> Joe A.
					Parrales, María G. Cornejo, Hector Florez</li>
				<li><strong>Sticky Floor and Glass Ceiling in Ecuador. The Evolution
						of the Gender Wage Gap, 2010-2021 </strong><br> Diego
					Linthon-Delgado, Lizethe Méndez-Heras, Gino Cornejo-Marcos</li>
				<li><strong>Technical indicator for a better intraday understanding
						of uptrend or downtrend the financial market using volume
						transactions as a trigger</strong><br> Franklin Gallegos-Erazo</li>
				<li><strong>The effects of Dry Spells on crop diversification in the
						Brazilian Northeast region </strong><br> Elena Piedra-Bonilla,
					Laís Oliveira, Denis Da Cunha, Marcelo Braga</li>
				<li><strong>Valuation of the impact of the Covid-19 global pandemic
						on the sector of professional, technical and administrative
						activities in Ecuador for the year 2020 </strong><br> Nelson
					Guillermo Granja Cañizares, Pablo Ricardo San Andrés Reyes, Ivonne
					Elizabeth Paredes Chévez</li>
				<li><strong>Web Service for document management of university degree
						projects </strong><br> Camilo Sarmiento, Leidy Lozano, Guillermo
					Gaona, Marcelo Leon</li>
			</ul></td>
	</tr>
</table>

<table class="table">
	<tr>
		<th width="10%" class="text-center">Time</th>
		<th class="text-center" colspan="2">Friday 28 October</th>
	</tr>
	<tr>
		<td>10:00-11:00</td>
		<td class="table-success" colspan="2"><strong>Keynote: Robert Laurini.
				A Research Agenda on Knowledge Management for Regional Policies</strong><br>
			Session Chair: Hector Florez</td>
	</tr>
	<tr>
		<td>11:00-13:00</td>
		<td width="45%" class="table-info"><strong>Data Analysis</strong><br>
			Session Chair: Fernando Yepes-Calderon
			<ul>
				<li><strong>Classifying Incoming Customer Messages for an E-Commerce
						Site using Supervised Learning </strong><br> Misael Andrey Albañil
					Sánchez, Ixent Galpin</li>
				<li><strong>Comparison of Machine Learning Models for Predicting
						Rainfall in Tropical Regions: The Colombian Case </strong><br>
					Carlos Andrés Rocha-Ruiz, Olmer García-Bedoya, Ixent Galpin</li>
				<li><strong>Deep Mining Covid-19 Literature </strong><br> Joshgun
					Sirajzade, Pascal Bouvry, Christoph Schommer</li>
				<li><strong>Keyword-based Processing for Assessing Short Answers in
						the Educational Field </strong><br> Javier Sanz-Fayos, Luis
					de-la-Fuente-Valentín, Elena Verdú</li>
				<li><strong>Statistical Characterization of Image Intensities in
						Regions Inside Arteries to Facilitate the Extraction of Center
						Lines in Atherosclerosis Frameworks </strong><br> Fernando
					Yepes-Calderon</li>
				<li><strong>Text Encryption with Advanced Encryption Standard (AES)
						for Near Field Communication (NFC) using Huffman Compression </strong><br>
					Oluwashola David Adeniji, Olaniyan Eliais Akinola, Ademola Olusola
					Adesina, Olamide Afolabi</li>
			</ul></td>
		<td width="45%" class="table-info"><strong>Health Care Information
				Systems </strong><br> Session Chair: Hector Florez
			<ul>
				<li><strong>AESRSA: A New Cryptography Key for Electronic Health
						Record Security </strong><br> Sunday Adeola Ajagbe, Hector Florez,
					Joseph Bamidele Awotunde</li>
				<li><strong>Detection of COVID-19 Using Denoising Autoencoders and
						Gabor Filters </strong><br> Jayalakshmi Saravanan, T. Ananth
					Kumar, Andrew C. Nwanakwaugwu, Sunday Adeola Ajagbe, Ademola T.
					Opadotun, Deborah D. Afolayan, Oluwafemi O. Olawoyin</li>
				<li><strong>Development of Mobile Applications to Save Lives on
						Catastrophic Situations </strong><br> Rocío Rodriguez-Guerrero,
					Alvaro A. Acosta, Carlos A. Vanegas</li>
				<li><strong>Internet of Things with Wearable Devices and Artificial
						Intelligence for Elderly Uninterrupted Healthcare Monitoring
						Systems </strong><br>Joseph Bamidele Awotunde, Sunday Adeola
					Ajagbe, Hector Florez</li>

			</ul></td>
	</tr>
	<tr>
		<td>13:00-14:00</td>
		<td class="table-active text-center" colspan="2">Lunch</td>
	</tr>
	<tr>
		<td>14:00-16:00</td>
		<td width="45%" class="table-info"><strong>ICT-Enabled Social
				Innovation</strong><br>Session Chair: Marco Antonio Vasquez Pauca<br>
			<ul>
				<li><strong>A Wayuunaiki Translator to Promote Ethno-tourism using
						Deep Learning Approaches </strong><br> Rafael Negrette, Ixent
					Galpin, Olmer García-Bedoya</li>
				<li><strong>Rating the acquisition of pre-writing skills in
						children: an analysis based on computer vision and data mining
						techniques in the Ecuadorian context </strong><br> Adolfo
					Jara-Gavilanes, Romel Ávila-Faicán, Vladimir Robles-Bykbaev, Luis
					Serpa-Andrade</li>
				<li><strong>Study techniques: procedure to promote the comprehension
						of texts in university students </strong><br> Eilen Lorena Pérez
					Montero, Yolanda Díaz Rosero</li>
				<li><strong>TPACK and Toondoo Digital Storytelling tool transform
						Teaching and Learning </strong><br> Serafeim A. Triantafyllou</li>
			</ul></td>
		<td width="45%" class="table-info"><strong>Image Processing &amp;
				Robotic Autonomy </strong><br>Session Chair: María F. Pollo-Cattaneo<br>
			<ul>
				<li><strong>Fully Automated Lumen Segmentation Method and BVS Stent
						Struts Detection in OCT images </strong><br>Julia Duda, Izabela
					Cywińska, Elżbieta Pociask</li>
				<li><strong>Predictive Modeling Toward the Design of a Forensic
						Decision Support System using Cheiloscopy for Identification from
						Lip Prints</strong><br> Agustin Sabelli, Parag Chatterjee, María
					F. Pollo-Cattaneo</li>
				<li><strong>Automotive industry applications based on Industrial
						Internet of Things (IIoT). A review </strong><br> Luis Carlos
					Guzman Mendoza, Juan Carlos Amaya, César A. Cárdenas, Carlos Andrés
					Collazos Morales</li>
				<li><strong>Implementation of an IoT Architecture for Automated
						Garbage Segregation </strong><br> Angel Antonio Rodríguez Varela,
					Germán A. Montoya, Carlos Lozano-Garzón</li>
			</ul></td>
	</tr>
	<tr>
		<td>16:00-18:00</td>
		<td class="table-info" colspan="2"><strong>Software Architectures
				&amp; Software Design Engineering</strong><br>Session Chair:
			Fernando Yepes-Calderon<br>
			<ul>
				<li><strong>Architecture on Knowledge Management Systems: its
						presence in the academic literature </strong><br>Luciano Straccia,
					María F Pollo-Cattáneo, Matías Giorda, M. Gracia Bongiorno, Adriana
					Maulini</li>
				<li><strong>Automatic Location and Suppression of Calcifications and
						Atheromas by Gradient Change in the Pattern of Intensities Inside
						the Carotid Artery </strong><br> Fernando Yepes-Calderon</li>
				<li><strong>Introducing Planet-Saving Strategies to Measure,
						Control, and Mitigate Greenhouse Gas Emissions and Water Misuse in
						Household Residences </strong><br>Ronald S. Marin Cifuentes,
					Adriana M. Florez Laiseca, Fernando Yepes-Calderon</li>
				<li><strong>Team Productivity in Agile Software Development: A
						Systematic Mapping Study </strong><br> Marcela Guerrero-Calvache,
					Giovanni Hernández</li>
			</ul></td>
	</tr>

</table>