<h3>Accepted Workshops</h3>

<h4>
	5th International Workshop on Applied Artificial Intelligence (WAAI) <a
		href="https://grupogemis.com.ar/icai/waai/2024/" target="_blank"><span
		class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Artificial
	Intelligence is an annual event intended to be an important forum of
	the Artificial Intelligence (AI) community, organized by the GRUPO
	GEMIS of the Universidad Tecnológica Nacional Facultad Regional Buenos
	Aires. The workshop aims to provide a forum for researchers and AI
	community members to discuss and exchange ideas and experiences on
	diverse topics of AI. It also seeks to strengthen a space for
	discussion of innovative ideas as well as development of emerging
	technologies and practical experiences related to Artificial
	Intelligence in general and each of its technologies in particular.
	Submissions are invited on both applications of AI and new tools and
	foundations. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=waai2024"
		target="_blank">https://easychair.org/conferences/?conf=waai2024</a> <br>
	<strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/utn.png" height="100">
</div>
<hr>

<h4>
	6th International Workshop on Applied Informatics for Economy, Society,
	and Development (AIESD) <a href="https://ecotec.edu.ec/aiesd-2024/"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> The workshop on Applied Informatics for
	Economy, Society and Development organized by the Universidad Ecotec
	(Ecuador) seeks to promote discussion and dissemination of current
	economics research, systems for territorial intelligence, e-
	participation, e-democracy and territorial economic development; with
	special emphasis on social and environmental development. The objective
	of this edition of AIESD is to discuss aspects related to computer
	science applied in society. The idea is to learn the different
	techniques and tools that are used to study social phenomena, economic
	growth, the interaction between economic analysis and decision-making.
	The workshop seeks to promote an atmosphere of dialogue among the
	community of professionals working on issues related to technology and
	its application in the economy as well as society. <br>Submission URL:
	<a href="https://easychair.org/conferences/?conf=aiesd2024"
		target="_blank">https://easychair.org/conferences/?conf=aiesd2024</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/ecotec.png" height="80">
</div>

<hr>

<h4>
	6th International Workshop on Data Engineering and Analytics (WDEA) <a
		href="https://www.utadeo.edu.co/es/noticia/convocatorias/home/1/sixth-icai-workshop-data-engineering-and-analytics-wdea-2024"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> Data has become pervasive. Data Analytics,
	Data Mining, Data Science, or more generally the practice of
	identifying patterns or constructing mathematical models based on data,
	is at the heart of most new business models or projects. This, coupled
	with what has been termed the Big Data phenomenon, used to describe
	challenging scenarios involving either datasets that are too big to be
	handled using traditional approaches, datasets with diverse formats and
	semantics, data streams that need to be actioned in real time, or a
	combination of these, has led to a whole new series of rapidly evolving
	tools, algorithms and methods which combine data engineering, computer
	science, statistics and mathematics. The aim of this workshop is to
	present recent results at the intersection of these areas, discussing
	new methods or algorithms, or applications including Machine Learning,
	Databases, optimization or any type of algorithms which consider
	managing or obtaining value from data. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=wdea2024"
		target="_blank">https://easychair.org/conferences/?conf=wdea2024</a> <br>
	<strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/utadeo.png" height="70">
</div>

<hr>
<h4>
	3rd International Workshop on Intelligent Transportation Systems and
	Smart Mobility Technology (WITS) <a
		href="https://sites.google.com/uan.edu.co/wits" target="_blank"><span
		class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> The digital transformation of traffic
	monitoring systems has led to an increased interest from the academic
	community, governments and the general public on the concept of smart
	mobility, consisting of the optimized use of transport networks through
	the automated analysis of traffic data and, especially, the
	implementation of intelligent transportation systems. This type of
	technology focuses on optimizing how people travel, decreasing traffic
	congestion, improving road safety, and enhancing governmental
	decision-making during the construction or expansion of infrastructure.
	The International Workshop on Intelligent Transportation Systems and
	Smart Mobility Technology (WITS), part of the International Conference
	on Applied Informatics (ICAI), is an annual event organized by
	Universidad Antonio Nariño in Bogotá, Colombia, and intended to be an
	important forum for the discussion and dissemination of contemporary
	research on the study and design of software providing integrated
	information in order to allow drivers, commuters and pedestrians to
	make a safer and more coordinated use of urban transport networks.
	Partic- ipants in the workshop will find a space for the exchange of
	innovative ideas and practical experiences on the use of this type of
	technology as well as for the establishment of collaborative networks.
	The work- shop welcomes submissions on both practical and theoretical
	proposals on ITSs and related technological solutions. <br>Submission
	URL: <a href="https://easychair.org/conferences/?conf=wits2024"
		target="_blank">https://easychair.org/conferences/?conf=wits2024</a> <br>
	<strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/uan.png" height="80">
</div>
<hr>

<h4>
	4th International Workshop on Knowledge Management, Innovation and
	Technologies (WKMIT) <a
		href="https://grupogemis.com.ar/icai/wkmit/2024/" target="_blank"><span
		class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> WKMIT is an annual event intended to be an
	important forum of the Knowledge Management and Innovation community,
	organized by the GRUPO GEMIS of the Universidad Tecnológica Nacional
	Facultad Regional Buenos Aires. The knowledge is a strategic resource
	to obtain better results in the organizations and its management
	promotes continuous improvement practices and innovation. A complete
	and integral knowledge management requires an approach from different
	perspectives: people, organizational culture, processes, tools and
	technologies, among others. The workshop aims at providing a forum for
	researchers and KM community members to discuss and exchange ideas and
	experiences on diverse topics of KM. It also seeks to strengthen a
	space for discussion of innovative ideas, development of emerging
	technologies and practical experiences related to Knowledge Management
	in general and each of its technologies in particular. <br>Submission
	URL: <a href="https://easychair.org/conferences/?conf=wkmit2024"
		target="_blank">https://easychair.org/conferences/?conf=wkmit2024</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/utn.png" height="100">
</div>
<hr>

<h4>
	1st International Workshop on Sustainability Challenges in Tourism and
	Smart Destinations (SCTSD) <a
		href="https://ots.upse.edu.ec/index.php/eventos/workshop"
		target="_blank"><span class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> IThe workshop on Sustainability Challenges
	in Tourism and Smart Destinations organized by the Universidad Estatal
	Peninsula de Santa Elena (Ecuador) seeks to promote discussion of the
	application of sustainability criteria for the development of tourist
	products and destinations, including management of natural and cultural
	resources, strategies and approaches for the promotion of tourist
	destinations. Socialize and disseminate the results of research in
	smart tourism in Ecuador and the world. Promote academic debate on
	experiences, innovation in the hotel and tourism field and new trends
	in the industry. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=sctsd2024"
		target="_blank">https://easychair.org/conferences/?conf=sctsd2024</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/upse.png" height="120">
</div>
<hr>

<h4>
	1st International Workshop on Systems Engineering Applied to Internet
	of Health Things (WSEAI) <a
		href="https://great-ufc.github.io/icaiw-wseai/" target="_blank"><span
		class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> The International Workshop on Systems
	Engineering Applied to the Internet of Health Things is an event that
	aims to be an important forum for the research community in the field
	of the Internet of Health Things (IoHT), organized as a partnership
	between GREat of the Universidade Federal Ceará, located in Brazil, and
	ITiSB of Universidad Andrés Bello, located in Chile. The workshop
	brings advanced research topics and challenges to be discussed by
	members of the IoHT community who will have an opportunity to exchange
	ideas and experiences in the field of IoHT. It also seeks to strengthen
	a space for discussing innovative ideas and the development of emerging
	technologies, proposing software and hardware systems, and practical
	experiences related to IoHT in general and each of its technologies.
	Submissions on new solutions to IoHT challenges and new tools are
	welcome. <br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=wseai2024"
		target="_blank">https://easychair.org/conferences/?conf=wseai2024</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/ufc.png" height="120"> <img src="../img/logos/uab.jpg"
		height="120"> <img src="../img/logos/uece.png" height="120"> <img
		src="../img/logos/ifma.png" height="120">
</div>

<hr>

<h4>
	3rd International Workshop on Systems Modeling (WSM) <a
		href="ws/wsm.html" target="_blank"><span
		class="fas fa-external-link-alt"></span></a>
</h4>
<p>
	<strong>Abstract:</strong> International Workshop on Systems Modeling
	(WSM) aims to bring together researchers and practitioners working in
	different areas of systems modeling to provide an opportunity for the
	modeling community to further advance the foundations of modeling and
	modeling in emerging areas. It is mainly focused on Business Process
	Modeling, Model-Driven Architecture, Model-Driven Engineering, Modeling
	in Software Engineering among other areas related to software modeling.
	<br>Submission URL: <a
		href="https://easychair.org/conferences/?conf=wsm2024" target="_blank">https://easychair.org/conferences/?conf=wsm2024</a>
	<br> <strong>Organized by:</strong>
</p>
<div class="text-center">
	<img src="../img/logos/ud.png" height="120">
</div>
<hr>



<h3>Important Dates</h3>
<ul>
	<li>Paper Submission: August 25, 2024</li>
	<li>Paper Notification: September 8, 2024</li>
	<li>Camera Ready: September 22, 2024</li>
	<li>Authors Registration: September 22, 2024</li>
</ul>

<h3>Submission Guidelines</h3>
<p class="text-justify">
	Authors must submit an original full paper (12 to 15 pages) that has
	not previously been published. All contributions must be written in
	English. Authors must follow one-column CEUR styles either for LaTeX or
	for MSWord (<a href="http://ceur-ws.org/Vol-XXX/CEURART.zip"
		target="_blank">http://ceur-ws.org/Vol-XXX/CEURART.zip</a>), for the
	preparation of their papers. <br>All submitted papers are reviewed by
	at least 2 experts. Workshops undergo a double-blind peer review
	process. The initial submission shall consist of a PDF file with all
	information identifying the authors removed, including any
	acknowledgment section. <br> We encourage authors of accepted papers to
	include their ORCIDs in their papers. In addition, the corresponding
	author of each accepted paper, acting on behalf of all of the authors
	of that paper, must complete and sign by hand an <a
		href="docs/CEUR_ICAIW_2024.pdf" target="_blank">Author Agreement</a>
	form.
</p>

<h3>Proceedings</h3>
<p>Proceedings will be submitted to CEUR Workshop Proceedings for online
	publication.</p>

<h3>Indexing</h3>
<p>Proceedings will be indexed in DBLP, Google Scholar, and Scopus.</p>


