<div class="col-lg-3 col-md-4 text-center">
	<img src="../img/logos/icai.png" width="200">
</div>
<div class="col-lg-9 col-md-12">
	<h4>
		<i><strong>7<sup>th</sup> International Conference on Applied
				Informatics
		</strong></i>
	</h4>
	<h5>
		24 to 26 October 2024 <br>Universidad Andrés Bello<br> Viña del Mar,
		Chile <img src="https://www.worldometers.info/img/flags/ci-flag.gif"
			height="16px">
	</h5>
</div>