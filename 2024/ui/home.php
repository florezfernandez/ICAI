<br>
<div class="row">
	<div class="col">
		<h3>ICAI 2024</h3>
		<p class="text-justify">
			The 7<sup>th</sup> International Conference on Applied Informatics
			(ICAI) aims to bring together researchers and practitioners working
			in different domains in the field of informatics in order to exchange
			their expertise and discuss the perspectives of development and
			collaboration
		</p>
		<p class="text-justify">
			ICAI 2024 will be held at the <strong>Universidad Andrés Bello</strong>
			located in <strong>Viña del Mar, Chile</strong>, from 24 to 26
			October 2024. It is organized by the Universidad Andrés Bello and the
			ITI Research Group which belongs to the Universidad Distrital
			Francisco Jose de Caldas.
		</p>
		<p class="text-justify">ICAI 2024 is proudly sponsored by ITI Research
			Group, Science-Based Platforms, and Springer.</p>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2024 Proceedings in CCIS </strong>
			</h5>
			ICAI 2024 proceedings in Communications in Computer and Information
			Science (CCIS) by Springer are now available at:
			<ul>
				<li>Volume 2236 <a
					href="https://link.springer.com/book/10.1007/978-3-031-75144-8"
					target="_blank">https://link.springer.com/book/10.1007/978-3-031-75144-8</a></li>
				<li>Volume 2237 <a
					href="https://link.springer.com/book/10.1007/978-3-031-75147-9"
					target="_blank">https://link.springer.com/book/10.1007/978-3-031-75147-9</a></li>
			</ul>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI 2024 Best Papers in SN Computer Science</strong>
			</h5>
			ICAI 2024 Best papers in SN Computer Science by Springer are now
			available at <br> <a
				href="https://link.springer.com/journal/42979/topicalCollection/AC_8a9a6dfdc7835249b1a19f50e6e3630d"
				target="_blank">https://link.springer.com/journal/42979/topicalCollection/AC_8a9a6dfdc7835249b1a19f50e6e3630d</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Workshops 2024 in CEUR-WS</strong>
			</h5>
			ICAI Workshops 2024 proceedings in CEUR Workshop Proceedgins are now
			available at volume 3795 <br> <a href="https://ceur-ws.org/Vol-3795"
				target="_blank">https://ceur-ws.org/Vol-3795</a>
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>Best Paper Award</strong>
			</h5>
			The paper <strong>Metaphor Identification and Interpretation in
				Corpora with ChatGPT </strong> <a
				href="https://link.springer.com/article/10.1007/s42979-024-03331-0"
				target="_blank"><span class="fas fa-external-link-alt"
				aria-hidden="true"></span></a> authored by <strong><i>Eduardo
					Puraivan, Irene Renau, Nicolás Riquelme </i></strong> from the <strong>Universidad
				Viña del Mar, Chile</strong> has been selected as <strong>Best Paper</strong>
			of ICAI 2024.
		</div>
		<div class="alert alert-success" role="alert">
			<h5>
				<strong>ICAI Certificates</strong>
			</h5>
			ICAI 2024 certificates are available at: <a
				href="https://icai-c.itiud.org/certificate.php?edition=2024"
				target="_blank">ICAI-C</a>
		</div>
		<p>
			Authors of selected papers will be invited to submit a extended paper
			to the dedicated Special Issue <a
				href="https://www.mdpi.com/journal/mathematics/special_issues/FDRFRWJO8P"
				target="_blank"><strong>Mathematical Methods and Models Applied in
					Information Technology</strong></a> of the journal <strong>Mathematics</strong>
			by MDPI.
		</p>
		<p>ICAI has been previously held in:</p>
		<ul>
			<li>2023: Guayaquil, Ecuador <img
				src="https://www.worldometers.info/img/flags/ec-flag.gif"
				height="16px"></li>
			<li>2022: Arequipa, Peru <img
				src="https://www.worldometers.info/img/flags/pe-flag.gif"
				height="16px"></li>
			<li>2021: Buenos Aires, Argentina <img
				src="https://www.worldometers.info/img/flags/ar-flag.gif"
				height="16px"></li>
			<li>2020: Ota, Nigeria <img
				src="https://www.worldometers.info/img/flags/ni-flag.gif"
				height="16px"></li>
			<li>2019: Madrid, Spain <img
				src="https://www.worldometers.info/img/flags/sp-flag.gif"
				height="16px"></li>
			<li>2018: Bogota, Colombia <img
				src="https://www.worldometers.info/img/flags/co-flag.gif"
				height="16px"></li>
		</ul>
	</div>
</div>