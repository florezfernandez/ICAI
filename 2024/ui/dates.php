<h3>Important Dates</h3>
<ul>
	<li>Paper Submission:
		<ul>
			<li><strong>July 7, 2024</strong></li>
			<li><del>June 16, 2024</del></li>
		</ul>
	</li>
	<li>Paper Notification:
		<ul>			
			<li><strong>July 28, 2024</strong></li>
			<li><del>July 14, 2024</del></li>
		</ul>
	</li>
	<li>Camera Ready:
		<ul>
			<li><strong>August 25, 2024</strong></li>
			<li><del>August 11, 2024</del></li>
		</ul>
	</li>
	<li>Authors Registration:
		<ul>
			<li><strong>August 25, 2024</strong></li>
			<li><del>August 11, 2024</del></li>			
		</ul>
	</li>
</ul>