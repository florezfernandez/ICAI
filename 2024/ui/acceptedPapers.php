<h3>Accepted Papers</h3>
<h4>Artificial Intelligence</h4>
<ul>
	<li><strong>Artificial Intelligence-Based Quantification and Prognostic
			Assessment of CD3, CD8, CD146, and PDGF-Rβ Biomarkers in Sporadic
			Colorectal Cancer </strong><br> Florencia Adriana Lohmann, Martín
		Isac Specterman Zabala, Julieta Natalia Soarez, Maximiliano Dádamo,
		Mónica Alejandra Loresi, María de las Nieves Diaz, Walter Hernán
		Pavicic, Marcela Fabiana Bolontrade, Marcelo Raúl Risk, Juan Pablo
		Santino, Carlos Alberto Vaccaro, Tamara Alejandra Piñero</li>
	<li><strong>Automatic differentiation between coriander and parsley
			using MobileNetV2 </strong> <br>Ian Páez, José Arévalo, Mateo
		Martinez, Martin Molina, Robinson Guachi, D. H. Peluffo-Ordóñez,
		Lorena Guachi-Guachi</li>
	<li><strong>Automatic identification of forest areas in the ”Carolina”
			Park using ResNet50, EfficientNetB0 and VGG16: A case study </strong>
		<br>Julian Guapaz, Juan Pablo Jervis, Diego Haro, Jefferson Padilla,
		Robinson Guachi, D. H. Peluffo-Ordóñez, Lorena Guachi-Guachi</li>
	<li><strong>Core Concept Identification in Educational Resources via
			Knowledge Graphs and Large Language Models </strong> <br>Daniel
		Reales, Rubén Manrique, Christian Grévisse</li>
	<li><strong>Deep Convolutional Neural Network for Autonomic Function
			Estimation in Intensive Care Patients </strong> <br>Javier
		Zelechower, Eduardo San Roman, Ivan Huespe, Valeria Burgos, Jose
		Gallardo, Francisco Redelico, Marcelo Risk</li>
	<li><strong>Deep Learning Techniques for Oral Cancer Detection:
			Enhancing Clinical Diagnosis by ResNet and DenseNet Performance </strong>
		<br>Pablo Ormeño-Arriagada, Eduardo Navarro, Carla Taramasco, Gustavo
		Gatica, Juan Pablo Vásconez</li>
	<li><strong>Intrusion Detection: A Comparison Study of Machine Learning
			Models Using Unbalanced Dataset </strong> <br>Sunday Ajagbe, Joseph
		Bamidele Awotunde, Hector Florez</li>
	<li><strong>Machine Learning Operations Applied to Development and
			Model Provisioning </strong> <br>Oscar Mendez, Jorge Camargo, Hector
		Florez</li>
	<li><strong>MultiMF: a Deep Multimodal Academic Resources
			Recommendation System </strong> <br>Rafael Tejón, Juan Sanguino,
		Ruben Manrique</li>
	<li><strong>NetScribed: A deep learning approach for machine-based
			melody transcription of audio files </strong> <br>Francois Volschenk,
		Dustin van Der Haar</li>
	<li><strong>Safeguarding Patient Data: Advanced Security in Wireless
			Body Area Networks and AI-Driven Healthcare Systems </strong> <br>Vinesh
		Thiruchelvam, Reshiwaran Jegatheswaran, Julia Binti Juremi</li>
	<li><strong>YOLO Convolutional Neural Network for Building Damage
			Detection in Hydrometeorological Disasters Using Satellite Imagery </strong>
		<br>César Luis Moreno González, Germán A. Montoya, Carlos Lozano
		Garzón</li>
</ul>

<h4>Bioinformatics</h4>

<ul>
	<li><strong>Cytosine methylation and demethylation model ---
			equilibrium point and the influence of enzymes on cytosine forms
			levels </strong> <br>Krzysztof Fujarewicz, Karolina Kurasz</li>
	<li><strong>Extraction and selection of multi-omic features for the
			breast cancer survival prediction </strong> <br>Daria Kostka,
		Wiktoria Płonka, Roman Jaksik</li>
</ul>

<h4>Cloud Computing</h4>

<ul>
	<li><strong>Migration from On-Premises to Cloud: Challenges and
			Opportunities </strong> <br>Rossana M. C. Andrade, Wilson Castro,
		Leonan Carneiro, Erik Bayerlein, Icaro S. de Oliveira, Pedro A. M.
		Oliveira, Ismayle S. Santos, Tales P. Nogueira, Victória T. Oliveira</li>
</ul>
<h4>Data Analysis</h4>
<ul>
	<li><strong>Application of Predictive Techniques for Startup Survival:
			The Ecuadorian Case </strong> <br>Marcos Espinoza, Alejandra Colina
		Vargas</li>
	<li><strong>Assessing Player Contributions in League of Legends
			Matches: An Analytical Approach </strong> <br>Cesar Diaz, Manuel
		Perez Parra, Pau Soler Valadés, Aitor Mier Pons</li>
	<li><strong>Improvement of Acute and Chronic Nutritional Status by
			supplying a Metabolic Biostimulator to Children in High Risk of
			Malnutrition. Introducing a Technological Platform to Enable
			Automatic Analyses and Reporting </strong> <br>Juan Sebastian
		Serrano, Fernando Yepes-Calderon</li>
	<li><strong>Relationship between demographic, geographical and access
			to health services factors </strong> <br>Jhonatan Ortega, Johanna
		Torres, Cesar O. Diaz, Fernando Soler</li>

	<li><strong>RGB Image Reconstruction for Precision Agriculture: A
			Systematic Literature Review </strong> <br>Christian Unigarro, Hector
		Florez</li>



</ul>

<h4>Decision Systems</h4>
<ul>
	<li><strong>Developing AI-Driven Cross-Platform Geolocation for
			Enhanced Strategic Decision-Making </strong> <br>Angel Fiallos</li>
	<li><strong>From Data to Decisions: Performance Evaluation of Retail
			Recommender Systems </strong> <br>Juan Alberto Blanco-Serrano, Ixent
		Galpin</li>
	<li><strong>Optimizing Fraud Detection in Traffic Accident Insurance
			Claims through AI Models: Strategies and Challenges </strong> <br>Luis
		Miguel Mora-Escobar, Ixent Galpin</li>

</ul>

<h4>Game Development</h4>
<ul>
	<li><strong>Code Legends: RPG Game as a Support Tool for Programming in
			High School </strong> <br>Alailson E. S. Gomes, Vitor Márcio D. Mota,
		Pedro Almir M. Oliveira</li>
	<li><strong>Development of mental agility mini-games in a video game
			for early detection of mild cognitive impairment: an innovative
			approach in mental health </strong> <br>Gustavo Adolfo Lemos Chang,
		María de Lourdes Díaz Carrillo, Manuel Osmany Ramírez Pírez</li>
</ul>

<h4>Health Care Information Systems</h4>
<ul>
	<li><strong>Clustering-based Health Indicators for Health-related
			Quality of Life </strong> <br>Pedro A. M. Oliveira, Rossana M. C.
		Andrade, Pedro A. Santos Neto, Ismayle S. Santos, Evilasio C. Junior,
		Victória T. Oliveira, Nadiana K. N. Mendes</li>
	<li><strong>Comprehensive Monitoring System for High-Risk Pregnancies </strong>
		<br>Santiago Paeres, German Montoya, Carlos Andres Lozano Garzon</li>
	<li><strong>Development of a Software Prototype for Assisting People
			with Quadriplegia: An Approach Based on Interface Analysis and
			Computer Vision </strong> <br>Braian F. Ramírez, Daniel E. Torres,
		Lisseth T. Quilindo, Óscar A. Méndez</li>

</ul>

<h4>Interdisciplinary Information Studies</h4>
<ul>
	<li><strong>Analyzing emotional and attentional responses to
			promotional images using a remote eye-tracker device and face-reading
			techniques </strong> <br>Mariana Gómez-Mejía, Guillermo
		Rodríguez-Martínez</li>
	<li><strong>Effect of Early Intervention on Students in a CS1
			Programming Course </strong> <br>Jose Miguel Llanos-Mosquera, Julian
		Andres Quimbayo-Castro, Edisney Garcia-Perdomo, Alvaro Hernan
		Alarcon-Lopez</li>
	<li><strong>Impact of face inversion on eye-tracking data quality: a
			study using the Tobii T-120 </strong> <br>Guillermo
		Rodríguez-Martínez</li>
	<li><strong>Implementation of the ISP (In System Programming) method
			for data recovery on mobile devices </strong> <br>Lidice Haz, Jenny
		Garzón Balcazar, Jaime Orozco Iguasnia, Carlos Sánchez</li>
	<li><strong>Test and validation of a corn grain cleaning and sorting
			machine with smart system integration for agricultural production in
			Cabanaconde – Peru </strong> <br>Bryan Antony Quinta Ccosi</li>
</ul>

<h4>Learning Management Systems</h4>
<ul>
	<li><strong>Aids to Navigation: Learning resource in seafaring
			practices and contribution to the safety of vessels in the Province
			of Santa Elena </strong> <br>Melannie Ortega, Rosalba Rodríguez,
		Jazmín Mena, Byron Albuja</li>
	<li><strong>RasPatient Pi: A Low-cost Customizable LLM-based Virtual
			Standardized Patient Simulator </strong> <br>Christian Grévisse</li>
	<li><strong>User Experience Insights from a Virtual Reality Application
			for Second Language Learners </strong> <br>Cristina Suarez-Pareja,
		Alix E. Rojas</li>

</ul>

<h4>Natural Language Processing</h4>
<ul>
	<li><strong>Developing Nigeria Multilingual Languages Speech Datasets
			for Antenatal Orientation </strong> <br>Sunday Adeola Ajagbe</li>
	<li><strong>Statistical modeling of discourse genres: the case of the
			opinion column in Spanish </strong> <br>Rogelio Nazar</li>
	<li><strong>Metaphor identification and interpretation in corpora with
			ChatGPT </strong> <br>Eduardo Puraivan, Irene Renau, Nicolás Riquelme</li>
</ul>

<h4>Social and Behavioral Applications</h4>
<ul>
	<li><strong>Development of a Portable Educational Mechatronic Device to
			Improve Attention and Memory in Children with Attention Deficit
			Hyperactivity Disorder (ADHD) from the Age of Three - Nubox </strong>
		<br>Angie Luisa Herrera Poma, Alexander Carlos Mendoza Puris, Jose
		Alexis Del Aguila Ramos</li>
	<li><strong>Emotions, Attitudes, and Challenges in the Perception of
			Artificial Intelligence in Social Research </strong> <br>Simone
		Belli, Marcelo Leon</li>
	<li><strong>Towards an Academic Social Network to Support Students
			Decision Making Processes </strong> <br>Hector Florez, Dana Macias</li>
</ul>

<h4>Software and Systems Modeling</h4>
<ul>

	<li><strong>Design of a functional block for testing analog and
			mixed-signal integrated circuits </strong> <br>José L.
		Simancas-García, Farid A. Meléndez-Pertuz, Ramón E. R. González, César
		A. Cárdenas, César Mora, Carlos Andrés Collazos Morales</li>
	<li><strong>Events Correlations for Fault Identification in GPON
			Networks </strong> <br>Manoel Lopes Filho, Danilo Vasconcelos,
		Rossana Andrade, Alex Ramos, Ismayle Santos</li>
</ul>

<h4>Software Architectures</h4>
<ul>
	<li><strong>A Study of Software Architects’ Cognitive Approaches:
			Kolb’s Learning Styles Inventory in Action </strong> <br>Mauricio
		Hidalgo, Hernán Astudillo, Laura M. Castro</li>
	<li><strong>Implementing Free TLS Certificates for Virtual Services: An
			Experimental Approach in Proxmox VE </strong> <br>Milton Escobar,
		Verónica Tintín, Raul Gallegos</li>
	<li><strong>NOTORIOUS: agNOsTic sOftware aRchItecture fOr aUtomated
			diagnoSis for GPONs </strong> <br>Ronaldo T. P. Milfont, Rubens A. S.
		Sousa, Rossana M. C. Andrade, Danilo R. Vasconcelos</li>
	<li><strong>Performance Analysis of MIMO-OFDM Systems in 5G Wireless
			Networks </strong> <br>Akande Hakeem Babalola, Oloyede Ayopo
		Abdulkarim, Shakirat Aderonke Salihu, Taibat O. Adebakin</li>
	<li><strong>SST: A Tool to Support the Triage of Security Smells in
			Microservice Applications </strong> <br>Francisco Ponce, Andrea
		Malnati, Roberto Negro, Francesca Arcelli Fontana, Hernán Astudillo,
		Antonio Brogi, Jacopo Soldani</li>
</ul>