<h3>Venue</h3>
<p>
	ICAI 2024 will be held at the <strong>Universidad Andrés Bello</strong>
	located in <strong>Viña del Mar, Chile</strong>. The Universidad Andrés
	Bello is a private university founded in Santiago in 1988. It currently
	has three campuses in Santiago, Viña del Mar, and Concepción and is the
	largest university in Chile in terms of number of students. Its
	presence expands to various regions of the country through its various
	modes of study (online, hybrid and face-to-face) in addition to being
	the sponsoring entity of the Instituto Profesional AIEP, an institution
	that has about 100,000 students distributed from Calama to Castro.
</p>
<p>The university mission is to be a university that offers those who
	aspire to progress, an inclusive educational experience of excellence
	for a globalized world, supported by the critical cultivation of
	knowledge, and the systematic generation of new knowledge.</p>
<p>The Institutional Strategic Plan of the Universidad Andrés Bello
	corresponds to the participatory work of various areas of the
	University, which converges in a document that guides the institutional
	action for the next five years. Aligned with the mission, vision and
	institutional purposes, it establishes priorities and a series of
	purposes that constitute a roadmap to follow for the different areas of
	our house of studies. Today, the Universidad Andrés Bello has six years
	of institutional accreditation in all areas.</p>
<iframe
	src="https://www.google.com/maps/d/embed?mid=1b5y_7NLymvXgIK_Uda2-uTvtPh8mOJU&ehbc=2E312F"
	width="640" height="480"></iframe>

<h4>Transportation</h4>
<p>Attendees must arrive to the Arturo Merino Benitez International
	Airport (SCL). There are the following options from the SCL airport to
	Viña del Mar:</p>
<ul>
	<li>Bus transportation
		<ul>
			<li>From the SCL airport to the Terminal Alameda TURBUS in Santiago
				<ul>
					<li>Schedule: Monday to Friday from 4:30 to 23:55, every 15
						minutes. Saturday and Sunday from 5:30 to 23:55, every 15 minutes</li>
					<li>Fare: 1.900 CLP (2 USD aprox)</li>
				</ul>
			</li>
			<li>From the Terminal Alameda TURBUS in Santiago to Bus Terminal in Viña del Mar
				<ul>
					<li>Schedule: Monday to Friday from 4:30 to 23:55, every 30
						minutes. Saturday and Sunday from 5:30 to 23:55, every 30 minutes</li>
					<li>Fare: 4.900 CLP (5.3 USD aprox)</li>
				</ul>
			</li>
		</ul>

	</li>
	<li>Taxi transportation
		<ul>
			<li>Fare: 95.000 CLP (102 USD aprox)</li>
		</ul>
	</li>
</ul>
