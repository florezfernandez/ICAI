<?php
if (isset($_POST["submit"])) {
    $workshop = $_POST["workshop"];
    $number = $_POST["number"];
    $title = $_POST["title"];
    $authors = $_POST["authors"];
    $authors = str_replace("\n", PHP_EOL, $authors);
    $corresponding = $_POST["corresponding"];
    $correspondingEmail = $_POST["correspondingEmail"];
    $dir = $workshop . "/" . date("YmdHis") . "_" . $number;
    mkdir($dir, 0777);
    $localPathPDF = $_FILES['pdf']['tmp_name'];
    copy($localPathPDF, $dir . "/Paper." . pathinfo($_FILES['pdf']['name'], PATHINFO_EXTENSION));
    $localPathSource = $_FILES['source']['tmp_name'];
    copy($localPathSource, $dir . "/Source." . pathinfo($_FILES['source']['name'], PATHINFO_EXTENSION));
    $localPathAgreement = $_FILES['agreement']['tmp_name'];
    copy($localPathAgreement, $dir . "/AUTHOR-AGREEMENT-icaiw_" . strtolower($workshop) . "_" . $number . "." . pathinfo($_FILES['agreement']['name'], PATHINFO_EXTENSION));
    $localPathChanges = $_FILES['changes']['tmp_name'];
    copy($localPathChanges, $dir . "/Changes." . pathinfo($_FILES['changes']['name'], PATHINFO_EXTENSION));
    if (! isset($_GET["w"])) {
        $localPathInvoice = $_FILES['invoice']['tmp_name'];
        copy($localPathInvoice, $dir . "/Invoice." . pathinfo($_FILES['invoice']['name'], PATHINFO_EXTENSION));
    }
    $message = "Dear " . $corresponding . "\n\n";
    $message .= "You sent the camera-ready files with the following informaton:\n";
    $message .= "- Workshop: " . $workshop . "\n";
    $message .= "- Submission number in Easychair: " . $number . "\n";
    $message .= "- Submission title: " . $title . "\n";
    $message .= "- Authors:\n" . $authors . "\n";
    $message .= "Your paper will be published in CEUR Workshop Proceedings\n\n";
    $message .= "--\n";
    $message .= "ICAI Workshops Committee";
    $to = $corresponding . " <" . $correspondingEmail . ">";
    $subject = "[" . $workshop . "] Paper " . $number . " - Camera-Ready Submission";
    $header = array(
        "From" => "ICAI Contact <contact@itiud.org>",
        "Cc" => "Hector Florez <haflorezf@udistrital.edu.co>",
        "Reply-To" => $corresponding . " <" . $correspondingEmail . ">"
    );
    mail($to, $subject, $message, $header);
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ICAI 2024</title>
<link rel="icon" type="image/png" href="../img/logos/icai2.png" />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://www.gstatic.com/charts/loader.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<h3 class="mt-3">ICAI Workshops Camera-Ready Submission</h3>
				<h3 class="mt-3">For publication in CEUR Workshop Proceeding</h3>
			</div>
		</div>
		<hr>
		<?php if(isset($_POST["submit"])) { ?>
		<div class="alert alert-success" role="alert">
			Dear <strong><?php echo $corresponding ?>.</strong><br>Thank you for
			sending your camera-ready files<br>A confirmation email has been sent to <?php echo $correspondingEmail ?>. This email might be received in the spam folder.
			
		</div>
		<?php }else{?>		
		<div class="row">
			<div class="col text-center">
				<h5>The corresponding author must fill in the following form to
					submit the camera-ready files</h5>
			</div>
		</div>
		<form method="post" action="?" class="bootstrap-form needs-validation"
			enctype="multipart/form-data">
			<div class="row">

				<div class="col">
					<div class="form-group mt-3">
						<label><strong>Workshop</strong></label> <select
							class="form-select" name="workshop">
							<option value="WAAI">WAAI</option>
							<option value="AIESD">AIESD</option>
							<option value="WDEA">WDEA</option>
							<option value="WITS">WITS</option>
							<option value="WKMIT">WKMIT</option>
							<option value="SCTSD">SCTSD</option>
							<option value="WSEAI">WSEAI</option>
							<option value="WSM">WSM</option>
						</select>
					</div>
					<div class="form-group mt-3">
						<label><strong>Submission number in Easychair</strong><br> <span
							class="text-muted">Max value: 15 </span></label> <input
							type="number" class="form-control" name="number" min="1" max="15"
							step="1" required>
					</div>
					<div class="form-group mt-3">
						<label><strong>Submission title</strong></label> <input
							type="text" class="form-control" name="title" required>
					</div>
					<div class="form-group mt-3">
						<label><strong>Authors</strong><br> <span class="text-muted">Write
								one author per line using the following format:<br>Name, email,
								affiliation, city, country, orcid id (if available)<br>Example:<br>Hector
								Florez,hf@ud.edu,Universidad
								Distrital,Bogota,Colombia,0000-0002-5339-4459
						</span></label>
						<textarea class="form-control" name="authors" rows="3" required></textarea>
					</div>
					<div class="form-group mt-3">
						<label><strong>Corresponding Author</strong><br> <span
							class="text-muted">Write the name of the corresponding author </span></label>
						<input type="text" class="form-control" name="corresponding"
							required>
					</div>
					<div class="form-group mt-3">
						<label><strong>Corresponding Author Email</strong><br> <span
							class="text-muted">Write the email of the corresponding author</span></label>
						<input type="email" class="form-control" name="correspondingEmail"
							required>
					</div>
				</div>
				<div class="col">
					<div class="form-group mt-3">
						<label><strong>Paper in PDF format</strong><br> <span
							class="text-muted">Use CEUR template <a
								href="http://ceur-ws.org/Vol-XXX/CEURART.zip" target="_blank">http://ceur-ws.org/Vol-XXX/CEURART.zip</a>
						</span></label> <input class="form-control" type="file" name="pdf"
							required>
					</div>
					<div class="form-group mt-3">
						<label><strong>Paper source files</strong><br> <span
							class="text-muted">Word users: docx file<br>LaTeX users: zip file
						</span></label> <input class="form-control" type="file"
							name="source" required>
					</div>
					<div class="form-group mt-3">
						<label><strong>Author agreement</strong><br> <span
							class="text-muted">The corresponding author of each accepted
								paper, acting on behalf of all of the authors of that paper,
								must complete and sign by hand the <a
								href="../docs/CEUR_ICAIW_2024.pdf" target="_blank">Author
									Agreement</a> form
						</span></label> <input class="form-control" type="file"
							name="agreement" required>
					</div>
					<div class="form-group mt-3">
						<label><strong>Letter of changes</strong><br> <span
							class="text-muted">Use the <a
								href="../docs/Letter_of_Changes.docx" target="_blank">Letter of
									Changes</a> format and save it as PDF
						</span></label> <input class="form-control" type="file"
							name="changes" required>
					</div>
					<?php if(!isset($_GET["w"])) { ?>
					<div class="form-group mt-3">
						<label><strong>Invoice</strong><br> <span class="text-muted">Register
								and pay the registration at:<br> <a
								href="https://easychair.org/my/conference?conf=icai_2024"
								target="_blank">https://easychair.org/my/conference?conf=icai_2024</a>
						</span></label> <input class="form-control" type="file"
							name="invoice" required>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-check mt-3">
						<input class="form-check-input" type="checkbox"
							name="checkTemplate" required> <label class="form-check-label">As
							corresponding author, on behalf of all authors, I certify that I
							<span class="text-danger">selected correctly the workshop</span>
							and I filled in correctly the submission information
						</label>
					</div>
					<div class="form-check mt-3">
						<input class="form-check-input" type="checkbox"
							name="checkTemplate" required> <label class="form-check-label">As
							corresponding author, on behalf of all authors, I certify that
							the paper meets the <span class="text-danger">CEUR template</span>
							one column
						</label>
					</div>
					<div class="form-check mt-3">
						<input class="form-check-input" type="checkbox"
							name="checkAuthors" required> <label class="form-check-label">As
							corresponding author, on behalf of all authors, I certify that
							the <span class="text-danger">authors have not changed</span>
							from the original submission
						</label>
					</div>
					<div class="form-check mt-3">
						<input class="form-check-input" type="checkbox" name="checkPaper"
							required> <label class="form-check-label">As corresponding
							author, on behalf of all authors, I certify that the paper
							includes <span class="text-danger">authors names, affiliations,
								emails, and ORCIDs</span> (if available)
						</label>
					</div>
					<div class="form-check mt-3">
						<input class="form-check-input" type="checkbox"
							name="checkAgreement" required> <label class="form-check-label">As
							corresponding author, on behalf of all authors, I certify that
							the Author Agreement includes <span class="text-danger">contribution
								title, authors, corresponding author, location, date, and
								handwritten signature</span>
						</label>
					</div>
					<div class="form-check mt-3">
						<input class="form-check-input" type="checkbox"
							name="checkChanges" required> <label class="form-check-label">As
							corresponding author, on behalf of all authors, I certify that <span
							class="text-danger">every comment done by the reviewers was
								revised and responded</span> in the letter of changes
						</label>
					</div>
					<input type="submit" class="btn btn-primary mt-3" name="submit"
						value="Submit">
				</div>
			</div>
		</form>
		<?php } ?>
		<div class="text-center text-muted">
			&copy; ITI Research Group<br>2018 - <?php echo date("Y")?> All rights reserved
    		</div>
	</div>
</body>
</html>